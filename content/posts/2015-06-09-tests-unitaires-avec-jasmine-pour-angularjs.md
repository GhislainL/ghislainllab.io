---
title: Tests unitaires avec Jasmine pour AngularJS
author: Ghislain

date: 2015-06-09T07:30:21+00:00
url: /2015/06/09/tests-unitaires-avec-jasmine-pour-angularjs/
categories:
  - JavaScript
tags:
  - Angular
  - Jasmine
  - Karma
  - test unitaire
  - unit testing

---
Lors des précédents articles, nous avons construits une application AngularJS qui effectue quelques calculs. Pour s’assurer que les fonctions en charge des calculs restent valide quand l’application évolue, les tests unitaires sont nécessaires. Comment s’y prendre dans l’écosystème AngularJS ?

Ce qui m’a le plus déstabilisé quand j’ai abordé ce sujet réside dans la compréhension des outils utilisés.

Il y a pléthore de tutoriels mais peu expliquent a quoi servent chaque utilitaire. Nous allons donc découvrir comment effectuer un test unitaire avec Jasmine pour AngularJS.

## Les outils {#les-outils}

Nous allons découvrir les deux outils nécessaires pour effectuer les tests.

### Karma {#karma}

> A simple tool that allows you to execute JavaScript code in multiple real browsers.

> The main purpose of Karma is to make your test-driven development easy, fast, and fun.

Tiré de npm

Karma permet d’exécuter du code JavaScript dans plusieurs browsers. Il est possible de sélectionner un ou plusieurs browsers lors de l’installation des outils.

Ensuite il faut choisir la bibliothèque/framework de test souhaité. Jasmine, QUnit, Mocha. Pour faciliter la découverte, j’ai utilisé Jasmine. Mais c’est quoi exactement ?

[<img class="alignnone size-medium wp-image-855" src="/wp-content/uploads/2015/06/karma-300x135.jpg?fit=300%2C135" alt="karma" data-recalc-dims="1" />][1]

### Jasmine {#jasmine}

Il s’agit d’un framework de behavior driven development pour tester du code JavaScript.

Deux avantages de ce framework :

1 – il est indépendant d’autres frameworks JS

2 – il ne nécessite pas un DOM

De plus la syntaxe est lisible et rapidement appréhendable, comme nous le verrons plus loin.

[<img class="alignnone size-medium wp-image-856" src="/wp-content/uploads/2015/06/jasmine-300x82.png?fit=300%2C82" alt="jasmine" data-recalc-dims="1" />][2]

## Installation et configuration {#installation-et-configuration}

Maintenant que nous avons découvert les outils, nous allons les installer et les configurer. En prérequis, il est nécessaire avoir installé nodejs et bower.

{{< highlight powershell>}}# ouvrir la console console npm
# se placer dans le répertoire du projet
cd C:UsersGhislainDocumentsGitHubng-SealedSpaceTimer

# installer karma
npm install karma --save-dev

# choisir quel "browser" utiliser.
# du classique Chrome, Firefox à des outils comme PhantomJS ou BrowserStack
# http://karma-runner.github.io/0.12/config/browsers.html
npm install karma-chrome-launcher

# installer jasmine
npm install karma-jasmine --save-dev

# faciliter l'appel a karma en ligne de commande
npm install -g karma-cli

# installer le mock pour angular afin de tester
bower install angular-mocks –save-dev

# créer le fichier karma.conf.js.
# répondre aux questions pour compléter le fichier
karma init karma.conf.js

# lancer les tests, si la surveillance des modifications est activée,
# alors à chaque modification les tests seront exécutés
karma start karma.conf.js
# lancer les tests une seule fois
karma start karma.conf.js --single-run
{{< / highlight >}}

## Ecrire les tests {#ecrire-les-tests}

### Description {#description}

Jasmine utilise une syntaxe basée sur l’idée d’expectation. Les tests sont regroupées dans des zones _describe_. Chaque test début par _it_ suivi du message de description du test puis le contenu du test. Le test contient une ou plusieurs expectations décrites par _expect_. Une expectation utilise un _matcher_ (cf le paragraphe dédié) qui matérialise la condition à valider.

{{< highlight js >}}describe('section des tests de la fonctionnalité F1', function() {
   it('devrait etre egal a 1 dans le cas d’usage U2',
      inject(function($controller){
         // initialiser les variables pour le cas U2
         expect($resultat).toBe(1);
      }
));
{{< / highlight >}}


Jasmine permet de tester les contrôleurs, les directives, les filtres, les services, les factory. Je n’ai pour l’instant testé sur les contrôleurs mais théorique Jasmine permet de tester tous les concepts d’AngularJS.

### Les Matchers {#les-matchers}

Les _matchers_ constituent les mécanismes de base pour valider les expectations d’un test. Nativement Jasmine propose un bon nombre de possibilité. L’avantage réside dans le fait qu’il est possible de créer ses propres _matchers_ pour des besoins spécifiques.

<http://jasmine.github.io/2.0/custom_matcher.html>

{{< highlight js >}}expect(fn).toThrow(e);
 expect(instance).toBe(instance);
 expect(mixed).toBeDefined();
 expect(mixed).toBeFalsy();
 expect(number).toBeGreaterThan(number);
 expect(number).toBeLessThan(number);
 expect(mixed).toBeNull();
 expect(mixed).toBeTruthy();
 expect(mixed).toBeUndefined();
 expect(array).toContain(member);
 expect(string).toContain(substring);
 expect(mixed).toEqual(mixed);
 expect(mixed).toMatch(pattern);
{{< / highlight >}}


source : <https://github.com/JamieMason/Jasmine-Matchers>

### Quelques erreurs {#quelques-erreurs}

Lors de l’élaboration des tests unitaires avec Jasmine pour AngularJS, j’ai essuyé quelques plâtres.

_ERROR Uncaught ReferenceError: angular is not defined_

=> dans karma.conf.js s’assurer que angular.js est chargé avant l’appel à angular.

_module undefined_

=> dans karma.conf.js charger tous les modules de l’application dans le beforeEach. Si on se contente de ceux du contrôleur, cela va échouer.

_Error: [ng:areq] Argument ‘HomeController’ is not a function, got undefined_

=> le fichier JS définissant le contrôleur est-il bien chargé ? Vérifier les lignes d’inclusion dans le fichier karma.config.js

### Exemple test d’un Controlleur {#exemple-test-dun-controlleur}

{{< highlight js >}}describe('HomeCtrl', function() {
   var $scope, $controller;
   // chargement des modules avant chaque test
   beforeEach(function(){
      module('MyApp');
      module('timer');
      module('ngRoute');
      module('ui.bootstrap');
      module('pascalprecht.translate');
   });

   // création du contexte avant chaque test
   beforeEach(inject(function($rootScope, _$controller_){
      $scope = $rootScope.$new();
      $controller = _$controller_;

      $controller('HomeCtrl', {'$scope': $scope});
   }));

   // extrait de la zone des tests des volumes
   describe('Volume', function() {
      it('compute rect volume 1', inject(function($controller){
         $scope.shape = 'rectangle';
         $scope.hauteur = 1;
         $scope.largeur = 1;
         $scope.longueur = 1;

         $vol = $scope.computeVolume();
         expect($vol).toBe(1);
      }));
  });

  // extrait de la zone des tests du temps restant
  describe('Time left', function() {
      it('compute time left in a rect volume 1m3 for 2 person',
         inject(function($controller){
            $scope.shape = 'rectangle';
            $scope.hauteur = 1;
            $scope.largeur = 1;
            $scope.longueur = 1;
            $scope.nbPersonne = 2;

            $timeLeft = $scope.computeTimeLeft();
            expect($timeLeft).toBeCloseTo(2700);
         }
      ));
   });
});
{{< / highlight >}}


[<img class="wp-image-860 size-full" src="/wp-content/uploads/2015/06/KarmaStart.png?fit=988%2C477" alt="KarmaStart" srcset="/wp-content/uploads/2015/06/KarmaStart.png?resize=300%2C145 300w, /wp-content/uploads/2015/06/KarmaStart.png?w=988 988w" sizes="(max-width: 988px) 100vw, 988px" data-recalc-dims="1" />][3]  
_Invite de commande de l’exécution d’un test unitaire_

Nous avons débroussaillé les tests unitaires avec Jasmine pour AngularJS. Installation et configuration de l’environnement, description des tests Jasmine et écriture d’un test pour un contrôleur. Jasmine propose encore beaucoup de fonctionnalités. Mais la prochaine étape sera d’aborder les test e2e (end to end) qui permettent d’effectuer des tests fonctionnels pour simuler le comportement d’un utilisateur.

**Références:**

[Test unitaire – site officiel AngularJS][4]

[Karma – site officiel][5]

[Jasmine – site officiel][6]

 [1]: /wp-content/uploads/2015/06/karma.jpg
 [2]: /wp-content/uploads/2015/06/jasmine.png
 [3]: /wp-content/uploads/2015/06/KarmaStart.png
 [4]: https://docs.angularjs.org/guide/unit-testing
 [5]: http://karma-runner.github.io
 [6]: http://jasmine.github.io