---
title: 'Mail facile sur un environnement de développement : smtp4dev'
author: Ghislain

date: 2014-06-16T08:30:57+00:00
url: /2014/06/16/mail-facile-sur-un-environnement-de-developpement-smtp4dev/
categories:
  - SharePoint
tags:
  - Outils
  - SharePoint 2010
  - SharePoint 2013
  - smtp4dev

---
**smtp4dev** est un outil très pratique quand on veut tester les envois de mail dans un environnement de dev. Pour tester des workflows avec assignation de tache ou encore l’envoi de digest, cela devient rapidement indispensable.

# Configuration de l’envoi de mail de la ferme {#configuration-de-lenvoi-de-mail-de-la-ferme}

Configurer la ferme pour que le serveur SMTP soit le serveur et configurer les adresses mails sortantes et replyTo.

{{< highlight powershell>}}$SMTPServer = $env:computername
$EmailAddress = "farm@" + $SMTPServer
$ReplyToEmail = "replyTo@" + $SMTPServer

Try
{
Write-Host "Configuring Outgoing Email: " -NoNewLine
$loadasm = [System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint")
$SPGlobalAdmin = New-Object Microsoft.SharePoint.Administration.SPGlobalAdmin
$SPGlobalAdmin.UpdateMailSettings($SMTPServer, $EmailAddress, $ReplyToEmail, 65001)
Write-Host "OK" -ForegroundColor Green
}
Catch
{
Write-Host "Failed" -ForegroundColor Red
}{{< /highlight >}}

Juste pour vérifier le résultat dans la centrale d’administration :

[<img class="alignnone wp-image-82 size-full" src="/wp-content/uploads/2014/06/smtp4dev1.png?fit=562%2C297" alt="smtp4dev1" srcset="/wp-content/uploads/2014/06/smtp4dev1.png?resize=300%2C159 300w, /wp-content/uploads/2014/06/smtp4dev1.png?w=562 562w" sizes="(max-width: 562px) 100vw, 562px" data-recalc-dims="1" />][1]

# Configuration de smtp4dev {#configuration-de-smtp4dev}

Lancer smtp4dev, cliquer sur Options.

Dans l’onglet Server, changer la valeur du champ Domain Name pour le nom du serveur.

[<img class="alignnone wp-image-81 size-full" src="/wp-content/uploads/2014/06/smtp4dev2.png?fit=639%2C577" alt="smtp4dev2" srcset="/wp-content/uploads/2014/06/smtp4dev2.png?resize=300%2C271 300w, /wp-content/uploads/2014/06/smtp4dev2.png?w=639 639w" sizes="(max-width: 639px) 100vw, 639px" data-recalc-dims="1" />][2]

# Test de l’environnement {#test-de-lenvironnement}

Le plus simple pour tester le bon fonctionnement est d’abonner un utilisateur à une alerte et voilà le travail :

[<img class="alignnone wp-image-83 size-full" src="/wp-content/uploads/2014/06/smtp4dev3.png?fit=522%2C374" alt="smtp4dev3" srcset="/wp-content/uploads/2014/06/smtp4dev3.png?resize=300%2C215 300w, /wp-content/uploads/2014/06/smtp4dev3.png?w=522 522w" sizes="(max-width: 522px) 100vw, 522px" data-recalc-dims="1" />][3]

smtp4dev permet de voir un mail d’un point de vue technique mais pas de le visualiser. Pour cela il faut installer un logiciel de messagerie.

[http://smtp4dev.codeplex.com/][4]

 

 [1]: /wp-content/uploads/2014/06/smtp4dev1.png
 [2]: /wp-content/uploads/2014/06/smtp4dev2.png
 [3]: /wp-content/uploads/2014/06/smtp4dev3.png
 [4]: http://smtp4dev.codeplex.com/ "smtp4dev sur codeplex"