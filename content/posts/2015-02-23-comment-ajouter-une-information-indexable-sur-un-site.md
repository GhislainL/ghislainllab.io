---
title: Comment ajouter une information indexable sur un site ?
author: Ghislain

date: 2015-02-23T08:24:09+00:00
url: /2015/02/23/comment-ajouter-une-information-indexable-sur-un-site/
categories:
  - PowerShell
  - SharePoint
tags:
  - crawled property
  - IndexedPropertyKeys
  - managed property
  - PowerShell
  - recherche
  - search
  - SharePoint
  - SharePoint 2013

---
Avez-vous déjà eu besoin d’ajouter de l’information sur vos sites ? Les property bags sont une solution. Et si vous voulez utiliser cette information dans la recherche ? Les propriétés indexées sont la solution.

[<img class="alignnone wp-image-507 size-full" src="/wp-content/uploads/2015/02/indexedproperty1.png?fit=582%2C145" alt="IndexedProperty" srcset="/wp-content/uploads/2015/02/indexedproperty1.png?resize=300%2C75 300w, /wp-content/uploads/2015/02/indexedproperty1.png?w=582 582w" sizes="(max-width: 582px) 100vw, 582px" data-recalc-dims="1" />][1]

Une propriété indexée est un objet similaire à un couple (clé ; valeur) dans le property bag à la différence près qu’elle est indexée par le service de recherche. Après un crawl complet, elle apparait dans les crawled properties. Vous pouvez donc créer une propriété gérée liée à celle-ci. Après un second crawl complet, vous pouvez utiliser cette propriété dans la recherche, notamment dans le refinement panel.

Concrètement l’un de mes projets actuels consiste en la refonte en un seul service de plusieurs services hétérogènes. Je dis hétérogène autant d’un point de vue fonctionnel (social, publication, partage documentaire) que technique car il y a des plateformes Sharepoint 2007, 2010 et 2013. Après la partie migration et récupération de l’existant, je cherchais un moyen pour identifier le type de chaque site dans la recherche.

# Méthode {#méthode}

**1. Créer les propriétés indexées**

Au niveau de chaque site, créer une propriété indexée nommée GlaType.

Tools => http://dev.contoso.com et http://cth.contoso.com

Business => http://bi.contoso.com et http://performancepoint.contoso.com

Collab => http://team.contoso.com

{{< highlight powershell>}}# add an indexed property
$web = Get-SPWeb "http://team.contoso.com"
$web.AllProperties["GlaType"] = "Collab"
$web.IndexedPropertyKeys.Add("GlaType")
$web.Update()
# display all indexed properties
$web.IndexedPropertyKeys</pre>

**2. Lancer un crawl complet**

**3. Créer des propriétés gérées**

Vous remarquerez que la propriété indexée GlaType apparait bien dans la centrale d’administration.

[<img class="wp-image-465 size-full" src="/wp-content/uploads/2015/01/indexedproperty.png?fit=528%2C407" alt="IndexedProperty" srcset="/wp-content/uploads/2015/01/indexedproperty.png?resize=300%2C231 300w, /wp-content/uploads/2015/01/indexedproperty.png?w=528 528w" sizes="(max-width: 528px) 100vw, 528px" data-recalc-dims="1" />][2]  
_Nouvelle propriété indexée disponible_

Créer une propriété gérée ManagedGlaType liée à la propriété indexée GlaType. La configuration de la propriété gérée : Searchable, Queryable, Retrievable, Refinable, Sortable.

[<img class="wp-image-466 size-full" src="/wp-content/uploads/2015/01/newmanagedproperty.png?fit=890%2C122" alt="NewManagedProperty" srcset="/wp-content/uploads/2015/01/newmanagedproperty.png?resize=300%2C41 300w, /wp-content/uploads/2015/01/newmanagedproperty.png?resize=1024%2C141 1024w, /wp-content/uploads/2015/01/newmanagedproperty.png?w=1055 1055w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][3]  
_Nouvelle propriété gérée mappée à la propriété indexée_

**4. Lancer un full crawl**

**5. Configurer le refinement panel de la recherche**

[<img class="alignnone wp-image-502 size-large" src="/wp-content/uploads/2015/02/searchresults1-1024x599.gif?fit=890%2C521" alt="SearchResults" data-recalc-dims="1" />][4]

 

Vous avez maintenant la possibilité de trier les résultats à l’aide de la nouvelle propriété gérée ManagedGlaType.

Dans un prochain article, nous aborderons les modèles d’affichage pour exploiter cette propriété.

 [1]: /wp-content/uploads/2015/02/indexedproperty1.png
 [2]: /wp-content/uploads/2015/01/indexedproperty.png
 [3]: /wp-content/uploads/2015/01/newmanagedproperty.png
 [4]: /wp-content/uploads/2015/02/searchresults1.gif