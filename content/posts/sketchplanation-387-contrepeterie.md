---
title: Contrepèterie
author: Ghislain
date: 2020-01-11
url: /un-dessin/contrepeterie/
categories:
  - Un dessin
tags:
  - sketchplanations

---

![Contrepeterie](/un-dessin/sketchplanations-387-contrepeterie.jpg)

La contrepèterie est un jeu de mots consistant à permuter certains phonèmes ou syllabes d'une phrase afin d'en obtenir une nouvelle, présentant souvent un sens indécent masqué par l'apparente innocence de la phrase initiale. (1)

> Pour atteindre une plus grande efficacité, la contrepèterie demande même le concours de trois personnes : celle qui dit le contrepet, celle qui le comprend et celle à qui la signification cachée échappe complètement, le plaisir des deux premières étant décuplé par l'incompréhension de la troisième. - Claude Gagnière

En anglais contrepèterie se dit spoonerism, ainsi nommé après le Révérend William Archibald Spooner qui était réputé pour en faire beaucoup. 

Traduction de la publication de [Sketchplanations](https://www.sketchplanations.com/post/105719386456/spoonerisms-named-after-a-reverend-remarkably)

1 : [wikipedia](https://fr.wikipedia.org/wiki/Contrepèterie)