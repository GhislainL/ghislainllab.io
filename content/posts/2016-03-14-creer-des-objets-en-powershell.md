---
title: Créer des objets en PowerShell
author: Ghislain

date: 2016-03-14T00:00:00+00:00
url: /2016/03/14/creer-des-objets-en-powershell/
categories:
  - PowerShell
---
Dernièrement j’ai travaillé avec PowerShell et j’avais besoin de passer beaucoup de paramètres entre des fonctions. Pour améliorer la lisibilité et la maintenabilité, j’ai donc regardé comment créer des objets en PowerShell. Je partage avec vous les 3 méthodes que j’ai rencontrées :

  * Version simple : New-Object
  * Version élaborée : New-Module –AsCustomObject
  * Version classe : Class (PowerShell v5)

## New-Object {#new-object}

Simple et efficace **New-Object** crée un objet qui n’attend que d’être construit par l’ajout de membres que ce soit des propriétés ou des méthodes.<figure class="highlight"> 

{{< highlight powershell>}}function Get-Parameters(){
     #new object
     $parameters = New-Object -TypeName PSObject
     #Add-Member
     Add-Member -InputObject $parameters -MemberType NoteProperty -Name param1 -Value "value1"
     Add-Member -InputObject $parameters -MemberType NoteProperty -Name param2 -Value "value2"
     Add-Member -InputObject $parameters -MemberType NoteProperty -Name param3 -Value "value3"
     #add method     
     $method = {         Get-Date -f hh:mm:ss.fff     }
     Add-Member -InputObject $parameters -MemberType ScriptMethod -Name GetInvok -Value $method     
     return $parameters
}

function Start-ProcessWithParams($Parameters) {
     $Parameters.param1
     $Parameters.param2
     $Parameters.param3
     $Parameters.GetInvok()
}  

$params = Get-Parameters 
$params.GetInvok() 
Start-ProcessWithParams -Parameters $params
{{< / highlight >}}

Output 

{{< highlight bash>}}11:13:12.821
value1 
value2 
value3 
11:13:12.837 
{{< / highlight >}}

On remarque bien que la méthode est exécutée à l’appel

{{< highlight powershell>}}# une notation courte 
function Get-ParametersShort(){
     $properties = @{
         param1 = 'value1'
         param2 = 'value2'
         param3 = 'value3'
     }
     $parameters = new-object psobject -Property $properties
     return $parameters 
} 
 
# une notation plus courte 
function Get-ParametersShorter(){
     $parameters = [PSCustomObject]@{
         param1 = 'value1'
         param2 = 'value2'
         param3 = 'value3'
     }
     return $parameters 
} 
{{< / highlight >}}

## New-Module -AsCustomObject {#new-module--ascustomobject}

Cette méthode est plus élaborée et permet d’obtenir un objet plus adapté car plus paramétrable. Il est possible de préciser le type de la propriété et les fonctions sont convenablement supportées.<figure class="highlight"> 

{{< highlight powershell>}}function Get-Parameters2(){
     $parameters = New-Module -AsCustomObject -ScriptBlock {
          [string]$param1='value1'
          [string]$param2='value2'
          [string]$param3='value3'
          Function GetInvok { Get-Date -f hh:mm:ss.fff}
          Export-ModuleMember -Variable * -Function *
     }     
     return $parameters
}
{{< / highlight >}}

Si on met plusieurs objets dans un tableau, il est possible d’utiliser les si pratiques Sort, Select et Where.

## Class avec PowerShell v5 {#class-avec-powershell-v5}

PowerShell v5 introduit les classes ce qui rapproche encore plus PowerShell de C#.

{{< highlight powershell>}}Class Param { 
    [string]param1 
    [string]param2 
    [string]param3 
} 

$parameters = New-Object Param 
$parameters.param1 = 'value1' 
$parameters.param2 = 'value2' 
$parameters.param3 = 'value3'
{{< / highlight >}}


Plus d’informations sur les classes de PowerShell v5 sur le blog **Hey, Scripting Guy! Blog**

  * [Introduction to PowerShell 5 Classes][1]
  * [PowerShell 5: Create Simple Class][2]
  * [Adding Enums to PowerShell 5 Classes][3]
  * [Creating Instances of PowerShell 5 Classes][4]

 [1]: https://blogs.technet.microsoft.com/heyscriptingguy/2015/08/31/introduction-to-powershell-5-classes/
 [2]: https://blogs.technet.microsoft.com/heyscriptingguy/2015/09/01/powershell-5-create-simple-class/
 [3]: https://blogs.technet.microsoft.com/heyscriptingguy/2015/09/04/adding-methods-to-a-powershell-5-class/
 [4]: https://blogs.technet.microsoft.com/heyscriptingguy/2015/09/03/creating-instances-of-powershell-5-classes/