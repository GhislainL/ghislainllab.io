---
title: Effectuer des tests de charge SharePoint avec Visual Studio
author: Ghislain

date: 2015-02-09T08:39:06+00:00
url: /2015/02/09/effectuer-des-tests-de-charge-sharepoint-avec-visual-studio/
categories:
  - dotNet
  - SharePoint
tags:
  - Load testing
  - SharePoint
  - SharePoint 2013
  - Test de charge
  - Visual Studio

---
Vous avez une plateforme flambant neuve qui répond aux prérequis de SharePoint. Normalement votre sizing permet de répondre à la qualité de service demandée (nombre d’utilisateurs, temps de réponse,…). Mais comment vous en assurer ?

Visual Studio, dans sa version Ultimate, vous propose de créer des tests de charge. Comme me l’avait fait remarquer [Guillaume Meyer][1], les résultats sont purement techniques. Ils ne prennent pas en compte le ressenti des utilisateurs notamment avec le javascript, le lazy loading et les choses qui améliorent l’UX.

Le schéma ci-dessous explique la constitution d’un projet de test de performance :

[<img class="alignnone size-full wp-image-490" src="/wp-content/uploads/2015/02/diagramtestdeperf.png?fit=400%2C319" alt="DiagramTestDePerf" srcset="/wp-content/uploads/2015/02/diagramtestdeperf.png?resize=300%2C239 300w, /wp-content/uploads/2015/02/diagramtestdeperf.png?w=400 400w" sizes="(max-width: 400px) 100vw, 400px" data-recalc-dims="1" />][2]

Un test de performance est le plus petit élément du projet. Vous disposez d’un cas d’utilisation enregistré comme un ensemble d’actions de navigation reproductible.

Un test de charge est composé d’un ensemble de paramètre permettant d’exécuter un ou plusieurs tests de performance. Vous pouvez définir les points suivants :

  * Le type de montée en charge : par pallier d’utilisateurs, par des tests de performances mélangés ou séquentiels
  * La répartition en pourcentage de l’utilisation des différents tests de performances
  * La répartition en pourcentage des types de connexions réseaux des utilisateurs simulés
  * La répartition en pourcentage des types de navigateurs des utilisateurs simulés
  * Les compteurs à surveiller sur une sélection de plateforme

Une fois le paramétrage effectué, vous lancez votre test de charge. Après un café, vous disposez d’un ensemble de résultat soit sous forme de tableaux soit sous forme de graphiques.

[<img class="wp-image-493 size-large" src="/wp-content/uploads/2015/02/loadtestresume-1024x358.png?fit=890%2C311" alt="LoadTestResume" srcset="/wp-content/uploads/2015/02/loadtestresume.png?resize=300%2C105 300w, /wp-content/uploads/2015/02/loadtestresume.png?resize=1024%2C358 1024w, /wp-content/uploads/2015/02/loadtestresume.png?w=1260 1260w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][3]  
_Résultats en tableau de l’exécution d’un test de charge_ [<img class="wp-image-492 size-large" src="/wp-content/uploads/2015/02/loadtestgraphiques-1024x356.png?fit=890%2C310" alt="LoadTestGraphiques" srcset="/wp-content/uploads/2015/02/loadtestgraphiques.png?resize=300%2C104 300w, /wp-content/uploads/2015/02/loadtestgraphiques.png?resize=1024%2C356 1024w, /wp-content/uploads/2015/02/loadtestgraphiques.png?w=1261 1261w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][4]  
_Résultats en graphique de l’exécution d’un test de charge_

Vous disposez d’un moyen simple pour créer un test de charge proposant un paramétrage relativement avancé  à partir d’un IDE. Cela permet de vérifier le bon sizing de la ferme, de répérer les goulets d’étrangelement (software ou hardware avec les indicateurs). Pour approfondir les tests ou vérifier le cheminement de bout en bout sur les équipements réseaux, la production dispose certainement de son propre outil.

**Pour aller plus loin :**

[Le blog de Karthikeyan Subburam][5]

<!--more-->

**Ci-dessous les captures d’écrans :**

[<img class="wp-image-477 size-large" src="/wp-content/uploads/2015/02/load1.png?fit=890%2C614" alt="Load1" srcset="/wp-content/uploads/2015/02/load1.png?resize=300%2C207 300w, /wp-content/uploads/2015/02/load1.png?w=954 954w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][6]  
_Type de project dans Visual_ [<img class="wp-image-478 size-large" src="/wp-content/uploads/2015/02/load2.png?fit=890%2C596" alt="Load2" srcset="/wp-content/uploads/2015/02/load2.png?resize=300%2C201 300w, /wp-content/uploads/2015/02/load2.png?w=1003 1003w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][7]  
_Créer un test de performance_ [<img class="wp-image-479 size-large" src="/wp-content/uploads/2015/02/load3.png?fit=890%2C624" alt="Load3" srcset="/wp-content/uploads/2015/02/load3.png?resize=300%2C210 300w, /wp-content/uploads/2015/02/load3.png?w=974 974w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][8]  
_Créer un test de charge_ [<img class="wp-image-480 size-full" src="/wp-content/uploads/2015/02/load4.png?fit=694%2C480" alt="Load4" srcset="/wp-content/uploads/2015/02/load4.png?resize=300%2C207 300w, /wp-content/uploads/2015/02/load4.png?w=694 694w" sizes="(max-width: 694px) 100vw, 694px" data-recalc-dims="1" />][9]  
_Assistant de création d’un test de charge_ [<img class="wp-image-476 size-thumbnail" src="/wp-content/uploads/2015/02/load10-150x150.png?resize=150%2C104" alt="Load10" data-recalc-dims="1" />][10]  
_Choisir la durée_ [<img class="wp-image-481 size-thumbnail" src="/wp-content/uploads/2015/02/load5-150x150.png?resize=150%2C104" alt="Load5" data-recalc-dims="1" />][11]  
_Choix du type de montée en charge_ <img class="wp-image-485 size-thumbnail" src="/wp-content/uploads/2015/02/load9-150x150.png?resize=150%2C104" alt="Load9" data-recalc-dims="1" />  
_Choix des indicateurs_ [<img class="wp-image-482 size-thumbnail" src="/wp-content/uploads/2015/02/load6-150x150.png?resize=150%2C104" alt="Load6" data-recalc-dims="1" />][12]  
_Répartition en test de charge_ [<img class="wp-image-482 size-thumbnail" src="/wp-content/uploads/2015/02/load6-150x150.png?resize=150%2C104" alt="Load6" data-recalc-dims="1" />][12]  
_Choix des navigateurs_

 [1]: http://fr.linkedin.com/in/guillaumemeyer
 [2]: /wp-content/uploads/2015/02/diagramtestdeperf.png
 [3]: /wp-content/uploads/2015/02/loadtestresume.png
 [4]: /wp-content/uploads/2015/02/loadtestgraphiques.png
 [5]: http://www.mstechbits.com/visual-studio/additional-properties-to-be-configured-for-web-performance-test-editor-webtest-for-visual-studio-2013-part-4/
 [6]: /wp-content/uploads/2015/02/load1.png
 [7]: /wp-content/uploads/2015/02/load2.png
 [8]: /wp-content/uploads/2015/02/load3.png
 [9]: /wp-content/uploads/2015/02/load4.png
 [10]: /wp-content/uploads/2015/02/load10.png
 [11]: /wp-content/uploads/2015/02/load5.png
 [12]: /wp-content/uploads/2015/02/load6.png