---
title: Contenus réutilisables au sein des pages de contenu
author: Ghislain

date: 2015-02-16T08:37:41+00:00
url: /2015/02/16/contenus-reutilisables-au-sein-des-pages-de-contenu/
categories:
  - SharePoint
tags:
  - publishing
  - SharePoint
  - SharePoint 2013

---
Un site de publication contient des éléments qui reviennent fréquemment dans les pages. Il peut s’agir de mentions légales, de signatures, de catch phrase et d’une mise en forme. Si un jour vous devez les faire évoluer sans un mécanisme adapté, vous risquez de vous arracher les cheveux. Heureusement, SharePoint 2013 propose les **contenus réutilisables** parmi les fonctionnalités de publication.

Les contenus réutilisables sont centralisés dans une liste dans le site racine de la collection de site. Tous les sites de la collection de site peuvent utiliser les contenus réutilisables à condition que la fonctionnalité de publication soit activée.

Pour en profiter, il faut activer la fonctionnalité de scope collection de site **SharePoint Server Publication Infrastructure**. Puis activer la fonctionnalité de scope site **SharePoint Server Publishing** sur chaque site sur lequel vous souhaitez vous en servir.

Dans le contenu du site, vous verrez une nouvelle liste nommée **Reusable Content**. A la création elle contient 3 éléments : Byline, Copyright et Quota.

[<img class="wp-image-426 size-full" src="/wp-content/uploads/2015/01/sitecontent.png?resize=877%2C532" alt="SiteContent" data-recalc-dims="1" />][1]  
_Contenu du site – liste Contenu réutilisable_

Vous avez la possibilité de créer d’autres contenus réutilisables. Il peut s’agir de texte ou de HTML (éditeur texte intégré ou HTML personnalisé). Il faut porter une attention particulière à l’option **Automatic Update**, qui permet de propager les modifications du contenu réutilisable dans toutes les pages l’utilisant.

[<img class="wp-image-423 size-full" src="/wp-content/uploads/2015/01/createreusablecontent.png?fit=593%2C659" alt="CreateReusableContent" srcset="/wp-content/uploads/2015/01/createreusablecontent.png?resize=270%2C300 270w, /wp-content/uploads/2015/01/createreusablecontent.png?w=593 593w" sizes="(max-width: 593px) 100vw, 593px" data-recalc-dims="1" />][2]  
_Création/édition d’un contenu réutilisable_

Si vous éditez une page, dans l’onglet **Insert** du ruban vous remarquer un bouton **Reusable Content**. Il permet d’insérer dans la page une « référence » vers le contenu réutilisable sélectionné.

[<img class="wp-image-424 size-full" src="/wp-content/uploads/2015/01/insertreusablecontent.png?fit=890%2C515" alt="InsertReusableContent" srcset="/wp-content/uploads/2015/01/insertreusablecontent.png?resize=300%2C174 300w, /wp-content/uploads/2015/01/insertreusablecontent.png?resize=1024%2C592 1024w, /wp-content/uploads/2015/01/insertreusablecontent.png?w=1279 1279w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][3]  
_Insérer un contenu réutilisable dans le contenu d’une page_

Comme l’option **Automatic Update** est cochée, en mettant en jour le contenu réutilisable, les modifications sont répercutées.

[<img class="wp-image-427 size-full" src="/wp-content/uploads/2015/01/reusablecontentupdated.png?fit=890%2C161" alt="UpdateReusableContent" data-recalc-dims="1" />][4]  
_Exemple : modification d’un contenu réutilisable_

[<img class="wp-image-425 size-full" src="/wp-content/uploads/2015/01/reusablecontentupdated.png?resize=890%2C288" alt="ReusableContentUpdated" data-recalc-dims="1" />][5]

Exemple : impact de la modification d’un contenu réutilisable

Souvent regardée de haut, la fonctionnalité Contenu Réutilisable pourra vous être utile. Elle se base sur un principe simple mais efficace qui permet de centraliser au niveau d’une collection de site des contenus pour les maintenir plus facilement.

 [1]: /wp-content/uploads/2015/01/sitecontent.png
 [2]: /wp-content/uploads/2015/01/createreusablecontent.png
 [3]: /wp-content/uploads/2015/01/insertreusablecontent.png
 [4]: /wp-content/uploads/2015/01/updatereusablecontent.png
 [5]: /wp-content/uploads/2015/01/reusablecontentupdated.png