---
title: Géolocalisation dans SQL Server
author: Ghislain

date: 2018-05-03T00:00:00+00:00
url: /2018/05/03/geolocalisation-dans-sql-server/
featuredImage: /wp-content/uploads/2018/05/SQL-geolocation-1.png
categories:
  - Databases
tags:
  - SQL Server

---
Après avoir travaillé sur la géolocalisation en MongoDB, j’ai voulu découvrir l’équivalent sur SQL Server.

Il existe 2 types pour représenter les informations de type géolocalisation : _geography_ et _geometry_. La différence entre les 2 réside dans le fait que le premier prend en compte la courbure de la terre. C’est négligable sur de petites distances et [pour les personnes qui pensent encore que la terre est plate][1].

Créer une table GeoTable avec une colonne de type _geography_.

{{< highlight sql >}}CREATE TABLE GeoTable   
    ( id int IDENTITY (1,1),  
    GeoCol1 geography not null,
    GeoCol2 AS GeoCol1.STAsText() );  
GO
{{< /highlight >}}

Insérer quelques points pour l’exemple.

{{< highlight sql >}}INSERT INTO GeoTable (GeoCol1) Values (geography::Parse('POINT (0 0)'));
INSERT INTO GeoTable (GeoCol1) Values (geography::Parse('POINT (0 1)'));
INSERT INTO GeoTable (GeoCol1) Values (geography::Parse('POINT (0 2)'));
INSERT INTO GeoTable (GeoCol1) Values (geography::Parse('POINT (0 3)'));
INSERT INTO GeoTable (GeoCol1) Values (geography::Parse('POINT (0 4)'));
INSERT INTO GeoTable (GeoCol1) Values (geography::Parse('POINT (0 5)'));
{{< /highlight >}}

{{< highlight plain >}}1	0xE6100000010C00000000000000000000000000000000	POINT (0 0)
2	0xE6100000010C000000000000F03F0000000000000000	POINT (0 1)
3	0xE6100000010C00000000000000400000000000000000	POINT (0 2)
4	0xE6100000010C00000000000008400000000000000000	POINT (0 3)
5	0xE6100000010C00000000000010400000000000000000	POINT (0 4)
6	0xE6100000010C00000000000014400000000000000000	POINT (0 5)
{{< /highlight >}}

Trouver les 3 points les plus proches d’un point donné.

{{< highlight sql >}}
DECLARE @p geography = 'POINT(0 3)';  
SELECT TOP(3) id, GeoCol2 FROM GeoTable
ORDER BY GeoCol1.STDistance(@p);  
{{< /highlight >}}

{{< highlight plain >}}
4	POINT (0 3)
3	POINT (0 2)
5	POINT (0 4)
{{< /highlight >}}

Mon exemple est basique et l’outil propose plus de possibilités. Outre le point, il existe des lignes, des polygones, des courbes. De même, il est possible de savoir si deux ensembles sont disjoints ou se touchent et de déterminer la ou les intersections.

[SQL Server 2017 &#8211; Créer, construire et interroger des instances geography][2]

 [1]: https://www.nationalgeographic.fr/sciences/un-francais-sur-10-pense-que-la-terre-est-plate
 [2]: https://docs.microsoft.com/fr-fr/sql/relational-databases/spatial/create-construct-and-query-geography-instances?view=sql-server-2017