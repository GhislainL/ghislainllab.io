---
title: Apophénie
author: Ghislain
date: 2019-10-26
url: /un-dessin/apophenie/
categories:
  - Un dessin
tags:
  - sketchplanations

---

![Apophénie](/un-dessin/sketchplanations-apophenie.jpg)

Trouver du sens et des connexions dans des choses indépendantes, comme voir des visages sur des toats, un bonhomme sur la lune ou encore des nuages  fantastiques. La recherche de modèles et de sens dans des données aléatoires semble faire partie de notre nature.

Traduction de la publication de [Sketchplanations](https://www.sketchplanations.com/post/188611704726/apophenia-finding-meaning-and-connections-in)