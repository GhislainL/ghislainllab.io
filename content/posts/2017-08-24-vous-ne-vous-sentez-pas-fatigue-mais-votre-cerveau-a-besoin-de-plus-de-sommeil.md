---
title: Vous ne vous sentez pas fatigué mais votre cerveau a besoin de plus de sommeil
author: Ghislain

date: 2017-08-24T00:00:00+00:00
url: /2017/08/24/vous-ne-vous-sentez-pas-fatigue-mais-votre-cerveau-a-besoin-de-plus-de-sommeil/
featuredImage: /wp-content/uploads/2017/08/tired.jpg
categories:
  - Développement personnel
tags:
  - Art Markman
  - repos
  - sommeil

---
Vous ne vous sentez peut-être pas fatigué, mais votre déficit de sommeil diminue probablement votre fonctionnement cognitif.

Cet article a été originellement publié le 6 juin 2017 par Art Markman sous le titre [**You Might Not Feel Tired, But Your Brain Needs More Sleep**][1]

Dormez-vous assez ? Avant de répondre, essayez ceci : durant votre journée de travail, asseyez-vous et fermez les yeux. Pour les prochaines minutes, concentrez-vous sur votre respiration, en faisant attention à chaque inspiration et expiration. Il s’agit d’un exercice standard de pleine conscience (mindfulness). Si vous devenez somnolent, désolé, mais vous ne dormez pas assez.

Vous vous sentez raisonnablement reposé en vous rendant à votre travail. Vous sortez peut-être énergisé par les idées résultantes du brainstorming (1) avec votre équipe. Mais ces suppositions subjectives indiquent vraisemblablement le niveau d’énergie à un instant donné – et non si vous êtes bien reposé. Voici pourquoi votre cerveau a probablement besoin de plus de sommeil que vous ne lui donnez, même si vous ne vous sentez pas somnolent à cet instant-même.

## Chacun réagit différemment au manque de sommeil {#chacun-réagit-différemment-au-manque-de-sommeil}

Une personne lambda a besoin d’environ 8 heures de sommeil, bien qu’il existe des grosses différences entre les individus concernant la durée optimale de sommeil et que la quantité de sommeil diminue avec l’âge. Toutefois, la majorité des gens n’ont pas la quantité de sommeil dont ils ont besoin; une étude suggère (2) que 6h de sommeil – ce qui peut sembler raisonnable – peut être aussi mauvais que pas de sommeil du tout.

Les êtres humains sont des créatures étonnamment résilientes ; nos corps et esprits sont plutôt doués pour s’accommoder de choses qui ne sont pas vraiment bonnes pour nous, comme la privation chronique de sommeil. Il est facile de voir comment les gens rentrent dans un schéma où ils manquent de sommeil, et concluent ensuite qu’il s’agit de leur besoin normal.

Même après une journée de travail vraiment longue, vous avez besoin de décompresser durant la soirée, alors vous consultez les réseaux sociaux ou regardez un peu la télévision. Avant de vous en rendre compte, il est déjà une heure après l’heure où vous auriez dû dormir. Les weekends, vous sortez et rester tard pour un concert ou un film avec l’espoir de rattraper votre sommeil dans la matinée, mais votre réveil interne vous réveille aussi surement que le réveil le fait en semaine. Sans le savoir, vous avez accumulé une belle dette de sommeil.

Vous ne vous sentez peut-être qu’un peu fatigué et un café supplémentaire vous permettra de très bien vous en sortir. Mais sous la surface, votre fonctionnement cognitif paie un prix invisible. Voici quelques-uns des domaines où votre cerveau pourrait être en difficulté, même si vous n’en avez pas conscience.

## Concentration {#concentration}

L’une des premières victimes du manque de sommeil est votre capacité de concentration (3). Il est plus dur de rester concentrer sur ce que vous faites quand vous êtes fatigué.

C’est particulièrement vrai quand vous fixez le rythme de votre travail. Les jours de fatigue, vous vous verrez changer de taches et être distrait par les nouveaux emails, les voix en provenance du bureau voisin, et même les pensées qui traversent votre esprit. Sans compter sur les micro-siestes durant la lecture d’un document épais.

## Mémoire de travail {#mémoire-de-travail}

Le manque de sommeil réduit aussi la capacité de votre mémoire de travail (4). La mémoire de travail correspond à la quantité d’informations que vous pouvez garder à l’esprit à un instant t. Plus la capacité de votre mémoire de travail est grande, plus vous serez capable d’avoir des pensées complexes. Résoudre les puzzles mentaux les plus difficiles est une tâche pour votre mémoire (5) – pour trouver une bonne solution, vous avez besoin d’extraire les informations pertinentes de votre mémoire. Mais quand votre mémoire de travail est diminuée, il vous faudra plus de temps pour trouver ce dont vous avez besoin – et par conséquent prendre les bonnes décisions.

## Apprentissage {#apprentissage}

Troisièmement, quand vous manquez de sommeil, il est plus difficile de se souvenir de nouvelles choses. Le sommeil possède deux effets différents sur la mémoire : le souvernir est l’un d’eux. Cela joue aussi sur le fonctionnement de l’hippocampe (6), qui est crucial dans l’apprentissage de nouvelles choses. Et malheureusement, boire du café n’améliorera pas le fonctionnement de votre hippocampe, même si vous avez l’impression d’être plus alerte.

Croyez-le ou non, vous continuez à créer des souvenirs durant votre sommeil, dans un procédé nommé « consolidation » (7). Cela correspond au moment où votre cerveau assemble les expériences récentes que vous avez eu en des souvenirs cohérents pour les stocker. Si vous ne dormez pas assez, ce procédé de consolidation ne peut pas se dérouler efficacement, et à votre réveil, vous aurez du mal à vous souvenir des nouvelles informations rencontrées.

## Humeur {#humeur}

Le manque de sommeil joue aussi sur votre humeur – comme vous vous en doutiez déjà. Cependant nous avons tendance à attribuer « être mauvaise humeur » à un large panel d’expériences. Peut-être que vous attribuer cela à la demande ennuyeuse de votre chef ce matin, au trajet matinal pire que d’habitude qui vous a mis les nerfs à fleur de peau. Mais il est fort possible que ces choses vous auraient moins irritées si vous étiez plus reposé.

En fait, perturber votre cycle de sommeil peut augmenter les symptômes de dépression (8). Cela ne veut pas dire que vous êtes cliniquement en dépression (ou même fatigué) uniquement parce que vous trouvez difficile d’être gentil avec vos collègues de travail. Mais votre manque de sommeil peut être le coupable.

## Dommage à long terme {#dommage-à-long-terme}

Enfin la qualité de votre sommeil peut avoir des conséquences à long terme sur votre cerveau. En vieillissant – disons dans la quarantaine et cinquantaine – le manque de sommeil a un impact moins important que quand on est plus jeune, vingtaine ou trentaine. Avec le temps, il devient donc plus difficile de dire si vous avez suffisamment dormi. Ce qui ne veut pas dire pour autant que vous pouvez croire vos instincts subjectifs quand vous êtes jeunes.

Mieux vous dormez – quel que soit votre âge –, meilleure est la protection que vous construisez pour votre cerveau dans les années futures. De mauvaises habitudes de sommeil, même au milieu de votre vie, sont associées à des problèmes cognitifs plus importants, comme la démence sénile, plus tard dans la vie.

Vous ne pensez probablement pas que le sommeil soit très important. Votre vie ne s’écroule pas lorsque vous dormez moins que ce que vous ne devriez durant une nuit. Mais cela vous diminue de manière lente et invisible. Dans la durée, le manque de sommeil diminue réellement votre productivité et vos fonctions cognitives. Alors ce soir, éteignez la télévision, éloignez-vous de votre téléphone, sautez dans votre lit et reposez-vous. Vous vous remercierez plus tard – et pas seulement demain mais aussi dans des années.

**A propos de l’auteur** :

Le docteur [Art Markman][2] est un professeur de psychologie et marketing à l’Université du Texas à Austin et le directeur et fondateur du programme « Human Dumensions of Organizations ». Art est l’auteur de Smart Thinking, Habits of Leadership, Smart Change et Brain Briefs. Ainsi que Two guys on your head avec Bod Duke, qui traite sur l’usage de la motivation pour changer votre comportement au travail et à la maison.

**Références**

1 : https://www.fastcompany.com/3067587/why-other-people-wreck-brainstorms-and-how-to-stop-them

2 : https://www.fastcompany.com/3057465/why-six-hours-of-sleep-is-as-bad-as-none-at-all

3 : http://oem.bmj.com/content/57/10/649.short

4 : http://psycnet.apa.org/record/2010-11746-004

5 : https://www.fastcompany.com/40422768/heres-exactly-how-to-procrastinate-on-really-hard-projects

6 : https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2783639/

7 : http://www.sciencedirect.com/science/article/pii/0301051178900315

8 : http://www.sciencedirect.com/science/article/pii/S0006322399001250

 [1]: https://www.fastcompany.com/40427741/you-might-not-feel-tired-but-your-brain-needs-more-sleep
 [2]: http://liberalarts.utexas.edu/psychology/faculty/markman