---
title: Utiliser PowerShell avec SharePoint 2007
author: Ghislain

date: 2016-05-23T00:00:00+00:00
url: /2016/05/23/utiliser-powershell-avec-sharepoint-2007/
categories:
  - PowerShell
  - SharePoint

---
Maintenant il est commun de manipuler SharePoint avec du PowerShell. Alors quand vous devez retravailler avec MOSS / WSS3, vous avez probablement ressenti – comme moi &#8211; ce petit moment de tristesse. Dans cet article nous verrons comment nous passer des snapins et utiliser PowerShell avec SharePoint 2007.

Avant de manipuler les objets, il faut charger l’assembly Microsoft.SharePoint<figure class="highlight"> 

{{< highlight powershell>}}[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint") {{< / highlight  >}}

Ensuite vous pourrez manipuler les objets SP* en PowerShell.<figure class="highlight"> 

{{< highlight powershell>}}#Get-SPFarm 

$farm = Get-SPFarm  
$farm = [Microsoft.SharePoint.Administration.SPFarm]::Local 


#Get-SPWebApplication  
$webApplication = Get-SPWebApplication http://mywebapp 
$webApplication = [Microsoft.SharePoint.Administration.SPWebApplication]::Lookup("http://mywebapp")  

 
#Get-SPSite  
$site = Get-SPSite http://mysitecollection 
$site = New-Object Microsoft.SharePoint.SPSite("http://mysitecollection") 

 
#Get-SPWeb  
$site = Get-SPWeb http://mysitecollection/subsite 
$web = (New-Object Microsoft.SharePoint.SPSite("http://mysitecollection/subsite")).OpenWeb() 
$web = (New-Object Microsoft.SharePoint.SPSite("http://mysitecollection")).allwebs["subsite"] {{< / highlight  >}}

Vous disposez ainsi des objets élémentaires pour interagir avec SharePoint 2007. Si les objets que vous utilisez n’ont pas évolués entre les versions de SharePoint, vos scripts fonctionnent sur vos installations onPremises.