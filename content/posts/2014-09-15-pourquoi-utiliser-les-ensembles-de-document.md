---
title: Pourquoi utiliser les ensembles de document ?
author: Ghislain

date: 2014-09-15T07:12:01+00:00
url: /2014/09/15/pourquoi-utiliser-les-ensembles-de-document/
categories:
  - SharePoint
tags:
  - ECM
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013

---
Les ensembles de document (document set) permettent de gérer un ensemble d’éléments comme une seule entité. Il s’agit d’un type de contenu traditionnel ce qui veut dire entre autre qu’il peut porter des métadonnées et servir de base pour construire de nouveaux types de contenu.

Cela peut vous être utile pour gérer :

  * les réponses aux projets. Vous vous organisez pour avoir un fichier Devis, un fichier Présentation et un fichier Cahier des Charges.
  * les réponses à un appel d’offre. Vous groupez l’expression de besoins, avec les réponses et un tableau comparatif des réponses.

# Fonctionnalités {#fonctionnalités}

  * **Type d’éléments constituants** : vous avez la possibilité de limiter les types de contenu des éléments inclus dans un ensemble de document. Uniquement des documents par exemple.

[<img class="alignnone wp-image-239 size-medium" src="/wp-content/uploads/2014/09/docset1-300x122.png?fit=300%2C122" alt="docSet1" srcset="/wp-content/uploads/2014/09/docset1.png?resize=300%2C122 300w, /wp-content/uploads/2014/09/docset1.png?w=835 835w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />][1]

  * **Contenu par défaut** : vous pouvez définir quelques sont les documents présents à la création d’un élément.

[<img class="alignnone wp-image-240 size-medium" src="/wp-content/uploads/2014/09/docset2-300x75.png?fit=300%2C74" alt="docSet2" data-recalc-dims="1" />][2]

  * **Métadonnées partagées** : vous pouvez définir quelles métadonnées définies au niveau de l’ensemble de document seront propagées aux éléments constituants

[<img class="alignnone wp-image-241 size-medium" src="/wp-content/uploads/2014/09/docset3-300x125.png?fit=300%2C125" alt="docSet3" srcset="/wp-content/uploads/2014/09/docset3.png?resize=300%2C125 300w, /wp-content/uploads/2014/09/docset3.png?w=335 335w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />][3]

  * **Page d’accueil personnalisable** : par défaut elle affiche uniquement les métadonnées et le contenu de l’ensemble de contenu. Vous avez la possibilité de personnaliser cette page de webpart.

[<img class="alignnone wp-image-242 size-medium" src="/wp-content/uploads/2014/09/docset4-300x207.png?fit=300%2C207" alt="docSet4" srcset="/wp-content/uploads/2014/09/docset4.png?resize=300%2C207 300w, /wp-content/uploads/2014/09/docset4.png?w=804 804w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />][4]

  * **Versioning **: vous disposez de 2 versionings. Le premier au niveau des éléments constituants fonctionne de manière classique. Le second au niveau de l’ensemble de document est manuel, vous devez passer par un onglet spécifique dans le ruban.

[<img class="alignnone wp-image-243 size-medium" src="/wp-content/uploads/2014/09/docset5-300x86.png?fit=300%2C85" alt="docSet5" data-recalc-dims="1" />][5]

  * **Action dans le flux de travail** (compatibilité SharePoint 2010)

[<img class="alignnone wp-image-238 size-medium" src="/wp-content/uploads/2014/09/docset6-300x161.png?fit=300%2C161" alt="docSet6" srcset="/wp-content/uploads/2014/09/docset6.png?resize=300%2C161 300w, /wp-content/uploads/2014/09/docset6.png?w=368 368w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />][6]

 

# Bon à savoir : {#bon-à-savoir}

  * Pas d’archivage local (in-place)
  * Pas d’espace de travail lié au document (document workplace)
  * L’archivage transforme le document set en archive zip.

[<img class="alignnone wp-image-244 size-medium" src="/wp-content/uploads/2014/09/docset10-300x102.png?fit=300%2C102" alt="docSet10" srcset="/wp-content/uploads/2014/09/docset10.png?resize=300%2C102 300w, /wp-content/uploads/2014/09/docset10.png?w=1000 1000w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />][7]

 

# Comment les activer ? {#comment-les-activer-}

Pour utiliser les ensembles de document dans une collection de site, vous devez active la fonctionnalité Document Set.

  * Sur la page Site Settings, dans la section Site Collection Administration, cliquer sur Site collection features.
  * Dans la page des fonctionnalités, cliquer sur le bouton Activer associé à Document Sets.

**Références **:

[Document Sets planning (SharePoint Server 2010)][8]

[Plan document sets in SharePoint Server 2013][9]

 

 [1]: /wp-content/uploads/2014/09/docset1.png
 [2]: /wp-content/uploads/2014/09/docset2.png
 [3]: /wp-content/uploads/2014/09/docset3.png
 [4]: /wp-content/uploads/2014/09/docset4.png
 [5]: /wp-content/uploads/2014/09/docset5.png
 [6]: /wp-content/uploads/2014/09/docset6.png
 [7]: /wp-content/uploads/2014/09/docset10.png
 [8]: http://technet.microsoft.com/en-us/library/ff603637%28office.14%29.aspx
 [9]: http://technet.microsoft.com/en-us/library/ff603637%28v=office.15%29.aspx