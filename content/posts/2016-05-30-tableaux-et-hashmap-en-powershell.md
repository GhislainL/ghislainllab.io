---
title: Tableaux et Hashmap en PowerShell
author: Ghislain

date: 2016-05-30T00:00:00+00:00
url: /2016/05/30/tableaux-et-hashmap-en-powershell/
categories:
  - PowerShell

---
Dans un post précédent nous avons abordé la création d’objet en PowerShell, ajourd’hui nous parlons des structures de données plus simples avec les tableaux et hashmap en PowerShell. Il s’agit plus d’un aide-mémoire que d’un article d’actualité.

# Tableau {#tableau}

Wikipedia présente le tableau comme _une structure de données qui consiste en un ensemble d’éléments ordonnés accessibles par leur indice (ou index)_.<figure class="highlight"> 

{{< highlight powershell>}}# instancier 
[System.Collections.ArrayList]$gothamHeroes = @("Batman", "Robin", "Deadpool")
# [System.Collections.ArrayList] permet d'utiliser les méthodes Remove() et RemoveAt()

# afficher
$gothamHeroes

# ajouter un élément
$gothamHeroes += "Nightwing"

# obtenir l'index d'un élément
$gothamHeroes.IndexOf("Deadpool")

# supprimer un élément :
## par son index
$gothamHeroes.RemoveAt("2")
## par sa valeur
$gothamHeroes.Remove("Deadpool")

# savoir si une valeur est présente
$gothamHeroes.Contains("Joker")
$gothamHeroes -contains "Joker"{{< / highlight  >}}

Simple d’utilisation, il est malgré tout assez limité et ne répond pas à toutes les utilisations.

# Hashmap {#hashmap}

Le hashmap ou tableau associatif _est un type de données associant à un ensemble de clefs un ensemble correspondant de valeurs._ C’est donc une structure qui contient des paires de clé-valeur, sachant que les clés sont uniques.<figure class="highlight"> 

{{< highlight powershell>}}# instancier
$gothamHeroes = @{"Batman" = "Bruce Wayne"; "Robin" = "Dick Grayson" ; "Deadpool" = "Wade Wilson" }

# afficher
$gothamHeroes

# modifier une paire
$gothamHeroes.Set_Item("Robin", "Jason Todd")

# ajouter une paire
$gothamHeroes.Add("Nightwing", "Dick Grayson")

# supprimer une paire
$gothamHeroes.Remove("Deadpool")

# récupérer la valeur associée à la clé
$gothamHeroes.Get_Item("Robin")
&gt; Jason Todd

# savoir si la collection contient une paire par sa clé
$gothamHeroes.ContainsKey("Joker")
&gt; False

# savoir si la collection contient une paire par sa valeur
$gothamHeroes.ContainsValue("Damian Wayne")
&gt; False{{< / highlight  >}}

A garder sous le coude.