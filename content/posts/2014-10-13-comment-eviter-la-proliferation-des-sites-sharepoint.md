---
title: Comment éviter la prolifération des sites SharePoint ?
author: Ghislain

date: 2014-10-13T07:54:00+00:00
url: /2014/10/13/comment-eviter-la-proliferation-des-sites-sharepoint/
categories:
  - SharePoint
tags:
  - ECM
  - SharePoint 2013

---
Pour éviter la prolifération des sites, vous avez besoin de définir un cycle de vie pour vos collections de site. Il s’agit d’une des composantes d’un plan de gouvernance. Pour gérer le cycle de vie des sites,  SharePoint vous propose OOB les outils suivants :

  * Confirmation de l’utilisation des sites et suppression
  * Stratégie de site

<table style="height: 102px;" width="504">
  <tr>
    <td width="205">
    </td>
    
    <td width="100">
      <strong>SP2010</strong>
    </td>
    
    <td width="100">
      <strong>SP2013</strong>
    </td>
  </tr>
  
  <tr>
    <td width="205">
      <strong>Site Usage Confirmation and Deletion</strong>
    </td>
    
    <td width="100">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="100">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="205">
      <strong>Stratégie de site / Site Policies</strong>
    </td>
    
    <td width="100">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="100">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

La stratégie propose plus de paramétrage et de précision tout en s’appliquant sans intervention humaine. Tandis que la confirmation d’usage, plus violente et moins precise nécessite une intervention des propriétaires des collections de site.

# Confirmation de l’utilisation des sites / Site Usage Confirmation and deletion {#confirmation-de-lutilisation-des-sites--site-usage-confirmation-and-deletion}

Vous pouvez configurer cette fonctionnalité dans la centrale d’administration au niveau de chaque application web. Les options sont simples et s’appliquent à toutes les collections de site de l’application web.

  * Notification : 
      * SharePoint envoie un mail aux propriétaires de la collection X jours après sa création ou la dernière confirmation d’utilisation ;
      * Vous définissez aussi quand est vérifié l’utilisation du site et l’envoie de message. Tous les jours à minuit par exemple
  * Suppression automatique : vous pouvez choisir de supprimer automatiquement un site après Y absence de confirmation au mail d’utilisation.

[<img class="wp-image-255 size-large" src="/wp-content/uploads/2014/10/sp2013-siteusedeletion-1024x532.png?fit=890%2C462" alt="SP2013-SiteUseDeletion" srcset="/wp-content/uploads/2014/10/sp2013-siteusedeletion.png?resize=300%2C156 300w, /wp-content/uploads/2014/10/sp2013-siteusedeletion.png?resize=1024%2C532 1024w, /wp-content/uploads/2014/10/sp2013-siteusedeletion.png?w=1046 1046w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][1]  
_SP2013 – Site use confirmation_

# Stratégie de site {#stratégie-de-site}

Les stratégies de site sont disponibles depuis SharePoint 2013. Vous disposez d’un mécanisme de gestion de vie pour une collection de site similaire à celui pour un type de contenu.

Vous disposez de 3 options pour construire votre stratégie:

  * Ne pas fermer ou supprimer le site automatiquement
  * Supprimer les sites automatiquement

[<img class="wp-image-256 size-medium" src="/wp-content/uploads/2014/10/sitepolicy-option2-300x208.png?fit=300%2C207" alt="SitePolicy-option2" data-recalc-dims="1" />][2]  
_SP2013 – Site Policy : option de suppression_

  * Fermer et supprimer les sites automatiquement

[<img class="wp-image-257 size-medium" src="/wp-content/uploads/2014/10/sitepolicy-option3-300x226.png?fit=300%2C225" alt="SitePolicy-option3" data-recalc-dims="1" />][3]  
_SP2013 – Site policy : archiver puis supprimer_

Vous pouvez spécifier qu’une collection de site fermée soit automatiquement placée en lecture seule. Auquel cas, un bandeau en haut de page prévient les utilisateurs que le site n’est plus modifiable. Si vous avez activé la suppression automatique, le bandeau affichera la date de suppression.

[<img class="wp-image-254 size-full" src="/wp-content/uploads/2014/10/sitepolicies2.gif?fit=755%2C515" alt="SitePolicies2" data-recalc-dims="1" />][4]  
_SP2013 – Appliquer une site policy_

Vous définissez vos stratégies de site dans le site racine de la collection de site. Puis vous pouvez sélectionner la stratégie pour chaque site indépendamment.

**Références :**

[Vue d’ensemble des stratégies de site dans SP2013][5]

[Gérer les collections de sites non utilisées dans SP2013][6]

 [1]: /wp-content/uploads/2014/10/sp2013-siteusedeletion.png
 [2]: /wp-content/uploads/2014/10/sitepolicy-option2.png
 [3]: /wp-content/uploads/2014/10/sitepolicy-option3.png
 [4]: /wp-content/uploads/2014/10/sitepolicies2.gif
 [5]: http://technet.microsoft.com/fr-FR/library/jj219569%28v=office.15%29.aspx
 [6]: http://technet.microsoft.com/fr-fr/library/cc262420%28v=office.15%29.aspx