---
title: Meilleur moyen OOB pour dupliquer une collection de site
author: Ghislain

date: 2015-01-26T08:38:14+00:00
url: /2015/01/26/meilleur-moyen-oob-pour-dupliquer-une-collection-de-site/
categories:
  - PowerShell
  - SharePoint
tags:
  - Backup-SPSite
  - Export-SPWeb
  - Import-SPWeb
  - PowerShell
  - Restore-SPSite
  - SharePoint
  - SharePoint 2013

---
Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

](http://technet.microsoft.com/fr-fr/library/ff607613%28v=office.15%29.aspx)

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

](http://technet.microsoft.com/fr-fr/library/ff607613%28v=office.15%29.aspx)

](http://technet.microsoft.com/fr-fr/library/ff607895%28v=office.15%29.aspx)

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

](http://technet.microsoft.com/fr-fr/library/ff607613%28v=office.15%29.aspx)

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

](http://technet.microsoft.com/fr-fr/library/ff607613%28v=office.15%29.aspx)

](http://technet.microsoft.com/fr-fr/library/ff607895%28v=office.15%29.aspx)

](http://technet.microsoft.com/fr-fr/library/ff607901%28v=office.15%29.aspx)

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

](http://technet.microsoft.com/fr-fr/library/ff607613%28v=office.15%29.aspx)

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

](http://technet.microsoft.com/fr-fr/library/ff607613%28v=office.15%29.aspx)

](http://technet.microsoft.com/fr-fr/library/ff607895%28v=office.15%29.aspx)

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

](http://technet.microsoft.com/fr-fr/library/ff607613%28v=office.15%29.aspx)

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

[Dernièrement un utilisateur me contacte après avoir créé un sous-site avec des formulaires InfoPath et des workflows associés à des listes : « J’aimerai en faire une nouvelle collection de site dans un autre environnement ». Vous imaginez mon embarras face à sa demande. Pour éviter de me retrouver dans cette situation, je partage cette synthèse avec vous.

A la main des administrateurs du site, l’option **Sauvegarder en tant que modèle** qui permet de créer un modèle réutilisable à l’intérieur de la collection de site. Parmi les limitations, la perte des permissions, des workflows en cours, de l’historique des éléments, valeurs des champs taxonomie, valeurs des champs personnes et groupes, pages de publications.

<https://support.office.com/en-ie/article/Save-a-SharePoint-site-as-a-template-5a4eb024-d1fa-4681-a4ac-e6bd1e3209ba#__toc247465693>

Les administrateurs de la ferme disposent de bien plus d’option :

  * Backup/restore qui fonctionne sur un objet SPSite
  * Export/import qui fonctionne sur un objet SPWeb ou SPList
  * Sauvegarde de la base de contenu si la collection de site est dans une base dédiée
  * Sauvegarde par l’interface de la centrale d’administration qui permet d’accéder à l’équivalent des commandes PowerShell

[<img class="alignnone size-full wp-image-416" src="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?fit=389%2C93" alt="2015-01-05 15_57_18-mRemoteNG - confCons.GLA.20140211.xml" srcset="/wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?resize=300%2C72 300w, /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png?w=389 389w" sizes="(max-width: 389px) 100vw, 389px" data-recalc-dims="1" />][1]

Le tableau ci-dessous apporte une vue synthétique des possibilités de chaque couple de commande

<table>
  <tr>
    <td width="163">
    </td>
    
    <td width="158">
      Backup / Restore
    </td>
    
    <td width="156">
      Export / Import
    </td>
    
    <td width="143">
      Content database attachment
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Audit event
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & doclib
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Custom list & site properties
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Event receiver
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Versioning
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2015/01/partial.png"><img class="alignnone size-full wp-image-418" src="/wp-content/uploads/2015/01/partial.png?fit=16%2C16" alt="partial" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td width="163">
      Workflow
    </td>
    
    <td width="158">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="156">
      <a href="/wp-content/uploads/2014/10/addredx2.png"><img class="alignnone size-full wp-image-258" src="/wp-content/uploads/2014/10/addredx2.png?fit=16%2C16" alt="addRedX2" data-recalc-dims="1" /></a>
    </td>
    
    <td width="143">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
  </tr>
</table>

**Note 1 :** Si vous avez déployé du code personnalisé, il faudra le déployer dans l’environnement cible.

**Note 2 :** Pour un backup/restore, il faut que le site de réception soit du même modèle que celui sauvegardé.

Au final, voici les méthodes préconisées classées selon leur efficacité :

  * Attacher la base de contenu ;
  * Backup/restore
  * Export/import

**Références :**

](http://technet.microsoft.com/fr-fr/library/ff607613%28v=office.15%29.aspx)

](http://technet.microsoft.com/fr-fr/library/ff607895%28v=office.15%29.aspx)

](http://technet.microsoft.com/fr-fr/library/ff607901%28v=office.15%29.aspx)

](http://technet.microsoft.com/fr-fr/library/ff607788%28v=office.15%29.aspx)

<https://naimmurati.wordpress.com/2013/04/13/site-collection-backuprestore-vs-site-exportimport-with-focus-on-running-workflows-version-history-and-auditing/>

 [1]: /wp-content/uploads/2015/01/2015-01-05-15_57_18-mremoteng-confcons-gla-20140211-xml.png