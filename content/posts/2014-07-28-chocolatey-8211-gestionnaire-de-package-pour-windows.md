---
title: Chocolatey – gestionnaire de package pour Windows
author: Ghislain

date: 2014-07-28T07:30:38+00:00
url: /2014/07/28/chocolatey-8211-gestionnaire-de-package-pour-windows/
categories:
  - PowerShell
tags:
  - Chocolatey
  - Outils

---
Dernièrement j’ai découvert **Chocolatey**, il s’agit d’un gestionnaire de paquets pour Windows en ligne de commande. Ces outils surtout connus dans le monde Unix, permettent de gérer de manière centralisée l’installation, la mise à jour et la suppression des logiciels sur un système informatique. Je me souviens ces années avec les commandes apt-get de Debian, pacman d’Archlinux, emerge de Gentoo.

Version actuelle : 0.9.8.27

1996 paquets dans le dépôt (repository)

Pré-requis:

  * Windows XP/Vista/7/8/2003/2008
  * .NET Framework 4.0
  * PowerShell 2.0

**Installation de Chocolatey** dans une invite de commande en mode admin

{{< highlight plain >}}

Ou dans une console powershell en mode admin

{{< highlight powershell>}}iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1')){{< /highlight >}}

**Rechercher un package**

{{< highlight plain >}}

**Installer un package**

{{< highlight plain >}}

**Installer un package dans un autre repertoire que celui par défaut**

{{< highlight plain >}}

[<img class="alignnone size-full wp-image-163" src="/wp-content/uploads/2014/07/chocolatey-1.png?fit=677%2C618" alt="Chocolatey-1" srcset="/wp-content/uploads/2014/07/chocolatey-1.png?resize=300%2C274 300w, /wp-content/uploads/2014/07/chocolatey-1.png?w=677 677w" sizes="(max-width: 677px) 100vw, 677px" data-recalc-dims="1" />][1]

**Mettre à jour un package**

{{< highlight plain >}}

**Mettre à jour tous les packages**

{{< highlight plain >}}

**Supprimer un package**

{{< highlight plain >}}

[<img class="alignnone size-full wp-image-164" src="/wp-content/uploads/2014/07/chocolatey-2.png?fit=677%2C342" alt="Chocolatey-2" srcset="/wp-content/uploads/2014/07/chocolatey-2.png?resize=300%2C152 300w, /wp-content/uploads/2014/07/chocolatey-2.png?w=677 677w" sizes="(max-width: 677px) 100vw, 677px" data-recalc-dims="1" />][2]

**Mettre à jour Chocolatey**

{{< highlight plain >}}

**Supprimer Chocolatey**

Supprimer le répertoire de Chocolatey (par défaut : C:ProgramDatachocolatey) et la variable d’environnement ChocolateyInstall. La base de registre ne comporte pas d’information.

Si vous souhaitez conserver une interface graphique, **ChocolateyGUI** répond à vos attentes.

[<img class="alignnone size-full wp-image-162" src="/wp-content/uploads/2014/07/chocolateygui-1.png?fit=890%2C514" alt="ChocolateyGUI-1" srcset="/wp-content/uploads/2014/07/chocolateygui-1.png?resize=1024%2C592 1024w, /wp-content/uploads/2014/07/chocolateygui-1.png?w=1037 1037w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][3]

Après un survol rapide, j’ai trouvé un outil intéressant et pratique, qui permet de gagner du temps sur des taches à peu de valeur ajoutée.

**Références :**

[Chocolatey.org][4]

[List of software package management systems – Windows][5]

[Scott Hanselman – Is the Windows user ready for apt-get?][6]

 [1]: /wp-content/uploads/2014/07/chocolatey-1.png
 [2]: /wp-content/uploads/2014/07/chocolatey-2.png
 [3]: /wp-content/uploads/2014/07/chocolateygui-1.png
 [4]: http://chocolatey.org/
 [5]: http://en.wikipedia.org/wiki/List_of_software_package_management_systems#Microsoft_Windows
 [6]: //www.hanselman.com/blog/IsTheWindowsUserReadyForAptget.aspx