---
title: "Internationalisation d'AngularJS et utilisation d'un timer"
author: Ghislain

date: 2015-05-12T07:30:22+00:00
url: /2015/05/12/internationalisation-d039angularjs-et-utilisation-d039un-timer/
categories:
  - JavaScript
tags:
  - Angular
  - Bootstrap

---
Je continue ma découverte d’AngularJS avec une application de calcul qui aborde l’internationalisation, l’utilisation d’un timer et Bootstrap. Elle serait utile dans un film d’action ou de science-fiction. Fréquemment des personnages se retrouvent menacés d’asphyxie dans un lieu clos. Cette application leur fournirait un compte à rebours en fonction du volume de la pièce et du nombre de personne.

[<img class="wp-image-788 size-large" src="/wp-content/uploads/2015/05/sealedspacetimer-1024x449.png?fit=1020%2C447" alt="Sealed space timer - page d'accueil" srcset="/wp-content/uploads/2015/05/sealedspacetimer.png?resize=300%2C132 300w, /wp-content/uploads/2015/05/sealedspacetimer.png?resize=1024%2C449 1024w" sizes="(max-width: 1024px) 100vw, 1024px" data-recalc-dims="1" />][1]  
_Sealed space timer – page d’accueil_

## Timer {#timer}

Pour le compte à rebours j’ai utilisé **Angular Timer** de Siddique Hameed. Il s’agit d’une directive qui permet de facilement créer des chronomètres comme des comptes à rebours.

Le code suivant permet d’afficher un timer avec les caractéristiques suivantes :

  * un compte à rebours
  * le décompte au format hh:mm:ss
  * mise à jour toutes les secondes
  * démarrage manuel
  * durée du compte à rebours égale à la valeur retournée par la fonction computeTimeLeft()

{{< highlight xml >}}<timer interval="1000" countdown="computeTimeLeft()" autostart="false">
   ::
</timer></pre>

Pour récupérer la valeur du timer, il faut passer par l’événement timer-tick. J’étais un peu désarçonné par cette logique mais ça fonctionne. Initialement j’ai voulu que la couleur de la zone contenant le timer passe du jaune au rouge en fonction du décompte. Techniquement c’est faisable mais visuellement ce n’était pas concluant.

{{< highlight js >}}$scope.$on('timer-tick', function (event, data) {
   if ($scope.timerRunning ){
      $scope.computedValue = computeIt(data.millis);
      $scope.$apply();
   }
});</pre>

## Internationalisation d’AngularJS {#internationalisation-dangularjs}

L’internationalisation (i18n) et la localisation (l10n) permettent de développer des applications adaptées au contexte international. Ainsi le contenu est traduit et les conventions locales sont respectées. Le guide d’AngularJS contient une section traitant du sujet : <https://docs.angularjs.org/guide/i18n>

Il existe plusieurs composants répondant au besoin mais le fonctionnement reste similaire:

  * Utiliser un filtre
  * Améliorer l’architecture en passant par un service
  * Interpolation
  * Pluralization

Pour aller plus loin, l’article [Je continue ma découverte d’AngularJS avec une application de calcul qui aborde l’internationalisation, l’utilisation d’un timer et Bootstrap. Elle serait utile dans un film d’action ou de science-fiction. Fréquemment des personnages se retrouvent menacés d’asphyxie dans un lieu clos. Cette application leur fournirait un compte à rebours en fonction du volume de la pièce et du nombre de personne.

[<img class="wp-image-788 size-large" src="/wp-content/uploads/2015/05/sealedspacetimer-1024x449.png?fit=1020%2C447" alt="Sealed space timer - page d'accueil" srcset="/wp-content/uploads/2015/05/sealedspacetimer.png?resize=300%2C132 300w, /wp-content/uploads/2015/05/sealedspacetimer.png?resize=1024%2C449 1024w" sizes="(max-width: 1024px) 100vw, 1024px" data-recalc-dims="1" />][1]  
_Sealed space timer – page d’accueil_

## Timer {#timer-1}

Pour le compte à rebours j’ai utilisé **Angular Timer** de Siddique Hameed. Il s’agit d’une directive qui permet de facilement créer des chronomètres comme des comptes à rebours.

Le code suivant permet d’afficher un timer avec les caractéristiques suivantes :

  * un compte à rebours
  * le décompte au format hh:mm:ss
  * mise à jour toutes les secondes
  * démarrage manuel
  * durée du compte à rebours égale à la valeur retournée par la fonction computeTimeLeft()

{{< highlight xml >}}<timer interval="1000" countdown="computeTimeLeft()" autostart="false">
   ::
</timer></pre>

Pour récupérer la valeur du timer, il faut passer par l’événement timer-tick. J’étais un peu désarçonné par cette logique mais ça fonctionne. Initialement j’ai voulu que la couleur de la zone contenant le timer passe du jaune au rouge en fonction du décompte. Techniquement c’est faisable mais visuellement ce n’était pas concluant.

{{< highlight js >}}$scope.$on('timer-tick', function (event, data) {
   if ($scope.timerRunning ){
      $scope.computedValue = computeIt(data.millis);
      $scope.$apply();
   }
});</pre>

## Internationalisation d’AngularJS {#internationalisation-dangularjs-1}

L’internationalisation (i18n) et la localisation (l10n) permettent de développer des applications adaptées au contexte international. Ainsi le contenu est traduit et les conventions locales sont respectées. Le guide d’AngularJS contient une section traitant du sujet : <https://docs.angularjs.org/guide/i18n>

Il existe plusieurs composants répondant au besoin mais le fonctionnement reste similaire:

  * Utiliser un filtre
  * Améliorer l’architecture en passant par un service
  * Interpolation
  * Pluralization

Pour aller plus loin, l’article](http://blog.trifork.com/2014/04/10/internationalization-with-angularjs/) détaille l’aspect technique.

Mon choix s’est porté sur **Angular-Translate**.

Les traductions sont stockées au format JSON dans des fichiers séparés. Les fichiers sont chargés uniquement si c’est nécessaire. La syntaxe dans le HTML est facile et – même si je n’en ai pas eu besoin – il est possible d’insérer des variables dans ces textes.

{{< highlight xml >}}<a class="navbar-brand" href="#/home">SITE_TITLE</a>
<a href="" ng-click="changeLanguage('fr')" translate="LANG_BTN_FR"></a></pre>

{{< highlight js >}}# configuration
angular.module('MyApp').config(['$translateProvider',
   function($translateProvider) {
      $translateProvider.useStaticFilesLoader({ prefix: 'lang/locale-', suffix: '.json' });
      $translateProvider.preferredLanguage('fr');
   }
]);

# fonction de changement de langue
angular.module('MyApp').controller('LanguageCtrl', ['$translate', '$scope',
   function ($translate, $scope) {
      $scope.changeLanguage = function (langKey) {
         $translate.use(langKey);
      };
   }
]);</pre>

{{< highlight js >}}{
   "SITE_TITLE" : "Sealed space timer",
   "LANG_BTN_FR" : "FR"
}</pre>

Efficace et rapide à mettre en place, Angular-Translate permet de gérer l’internationalisation avec succès.

## Bootstrap {#bootstrap}

Afin de rendre l’aspect utilitaire et urgent du thème, j’ai opté pour le thème United de Bootswatch pour sa dominante orange.

Les glyphicons permettent d’insérer des icones dans une interface et s’intègrent dans l’interface. L’avantage réside dans le fait qu’on peut les intégrer dans d’autres composants (bouton, texte, input, …).

Angular-UI Bootstrap adapte certains composants Bootstrap pour qu’ils s’intègrent à AngularJS.

Bootstrap permet de réaliser une interface propre et efficace rapidement. En contre partie, je trouve que le code HTML s’alourdit, même s’il reste lisible.

## Hypothèse de calcul {#hypothèse-de-calcul}

**Mise en garde:** cette application a pour objectif de découvrir AngularJS, pas de fournir un outil sûr pour calculer le temps avant suffocation dans un lieu clos.

Les hypothèses prises pour les calculs dans cette application :

  * le temps limite (en heures) durant lequel ont peut séjourner (au repos) dans un local dépourvu de moyens d’aération est donné en divisant le volume de la pièce (en m³) par le nombre d’occupants et de multiplier le résultat par le coefficient 1,5
  * le volume moyen occupé par un être humain est de 75 litres. Pour des questions pratiques, disons 100 litres soit 0,1m3.

**Demo:**

<http://blog.ghislain-lerda.com/demo/ng-SealedSpaceTimer/index.html>

**Sources:**

<https://github.com/GhislainL/ng-SealedSpaceTimer>

**Références:**

[Angular Timer][2]

[Angular-Translate][3]

[Angular-UI Bootstrap][4]

[Bootstrap – Glyphicons][5]

[Bootstrap – Thème United de Bootswatch][6]

 [1]: /wp-content/uploads/2015/05/sealedspacetimer.png
 [2]: http://siddii.github.io/angular-timer/
 [3]: http://angular-translate.github.io/
 [4]: http://angular-ui.github.io/bootstrap/
 [5]: http://getbootstrap.com/components/#glyphicons
 [6]: https://bootswatch.com/united/