---
title: Pourquoi lier un modèle de document à un type de contenu ?
author: Ghislain

date: 2014-10-20T08:29:30+00:00
url: /2014/10/20/pourquoi-lier-un-modele-de-document-a-un-type-de-contenu/
categories:
  - SharePoint
tags:
  - ECM
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013

---
<p style="text-align: justify;">
  L&rsquo;un des avantages dont vous disposez en utilisant les types de contenu est la possibilité d&rsquo;associer un modèle de document. Il s&rsquo;agit d&rsquo;un document dont le format, le contenu et l&rsquo;aspect sont prédéfinis. Vous ne pouvez associer qu&rsquo;un seul modèle par type de contenu.
</p>

<p style="text-align: justify;">
  Ainsi à chaque fois que vous créerez un contrat dans votre bibliothèque de contrat, il aura le même aspect et contenu puis vous pourrez adapter son contenu au client. Pour aller plus loin, il porte les métadonnées du type de contenu. Par exemple notre contrat porte le nom et l&rsquo;adresse du client, le département en charge avec le contact et le montant.
</p>

[<img class="wp-image-278 size-full" src="/wp-content/uploads/2014/10/typedecontenucontrat.png?fit=651%2C238" alt="TypeDeContenuContrat" srcset="/wp-content/uploads/2014/10/typedecontenucontrat.png?resize=300%2C110 300w, /wp-content/uploads/2014/10/typedecontenucontrat.png?w=651 651w" sizes="(max-width: 651px) 100vw, 651px" data-recalc-dims="1" />][1]  
_Type de contenu : Contrat_

# Document Information Panel {#document-information-panel}

<p style="text-align: justify;">
  <span style="font-size: 10pt; font-family: 'Georgia', 'serif'; color: #333333;">Vous pouvez consulter et modifier ces métadonnées directement dans les logiciels pour des documents de la suite Office. Le DIP affiche ces informations tout en reconnaissant le type et les contraintes associées.</span>
</p>

<img class="wp-image-276 size-full" src="/wp-content/uploads/2014/10/dip.png?fit=886%2C139" alt="DIP" srcset="/wp-content/uploads/2014/10/dip.png?resize=300%2C47 300w, /wp-content/uploads/2014/10/dip.png?w=886 886w" sizes="(max-width: 886px) 100vw, 886px" data-recalc-dims="1" />  
_Document Information Panel_

# Quickpart {#quickpart}

<p style="text-align: justify;">
  <span style="font-size: 10pt; font-family: 'Georgia', 'serif'; color: #333333;">Vous avez l&rsquo;avantage avec Word d&rsquo;avoir une intégration encore plus poussée avec les quickparts. Il s&rsquo;agit d&rsquo;ajouter les métadonnées dans le modèle. Ainsi quand vous créez un document à partir de celui-ci, une partie du contenue est créée dynamiquement. Vous pouvez utiliser aussi bien les métadonnées génériques que celle du type de contenu.</span>
</p>

<img class="wp-image-274 size-full" src="/wp-content/uploads/2014/10/modele2.png?fit=890%2C453" alt="Modele2" srcset="/wp-content/uploads/2014/10/modele2.png?resize=300%2C153 300w, /wp-content/uploads/2014/10/modele2.png?resize=1024%2C522 1024w, /wp-content/uploads/2014/10/modele2.png?w=1440 1440w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />  
_Modèle Word – insertion de quickparts_

Voici le résultat en action, lors de la création d’un nouveau document de type Contrat.

[<img class="wp-image-280 size-full" src="/wp-content/uploads/2014/10/modeleanimationquickparts.gif?fit=870%2C525" alt="ModeleAnimationQuickparts" data-recalc-dims="1" />][2]  
_Modèle Word – fonctionnement des Quickparts_

**Astuce:**

Si vous avez des documents portant préalablement des métadonnées, SharePoint liera automatiquement les propriétés aux métadonnées SP portant le même nom.

 [1]: /wp-content/uploads/2014/10/typedecontenucontrat.png
 [2]: /wp-content/uploads/2014/10/modeleanimationquickparts.gif