---
title: Utiliser Serilog avec DotNet Core 2.1
author: Ghislain

date: 2018-08-20T06:45:52+00:00
url: /2018/08/20/serilog-dotnet-core-2-1/
featuredImage: /wp-content/uploads/2018/08/DotNetCoreSerilog.png
categories:
  - DotNet Core
tags:
  - DotNet Core
  - Serilog
  - SQL Server

---
Utiliser Serilog avec DotNet Core 2.1 m&#8217;a permis de mettre facilement en place des logs dans une application DotNet Core. Par défaut, ils sont écrits dans un fichier texte mais pour l&#8217;exemple, j&#8217;ai choisi une base SQL Server.

## Qu&#8217;est ce que Serilog ?

Serilog est une bibliothèque pour faciliter les logs applicatifs dans les applications DotNet et DotNet Core (1). Je ne vous ferai pas l&#8217;affront d&#8217;expliquer l&#8217;utilité de logger ce qu&#8217;il se passe dans vos applications.

Plusieurs puits (sinks) existent pour stocker les logs sur différents supports (fichier, SQL, noSQL, Splunk) (2).

La différence majeure par rapport aux autres bibliothèques de log est qu&#8217;elle propose un mécanisme permettant d&#8217;avoir des méta données sur les événements survenus. Cela permet d&#8217;exploiter plus facilement ces logs que des logs en texte brut.

## Installation

Pour un projet DotNet Core de type application web ou API Web, il faut installer :
  
&#8211; Serilog.AspNetCore : le package spécifique à DotNet Core
  
&#8211; Serilog.Settings.Configuration : le package qui permet de passer la configuration depuis le fichier de configuration appsettings.json (3)
  
&#8211; Serilog.Sinks.MSSqlServer : le puit pour SQL Server (4)

Ensuite il faut nettoyer la solution du mécanisme de log par défaut. Ce dernier a le mérite d&#8217;exister mais on va le remplacer par quelque chose de sérieux. Voici ce à quoi ressemble un program.cs pour faire fonctionner Serilog. Il y a 3 étapes : la récupération de la configuration, la création du logger et l&#8217;enregistrement.

{{< highlight csharp >}}public class Program
    {
        public static IConfiguration Configuration { get; } = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
        .Build();

        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(Configuration)
            .CreateLogger();
            try
            {
                var host = CreateWebHostBuilder(args).Build();
                host.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Error on host.Run()");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =&gt;
        WebHost.CreateDefaultBuilder(args)
        .UseStartup()
        .UseSerilog();
    }
{{< /highlight >}}

Prochaine étape, la configuration. Pour cela il y a 2 options : configuration par code dans le program.cs ou configuration par le fichier de configuration appsettings.json. Dans la majorité des cas, cette seconde méthode est suffisante. Voici à quoi ressemble le JSON de configuration :

{{< highlight json >}}{
// configuration
  "Serilog": {
    "Using": [ "Serilog.Sinks.MSSqlServer" ],
    "MinimumLevel": "Information",
    "WriteTo": [
      {
        "Name": "MSSqlServer",
        "Args": {
          "connectionString": "Data Source=Server;Initial Catalog=DataBase;Trusted_Connection=True;MultipleActiveResultSets=true",
          "tableName": "Logs",
          "autoCreateSqlTable": true
        }
      }
    ]
  }
}
{{< /highlight >}}

**autoCreateSqlTable** à true permet de créer automatiquement la table. Si vous voulez le faire à la main, le code SQL est disponible dans le depot Github (4).

**MinimumLevel** permet de spécifier quel niveau minimum doit être logger. Voici les niveaux d&#8217;importances des événements comme décrit par Sérilog (5):

  1. **Verbose** &#8211; tracing information and debugging minutiae; generally only switched on in unusual situations
  2. **Debug** &#8211; internal control flow and diagnostic state dumps to facilitate pinpointing of recognised problems
  3. **Information** &#8211; events of interest or that have relevance to outside observers; the default enabled minimum logging level
  4. **Warning** &#8211; indicators of possible issues or service/functionality degradation
  5. **Error** &#8211; indicating a failure within the application or connected system
  6. **Fatal** &#8211; critical errors causing complete failure of the application

## Utilisation

Par défaut, Serilog va logger pleins d&#8217;événement en fonction du MinimumLevel spécifié dans la configuration. Pour ajouter ses propres traces, il faut passer par l&#8217;objet **Log** qui permet d&#8217;appeler la méthode associée au niveau de criticité voulu.

{{< highlight csharp >}}using Serilog;
// id is defined 

// writes a warning
Log.Warning($"Get item with id '{id}' not found");
{{< /highlight >}}

Dorénavant, les logs de l&#8217;application sont consultables dans une table SQL.

**Références :**

(1) : <https://serilog.net/>
  
(2) : <https://github.com/serilog/serilog/wiki/Provided-Sinks>
  
(3) : <https://github.com/serilog/serilog-settings-configuration>
  
(4) : <https://github.com/serilog/serilog-sinks-mssqlserver>
  
(5) : <https://github.com/serilog/serilog/wiki/Writing-Log-Events>