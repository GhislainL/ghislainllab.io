---
title: Logger des événements client avec JSNLog
author: Ghislain

date: 2018-10-08T06:00:39+00:00
url: /2018/10/08/logger-des-evenements-client-jsnlog/
featuredImage: /wp-content/uploads/2018/10/DotNetCoreJSNLog.png
categories:
  - DotNet Core
  - JavaScript
tags:
  - Angular
  - DotNet Core
  - JavaScript
  - JSNLog
  - Logging
  - Serilog

---
Continuons l&#8217;exploration des solutions de log et voyons comment logger des événements client dans une application web. Prenons une application web Angular couplée à une WebAPI DotNet Core. JSNLog couplé à Serilog vous permettra de logger depuis Angular des informations pour les intégrer dans vos logs serveurs &#8220;classique&#8221;.

## JavaScript dans un site asp.net core MVC

Dans Visual Studio, récupérer le package via Package Manager :
  
{{< highlight powershell >}}PM> Install-Package JSNLog{{< /highlight >}}

Dans le startup.cs :

{{< highlight csharp >}}using JSNLog;

public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
{
   // ...
   // Configure JSNLog
   var jsnlogConfiguration = new JsnlogConfiguration();
   app.UseJSNLog(new LoggingAdapter(loggerFactory), jsnlogConfiguration);

   app.UseStaticFiles();

   // ...
}
{{< /highlight >}}

Enregistrer le script jsnlog pour qu&#8217;il soit disponible partout

{{< highlight csharp >}}//Add to _Layout.cshtml*@

{{< /highlight >}}

Dans le code Javascript

{{< highlight js >}}// log text
JL().trace("log message");
JL().warn("log message");
// log JSON
JL().fatal({"i": 5, "j": 6});
{{< /highlight >}}

![JSNLog - Intellisense](/wp-content/uploads/2018/08/JSNLog-intellisense.png "JSNLog - Intellisense")

## Angular

Récupérer le package via npm :
  
{{< highlight ps >}}npm install jsnlog -save{{< /highlight >}}

Ensuite il faut modifier son application Angular pour prendre en utiliser ce nouvel outil.

{{< highlight js >}}// nouveaux imports
import { JL } from 'jsnlog';
import { Inject } from '@angular/core';

export class LoginComponent implements OnInit, OnDestroy {
// nouvelle propriété
JL: JL.JSNLog;
// injection dans le constructeur et initialisation
constructor(@Inject('JSNLOG') JL: JL.JSNLog, private alertService: AlertService, private authService: AuthService, private configurations: ConfigurationService) {
this.JL = JL;
}

// utilisation
ngOnInit() {
this.JL().error("Login - ngOnInit");
this.JL().info("Login - login");
}
{{< /highlight >}}

<img class="wp-image-315 size-full" src="https://ghislain-lerda.fr/wp-content/uploads/2018/08/JSNLog-Serilog-SQL.png" alt="&quot;<yoastmark" srcset="https://ghislain-lerda.fr/wp-content/uploads/2018/08/JSNLog-Serilog-SQL.png 982w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/JSNLog-Serilog-SQL-300x87.png 300w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/JSNLog-Serilog-SQL-768x224.png 768w" sizes="(max-width: 982px) 100vw, 982px" />

Comme vous le voyez, les logs contiennent des informations issues d&#8217;Angular mais aussi des WebAPI.

**Références :**
  
[JSNLog][1]
  
[JSNLog &#8211; Documentation &#8211; Angular2][2]

 [1]: http://jsnlog.com
 [2]: http://jsnlog.com/Documentation/HowTo/Angular2Logging