---
title: Exécuter rapidement plusieurs opérations CRUD dans SharePoint
author: Ghislain

date: 2016-06-15T00:00:00+00:00
url: /2016/06/15/executer-rapidement-plusieurs-operations-crud-dans-sharepoint/
categories:
  - PowerShell
  - SharePoint

---
La création d’un élément dans une liste SharePoint avec la méthode **Add()** est _rapide_. Répétée des centaines de fois, l’opération devient très longue. La méthode **SPWeb.ProcessBatchData()** permet de gagner énormément de temps en exécutant plusieurs requêtes dans une transaction. Mieux encore, cela permet d’exécuter rapidement plusieurs opérations CRUD dans SharePoint.

Prenons comme cas pratique, l’ajout de plusieurs données stockées dans un fichier csv nommé contacts.csv. Le script PowerShell va créer un fichier CamlBatch.txt contenant le CAML nécessaire à la méthode **SPWeb.ProcessBatchData()**. Ce fichier sera ensuite utilisé pour effectué toutes les opérations.<figure class="highlight"> 

{{< highlight plain >}}Name;Email
Pomme;pomme@gmail.com
Poire;poire@gmail.com
Banane;banane@gmail.com{{< /highlight  >}}

 <figure class="highlight"> 

{{< highlight powershell>}}Add-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue
$dirpck = Split-Path $MyInvocation.MyCommand.Path

#variables
$siteUrl = "http://www.contoso.com/sites/exchangecontacts"
$contactsListName = "contacts"
$CsvPath = "$dirpck\Contacts.csv"
$BatchPath = "$dirpck\CamlBatch.txt"

function CreateBatchFile($CsvPath, $BatchPath, [string]$GuidList){
	# Clean and add content
	Set-Content $BatchPath '<?xml version="1.0" encoding="UTF-8"?><ows:Batch OnError="Return">'
	
	$contacts = Import-Csv -Path $CsvPath -Delimiter ";"
	$idx = 1
	foreach ($contact in $contacts) 
	{ 
		$str = "<Method ID=`"$idx`"><SetList>$GuidList</SetList>" +
                "<SetVar Name=`"ID`">New</SetVar>" +
                "<SetVar Name=`"Cmd`">Save</SetVar>" +
                "<SetVar Name=`"urn:schemas-microsoft-com:office:office#Title`">$($contact.Name)</SetVar>" +
                "<SetVar Name=`"urn:schemas-microsoft-com:office:office#Email`">$($contact.Email)</SetVar>" +
            "</Method>"
		
		# Add content
		Add-Content $BatchPath $str
		$idx++
	}
	
	Add-Content $BatchPath "</ows:Batch>"
}

# init variables
$web = Get-SPWeb $SiteUrl
$contactsList = $web.Lists.TryGetList($contactsListName)
$guid = $contactsList.ID.ToString()

# create CAML file for the batch processing
CreateBatchFile -CsvPath $CsvPath -BatchPath $BatchPath -GuidList $guid
# read CAML file
$batchcontent = Get-Content $BatchPath
# process batch
$web.ProcessBatchData($batchcontent)
# clean
$web.Dispose()
{{< / highlight >}}


Pour la culture, voici à quoi ressemble le CAML généré. Vous remarquerez que c’est verbeux mais facilement appréhendable.


{{< highlight xml >}}
<?xml version="1.0" encoding="UTF-8"?>
<ows:Batch OnError="Return">
   <Method ID="1">
      <SetList>04728472-d93f-4d4c-9f11-97328d7ef28d</SetList>
      <SetVar Name="ID">New</SetVar>
      <SetVar Name="Cmd">Save</SetVar>
      <SetVar Name="urn:schemas-microsoft-com:office:office#Title">Pomme</SetVar>
      <SetVar Name="urn:schemas-microsoft-com:office:office#Email">pomme@gmail.com</SetVar>
   </Method>
   <Method ID="2">
   <SetList>04728472-d93f-4d4c-9f11-97328d7ef28d</SetList>
      <SetVar Name="ID">New</SetVar>
      <SetVar Name="Cmd">Save</SetVar>
      <SetVar Name="urn:schemas-microsoft-com:office:office#Title">Poire</SetVar>
      <SetVar Name="urn:schemas-microsoft-com:office:office#Email">poire@gmail.com</SetVar>
   </Method>
   <Method ID="3">
      <SetList>04728472-d93f-4d4c-9f11-97328d7ef28d</SetList>
      <SetVar Name="ID">New</SetVar>
      <SetVar Name="Cmd">Save</SetVar>
      <SetVar Name="urn:schemas-microsoft-com:office:office#Title">Banane</SetVar>
      <SetVar Name="urn:schemas-microsoft-com:office:office#Email">banane@gmail.com</SetVar>
   </Method>
</ows:Batch>
{{< /highlight >}}

![SP2013 - Eléments créés][1]

Quand l’import Excel est limité, la méthode ProcessBatchData() peut être une bonne solution. Simple d’utilisation, elle vous permettra de gagner du temps.

 [1]: /wp-content/uploads/2016/06/contacts.PNG