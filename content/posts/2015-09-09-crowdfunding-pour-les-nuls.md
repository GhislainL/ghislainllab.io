---
title: Crowdfunding pour les nuls
author: Ghislain

date: 2015-09-09T07:24:05+00:00
url: /2015/09/09/crowdfunding-pour-les-nuls/
categories:
  - Non classé
tags:
  - crowdequity
  - crowdfunding
  - crowdlending
  - Développement personnel
  - fintech

---
Le crowdfunding est un système en pleine croissance qui permet au citoyen lambda de redonner une fonction sociale à l’argent alors que les institutions ne jouent plus leur rôle. Un artiste avec du talent peut créer son oeuvre s’il trouve un support suffisant là où les producteurs ont vu trop de risque. Une PME peut obtenir une financement pour acheter un équipement alors que les banques ont vu trop de risque. Si vous voulez allez plus loin que les clichés, je vous propose mon défrichage avec le crowdfunding pour les nuls

[<img class="wp-image-919 size-full" src="/wp-content/uploads/2015/09/Crowdfunding.png?fit=644%2C557" alt="Crowdfunding" srcset="/wp-content/uploads/2015/09/Crowdfunding.png?resize=300%2C259 300w, /wp-content/uploads/2015/09/Crowdfunding.png?w=644 644w" sizes="(max-width: 644px) 100vw, 644px" data-recalc-dims="1" />][1]  
_Crowdfunding – vision synthétique_

Même si les plateformes étudient et sélectionnent les dossiers des entreprises avant de les proposer, le risque reste présent. Il faut conserver à l’esprit qu’il s’agit d’un investissement et que tout investissement comporte des risques. Le défaut de paiement existe, la perte en capital existe, le défaut de liquidité existe aussi. Le système du crowdlending et du crowdequity possède les même risques que leur pendant classique. D’ailleurs des rapprochements s’opèrent entre les plateformes et les assureurs.

[6 conseils pour déterminer le montant idéal à investir dans un projet de crowdfunding (Crowdfunding Superstars)][2]

[Les risques du crowdfunding: le risque de liquidité (Crowdfunding Superstars)][3]

[1 Million d’euros de risque de défauts de paiement … déjà ! (Crowdlending.fr)][4]

Pour avoir une idée des plateformes existantes :

[Comparatif des plateformes de prêt pour les entreprises (Crowdlending.fr)][5]

[Comparatif crowdfunding financement participatif (Café de la bourse)][6]

Pour se tenir informer :

[Crowdfunding Superstars][7]

[Crowdlending.fr][8]

[Finance pratique – blog][9]

[Le crowdfunding est un système en pleine croissance qui permet au citoyen lambda de redonner une fonction sociale à l’argent alors que les institutions ne jouent plus leur rôle. Un artiste avec du talent peut créer son oeuvre s’il trouve un support suffisant là où les producteurs ont vu trop de risque. Une PME peut obtenir une financement pour acheter un équipement alors que les banques ont vu trop de risque. Si vous voulez allez plus loin que les clichés, je vous propose mon défrichage avec le crowdfunding pour les nuls

[<img class="wp-image-919 size-full" src="/wp-content/uploads/2015/09/Crowdfunding.png?fit=644%2C557" alt="Crowdfunding" srcset="/wp-content/uploads/2015/09/Crowdfunding.png?resize=300%2C259 300w, /wp-content/uploads/2015/09/Crowdfunding.png?w=644 644w" sizes="(max-width: 644px) 100vw, 644px" data-recalc-dims="1" />][1]  
_Crowdfunding – vision synthétique_

Même si les plateformes étudient et sélectionnent les dossiers des entreprises avant de les proposer, le risque reste présent. Il faut conserver à l’esprit qu’il s’agit d’un investissement et que tout investissement comporte des risques. Le défaut de paiement existe, la perte en capital existe, le défaut de liquidité existe aussi. Le système du crowdlending et du crowdequity possède les même risques que leur pendant classique. D’ailleurs des rapprochements s’opèrent entre les plateformes et les assureurs.

[6 conseils pour déterminer le montant idéal à investir dans un projet de crowdfunding (Crowdfunding Superstars)][2]

[Les risques du crowdfunding: le risque de liquidité (Crowdfunding Superstars)][3]

[1 Million d’euros de risque de défauts de paiement … déjà ! (Crowdlending.fr)][4]

Pour avoir une idée des plateformes existantes :

[Comparatif des plateformes de prêt pour les entreprises (Crowdlending.fr)][5]

[Comparatif crowdfunding financement participatif (Café de la bourse)][6]

Pour se tenir informer :

[Crowdfunding Superstars][7]

[Crowdlending.fr][8]

[Finance pratique – blog][9]

](https://twitter.com/Fin_Part)

 [1]: /wp-content/uploads/2015/09/Crowdfunding.png
 [2]: http://www.crowdfunding-superstars.com/6-conseils-pour-determiner-le-montant-ideal-a-investir-dans-un-projet-de-crowdfunding/
 [3]: http://www.crowdfunding-superstars.com/les-risques-du-crowdfunding-le-risque-de-liquidite/
 [4]: http://www.crowdlending.fr/1-million-deuros-de-risque-de-defauts-de-paiement-deja/
 [5]: http://www.crowdlending.fr/comparatif-des-plateformes-de-pret-pour-les-entreprises/
 [6]: https://www.cafedelabourse.com/archive/article/comparatif-crowdfunding-financement-participatif
 [7]: http://www.crowdfunding-superstars.com
 [8]: http://www.crowdlending.fr/
 [9]: http://blog.financepratique.fr/