---
title: 'Exécuter du PowerShell depuis C#'
author: Ghislain

date: 2015-09-15T06:54:19+00:00
url: /2015/09/15/executer-du-powershell-depuis-c/
categories:
  - dotNet
  - PowerShell
tags:
  - NuGet
  - PowerShell
  - SharePoint
  - SharePoint 2013
  - System.Management.Automation

---
Semaine dernière, un collègue me demande comment exécuter du PowerShell depuis C#. Plus exactement  depuis une page web afin de faciliter certaines actions d’administration SharePoint.

## Préparation {#préparation}

Pour commencer, il faut utiliser la classe PowerShell du namespace **System.Management.Automation**. Vous pouvez récupérer la DLL System.Management.Automation.dllen passant par NuGet.

[System.Management.Automation][1]

[System.Management.Automation (PowerShell 3.0)][2]

## Code {#code}

### Exécuter du PowerShell {#exécuter-du-powershell}

{{< highlight csharp >}}using (PowerShell ps = PowerShell.Create()){
   // construire la commande avec ps.AddX()
   // exécuter la commande avec ps.Invoke()
   // travailler avec le résultat ou l'erreur
}{{< /highlight >}}

 

{{< highlight csharp >}}// PS> Get-P¨rocess "svchost"
ps.AddCommand("Get-Process");
ps.AddArgument("svchost");
// equivalent à 
ps.AddCommand("Get-Process");
ps.AddParameter("Name", "svchost");
// ou encore 
ps.AddScript("Get-Process | ? { $_.ProcessName -like 'svchost' }");{{< /highlight >}}

 

{{< highlight csharp >}}// PS> Get-Process -Name "svchost" -ComputerName "GLA-PC"
// version 1
ps.AddCommand("Get-Process");
ps.AddParameter("Name", "svchost");
ps.AddParameter("ComputerName", "GLA-PC");

// version 2
ps.AddCommand("Get-Process");
Dictionary<string, string> param = new Dictionary<string, string>();
param.Add("Name", "svchost");
param.Add("ComputerName", "GLA-PC");
ps.AddParameters(param);{{< /highlight >}}

{{< highlight csharp >}}// PS> Get-Process | Sort-Object -Descending id
ps.AddCommand("Get-Process");
ps.AddCommand("Sort-Object");
ps.AddParameter("descending");
ps.AddArgument("id");{{< /highlight >}}

La logique par défaut de la classe consiste à lier les commandes entre elles par des pipes. Si vous voulez exécuter des commandes sans lien entre elles, il faut utiliser _PowerShell.AddStatement()_ qui n’est disponible que dans Automation pour Powershell 3.0

{{< highlight csharp >}}ps.AddScript("Add-PSSnapin Microsoft.SharePoint.PowerShell");
ps.AddStatement().AddScript("Get-SPUser –Identity ‘AD\GLA’ –Web ‘http://www.contoso.com’) | % { $_.DisplayName}");{{< /highlight >}}

équivalent à

{{< highlight powershell>}}Add-PSSnapin Microsoft.SharePoint.PowerShell
Get-SPUser –Identity ‘AD\GLA’ –Web ‘http://www.contoso.com’) | % { $_.DisplayName};{{< /highlight >}}

Ensuite on appelle _Powershell.Invoke()_ pour une exécution synchrone ou _PowerShell.BeginInvoke()_ pour une exécution asynchrone.

{{< highlight csharp >}}var results = ps.Invoke();{{< /highlight >}}

### Gérer les résultats {#gérer-les-résultats}

Les résultats sont contenus dans une collection de [PSObject][3]. Vous pouvez ensuite les parcourir et les manipuler pour réaliser votre dessein.

{{< highlight csharp >}}if (results.Count > 0){
   foreach (var psObject in results){
      System.Console.WriteLine(psObject.BaseObject.ToString());
      System.Console.WriteLine(psObject.Members[“member”].Value);
   }
}{{< /highlight >}}

### Gérer les erreurs {#gérer-les-erreurs}

Vous pourrez trouver les messages, warning et erreurs en utilisant la propriété _PowerShell.Streams_. Si vous appelez plusieurs fois _PowerShell.Invoke()_, pensez à nettoyer les flux avant chaque appel à l’aide de _Clear()_ (exemple : ps.Streams.Error.Clear())

{{< highlight csharp >}}if (ps.Streams.Error.Count > 0){
   System.Console.WriteLine("ERROR :");
   Collection<ErrorRecord> errors = ps.Streams.Error.ReadAll();
   if (errors != null || errors.Count > 0){
      foreach (ErrorRecord er in errors){
         System.Console.WriteLine(er.Exception.ToString());
      }
   }
}{{< /highlight >}}

## Configurer l’environnement {#configurer-lenvironnement}

A ce stade de l’article, vous pouvez exécuter du PowerShell depuis C#. C’est bien mais d’une utilité relative (par rapport à un script ou un module). Par contre depuis asp.net – vous en conviendrez – cela prend plus de sens.

Quelques messages et exceptions qui peuvent survenir :

> Dynamic operations can only be performed in homogenous AppDomain.

> An exception of type ‘System.TypeInitializationException’ occurred in System.Management.Automation.dll but was not handled in user code

> Additional information: The type initializer for ‘System.Management.Automation.SessionStateScope’ threw an exception.

Pour résoudre ce problème, vous devez modifier le fichier web.config du site qui execute la page. Trouver la ligne contenant legacyCasModel et passer la valeur à false comme ci-après :

Si vous travaillez avec un timerjob SharePoint, il vous faudra éditer le fichier HIVE\BIN\OWSTIMER.EXE.CONFIG (source : [SoftLanding.ca][4])

**Références:**

[MSDN – System.Management.Automation.PowerShell][5]

[NuGet.org – System.Management.Automation][1]

[NuGet.org – System.Management.Automation (PowerShell 3.0)][2]

 [1]: https://www.nuget.org/packages/System.Management.Automation
 [2]: https://www.nuget.org/packages/System.Management.Automation_PowerShell_3.0
 [3]: https://msdn.microsoft.com/en-us/library/system.management.automation.psobject%28v=vs.85%29.aspx
 [4]: http://softlanding.ca/blog/executing-powershell-in-a-sharepoint-2013-timer-job-part-1-%282%29
 [5]: https://msdn.microsoft.com/en-us/library/system.management.automation.powershell%28v=vs.85%29.aspx