---
title: Créer une application mobile avec Cordova
author: Ghislain

date: 2015-06-02T07:55:15+00:00
url: /2015/06/02/creer-une-application-mobile-avec-cordova/
categories:
  - JavaScript
tags:
  - Angular
  - Bootstrap
  - cordova
  - Windows Phone

---
Dans un article précédent, je vous avais présenté l’application [Sealed Space Timer][1] qui m’a permis de découvrir AngularJS. Vu les cas d’usage, il serait bon d’avoir une version mobile et pourquoi pas une application mobile. L’idée est de réaliser un portage de notre site AngularJS/Bootstrap vers une application mobile. Nous allons découvrir ensemble comment créer une application mobile avec Cordova.

## Cordova {#cordova}

Cordova est une framework de développement mobile qui permet de construire une application en utilisant JavaScript, HTML5, and CSS3 au lieu des APIs natives de la plateforme (iOS, Windows Phone, Android). Il est possible d’adresser les fonctionnalités de l’appareil (photo, contact, geolocalisation, …) à l’aide de plug-in qui font l’abstraction par rapport aux APIs natives. Les applications générées sont hybrides avec une partie qui englobe le HTML, CSS, JS et une autre partie – générée automatiquement par Cordova – qui s’occupe des spécificités liées à la plateforme.

[<img class="wp-image-843 size-large" src="/wp-content/uploads/2015/06/cordova-full1-1024x551.png?fit=1020%2C549" alt="cordova-full1" srcset="/wp-content/uploads/2015/06/cordova-full1.png?resize=300%2C162 300w, /wp-content/uploads/2015/06/cordova-full1.png?resize=1024%2C551 1024w, /wp-content/uploads/2015/06/cordova-full1.png?w=1300 1300w" sizes="(max-width: 1024px) 100vw, 1024px" data-recalc-dims="1" />][2]  
_Fonctionnement de Cordova (image de http://www.herewecom.fr (1))_

## Environnement {#environnement}

Au moment où j’écris cet article, mon environnement de développement consiste

  * Windows 10 Enterprise Insider Preview: version Enterprise nécessaire pour Hyper-V;
  * Visual Studio 2015 RC.

Il se peut que vous ne retrouviez pas exactement les mêmes comportements sur des environnements avec des versions plus classiques.

## Porter un site web en application mobile {#porter-un-site-web-en-application-mobile}

Au préalable, j’ai récupéré la dernière version du code source de SealedSpaceTimer depuis GitHub.

Lancer Visual Studio 2015 RC et créer un nouveau projet New Project > JavaScript > Apache Cordova Apps > Blank Windows Universal App (Apache Cordova)

[<img class="wp-image-839 size-large" src="/wp-content/uploads/2015/06/cordova-sealedspacetimer-project-1024x595.png?fit=1020%2C593" alt="cordova-sealedspacetimer-project" srcset="/wp-content/uploads/2015/06/cordova-sealedspacetimer-project.png?resize=300%2C174 300w, /wp-content/uploads/2015/06/cordova-sealedspacetimer-project.png?resize=1024%2C595 1024w, /wp-content/uploads/2015/06/cordova-sealedspacetimer-project.png?w=1414 1414w" sizes="(max-width: 1024px) 100vw, 1024px" data-recalc-dims="1" />][3]  
_Modèle de projet Cordova dans Visual Studio 2015_

 

**Note :** en partant du modèle Application Vide (Apache Cordova), je ne parviens pas à tester l’application Windows10 alors que l’application Android fonctionne.

Copier le fichier **bower.json** dans le répertoire **www.**

Dans un invite de commande (PowerShell ou cmd), taper la ligne de commande suivante pour récupérer les dépendances.

{{< highlight powershell>}}bower install
{{< / highlight >}}


Copier les répertoires **lang**, **scripts** et **views** dans le répertoire www.

Remplacer le contenu du fichier **index.html** de la solution par celui du fichier de l’application.

[<img class="wp-image-840 size-medium" src="/wp-content/uploads/2015/06/cordova-sealedspacetimer-solution-221x300.png?fit=221%2C300" alt="cordova-sealedspacetimer-solution" srcset="/wp-content/uploads/2015/06/cordova-sealedspacetimer-solution.png?resize=221%2C300 221w, /wp-content/uploads/2015/06/cordova-sealedspacetimer-solution.png?w=527 527w" sizes="(max-width: 221px) 100vw, 221px" data-recalc-dims="1" />][4]  
_Structure du répertoire www du projet Cordova dans Visual Studio 2015_

Modifier les références vers les fichiers JavaScript, vu que l’arborescence a changé.

Ajouter les références vers les fichiers JavaScript spécifiques à Cordova

{{< highlight html >}}<script src="cordova.js"></script>
<script src="scripts/platformOverrides.js"></script>
<script src="scripts/index.js"></script>
{{< / highlight >}}


Voila, le portage technique est réalisé. Voyons le résultat.

## Résultats du portage {#résultats-du-portage}

Dans Visual, cliquer pour lancer l’émulation avec Android avec Ripple et après un moment d’attente, voila le résultat :

[<img class="wp-image-821 size-large" src="/wp-content/uploads/2015/05/cordova-sealedspacetimer-1.png?resize=1020%2C550" alt="cordova-sealedspacetimer-1" data-recalc-dims="1" />][5]  
_Emulation Android Ripple – portage direct (vertical)_ [<img class="wp-image-820 size-large" src="/wp-content/uploads/2015/05/cordova-sealedspacetimer-2-1024x463.png?fit=1020%2C461" alt="cordova-sealedspacetimer-2" srcset="/wp-content/uploads/2015/05/cordova-sealedspacetimer-2.png?resize=300%2C136 300w, /wp-content/uploads/2015/05/cordova-sealedspacetimer-2.png?resize=1024%2C463 1024w" sizes="(max-width: 1024px) 100vw, 1024px" data-recalc-dims="1" />][6]  
_Emulation Android Ripple – portage direct (horizontal)_

De retour dans Visual, cliquer pour lancer l’émulation Windows Phone 10.0.1.0 et après un moment d’attente, voila le résultat :

[<img class="alignnone size-medium wp-image-838" src="/wp-content/uploads/2015/06/cordova-sealedspacetimer-visual-w10-300x19.png?fit=300%2C19" alt="cordova-sealedspacetimer-visual-w10" srcset="/wp-content/uploads/2015/06/cordova-sealedspacetimer-visual-w10.png?resize=300%2C19 300w, /wp-content/uploads/2015/06/cordova-sealedspacetimer-visual-w10.png?w=650 650w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />][7]

[<img class="wp-image-831 size-full" src="/wp-content/uploads/2015/05/cordova-sealedspacetimer-win10.png?fit=963%2C527" alt="cordova-sealedspacetimer-win10" data-recalc-dims="1" />][8]  
_Emulation Windows 10 – portage direct (horizontal)_

Pour créer et tester le package sur iOS, il faut disposer d’une machine sous Mac OS X. Comme annoncé au début de l’article, je fais tourner un Windows 10 Enterprise donc pas d’émulation iOS.

Le modèle de projet utilisé pour porter l’application ne permet de déployer l’application sur un environnement 8.1. L’erreur ci-dessous attend les audacieux qui tenteraient la manipulation :

> Erreur Impossible de déployer une application Windows 10 sur une cible Windows 8.1.

## Conclusion {#conclusion}

Vous en conviendrez le portage vers une application mobile avec Cordova se fait sans trop de heurt. Si d’un point de vue technique tout se passe bien, d’un point de vue expérience utilisateur il faut revoir la copie. La prochaine étape sera de rajouter des fonctionnalités utilisant les spécificités des appareils mobiles.

**Références :**

[Apache Cordova – site officiel][9]

[Créer une application mobile avec Cordova/PhoneGap, AngularJS et Bootstrap – Microsoft Virtual Academy][10]

[Lien sur GitHub][11]

(1) – [Applications mobiles et frameworks – Here we com][12]

 [1]: http://blog.ghislain-lerda.com/javascript/internationalisation-angularjs-et-utilisation-timer/
 [2]: /wp-content/uploads/2015/06/cordova-full1.png
 [3]: /wp-content/uploads/2015/06/cordova-sealedspacetimer-project.png
 [4]: /wp-content/uploads/2015/06/cordova-sealedspacetimer-solution.png
 [5]: /wp-content/uploads/2015/05/cordova-sealedspacetimer-1.png
 [6]: /wp-content/uploads/2015/05/cordova-sealedspacetimer-2.png
 [7]: /wp-content/uploads/2015/06/cordova-sealedspacetimer-visual-w10.png
 [8]: /wp-content/uploads/2015/05/cordova-sealedspacetimer-win101.png
 [9]: http://cordova.apache.org/
 [10]: http://www.microsoftvirtualacademy.com/training-courses/creer-une-application-mobile-avec-cordova-phonegap-angularjs-et-bootstrap
 [11]: https://github.com/GhislainL/cordova.sealedSpaceTimer
 [12]: http://www.herewecom.fr/applications-mobiles-frameworks/