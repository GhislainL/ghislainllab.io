---
title: Etre fatigué n’est pas une médaille du mérite
author: Ghislain

date: 2017-01-18T00:00:00+00:00
url: /2017/01/18/etre-fatigue-nest-pas-une-medaille-du-merite/
featuredImage: /wp-content/uploads/2017/01/Dodo.jpeg
categories:
  - Développement personnel
tags:
  - Développement personnel
  - equilibre pro-perso
  - Jason Fried
  - productivité
  - repos

---
Trouver l’équilibre entre vie professionnelle et personnelle peut se révéler compliquer. Le temps etant une denrée limitée, nous sommes tenté d’empiéter sur notre temps de sommeil. J’ai lu et apprécié [**Being tired isn’t a badge of honor**][1] de Jason Fried &#8211; auteur de Rework (travailler autrement) et Remote (télétravail). Je vous propose donc la traduction de ce plaidoyer pour les bienfaits du sommeil pour le travail et votre bien-être.

## Etre fatigué n’est pas une médaille du mérite. {#etre-fatigué-nest-pas-une-médaille-du-mérite}

Chaque fois que je parle à une conférence, j’essaie de profiter un peu des autres présentations. J’ai tendance à me mettre dans le fond et écouter, observer, et ressentir la salle.

Récemment j’ai entendu quelque chose qui me dérange. Beaucoup d’entrepreneurs sur scène se sont vantés de ne pas dormir, parlant de leurs journées de 16 heures et en donnant l’impression que c’est à tout prix la voie à suivre. Mort au repos, disent-ils – nous avons tellement de travail qui nous attend.

Je pense que ce message est l’un des plus nocifs dans le milieu professionnel. L’épuisement soutenu n’est pas un rite de passage. C’est une marque de stupidité. Littéralement. Les scientifiques ont suggéré que les scores aux tests de QI baissent chaque jour que vous dormez moins que vous ne le feriez naturellement. Il ne faut pas longtemps pour voir la différence.

Les gens qui font régulièrement des journées de 16 heures sont épuisés. Ils sont simplement trop fatigués pour remarquer que leur travail en pâti.

Et le manque de sommeil fait plus que compromettre votre propre santé et créativité. Cela affecte les gens autour de vous. Quand vous manquez de sommeil, vous manquez de patiente. Vous êtes moins poli avec les gens, moins tolérant, moins compréhensif. Il est plus difficile de collaborer et de faire attention durant de longues périodes.

Les managers qui manquent de sommeil sont de mauvais managers, aussi, en raison du mauvais exemple qu’ils donnent. Pourquoi dépenser du temps et de l’argent pour recruter des gens brillants si c’est pour les mener dans un état d’insensibilité ?

Si le but de travailler beaucoup d’heures d’affilées est d’abattre plus de travail, alors que la qualité de votre travail vous importe, comment pouvez-vous justifier le manque de sommeil soutenu ? Les seules personnes qui tentent de le faire sont fatiguées et ne pensent pas clairement.

Un argument que j’entends beaucoup à propos de faire de gros horaires est que quand vous vous lancez, vous devez tout donner. Je comprends ce sentiment. Et il y a certainement une part de vérité dans cet argument.

Mais voici ce dont je suis le témoin encore et encore: les gens continuent de travailler comme ça. Nous sommes des êtres d’habitudes. Les choses que vous faites quand vous commencez quelque chose tendent à être les choses que vous continuez à faire. Si vous faites de gros horaires au départ, et que c’est la seule chose que vous connaissez, vous pouvez facilement vous conditionner pour penser qu’il s’agit de la seule manière de fonctionner. J’ai vu tellement d’entrepreneurs faire des surmenages en suivant ce modèle.

Il est donc important – particulièrement quand vous construisez vos habitues – d’avoir énormément de sommeil. Vous commencerez mieux, penserez mieux et serez un meilleur collègue et chef. Le sommeil est excellent pour la créativité. Le sommeil est un excellent pour résoudre les problèmes. N’est-ce pas ce que vous voulez le plus – et non le moins &#8211; au travail ?

Votre cerveau est toujours actif la nuit. Il travaille d’une manière qui est hors de votre portée durant la journée. Ne voulez-vous pas vous réveiller avec de nouvelles solutions dans la tête plutôt qu’avec des cernes sous les yeux ?

Oui, parfois des urgences nécessitent des heures supplémentaires. Et, oui, parfois les échéances sont figées et vous devrez vous investir plus sur la fin. Cela arrive. Et c’est acceptable, car l’épuisement n’est pas soutenu ; il est temporaire. De telles situations devraient être l’exception, pas la norme.

Sur le long terme, le travail n’est pas plus important que le sommeil. Si vous n’êtes pas sur de l’importance du sommeil, pensez à cela : vous mourrez plus rapidement d’une privation de sommeil que de nourriture.

Et, finalement, très peu de problèmes doivent être résolus à la 12e, 13e, 14e ou 15e heure d’une journée de travail. Presque tout peut attendre jusqu’au lendemain matin.

Chez [Basecamp][2], nous pensons que 40h par semaine est suffisant. Nous encourageons tout le monde à avoir une bonne nuit de sommeil, chaque nuit.

Bonne nuit, et dormez bien !

Et au passage, voici pourquoi [le PDG d’Aetna paie ses employés jusqu’à $500 pour qu’ils dorment bien][3]. La science le soutient.

 [1]: https://m.signalvnoise.com/being-tired-isn-t-a-badge-of-honor-fa6d4c8cff4e#.1hdi0ru7v
 [2]: https://basecamp.com/
 [3]: http://www.cnbc.com/2016/04/05/why-aetnas-ceo-pays-workers-up-to-500-to-sleep.html