---
title: SharePoint Permission Analyser
author: Ghislain

date: 2015-04-07T08:00:02+00:00
url: /2015/04/07/sharepoint-permission-analyser/
categories:
  - SharePoint
tags:
  - Outils
  - permissions
  - SharePoint
  - SharePoint 2013

---
Aujourd’hui j’ai découvert un utilitaire gratuit et opensource nommé SharePoint Permission Analyzer. Grace à cet outil vous pourrez avoir un état des lieux des permissions sur une collection de site à l’instant t.

Il vous permettra d’identifier les permissions assignées aux groupes et personnes. En terme de granularité, vous pouvez vérifier au niveau de la collection de site, du site, de la liste ou bibliothèque de document et même pousser jusqu’à l’élément ou document.

Sur une collection de site (SharePoint 2013) contenant une 30 de sites avec des héritages cassés, cela prend un peu plus de 5 minutes en s’arrêtant au niveau de la liste/bibliothèque. Je n’ai pas tenté en vérifiant les permissions au niveau des éléments, mais ça risque de prendre longtemps suivant la volumétrie.

## Interface graphique {#interface-graphique}

L’interface est propre et permet de trouver facilement ce que l’on cherche.

La zone de gauche (surlignée en bleu) présente l’arborescence de la collection de site. A la sélection d’un objet, la partie centrale est mise à jour. Vous noterez l’existence d’un filtre pour ne montrer que les objets sur lesquels l’héritage est cassé. Cela vous facilitera la vie.

La zone centrale (surlignée en vert) liste les groupes et les utilisateurs ayant des permissions sur l’objet sélectionné. Il est possible de déplier les groupes pour en connaitre les membres. A la sélection d’un groupe ou d’un utilisateur, la zone de droite est mise à jour.

La zone de droite (surlignée en rouge) détaille le niveau d’autorisation, ainsi que les autorisations le constituant, donc dispose le groupe et l’utilisateur sélectionné.

[<img class="wp-image-667 size-large" src="/wp-content/uploads/2015/03/PermissionReporting-1024x616.png?fit=1020%2C614" alt="SharePoint Permission Analyser - reporting" srcset="/wp-content/uploads/2015/03/PermissionReporting.png?resize=300%2C180 300w, /wp-content/uploads/2015/03/PermissionReporting.png?resize=1024%2C616 1024w" sizes="(max-width: 1024px) 100vw, 1024px" data-recalc-dims="1" />][1]  
_SharePoint Permission Analyser – reporting_

## Bonus {#bonus}

Vous avez la possibilité d’importer et d’exporter les analyses au format XML. Cela se montre pratique pour garder des traces et comparer les évolutions.

Petit plus, il s’exécute depuis le poste utilisateur avec les droits de la personne.

## Avis {#avis}

Au final, SharePoint Permission Analyser ne remplacera un outillage adapté mais saura trouver sa place dans votre boite à outils. Principalement car il s’agit d’un outil assez brut qui nécessite quelques efforts pour mettre en exergue les mauvaises pratiques.

[SharePoint Permission Analyser sur Codeplex][2]

 [1]: /wp-content/uploads/2015/03/PermissionReporting.png
 [2]: http://sppermissionanalyzer.codeplex.com/