---
title: Mise en place un concentrateur de type de contenu avec Powershell
author: Ghislain

date: 2015-01-12T08:28:38+00:00
url: /2015/01/12/mise-en-place-un-concentrateur-de-type-de-contenu-avec-powershell/
categories:
  - PowerShell
  - SharePoint
tags:
  - content type hub
  - ECM
  - PowerShell
  - SharePoint
  - SharePoint 2013

---
Dans l’article précédent, je vous ai montré [Dans l’article précédent, je vous ai montré][1] par l’interface graphique. Maintenant je passe à la vitesse supérieure en vous montrant comment industrialiser cela en PowerShell.

# Configurer le service de métadonnées gérées et le concentrateur de type de contenu {#configurer-le-service-de-métadonnées-gérées-et-le-concentrateur-de-type-de-contenu}

{{< highlight powershell>}}$MmsName = "Managed Metadata Service"
$HubUrl = "http://contenttypehub.contoso.com"
# set or change HubUri on the given Managed Metadata Service
# activate the Content Type Hub Syndication feature on the site collection (9a447926-5937-44cb-857a-d3829301c73b)
Get-SPServiceApplication -Name $MmsName | Set-SPMetadataServiceApplication -HubURI $HubUrl -Confirm:$false
    
# Enable content type syndication and content type Pushdown in Menaged Metadata Service
Get-SPMetadataServiceApplicationProxy -Identity $MmsName | Set-SPMetadataServiceApplicationProxy -ContentTypeSyndicationEnabled -ContentTypePushdownEnabled -Confirm:$false</pre>

<table>
  <tr>
    <td width="614">
      <strong>Astuce: </strong>
    </td>
  </tr>
  
  <tr>
    <td width="614">
      Par la centrale d’administration, il est impossible de modifier l’URL du CTH après l’avoir renseignée au niveau du MMS. Au besoin vous pouvez le faire avec la commande PowerShell<strong> Set-SPMetadataServiceApplication.</strong>
    </td>
  </tr>
</table>

# Créer des types de contenu {#créer-des-types-de-contenu}

Je vais juste montrer l’association du modèle de document au type de contenu.

{{< highlight powershell>}}Function AddTemplateToCT($ContentType, $TemplateName, $TemplatePath)
{
   $fileMode = [System.IO.FileMode]::Open
   try
   {
      #upload template
      $fileStream = New-Object "System.IO.FileStream" -ArgumentList $TemplatePath, $fileMode
      $ContentType.ResourceFolder.Files.Add($TemplateName, $fileStream, $true) | Out-Null
      $fileStream.Close()

      #set the current document template file to be the document template
      $ContentType.DocumentTemplate = $docTemp.TargetName
      #update the content type
      $ContentType.Update($true)
   }
   finally
   {
      $fileStream.Close()
   }
}<{{< /highlight >}}

# Publier (ou republier) les types de contenu {#publier-ou-republier-les-types-de-contenu}

{{< highlight powershell>}}$CTPublish = New-Object Microsoft.SharePoint.Taxonomy.ContentTypeSync.ContentTypePublisher ($HubUrl)
$spWeb = Get-SPWeb $HubUrl
# $ctToPublish contains the name of every content type to be published
foreach($ct in $ctToPublish)
{
   $SPCT = $spweb.ContentTypes[$ct]
   if($SPCT -ne $null)
   {
      $CTPublish.Publish($SPCT)
   }
}

$spWeb.Dispose(){{< /highlight >}}

# Exécuter les timer job {#exécuter-les-timer-job}

{{< highlight powershell>}}#Run the Content Type Hub timer job
$ContentTypeHubTimerJob = Get-SPTimerJob "MetadataHubTimerJob"
$ContentTypeHubTimerJob.RunNow()

#Run the Content Type Subscriber timer job for a specific Web Application
$ContentTypeSubscriberTimerJob = Get-SPTimerJob "MetadataSubscriberTimerJob" -WebApplication $webappUrl
$ContentTypeSubscriberTimerJob.RunNow(){{< /highlight >}}

Avec ces quelques lignes pour initier le mouvement, qu’est ce qui vous empêche encore de mettre en place un content type hub ?

 [1]: https://ghislainfo.wordpress.com/2015/01/05/comment-faciliter-lacces-aux-modeles-de-documents