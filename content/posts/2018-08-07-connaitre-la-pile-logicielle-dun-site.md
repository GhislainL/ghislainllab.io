---
title: Connaitre la pile logicielle d’un site
author: Ghislain

date: 2018-08-07T06:45:25+00:00
url: /2018/08/07/connaitre-la-pile-logicielle-dun-site/
featuredImage: /wp-content/uploads/2018/08/inspect4.jpg
categories:
  - Utilitaire
tags:
  - BuiltWith
  - software stack
  - Wappalyzer
  - WhatRuns

---
Comment connaitre la pile logicielle (software stack) d’un site ? Certains sites mettent à disposition leurs sources (notamment sur Github) ou une page avec la pile logicielle utilisée. Ensuite il existe la possibilité de contacter les personnes en charge du site (formulaire de contact, mail, réseau sociaux) et attendre une éventuelle réponse. Les plus joueurs regarderont le code source des pages à la recherche d’indices. Sinon il existe des sites et applications qui le font pour vous.

## Builtwith

BuiltWith est un site web qui analyse un site web et liste les technologies utilisées. Après plusieurs essais, je trouve qu&#8217;il s&#8217;agit de l&#8217;outil de le plus précis.

D&#8217;ailleurs, il exploite les analyses pour enrichir une base de données et faire des statistiques. Un bel outil dont je n&#8217;ai vu que la partie émergée de l&#8217;iceberg.

<https://builtwith.com/>

<img class="aligncenter size-full wp-image-231" src="http://ghislain-lerda.fr/wp-content/uploads/2018/08/builtwith.png" alt="" width="837" height="913" srcset="https://ghislain-lerda.fr/wp-content/uploads/2018/08/builtwith.png 837w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/builtwith-275x300.png 275w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/builtwith-768x838.png 768w" sizes="(max-width: 837px) 100vw, 837px" />

## Whatruns

Whatruns est une extension pour Chrome ou Firefox. Il suffit de cliquer sur le bouton Whatruns quand on est sur la page du site pour lancer l&#8217;analyse.  Mes essais donnent des résultats tout aussi satisfaisant qu&#8217;avec BuiltWith.

<https://www.whatruns.com/>

<img class="aligncenter size-full wp-image-230" src="http://ghislain-lerda.fr/wp-content/uploads/2018/08/WhatRuns.png" alt="" width="462" height="340" srcset="https://ghislain-lerda.fr/wp-content/uploads/2018/08/WhatRuns.png 462w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/WhatRuns-300x221.png 300w" sizes="(max-width: 462px) 100vw, 462px" />

## Wappalyzer

Wappalyzer est un site qui fonctionne comme BuiltWith. Par contre mes essaies montrent qu&#8217;il est moins précis : il détecte moins de choses et donne moins de précision sur chacune d&#8217;elle.

<https://www.wappalyzer.com/>

<img class="aligncenter wp-image-226 size-full" src="http://ghislain-lerda.fr/wp-content/uploads/2018/08/wappalyzer.png" alt="" width="853" height="355" srcset="https://ghislain-lerda.fr/wp-content/uploads/2018/08/wappalyzer.png 853w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/wappalyzer-300x125.png 300w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/wappalyzer-768x320.png 768w" sizes="(max-width: 853px) 100vw, 853px" />