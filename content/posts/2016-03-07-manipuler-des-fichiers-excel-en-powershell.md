---
title: Manipuler des fichiers Excel en PowerShell
author: Ghislain

date: 2016-03-07T00:00:00+00:00
url: /2016/03/07/manipuler-des-fichiers-excel-en-powershell/
categories:
  - PowerShell
tags:
  - Excel
---
Si vous devez manipuler des fichiers Excel en PowerShell, il existe deux solutions principales:

  * **COM objet** si l’application Excel est installé sur le serveur ou le PC;
  * **EPPlus**, une bibliothèque .Net pour manipuler des fichiers Excel.

## ComObject Excel.Application {#comobject-excelapplication}

Cette solution nécessite que Excel soit installé sur le serveur ou le PC exécutant le script PowerShell.
  
Condition contraignante et qui rend son utilisation limitée sur les serveurs.

{{< highlight powershell >}}$PathToXlsx = "" 
$SheetName = ""

$objExcel = New-Object -ComObject Excel.Application
# eviter l'ouverture d'Excel
$objExcel.Visible = $false
# ouvrir le classeur
$workBook = $objExcel.Workbooks.Open($PathToXlsx)
# ouvrir la feuille
$workSheet = $workBook.sheets.item($SheetName)
# lire la valeur d'une cellule
$cellValue = $workSheet.Range("A1").Text
# ecrire une valeur dans une cellule 
$workSheet.Cells.Item(A, 1) = "From PS with love"
$workSheet.Save()
$objExcel.Quit()
{{< /highlight >}}

## EPPlus {#epplus}

EPPlus est une bibliotheque .Net qui permet de lire et écrire des fichiers Excel .xlsx.
  
Vous vous rendrez compte sur le site officiel que EPPlus supporte beaucoup d’opérations.
  
[EPPlus][1]

Vous pouvez utiliser cette bibliothèque directement ou un wrapper.

### Natif {#natif}

{{< highlight powershell >}}$PathToXlsx = "" 
$SheetName = ""

Add-Type -Path 'C:\EPPlus\EPPlus.dll'

# ouvrir le classeur
$excelFile = New-Object System.IO.FileInfo($PathToXlsx)
$package = New-Object OfficeOpenXml.ExcelPackage($excelFile)
# ouvrir la feuille
$workSheet = package.Workbook.Worksheets[$SheetName] 
# lire la valeur d'une cellule
$cellValue = $workSheet.Cells[1, 1].Value
# ecrire une valeur dans une cellule 
$worksheet.Cells[1, 1].Value = "From PS with love"
$package.Save()
{{< /highlight >}} 

### Wrapper {#wrapper}

Vous pouvez utiliser des wrappers qui utilisent EPPlus. Cela peut faciliter la vie et la lecture du code.
  
Voici ceux que j’ai croisés :

  * [Excel Powershell Library][2]
  * [PowerShell Import-Excel][3]
  * [PSExcel][4]

Il est donc relativement aisé de lire, écrire et modifier un fichier Excel en PowerShell.
  
Mais en toute honnêteté, si vous n’avez que de la lecture à faire et que la situation le permet, optez pour le CSV.
  
Je trouve plus pratique de convertir le fichier Excel en fichier CSV après un peu de préparation (nettoyage des entêtes (espaces) et du contenu (;, [saut de ligne][5]).

 [1]: http://epplus.codeplex.com/
 [2]: https://excelpslib.codeplex.com/
 [3]: https://github.com/dfinke/ImportExcel
 [4]: https://github.com/RamblingCookieMonster/PSExcel
 [5]: https://www.ablebits.com/office-addins-blog/2013/12/03/remove-carriage-returns-excel/