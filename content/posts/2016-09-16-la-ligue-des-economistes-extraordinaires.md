---
title: La ligue des économistes extraordinaires
author: Ghislain

date: 2016-09-16T00:00:00+00:00
url: /2016/09/16/la-ligue-des-economistes-extraordinaires/
categories:
  - Développement personnel
tags:
  - La ligue des économistes extraordinaires
  - livres

---
Aujourd’hui je vais vous parler de **La ligue des économistes extraordinaires**. Il s’agit d’un livre paru en 2014 de Benoist Simmat &#8211; journaliste économique – et Vincent Caut &#8211; dessinateur. Une version augmentée est sortie avec en 2015. Là vous me demandez ce qu’il a de spécial ce livre avec sa couverture rigolote car ce n’est pas ce qui manque des livres sur les économistes ? Justement sa particularité réside dans le mélange équilibré entre connaissances et dessins, sérieux et humour.
  
En 2008 la Reine d’Angleterre demande lors de la London School of Economics “comment se fait-il que personne n’ai prévu la crise que nous traversons ?” Pour comprendre pourquoi nous en sommes arrivés là, ce livre nous propose de découvrir les grands économistes des 3 derniers siècles. Date à partir de laquelle on a commencé à aborder l’économie comme un système, alors qu’avant la richesse n’était qu’une notion d’accumulation.

![La ligue des économistes extraordinaires - couverture][1]

Chaque économiste a droit à une fiche qui reprend le modèle suivant :

  * Présentation civile
  * Vis sa vie
  * Thèse, antithèse, foutaise
  * Cas pratique, optionnel
  * Pourquoi il s’est planté, merci!
  * L’anecdote qui tue, optionnel
  * les grands écrits

## Les classiques XIXe {#les-classiques-xixe}

Les “peres de d’économie” ont tâtonné pour poser les bases de ce qui deviendra les sciences économiques. Beaucoup de pages ont été remplies à cette époque autant par des bêtises que des intuitions valides.
  
13 dont Adam Smith,Malthus, Ricardo, Marx, Pareto

![La ligue des économistes extraordinaires - Marx][2]

## Les révolutionnaires XXe {#les-révolutionnaires-xxe}

Le début du XX s’accompagne de nouveaux maux (inflation, chômage, déficit) et les économistes doivent les comprendre. Ils s’appliquent à détruire les théories de la génération précédente, tout en changeant la méthode en abandonnant les mathématiques.
  
11 dont Shumpeter, Keynes, Hayek, Galbraith

![La ligue des économistes extraordinaires - Friedman][3]

## Les contemporains XXIe {#les-contemporains-xxie}

Avec les crises consécutives, les économistes ont eu beaucoup de travail mais aussi beaucoup de motifs pour se prendre le bec sur les modèles théoriques qu’ils défendent.
  
11 dont Allais, Tobin, Stiglitz, Kahneman

![La ligue des économistes extraordinaires - Hindi][4]

En deux siècles et demi, l’économie a acquis de la légitimité grâce à un Nobel et des belles crises. Mais on ne sait toujours pas s’il s’agit d’une science sociale ou d’une science exacte. Gageons que les experts, journalistes et autres professions dont c’est le métier de faire semblant de comprendre vont continuer à gagner de l’argent. Les crises représentent leur meilleure opportunité pour expliquer comment sortir de la situation dans laquelle ils ont mis le monde.

Un livre plaisant que je vous conseille vivement pour votre culture personnelle.

 [1]: /wp-content/uploads/books/LigueEco-couv.jpg
 [2]: /wp-content/uploads/books/LigueEco-Marx.jpg
 [3]: /wp-content/uploads/books/LigueEco-Friedman.jpg
 [4]: /wp-content/uploads/books/LigueEco-Hindi.jpg