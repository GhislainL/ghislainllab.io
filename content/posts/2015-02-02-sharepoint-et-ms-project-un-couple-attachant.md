---
title: SharePoint et MS Project, un couple attachant
author: Ghislain

date: 2015-02-02T08:42:54+00:00
url: /2015/02/02/sharepoint-et-ms-project-un-couple-attachant/
categories:
  - SharePoint
tags:
  - MS Project 2013
  - SharePoint
  - SharePoint 2013

---
Dernièrement j’ai travaillé avec une communauté de chefs de projets et j’ai été surpris de les voir créer des diagrammes de Gantt dans Excel. Microsoft propose le logiciel **Project** qui permet de planifier et piloter les projets, gérer les ressources et budget, bref bien plus qu’un Gantt dans Excel. J’ai découvert Project durant mes études et n’en connait que la partie émergée. Par contre, collaborer autour de données dans SharePoint est plus dans mes cordes.

Quand vous travaillez sur un projet, il est bon d’impliquer les gens et pour cela de rendre visible les phases et deadlines. Vous conviendrez avec moi qu’un site SharePoint permet plus d’interactions et de collaborations qu’un fichier Excel dans un partage réseau. Comme une liste de tache dispose nativement d’une vue **Diagramme de Gantt**, vous avez la possibilité de rendre visible le déroulement du projet et de bénéficier des avantages liés aux taches (mail d’assignation, progression, …).

En passant à Project, vous garderez les avantages précédents grâce à l’intégration avec Sharepoint et en plus vous pourrez gérer les ressources et le budget. Au passage, l’interface de Project est plus conviviale que celle de la vue Gantt de SharePoint pour travailler sur des projets conséquents.

Vous pouvez commencer :

  * A partir d’une liste de type Taches dans SharePoint
  * A partir de MS Project
  * A partir d’un fichier Excel (d’un format bien défini)

Mais préalablement vous devez activer la fonctionnalité de site **Fonctionnalité Project**.

[<img class="alignnone size-full wp-image-452" src="/wp-content/uploads/2015/01/webfeature.png?fit=783%2C252" alt="WebFeature" srcset="/wp-content/uploads/2015/01/webfeature.png?resize=300%2C97 300w, /wp-content/uploads/2015/01/webfeature.png?w=783 783w" sizes="(max-width: 783px) 100vw, 783px" data-recalc-dims="1" />][1]

Maintenant vous pouvez synchroniser vos taches entre MS Project et une liste de tache SharePoint

[<img class="alignnone size-full wp-image-451" src="/wp-content/uploads/2015/01/taskslist.png?fit=548%2C233" alt="TasksList" srcset="/wp-content/uploads/2015/01/taskslist.png?resize=300%2C128 300w, /wp-content/uploads/2015/01/taskslist.png?w=548 548w" sizes="(max-width: 548px) 100vw, 548px" data-recalc-dims="1" />][2]

# Depuis SharePoint à partir d’une liste de taches existante {#depuis-sharepoint-à-partir-dune-listede-tachesexistante}

Dans le ruban de votre liste de tache, vous trouverez le bouton **Ouvrir avec Project** dans la section **Se connecter et exporter**.

Au clic, Project se lance et se synchronise avec la liste remontant ainsi toutes les taches existantes.

[<img class="alignnone size-full wp-image-453" src="/wp-content/uploads/2015/01/openinproject.png?fit=354%2C99" alt="OpenInProject" srcset="/wp-content/uploads/2015/01/openinproject.png?resize=300%2C84 300w, /wp-content/uploads/2015/01/openinproject.png?w=354 354w" sizes="(max-width: 354px) 100vw, 354px" data-recalc-dims="1" />][3]

 

# Depuis MS Project à partir d’une liste SharePoint existante {#depuis-ms-project-à-partir-dune-liste-sharepoint-existante}

Dans Project, vous avez le possiblité de créer un projet à partir d’une liste de taches existante.

En cliquant sur **New from SharePoint Tasks List**, vous renseignez l’URL de votre Site et après vérification vous pourrez sélectionner une liste de tache.

Au clic sur OK, Project crée un nouveau projet et le synchronise la liste.

[<img class="alignnone wp-image-443 size-large" src="/wp-content/uploads/2015/01/newfromsp-1024x221.png?fit=890%2C192" alt="NewFromSP" srcset="/wp-content/uploads/2015/01/newfromsp.png?resize=300%2C65 300w, /wp-content/uploads/2015/01/newfromsp.png?resize=1024%2C221 1024w, /wp-content/uploads/2015/01/newfromsp.png?w=1127 1127w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][4]

# Synchronisation depuis MS Project {#synchronisation-depuis-ms-project}

Si vous disposez d’un projet Project pré existant, vous pouvez le synchroniser avec une liste de tache dans SharePoint.

Deux options se présentent à vous depuis File > Save As > Sync with SharePoint

## Depuis MS Project dans une liste existante {#depuis-ms-project-dans-une-liste-existante}

[<img class="alignnone size-full wp-image-446" src="/wp-content/uploads/2015/01/save2.png?fit=683%2C301" alt="Save2" srcset="/wp-content/uploads/2015/01/save2.png?resize=300%2C132 300w, /wp-content/uploads/2015/01/save2.png?w=683 683w" sizes="(max-width: 683px) 100vw, 683px" data-recalc-dims="1" />][5]

## Depuis MS Project dans un nouveau site {#depuis-ms-project-dans-un-nouveau-site}

[<img class="alignnone size-full wp-image-445" src="/wp-content/uploads/2015/01/save1.png?fit=686%2C308" alt="Save1" srcset="/wp-content/uploads/2015/01/save1.png?resize=300%2C135 300w, /wp-content/uploads/2015/01/save1.png?w=686 686w" sizes="(max-width: 686px) 100vw, 686px" data-recalc-dims="1" />][6]

# Champs personnalisés {#champs-personnalisés}

Si vous avez des champs supplémentaires dans SharePoint, il est possible de les afficher dans MS Project.

Pour cela il faut :

<ol type="1">
  <li value="1">
    Ajouter un champ dans le projet MS Project.
  </li>
</ol>

[<img class="alignnone size-full wp-image-441" src="/wp-content/uploads/2015/01/customfields.png?fit=463%2C258" alt="CustomFields" srcset="/wp-content/uploads/2015/01/customfields.png?resize=300%2C167 300w, /wp-content/uploads/2015/01/customfields.png?w=463 463w" sizes="(max-width: 463px) 100vw, 463px" data-recalc-dims="1" />][7]

<ol type="1">
  <li value="2">
    Il faut gérer les champs de synchronisation avec SharePoint de manière à associer la colonne SP avec le champ Project.
  </li>
</ol>

[<img class="alignnone size-full wp-image-442" src="/wp-content/uploads/2015/01/mapping.png?fit=853%2C363" alt="Mapping" srcset="/wp-content/uploads/2015/01/mapping.png?resize=300%2C128 300w, /wp-content/uploads/2015/01/mapping.png?w=853 853w" sizes="(max-width: 853px) 100vw, 853px" data-recalc-dims="1" />][8]

**Note :** Lors d’une synchronisation, le fichier Project (.mpp) est sauvegardé automatiquement dans la bibliothèque de document Pièces jointes. Cela vous évitera de vous demander où trouver la dernière version du fichier.

Vous constaterez sur les deux captures suivantes que le rendu du diagramme de Gantt dans SharePoint est très proche de celui de Project.

[<img class="wp-image-444 size-large" src="/wp-content/uploads/2015/01/projectexample-1024x639.png?fit=890%2C556" alt="ProjectExample" srcset="/wp-content/uploads/2015/01/projectexample.png?resize=300%2C187 300w, /wp-content/uploads/2015/01/projectexample.png?resize=1024%2C639 1024w, /wp-content/uploads/2015/01/projectexample.png?w=1241 1241w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][9]  
_Ecran Gantt de MS Project_ [<img class="wp-image-448 size-full" src="/wp-content/uploads/2015/01/sp2.png?fit=890%2C415" alt="SP2" srcset="/wp-content/uploads/2015/01/sp2.png?resize=300%2C140 300w, /wp-content/uploads/2015/01/sp2.png?resize=1024%2C477 1024w, /wp-content/uploads/2015/01/sp2.png?w=1651 1651w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][10]  
_Gantt dans SharePoint 2013_

Remarquez au passage la frise chronologique qui permet de promouvoir certaines taches afin d’en améliorer la visibilité. Petit plus, le menu contextuel permet d’interagir en un clic avec les taches.

[<img class="wp-image-440 size-large" src="/wp-content/uploads/2015/01/chronologie-1024x299.png?fit=890%2C260" alt="Chronologie" srcset="/wp-content/uploads/2015/01/chronologie.png?resize=300%2C88 300w, /wp-content/uploads/2015/01/chronologie.png?resize=1024%2C299 1024w, /wp-content/uploads/2015/01/chronologie.png?w=1388 1388w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][11]  
_Menu contextuel dans la frise chronologique_

# Webpart spécifiques {#webpart-spécifiques}

En activant la fonctionnalité **Fonctionnalité Projet**, vous disposez de deux composants dans la section **Report de contenu** > **Chronologie & Récapitulatif de contenu**.

## Récapitulatif du projet/Project Summary {#récapitulatif-du-projetproject-summary}

Ce composant permet de visualiser les taches d’un projet et d’interagir avec elles aux travers de quatre affichages glissants:

<ul type="disc">
  <li>
    Compte à rebours
  </li>
  <li>
    Chronologie
  </li>
  <li>
    En retard
  </li>
  <li>
    A venir
  </li>
</ul>

[<img class="wp-image-450 size-full" src="/wp-content/uploads/2015/01/summarywebpart.gif?fit=890%2C195" alt="SummaryWebpart" data-recalc-dims="1" />][12]  
_WebPart Récapitulatif du Projet_

## Chronologie/Timeline {#chronologietimeline}

Ce composant permet d’afficher la chronologie d’une liste de tache. Simple mais utile.

 

Au final, synchroniser son projet MS Project avec une liste de projet dans SharePoint permet de partager de donner de la visibilité à votre projet. Vos collaborateurs disposeront d’un lieu centralisé de collaboration avec de composants graphiques sur lesquels s’appuyer. Ayant une connaissance de surface de Project, je vous conseille le blog – en anglais – de [Nenad Trajkovski][13] pour découvrir des actions avancées.

 [1]: /wp-content/uploads/2015/01/webfeature.png
 [2]: /wp-content/uploads/2015/01/taskslist.png
 [3]: /wp-content/uploads/2015/01/openinproject.png
 [4]: /wp-content/uploads/2015/01/newfromsp.png
 [5]: /wp-content/uploads/2015/01/save2.png
 [6]: /wp-content/uploads/2015/01/save1.png
 [7]: /wp-content/uploads/2015/01/customfields.png
 [8]: /wp-content/uploads/2015/01/mapping.png
 [9]: /wp-content/uploads/2015/01/projectexample.png
 [10]: /wp-content/uploads/2015/01/sp2.png
 [11]: /wp-content/uploads/2015/01/chronologie.png
 [12]: /wp-content/uploads/2015/01/summarywebpart.gif
 [13]: http://www.ntrajkovski.com/