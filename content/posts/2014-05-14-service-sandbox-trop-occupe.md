---
title: Service sandbox trop occupé
author: Ghislain

date: 2014-05-14T08:35:42+00:00
url: /2014/05/14/service-sandbox-trop-occupe/
categories:
  - PowerShell
  - SharePoint
tags:
  - PowerShell
  - SharePoint
  - SharePoint 2010

---
Quelle drôle de message que le suivant quand on souhaite activer une solution sandbox sous SharePoint 2010 :

> The sandboxed code execution request was refused because the Sandboxed Code Host Service was too busy to handle the request

Une recherche sur internet permet de trouver pleins de solutions. Une entrée sur [SharePoint Dev Blog][1] liste plusieurs sources possibles.

L’une d’elle vient d’une clé de registre dont la valeur est erronée. Ma modeste contribution est un bout de PowerShell qui permet d’automatiser la correction.

{{< highlight powershell>}}#Get account running SPUserCode
$service = Get-WmiObject win32_service | ? {$_.name -eq "SPUserCodeV4"}
$sid = (Get-SPManagedAccount –Identity $service.startname).Sid.Value
#Create driver to HKEY_USERS
$null = New-PSDrive -Name HKU   -PSProvider Registry -Root Registry::HKEY_USERS
#Move in registry
Set-Location HKU:
Set-Location $sid
Set-Location '.SoftwareMicrosoftWindowsCurrentVersionWinTrustTrust ProvidersSoftware Publishing'
#Get value
$v = Get-ItemProperty . -Name State
# Hex to decimal conversion: 0x23e00 =&gt; 146944
if($v.State.ToString() -eq 146944)
{
   write-host "No change needed there" -ForegroundColor Green
}
else
{
   Set-ItemProperty -Path . Name State -Value 146944
   write-host "Value changed" -ForegroundColor Red
}
{{< /highlight >}}

 

 

 [1]: http://blogs.msdn.com/b/sharepointdev/archive/2011/02/08/error-the-sandboxed-code-execution-request-was-refused-because-the-sandboxed-code-host-service-was-too-busy-to-handle-the-request.aspx