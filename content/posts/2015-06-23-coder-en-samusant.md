---
title: Coder en s’amusant
author: Ghislain
date: 2015-06-23T07:33:52+00:00
url: /2015/06/23/coder-en-samusant/
featuredImage: /wp-content/uploads/2015/06/logo_codingame_hud.png
categories:
  - Formation
tags:
  - algorithmie
  - codin’game
  - Développement personnel

---
Initialement j’avais prévu un article sur les tests end-to-end pour AngularJS mais ce n’est pas le cas. J’ai une bonne raison : j’ai joué. Plus précisément, j’ai révisé mon algorithmie et amélioré ma connaissance du JavaScript. Et oui, il est possible d’apprendre à coder en s’amusant et d’améliorer ses compétences de manière ludique.

## Coder en s’amusant {#coder-en-samusant}

Si vous êtes comme certains de mes collègues de boulots, vous allez me dire « pourquoi tu codes sur ton temps personnel ? ». Parce que se lutter contre Skynet, aider Batman ou le docteur Who, ça fait rêver. Outre l’aspect technique, Codin’game touche les références de l’univers geek communes à une majorité de développeur.

[Initialement j’avais prévu un article sur les tests end-to-end pour AngularJS mais ce n’est pas le cas. J’ai une bonne raison : j’ai joué. Plus précisément, j’ai révisé mon algorithmie et amélioré ma connaissance du JavaScript. Et oui, il est possible d’apprendre à coder en s’amusant et d’améliorer ses compétences de manière ludique.

## Codin’game {#codingame}

Quel est donc ce jeu qui m’a scotché et m’a gentiment poussé à me triturer les méninges ? Codin’game : challenge de programmation en ligne. Il s’agit d’un site proposant des challenges solo et multi. Les challenges solos sont classés par niveau de difficulté. Sauf quelques rares exceptions, il est possible de relever le challenge dans plusieurs langages (Bash, C, C++, C#, Closure, Dart, F#, Go, Groovy, Haskell, Java, JavaScript, Lua, ObjectiveC, OCaml, Pascal, Perl, PHP, Python, Python3, Ruby, Scala, VB.Net).

Le petit plus est la présence d’une communauté (forum et conseils) ainsi que la présence d’un classement. Mine de rien si on vient c’est pour progresser et aussi pour comparer nos solutions avec celles des autres participants.

[<img class="wp-image-869 size-large" title="Codin'game - Batman - Détecteur de chaleur" src="/wp-content/uploads/2015/06/Codingame-Batman-1024x496.png?fit=1020%2C494" alt="Codingame-Batman" />][1]
  
_Codin’game – Batman – Détecteur de chaleur_

## Coder en s’amusant {#coder-en-samusant-1}

Si vous êtes comme certains de mes collègues de boulots, vous allez me dire « pourquoi tu codes sur ton temps personnel ? ». Parce que se lutter contre Skynet, aider Batman ou le docteur Who, ça fait rêver. Outre l’aspect technique, Codin’game touche les références de l’univers geek communes à une majorité de développeur.

](https://www.codingame.com/replay/solo/44769519)

Il faut passer notre code sous le feu de plusieurs jeux d’essai pour le valider. Puis le soumettre au serveur qui le fait à son tour passer à d’autres jeux d’essai pour débloquer des achievements. Car la gamification fait des miracles, débloquer des achievements motive un peu plus.

[Mon profil sur Codin’game][2]

## Aller plus loin {#aller-plus-loin}

Je découvre réellement les jeux de code avec Codin’game, lequel m’a bien scotché. Jusqu’à présent j’avais un peu picoré les jeux suivants sans réellement accrocher :

](https://github.com/olistic/warriorjs) : JavaScript et intelligence artificielle

[Ruby warrior][3] : Ruby et intelligence artificielle

](http://leekwars.com/) : intelligence artificielle, langage spécifique

](https://www.checkio.org/) : Python

A vos claviers! 3. 2. 1. Happy coding!

 [1]: /wp-content/uploads/2015/06/Codingame-Batman.png
 [2]: https://www.codingame.com/profile/f71fa14b169702b56413113af18f26d2620249
 [3]: https://www.bloc.io/ruby-warrior/