---
title: Microsoft Learn
author: Ghislain
date: 2019-10-15
url: /2019/10/15/microsoft-learn/
categories:
  - Formation
tags:
  - Microsoft

---

Après Microsoft Virtual Academy, Microsoft a créé un nouveau site de formation en ligne pour ses technos/produits. Comme on pouvait s’y attendre, l’expérience utilisateur s’améliore et la qualité du contenu est au rendez-vous.

La gamification reste présente avec des niveaux de progression, des trophés, des badges. Il est possible de partager son profil pour montrer les sujets qui nous intéresse.

[Mon profil sur Microsoft Learn](https://docs.microsoft.com/fr-fr/users/ghislainl/)

Par contre, dans la continuité de sa stratégie cloud first, Microsoft a mis le paquet sur les formations Azure au détriment du on-premise. Pour une personne qui découvrirait les technologies MS, il lui serait impossible de savoir qu’il existe presque toujours un équivalent on-prem.

Je trouve aussi dommage qu’à chaque changement de plateforme de formation, Microsoft fait table rase des profils utilisateurs.

[Microsoft Learn](https://docs.microsoft.com/fr-fr/learn/)