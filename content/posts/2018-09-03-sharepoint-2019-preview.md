---
title: SharePoint 2019 Preview
author: admin7364

date: 2018-09-03T07:09:12+00:00
url: /2018/09/03/sharepoint-2019-preview/
featuredImage: /wp-content/uploads/2018/08/SharePoint2019.png
categories:
  - SharePoint
tags:
  - SharePoint 2019

---
SharePoint 2019 Preview est disponible depuis fin juillet. Cette sortie n&#8217;a pas fait beaucoup de bruit vu la période estivale.

En quelques mots, l&#8217;installation se passe super bien et plus rapidement qu&#8217;avec les versions précédentes.

## Choses importantes à noter

  * Refonte de l&#8217;interface graphique pour être plus conviviale. 
      * Listes et bibliothèques
      * Site d&#8217;équipe &#8211; page d&#8217;accueil
      * Site de communication &#8211; page d&#8217;accueil
      * L&#8217;éditeur de page et webpart est moins lourd. Il ressemble à Gutenberg de WordPress.
  * Nouvelles fonctionnalités 
      * connecteur pour PowerApps et Flow (o365)
      * upload des fichiers jusqu&#8217;à 15Go et un chemin de 400 caractères unicode
      * authentification auprès du serveur SMTP
      * intégration du SharePoint Framework
      * amélioration de la recherche, avec notamment des suggestions (type ahead)
  * Fonctionnalités dépréciées 
      * application SharePoint Designer
      * client Groove sync
      * services Access 2010 et 2013
      * service Infopath
      * service PerformancePoint
      * site manager
      * web service Lists
  * Fonctionnalités supprimées 
      * les solutions sandbox embarquant du code
      * les galleries PowerPivot
      * multi-tenant
      * service Visio &#8211; rendu avec silverlight

## Site de communication

<figure id="attachment_356" aria-describedby="caption-attachment-356" style="width: 900px" class="wp-caption alignnone"><img class="wp-image-356 size-large" src="https://ghislain-lerda.fr/wp-content/uploads/2018/08/SP2019-Communication-1024x527.png" alt="SharePoint 2019 - site de communication" width="900" height="463" srcset="https://ghislain-lerda.fr/wp-content/uploads/2018/08/SP2019-Communication-1024x527.png 1024w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/SP2019-Communication-300x154.png 300w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/SP2019-Communication-768x395.png 768w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/SP2019-Communication.png 1918w" sizes="(max-width: 900px) 100vw, 900px" /><figcaption id="caption-attachment-356" class="wp-caption-text">SharePoint 2019 &#8211; site de communication</figcaption></figure>

Ce modèle de site possède un rendu propre, dans le temps. Pour ne rien gâcher la prise en main est très rapide.

**Références : **

[Differences between SharePoint Server 2016 and 2019 Public Preview][1]

[SharePoint 2019 Public Preview Generally Available][2]

[**Download** SharePoint Server 2019 Public Preview][3]

 [1]: https://support.office.com/en-us/article/differences-between-sharepoint-server-2016-and-2019-public-preview-ba84c8a3-3ce2-4252-926e-c67654ceb4a3
 [2]: https://blogs.technet.microsoft.com/christianheim/2018/07/24/sharepoint-2019-public-preview-generally-available/
 [3]: https://www.microsoft.com/en-us/download/details.aspx?id=57169