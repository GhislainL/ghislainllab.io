---
title: Entity Framework Core 2.2 Preview 2 – données spatiales
author: admin7364

date: 2018-09-20T12:27:01+00:00
url: /2018/09/20/entity-framework-core-2-2-preview2-spatial/
featuredImage: /wp-content/uploads/2018/07/dotnetcore-sq.png
categories:
  - Databases
  - DotNet Core
tags:
  - DotNet Core
  - Entity Framework Core
  - Spatial
  - SQL Server

---
Ca y est, Entity Framework Core supporte les données spatiales avec la sortie de la version 2.2 Preview 2. Regardons comment nous en servir. <!--more-->

## Pre-requis

<p id="sdk-2.2.100-preview2" class="h5">
  Sans surprise, il faut le <a href="https://www.microsoft.com/net/download/dotnet-core/2.2">SDK 2.2.100-preview2</a> et la dernière version de <a href="https://visualstudio.microsoft.com/vs/preview/">VS 2017 Preview</a>.
</p>

<p class="h5">
  Rassurez vous, VS 2017 et VS 2017 Preview peuvent être installé côte à côté sur la même machine.
</p>

## Code

Ajouter le package Microsoft.EntityFrameworkCore.SqlServer.NetTopologySuite qui est disponible en **prerelease** pour l&#8217;instant.

<figure id="attachment_381" aria-describedby="caption-attachment-381" style="width: 957px" class="wp-caption alignnone"><img class="wp-image-381 size-full" src="https://ghislain-lerda.fr/wp-content/uploads/2018/09/NuGet-NetTopologySuite.png" alt="NuGet - NetTopologySuite" width="957" height="206" srcset="https://ghislain-lerda.fr/wp-content/uploads/2018/09/NuGet-NetTopologySuite.png 957w, https://ghislain-lerda.fr/wp-content/uploads/2018/09/NuGet-NetTopologySuite-300x65.png 300w, https://ghislain-lerda.fr/wp-content/uploads/2018/09/NuGet-NetTopologySuite-768x165.png 768w" sizes="(max-width: 957px) 100vw, 957px" /><figcaption id="caption-attachment-381" class="wp-caption-text">NuGet &#8211; NetTopologySuite</figcaption></figure>

Dans la preview 2, les type spatiaux ne sont pas supportés par la commande Scaffold-DbContext.

{{< highlight powershell >}}PM> Scaffold-DbContext "Data Source=..." Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models -Force
Could not find type mapping for column 'dbo.Events.EventLocation' with data type 'geography'. Skipping column.
{{< /highlight >}}

Il faut alors les ajouter manuellement dans les modèles.  A noter que l&#8217;approche _code first_ quant à elle fonctionne.

{{< highlight csharp >}}
using NetTopologySuite.Geometries;
//...
public partial class Events
{
   public int Id { get; set; }
   public string Title { get; set; }
   public Point EventLocation { get; set; }
}
{{< /highlight >}}

Une fois le package installé, il faut activer la gestion des données spatiales dans le DBContext en appelant UseNetTopologySuite() dans UseSqlServer() dans OnConfiguring() ou dans AddDbContext().

{{< highlight csharp >}}services.AddDbContext(
   options =>
      options.UseSqlServer("Data Source=...",
   sqlOptions => sqlOptions.UseNetTopologySuite()));
   {{< /highlight >}}

Il est maintenant possible de manipuler ces types.

{{< highlight csharp >}}
using (TestContext testContext = new TestContext())
{
    // Insert
    testContext.Events.AddRange(
        new Events
        {
            Id = 100,
            Title = "E14",
            EventLocation = new NetTopologySuite.Geometries.Point(1, 4) { SRID = 4326 }
        },
        new Events
        {
            Id = 101,
            Title = "E41",
            EventLocation = new NetTopologySuite.Geometries.Point(4, 1) { SRID = 4326 }
        }
    );

    testContext.SaveChanges();

    // display
    var v = testContext.Events;
    Console.WriteLine($"Title - EventLocation.AsText()");
    foreach (var e in v)
    {
        Console.WriteLine($"{e.Title} - {e.EventLocation.AsText()}");
    }
}
{{< /highlight >}}

et voici le résultat dans la console

{{< highlight plain >}}
Title - EventLocation.AsText()
E14 - POINT (1 4)
E41 - POINT (4 1)
{{< /highlight >}}

### ASP.Net Core MVC

J&#8217;ai testé avec ASP.Net Core MVC et la génération de contrôleur et vues fonctionne.

Il est possible d&#8217;aller plus loin, comme en cherchant les points à proximité d&#8217;un autre point.

{{< highlight csharp >}}public async Task Near()
{
   Point point = new Point(3, 0) { SRID = 4326 };
   return View(await _context.Events
      //.Where(e => e.EventLocation.Distance(point) < x)
      .OrderBy(e => e.EventLocation.Distance(point))
      .Take(3)
      .ToListAsync());
}
{{< /highlight >}}

et voici le résultat :

<figure id="attachment_391" aria-describedby="caption-attachment-391" style="width: 544px" class="wp-caption alignnone"><img class="wp-image-391 size-full" src="https://ghislain-lerda.fr/wp-content/uploads/2018/09/Near-Geo-1.png" alt="ASP.NET Core MVC - Geospatial" width="544" height="288" srcset="https://ghislain-lerda.fr/wp-content/uploads/2018/09/Near-Geo-1.png 544w, https://ghislain-lerda.fr/wp-content/uploads/2018/09/Near-Geo-1-300x159.png 300w" sizes="(max-width: 544px) 100vw, 544px" /><figcaption id="caption-attachment-391" class="wp-caption-text">ASP.NET Core MVC &#8211; Geospatial</figcaption></figure>

&nbsp;

Cette preview va dans la bonne direction.

DotNet Core devrait toucher un public plus large avec cette fonctionnalité qui était attendue depuis longtemps par la communauté.

**Références : **

[Announcing Entity Framework Core 2.2 Preview 2][1]

 [1]: https://blogs.msdn.microsoft.com/dotnet/2018/09/12/announcing-entity-framework-core-2-2-preview-2/