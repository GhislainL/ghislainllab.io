---
title: Liberté et compagnie de Isaac Getz et Brian M Carney
author: Ghislain

date: 2016-02-21T00:00:00+00:00
url: /2016/02/21/liberte-et-compagnie-de-isaac-getz-et-brian-m-carney/
categories:
  - Développement personnel
tags:
  - Brian M Carney
  - Isaac Getz
  - Liberté et compagnie
  - livres

---
**Liberté et Cie** de Isaac Getz et Brian M Carney est un livre qui aborde un sujet de plus en plus présent : la liberté dans les entreprises. A l’heure où la réponse la plus courante passe par une augmentation de bureaucratie et un contrôle plus autoritaire, il est nécessaire de savoir pourquoi.

![Liberté et compagnie de Isaac Getz et Brian M Carney][1]

En 1960 Douglas McGregor publie The Human side of enterprise dans lequel il oppose les théories X et Y:

  * Selon la théorie X qui repose sur le postulat selon lequel l’employé n’aime pas travailler. Il doit être contraint et dirigé pour travailler pour une entreprise.
  * Selon la théorie Y qui repose sur le postulat selon lequel l’employé aime travailler. S’il adhéré aux objectifs d’une entreprise, il se dirigera et se contrôlera lui-même.

Les auteurs mettent donc en oppositions les entreprises comment et les entreprises pourquoi. Dans les entreprises comment, les dirigeants expliquent aux employés comment ils doivent travailler et mettent en place des mesures pour cadrer, surveiller, cloisonner. Dans les entreprises pourquoi, les dirigeants partagent une vision de pourquoi la compagnie existent, veillent à la préserver pour que tous les employés la partagent et travaillent dans ce sens.

Certaines compagnie ont pris le pari de la liberté, de la confiance et de la transparence. Pour chacune d’entre elles prises pour exemple, nous découvrons l’approche d’une personne qui voulait concrétiser sa vision d’une compagnie différente. Les réussites, les anecdotes, les doutes, les difficultés, nous avons droit à des tranches de vie et de management. Le contenu est d’autant plus intéressant qu’il aborde autant le point de vue du dirigeant, des cadres et que celui des ouvriers.

  * FAVI, France, fonderie
  * IDEO,
  * Harley Davidson, Etats-Unis, constructeur de moto
  * Oticon, Denemark, prothèse auditive
  * Sun Hydraulics, Etats Unis, équipement industriel,
  * USAA, Etats-Unis, Assurances
  * Vertex, éditeur logiciel
  * SOL, Finlande, société de nettoyage
  * Richards Group, Etats Unis, publicité
  * Sea Smoke Cellars, Etats-Unis, viticulture
  * W. L. Gore and Associates, Etats-Unis, textile

Chacune de ses sociétés peut-être qualifiée de libérée mais son modèle diffère pourtant de celui des autres. De part la diversité des secteurs, des cultures d’entreprises, de l’implantation géographique, chaque exemple est unique ce qui permet d’ouvrir son horizon. Les auteurs mettent en avant une liste de points communs dont les suivants :

  * pas d’organigramme
  * ni parking réservés ni bureaux plus grand pour les dirigeants
  * pas de pointeuse et possibilité pour les employés de fixer leur emploi du temps
  * pas ou peu de manager et les salaries choisissent eux-même leurs leaders et définissent le contenu de leur poste
  * pas de processus de planification à long terme

Accorder de l’autonomie, des responsabilités, traiter en égaux, donner de la reconnaissance, … autant de leviers de la motivation qui sont mis en avant et utilisés. Si vous voulez approfondir ce sujet, je vous conseille **La vérité sur ce qui nous motive** de Dan Pink.

Liberté et Cie met en avant des réussites majeures mais il existe des sociétés libérés qui ont coulé et d’autres qui vivent sans faire de vagues. Une culture d’entreprise libérée est fragile, il est arrive qu’elle périsse après le départ de son initiateur. De plus, ces environnements ne sont pas adaptés à tous. Le responsabilisation et la transparence peuvent faire peur et représentent un poids qu’il faut vouloir porter, mais n’est-ce pas ainsi que l’on grandit ?

 [1]: /wp-content/uploads/books/LiberteCie.jpg