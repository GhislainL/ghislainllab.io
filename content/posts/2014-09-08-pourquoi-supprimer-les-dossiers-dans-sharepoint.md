---
title: Pourquoi supprimer les dossiers dans SharePoint?
author: Ghislain

date: 2014-09-08T07:18:13+00:00
url: /2014/09/08/pourquoi-supprimer-les-dossiers-dans-sharepoint/
categories:
  - SharePoint
tags:
  - ECM
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013

---
SharePoint est considéré par beaucoup d’utilisateurs comme un simple partage réseau. Par conséquent, ils s’en servent comme tel et mettent en place un classement par dossier. La profondeur de l’arborescence créée peut rapidement être vertigineuse ou excessivement large. Bref vous avez affaire à une jungle de dossiers.

Pourtant SharePoint propose des métadonnées, des informations qualifiant un élément (un document en l’occurrence). Ces métadonnées peuvent être structurées dans un type de contenu pour vous faciliter la vie.

Quels sont les avantages des métadonnées par rapport aux dossiers ?

Prenez comme exemple l’arborescence suivante :

[<img class="alignnone size-full wp-image-232" src="/wp-content/uploads/2014/09/arborescence.png?fit=200%2C398" alt="arborescence" data-recalc-dims="1" />][1]

Et le type de contenu suivant :

[<img class="alignnone size-full wp-image-233" src="/wp-content/uploads/2014/09/contenttype.png?fit=555%2C169" alt="contentType" srcset="/wp-content/uploads/2014/09/contenttype.png?resize=300%2C91 300w, /wp-content/uploads/2014/09/contenttype.png?w=555 555w" sizes="(max-width: 555px) 100vw, 555px" data-recalc-dims="1" />][2]

  * Les liens courts : ils ne comportent que le chemin jusqu’à la bibliothèque et le nom du fichier.
  * Les liens pérennes : modifier la métadonnée Année de 2013 à 2014 ne casse pas le lien, contrairement à déplacer le document.
  * La catégorisation multiple : un document peut facilement concerner la Communication et le Marketing avec les métadonnées. Mais comment le classer avec des dossiers, dans l’un, dans l’autre, dans les deux, dans un nouveau dossier ?
  * Le tri et groupement : afficher uniquement les documents de type Contrat est simple. Cela demande plus de temps dans une arborescence.
  * Les vues : il s’agit de plusieurs affichages qui répondent à un besoin fonctionnel. Impossible avec des dossiers
  * La flexibilité : à l’aide des vues, vous ne subissez plus la logique de la personne qui a créer l’arborescence
  * L’évolutivité : si vous devez ajouter un nouveau type de document, il vous suffit de le faire une fois. Dans une arborescence, vous devez créer X fois le même dossier.
  * L’intégration dans Office : les documents de la suite Office permettent de renseigner les métadonnées depuis le logiciel. L’intégrité de l’organisation des documents est assurée. De plus, vous pouvez utiliser ces métadonnées directement dans le document.

[<img class="alignnone size-full wp-image-234" src="/wp-content/uploads/2014/09/integrationoutlook.png?fit=704%2C271" alt="integrationOutlook" srcset="/wp-content/uploads/2014/09/integrationoutlook.png?resize=300%2C115 300w, /wp-content/uploads/2014/09/integrationoutlook.png?w=704 704w" sizes="(max-width: 704px) 100vw, 704px" data-recalc-dims="1" />][3]

Note : Le seul usage des dossiers reste pour les autorisations. Vous créez des répertoires sur lesquels vous cassez l’héritage des autorisations. Et dans les vues, il faut penser afficher les documents sans dossiers. Mais cela reste le seul cas dans cette méthode de stockage.

Au final, vous disposez d’un outil qui possède deux avantages : facilité d’organisation de l’information et encadrement des utilisateurs. Les métadonnées permettent de qualifier un document de manière simple et pérenne. Elles permettent aussi de garantir que l’ordre sera respecté et conservé durant l’usage.

Prochainement, je vous expliquerez pourquoi il est nécessaire d’utiliser les types de contenu.

 [1]: /wp-content/uploads/2014/09/arborescence.png
 [2]: /wp-content/uploads/2014/09/contenttype.png
 [3]: /wp-content/uploads/2014/09/integrationoutlook.png