---
title: Principe de Shirky
author: Ghislain
date: 2019-09-15
url: /un-dessin/principe-de-shirky/
categories:
  - Un dessin
tags:
  - sketchplanations
draft : true

---

![Principe de Shirky](/un-dessin/sketchplanations-shirky-principle.jpg)

Named after the brilliant Clay Shirky his principle broadly states that:

Institutions will try to preserve the problem to which they are the solution.
Sometimes this may manifest itself as blindness towards new or different ways of doing things, other times it’s the challenge of and innate resistance to making oneself obsolete. Examples include there being more money in life-long illnesses than cures, traditional newspapers’ resistance to alternative online news forms, planned obsolescence of products, committees requiring the presence of committees, or resistance to self-checkouts at a supermarket.

Some exceptions include Vitality health insurance in the UK that rewards a healthy lifestyle, and the lightbulb industry, in its move towards ultra long-lasting and efficient LED lights, boldly eliminating the need to replace most of its products for a lifetime. I’m still waiting for a breakdown service to reward people for taking care of their cars rather than focusing on fixing the breakdowns.

https://en.wikipedia.org/wiki/Clay_Shirky#Shirky_principle

aller plus loin avec  "modèle de la poubelle" (Anarchie organisée) : https://fr.wikipedia.org/wiki/Anarchie_organis%C3%A9e

Traduction de la publication de [Sketchplanations](https://www.sketchplanations.com/post/167369765942/goodharts-law-when-a-measure-becomes-a-target)