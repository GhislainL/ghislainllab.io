---
title: Microsoft Virtual Academy évolue
author: Ghislain

date: 2016-03-01T00:00:00+00:00
url: /2016/03/01/microsoft-virtual-academy-evolue/
categories:
  - Formation
tags:
  - Microsoft

---
Il y a environ un mois, MVA a évolué. La partie émergée de l’iceberg réside dans la fin du système de point et du classement. Mais cela s’inclue dans le cadre d’une homogénéisation de l’enseignement en ligne Microsoft. De plus, victime de son succès l’architecture derrière MVA évolue pour dispenser des connaissances dans de bonnes conditions. Un peu dans la lignée des changements du lecteur vidéo l’été dernier.

Dans le futur, les points gagnés devraient être compatible avec [Microsoft Tech Rewards][1]. Pour avoir une idée des évolutions, il faut jeter un oeil au challenge [Know it. Prove it][2].

Si comme moi, vous utilisez MVA depuis un moment, vous disposez d’un badge **MVA Founders Club** avec votre compteur de Legacy Points. Ci-dessous un extrait de mon profil. Il est possible de télécharger un transcript (PDF), je trouve dommage qu’il débute par les cours en cours.

![Extrait de mon transcript MVA][3]

La plateforme MVA propose du contenu de qualité et continue de s’améliorer. J’en profite et replonge dans le cours [Office 365 Fundamentals][4].

**Références**

[Changes to MVA points and your dashboard][5]

 [1]: https://rewards.msdn.microsoft.com "Microsoft Tech Rewards"
 [2]: https://borntolearn.mslearn.net/knowitproveit "Know it. Prove it"
 [3]: /wp-content/uploads/2016/02/MVA-Extract.png
 [4]: https://mva.microsoft.com/en-US/training-courses/office-365-fundamentals-8288 "Office 365 Fundamentals"
 [5]: https://borntolearn.mslearn.net/b/mva/archive/2016/01/28/changes-to-mva-points-and-your-dashboard "Changes to MVA points and your dashboard"