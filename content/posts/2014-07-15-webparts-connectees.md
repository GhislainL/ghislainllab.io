---
title: Webparts connectées
author: Ghislain

date: 2014-07-15T07:36:19+00:00
url: /2014/07/15/webparts-connectees/
categories:
  - PowerShell
  - SharePoint
tags:
  - PowerShell
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013
  - SPConnectWebParts
  - SPWebPartConnections

---
Dans SharePoint, connecter des webparts permet de rendre une webpart consommatrice dépendante de la valeur envoyée par la ou les webparts fournisseuses. Ce mécanisme existe sur certaines webparts de SharePoint – comme les vues de liste – et peut être implémenté sur des webparts personnalisées.

Concrètement on peut s’en servir pour créer facilement des filtres ou des interfaces de type master-detail. Pour l’exemple prenons un liste de recette que l’on aimerait filtrer par le type de recette (entrée, plat, dessert).

Les tables sont structurées comme suit :

– Recettes :

<table>
  <tr>
    <td>
      — Titre
    </td>
    
    <td>
      Texte
    </td>
  </tr>
</table>

<table>
  <tr>
    <td>
      — Recette
    </td>
    
    <td>
      Grand texte
    </td>
  </tr>
</table>

<table>
  <tr>
    <td>
      — Type de recette
    </td>
    
    <td>
      Choix (Aucun, Entrée, Plat, Dessert)
    </td>
  </tr>
</table>

– TypeRecette :

— Titre

Prenons une page avec 2 webparts de vue sur ces listes. Les vues sont personnalisées pour des besoins de clarté.

[<img class="alignnone wp-image-135 size-full" src="/wp-content/uploads/2014/07/master-detail-1.png?fit=728%2C259" alt="master-detail-1" srcset="/wp-content/uploads/2014/07/master-detail-1.png?resize=300%2C107 300w, /wp-content/uploads/2014/07/master-detail-1.png?w=728 728w" sizes="(max-width: 728px) 100vw, 728px" data-recalc-dims="1" />][1]

Editer la page, sélectionner la webpart consommatrice (Recettes), Connections, Get Filter Values From > webpart fournisseuse (TypeRecette).

[<img class="alignnone size-full wp-image-137" src="/wp-content/uploads/2014/07/master-detail-how1.png?fit=890%2C338" alt="master-detail-how1" srcset="/wp-content/uploads/2014/07/master-detail-how1.png?resize=300%2C114 300w, /wp-content/uploads/2014/07/master-detail-how1.png?resize=1024%2C389 1024w, /wp-content/uploads/2014/07/master-detail-how1.png?w=1063 1063w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][2]

Sélectionner les champs à lier, puis cliquer sur Finish :

–          Titre dans la webpart fournisseuse (TypeRecette)

–          TypeRecette dans la webpart concommatrice (Recettes)

[<img class="alignnone size-full wp-image-138" src="/wp-content/uploads/2014/07/master-detail-how2.png?fit=465%2C236" alt="master-detail-how2" srcset="/wp-content/uploads/2014/07/master-detail-how2.png?resize=300%2C152 300w, /wp-content/uploads/2014/07/master-detail-how2.png?w=465 465w" sizes="(max-width: 465px) 100vw, 465px" data-recalc-dims="1" />][3]

On remarque que l’interface à changer dans la webpart fournisseuse avec une flèche bidirectionnelle sur chaque ligne. Cette flèche signale que la valeur sert de filtre. Comme vous pouvez le constater sur l’animation ci-dessous, les 2 webparts sont maintenant liées.

[<img class="alignnone size-full wp-image-136" src="/wp-content/uploads/2014/07/master-detail-2.gif?fit=725%2C255" alt="master-detail-2.gif" data-recalc-dims="1" />][4]

Ce mécanisme natif dans SharePoint est simple à mettre en place. Il est un peu lent et nécessite un rechargement de la page mais se montre intéressant. Pour peu de données et/ou un affichage plus aguicheur, il est tout à fait envisageable de faire un développement personnalisé. Pourquoi pas avec KnockoutJS pour faire disparaitre le rechargement de la page.

La possibilité de connecter des webparts par PowerShell est un plus indéniable. Le code ci-dessous devrait vous en convaincre.

{{< highlight powershell >}}#####     User's variables     #####

$webUrl = "http://sps2013"
$pageFile = "Pages/newpage.aspx"
$consumerWebpartName = "Recettes"
$providerWebpartName = "TypeRecette"
####################################

$web = Get-SPWeb $webUrl
$page = $web.GetFile($pageFile)
$wpm = $page.GetLimitedWebPartManager([System.Web.UI.WebControls.WebParts.PersonalizationScope]::Shared)
# Get webparts
$consumerWebpart = $wpm.WebParts | Where-Object {$_.Title -eq $consumerWebpartName}
$providerWebpart = $wpm.WebParts | Where-Object {$_.Title -eq $providerWebpartName}
# Get the connection point
$consumerConnection = $wpm.GetConsumerConnectionPoints($consumerWebPart)["DFWP Filter Consumer ID"];
$providerConnection = $wpm.GetProviderConnectionPoints($providerWebPart)["DFWP Row Provider ID"];
# Create transformer
$transformer = New-Object Microsoft.SharePoint.WebPartPages.SPRowToParametersTransformer
$transformer.ConsumerFieldNames = @("TypeRecette");
$transformer.ProviderFieldNames = @("Title");
# Connect webparts
$newCon = $wpm.SPConnectWebParts($providerWebpart,$providerConnection,$consumerWebpart,$consumerConnection,$transformer)
$wpm.SPWebPartConnections.Add($newCon);
$wpm.Dispose()
$web.Dispose()
{{< /highlight >}}

 [1]: /wp-content/uploads/2014/07/master-detail-1.png
 [2]: /wp-content/uploads/2014/07/master-detail-how1.png
 [3]: /wp-content/uploads/2014/07/master-detail-how2.png
 [4]: /wp-content/uploads/2014/07/master-detail-2.gif