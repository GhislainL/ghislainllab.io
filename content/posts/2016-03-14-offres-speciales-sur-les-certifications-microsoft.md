---
title: Offres spéciales sur les certifications Microsoft
author: Ghislain

date: 2016-03-14T00:00:00+00:00
url: /2016/03/14/offres-speciales-sur-les-certifications-microsoft/
categories:
  - Formation
tags:
  - certification
  - Microsoft

---
Actuellement Microsoft propose deux offres spéciales pour avoir plus de chance de vous certifier: la **quinzaine de la certification** et le **booster pack**. Je vous propose une comparaison des deux offres.

# Quinzaine de la certif {#quinzaine-de-la-certif}

Il semble s’agir d’une offre conjointe de Microsoft et des centres de formations.

**Quand** : 29 mars au 8 avril 2016

**Prix** : 418,80€ TTC

**Contenu** :

  * la mise à disposition pendant 30 jours d’un test de pratique en ligne (en anglais).
  * une session de révision d’une demi journée délivrée par un instructeur certifié Microsoft
  * l’examen de certification

**Examens éligibles** :

Office 365 70-346 Managing Office 365 Identities and Requirements

Office 365 70-347 Enabling Office 365 Services

Azure 70-533 Implementing Microsoft Azure Infrastructure Solutions

Azure 70-532 Developing Microsoft Azure Solutions

Windows 10 70-697 Configuring Windows Devices

Windows Server 2012 70-410 Installing and Configuring Windows Server 2012

Windows Server 2012 70-411 Administering Windows Server 2012

Exchange 2013 70-341 Core Solutions of Microsoft Exchange Server 2013

SQL Server 2014 70-462 Administering Microsoft SQL Server Databases

C# 70-483 Programming in C#

# Booster pack {#booster-pack}

Il semble s’agir d’une offre conjointe de Microsoft et MindHub.

**Quand** : 1 mars 2016 au 31 aout 2016

**Prix et contenu** :

3 offres :

  * test de pratique + examen de certification : **200$ + taxes**
  * examen de certification + 4 nouvelles tentatives : **200$ + taxes**
  * test de pratique + examen de certification + 4 nouvelles tentatives : **225$ + taxes** (45$ pour la France)

Le test de pratique est soumis aux mêmes conditions que dans l’offre quinzaine de la certif. Le service est fourni par [MeasureUp][1]

**Examens éligibles** :

Les vouchers sont valides pour tous les examens 70, 74 et MBx.

Centre de passage de l’examen doit être référencé chez https://home.pearsonvue.com/

&#8211;

Vous disposez maintenant d’une synthèse pour faciliter votre choix. Alors Quinzaine de la certif ou Booster Pack ?

**Références** :

[Microsoft &#8211; Quinzaine de la certif][2]

[Microsoft &#8211; Booster Pack][3]

 [1]: http://www.measureup.com
 [2]: https://www.microsoft.com/fr-fr/learning/local-certifications-nouveautes.aspx
 [3]: https://www.microsoft.com/en-us/learning/offers.aspx