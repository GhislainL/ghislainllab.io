---
title: RACI
author: Ghislain
date: 2021-01-18
url: /un-dessin/raci/
categories:
  - Un dessin
tags:
  - sketchplanations
  - management
  - projet
  - methodologie
---

![RACI](/un-dessin/sketchplanations-RACI.png)

Cet acronyme sert de liste de contrôle pratique pour examiner et clarifier les rôles et les responsabilités dans les projets.
Vous pouvez créer une matrice d'attribution des responsabilités - avec ce qui doit se passer et les personnes impliquées identifiant la relation que chaque personne entretient pour chaque tâche.
En particulier, la chose la plus utile à faire peut parfois être de préciser qui n’a pas son mot à dire.

Cela se décompose un peu comme suit:
- **Responsible** / Réalisateur : ceux qui réalisent effectivement la tâche;
- **Accountable** / Approbateur : celui qui est responsable de la complétion de la tache et de l'atteinte des objectifs;
- **Consulted** / Consulté : ceux dont l'opinion sur la tâche est important même s'ils ne la réalise pas eux-même;
- **Informed** / Informé : ceux qui n'ont pas leur mot à dire mais qui sont tenu au courant de l'avancement.

Remarques: 
- il ne doit y avoir qu'un seul *accountable* pour éviter la **dilution de responsabilité**;
- l'inversion du R et du A entre les versions française et anglaise;
- la traduction française de Accountable n'est pas pertinente car elle manque de responsabilisation.

original : [Sketchplanations](https://sketchplanations.com/raci)
