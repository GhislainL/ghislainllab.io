---
title: SharePoint 2016 preview
author: Ghislain

date: 2015-08-25T09:47:28+00:00
url: /2015/08/25/sharepoint-2016-preview/
categories:
  - SharePoint
tags:
  - sharepoint 2016

---
Cela fait quelque mois que la communauté SharePoint attend la nouvelle version on-Premise. Annoncée pour aout 2015, Microsoft a attendu le 24 aout rendre public la SharePoint 2016 preview. En m’endormant hier soir après avoir vu le billet [Announcing availability of SharePoint Server 2016 IT Preview and cloud hybrid search][1] sur le blog Office , je me suis dis que la journée serait intéressante.

Plutôt que de vous faire une synthèse en plusieurs points sur cette preview, je vous propose un référentiel pour retrouver des informations.

Dernière mise à jour : 28/08/2015.

## Téléchargement {#téléchargement}

SharePoint Server 2016 IT Preview.iso

  * Taille de l’ISO 2.8Go
  * 180-day trial
  * product key: NQTMW-K63MQ-39G6H-B2CH9-FRDWJ
  * <http://www.microsoft.com/en-us/download/details.aspx?id=48712>

## Pré requis {#pré-requis}

  * Review [SharePoint Server 2013 system requirements][2]
  * [Un article complet de Bill Baer sur les nouveautés dans l’installation et le déploiement de SharePoint 2016][3]
  * Supported Operating System 
      * Windows Server 2012 R2
      * Windows Server Technical Preview 2

## Références {#références}

  * Ce qui est nouveaux ou améliorés : 
      * [New and improved features in SharePoint Server 2016 IT Preview][4]
  * Ce qui est vieux ou mort 
      * [What’s deprecated or removed from SharePoint Server 2016 IT Preview][5]
  * Ce qui pose problème 
      * [Known Issues in SharePoint Server 2016 IT Preview][6]

## Guides officiels {#guides-officiels}

  * [Cela fait quelque mois que la communauté SharePoint attend la nouvelle version on-Premise. Annoncée pour aout 2015, Microsoft a attendu le 24 aout rendre public la SharePoint 2016 preview. En m’endormant hier soir après avoir vu le billet [Announcing availability of SharePoint Server 2016 IT Preview and cloud hybrid search][1] sur le blog Office , je me suis dis que la journée serait intéressante.

Plutôt que de vous faire une synthèse en plusieurs points sur cette preview, je vous propose un référentiel pour retrouver des informations.

Dernière mise à jour : 28/08/2015.

## Téléchargement {#téléchargement-1}

SharePoint Server 2016 IT Preview.iso

  * Taille de l’ISO 2.8Go
  * 180-day trial
  * product key: NQTMW-K63MQ-39G6H-B2CH9-FRDWJ
  * <http://www.microsoft.com/en-us/download/details.aspx?id=48712>

## Pré requis {#pré-requis-1}

  * Review [SharePoint Server 2013 system requirements][2]
  * [Un article complet de Bill Baer sur les nouveautés dans l’installation et le déploiement de SharePoint 2016][3]
  * Supported Operating System 
      * Windows Server 2012 R2
      * Windows Server Technical Preview 2

## Références {#références-1}

  * Ce qui est nouveaux ou améliorés : 
      * [New and improved features in SharePoint Server 2016 IT Preview][4]
  * Ce qui est vieux ou mort 
      * [What’s deprecated or removed from SharePoint Server 2016 IT Preview][5]
  * Ce qui pose problème 
      * [Known Issues in SharePoint Server 2016 IT Preview][6]

## Guides officiels {#guides-officiels-1}

*](http://www.microsoft.com/en-us/download/details.aspx?id=48714) : This quick start guide provides an overview of SharePoint Server 2016 IT Preview requirements and installation to support Single Server Farm installations for evaluation purposes.

  * [SharePoint Server 2016 IT Preview Datasheet][7] : The SharePoint Server 2016 IT Preview Datasheet provides an overview of new capabilities and investments in SharePoint Server 2016 Preview.
  * [Cela fait quelque mois que la communauté SharePoint attend la nouvelle version on-Premise. Annoncée pour aout 2015, Microsoft a attendu le 24 aout rendre public la SharePoint 2016 preview. En m’endormant hier soir après avoir vu le billet [Announcing availability of SharePoint Server 2016 IT Preview and cloud hybrid search][1] sur le blog Office , je me suis dis que la journée serait intéressante.

Plutôt que de vous faire une synthèse en plusieurs points sur cette preview, je vous propose un référentiel pour retrouver des informations.

Dernière mise à jour : 28/08/2015.

## Téléchargement {#téléchargement-2}

SharePoint Server 2016 IT Preview.iso

  * Taille de l’ISO 2.8Go
  * 180-day trial
  * product key: NQTMW-K63MQ-39G6H-B2CH9-FRDWJ
  * <http://www.microsoft.com/en-us/download/details.aspx?id=48712>

## Pré requis {#pré-requis-2}

  * Review [SharePoint Server 2013 system requirements][2]
  * [Un article complet de Bill Baer sur les nouveautés dans l’installation et le déploiement de SharePoint 2016][3]
  * Supported Operating System 
      * Windows Server 2012 R2
      * Windows Server Technical Preview 2

## Références {#références-2}

  * Ce qui est nouveaux ou améliorés : 
      * [New and improved features in SharePoint Server 2016 IT Preview][4]
  * Ce qui est vieux ou mort 
      * [What’s deprecated or removed from SharePoint Server 2016 IT Preview][5]
  * Ce qui pose problème 
      * [Known Issues in SharePoint Server 2016 IT Preview][6]

## Guides officiels {#guides-officiels-2}

  * [Cela fait quelque mois que la communauté SharePoint attend la nouvelle version on-Premise. Annoncée pour aout 2015, Microsoft a attendu le 24 aout rendre public la SharePoint 2016 preview. En m’endormant hier soir après avoir vu le billet [Announcing availability of SharePoint Server 2016 IT Preview and cloud hybrid search][1] sur le blog Office , je me suis dis que la journée serait intéressante.

Plutôt que de vous faire une synthèse en plusieurs points sur cette preview, je vous propose un référentiel pour retrouver des informations.

Dernière mise à jour : 28/08/2015.

## Téléchargement {#téléchargement-3}

SharePoint Server 2016 IT Preview.iso

  * Taille de l’ISO 2.8Go
  * 180-day trial
  * product key: NQTMW-K63MQ-39G6H-B2CH9-FRDWJ
  * <http://www.microsoft.com/en-us/download/details.aspx?id=48712>

## Pré requis {#pré-requis-3}

  * Review [SharePoint Server 2013 system requirements][2]
  * [Un article complet de Bill Baer sur les nouveautés dans l’installation et le déploiement de SharePoint 2016][3]
  * Supported Operating System 
      * Windows Server 2012 R2
      * Windows Server Technical Preview 2

## Références {#références-3}

  * Ce qui est nouveaux ou améliorés : 
      * [New and improved features in SharePoint Server 2016 IT Preview][4]
  * Ce qui est vieux ou mort 
      * [What’s deprecated or removed from SharePoint Server 2016 IT Preview][5]
  * Ce qui pose problème 
      * [Known Issues in SharePoint Server 2016 IT Preview][6]

## Guides officiels {#guides-officiels-3}

*](http://www.microsoft.com/en-us/download/details.aspx?id=48714) : This quick start guide provides an overview of SharePoint Server 2016 IT Preview requirements and installation to support Single Server Farm installations for evaluation purposes.

  * [SharePoint Server 2016 IT Preview Datasheet][7] : The SharePoint Server 2016 IT Preview Datasheet provides an overview of new capabilities and investments in SharePoint Server 2016 Preview.
    
    *](http://www.microsoft.com/en-us/download/details.aspx?id=48713) : The SharePoint Server 2016 IT Preview Reviewer Guide provides an overview of new capabilities in SharePoint Server 2016 IT Preview.

## Installation {#installation}

  * [Installation simple serveur pas à pas avec captures d’écran][8] (mstechtalk.com)
  * [SP2016’s virtual machines are available in Azure][9] (Christopher Woodill)

## Nouveautés {#nouveautés}

  * [What’s New in SharePoint 2016: Features Overview][10] (Share Gate)

## Configuration {#configuration}

  * [Configuration du SMTP (outgoing e-mail)][11] (Florent Cazenave)
  * [Fast Site Collection][12] (Michael Nokhamzon)
  * [Streaming vidéos][13] (Florent Cazenave)

 [1]: https://blogs.office.com/2015/08/24/announcing-availability-of-sharepoint-server-2016-it-preview-and-cloud-hybrid-search/#.VdtPf4ZTomw.twitter
 [2]: https://technet.microsoft.com/library/cc262485%28office.15%29.aspx
 [3]: http://blogs.technet.com/b/wbaer/archive/2015/05/12/what-s-new-in-sharepoint-server-2016-installation-and-deployment.aspx
 [4]: https://technet.microsoft.com/en-us/library/mt346121%28v=office.16%29.aspx
 [5]: https://technet.microsoft.com/EN-US/library/mt346112%28v=office.16%29.aspx
 [6]: https://technet.microsoft.com/EN-US/library/mt346109%28v=office.16%29.aspx
 [7]: http://www.microsoft.com/en-us/download/details.aspx?id=48715
 [8]: http://mstechtalk.com/sharepoint-server-2016-installation-step-by-step/
 [9]: http://www.microsofttrends.com/2015/08/28/sharepoint-2016-vms-now-available-in-azure/
 [10]: http://en.share-gate.com/blog/whats-new-in-sharepoint-2016
 [11]: https://kazoumoulox.wordpress.com/2015/08/27/sp-2016-smtp-outgoing-e-mail/
 [12]: https://mickey75019.wordpress.com/2015/08/27/sharepoint-2016-fast-site-collection/
 [13]: https://kazoumoulox.wordpress.com/2015/08/27/sp-2016-video/