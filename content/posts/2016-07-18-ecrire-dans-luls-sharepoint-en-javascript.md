---
title: Ecrire dans l’ULS SharePoint en JavaScript
author: Ghislain

date: 2016-07-18T00:00:00+00:00
url: /2016/07/18/ecrire-dans-luls-sharepoint-en-javascript/
categories:
  - JavaScript
  - SharePoint

---
De nos jours, écrire dans les logs ULS de SharePoint depuis du code serveur ne pose aucun soucis. Les solutions sont éprouvées donc tout va bien. Par contre pour avoir des traces de ce qui se passe côté code client, c’est moins usité. Voyons comment écrire dans l’ULS SharePoint en JavaScript.

SharePoint met a disposition un webservice _/\_vti\_bin/diagnostics.asmx_ qui expose une méthode pour écrire dans l’ULS
  
avec la catégorie **SharePoint Foundation > Unified Logging Service** et le niveau **Verbose**.

SharePoint possède plusieurs bibliothèques JavaScript dont init.js qui contient la méthode ULSOnError(msg, url, line). D’ailleurs pour en profiter, vous devrez vous assurer que ULS est activé sinon rien n’apparaitra dans l’ULS.

{{< highlight js >}}
ULS.enable = true;
ULSOnError("Hello trace", document.location.href, 0); 
{{< / highlight >}}

Si vous voulez coder un mécanisme de log personnalisé avec le webservice _/\_vti\_bin/diagnostics.asmx_, c’est possible. Vous en apprendrez plus avec l’article :
  
[MSDN &#8211; Writing to the SharePoint Unified Logging Service from JavaScript][1]

Voila qui devrait vous aider.

 [1]: https://msdn.microsoft.com/en-us/library/office/hh803115(v=office.14).aspx