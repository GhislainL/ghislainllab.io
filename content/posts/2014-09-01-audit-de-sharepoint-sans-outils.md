---
title: Audit de SharePoint sans outils
author: Ghislain

date: 2014-09-01T10:47:26+00:00
url: /2014/09/01/audit-de-sharepoint-sans-outils/
categories:
  - PowerShell
  - SharePoint
tags:
  - PowerShell
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013
  - Visio
  - Write-Progress

---
Comment faire un audit d’un environnement SharePoint quand on ne dispose pas d’outil ?

On peut trouver sur codeplex des outils comme SharePoint Farm Poster ou Inventory Tool qui proposent des bonnes idées. Mais pour être plus adaptée aux besoins des environnements, il faudra utiliser PowerShell, Visio, Excel et de l’huile de coude.

Benjamin Niaulin a écrit un article permettant de créer un inventaire de SharePoint. Le script PowerShell permet de créer un fichier CSV contenant les données de l’environnement SharePoint. Puis on utilise l’assistant organigramme de Visio pour créer une hiérarchie à partie du CSV.

Comment ajouter du sens à cet organigramme pour aider à gérer l’environnement ?

Visio propose une fonctionnalité nommée Graphique de données (Data graphic en anglais) qui permet d’afficher des indicateurs en fonction des valeurs. Cela peut se traduire visuellement par l’ajout d’une vignette, d’une icône ou d’une couleur.

[<img class="alignnone size-full wp-image-226" src="/wp-content/uploads/2014/09/visio-extract.png?fit=326%2C168" alt="Visio-extract" srcset="/wp-content/uploads/2014/09/visio-extract.png?resize=300%2C155 300w, /wp-content/uploads/2014/09/visio-extract.png?w=326 326w" sizes="(max-width: 326px) 100vw, 326px" data-recalc-dims="1" />][1]

Parmi les informations facilement accessibles :

  * La taille de la collection de site
  * Le pourcentage de remplissage d’une collection de site
  * RequestAccessEmail
  * La dernière utilisation
  * Le nombre de contribution au newsfeed
  * Le nombre de liste
  * Le nombre d’utilisateurs

Astuce 1 : Comment sélectionner des objects/propriétés qui n’existent pas avec Select-Object ?

{{< highlight powershell>}}Select-Object Title, Url, {$_.Lists.Count}, @{Name="Size (Recursive)"; Expression = {GetWebSizes -StartWeb $_.URL }}</pre>

Astuce 2 : Comment afficher facilement une barre de progression dans un script Powershell? En utilisant la commande [Write-Progress][2]

{{< highlight powershell>}}$webapp.Sites | Get-SPWeb -Limit All | Select-Object Title […]|
%{
    Write-Progress -Id 3 -Activity 'Processing web ' -Status $_.Url -PercentComplete (($idxWebs / $nbWebs) * 100)
    $idxWebs++
} | Export-Csv [...]</pre>

Phil Childs propose un script pour connaitre la taille d’un SPWeb de manière récursive qui se montre très utile.

On peut ensuite créer des indicateurs en fonction des valeurs. Les captures ci-dessous montrent un indicateur basé sur le ratio d’utilisation du quota et le rendu.

[<img class="alignnone size-full wp-image-227" src="/wp-content/uploads/2014/09/visio-ratio.png?fit=622%2C470" alt="Visio-Ratio" srcset="/wp-content/uploads/2014/09/visio-ratio.png?resize=300%2C227 300w, /wp-content/uploads/2014/09/visio-ratio.png?w=622 622w" sizes="(max-width: 622px) 100vw, 622px" data-recalc-dims="1" />][3]

Ensuite, il reste à trouver des indicateurs pertinents par rapport à l’utilisation de l’environnement que l’on peut faire ressortir dans Excel :

  * les créations de sites dans le temps
  * la répartition entre les différents modèles de site
  * la durée d’utilisation moyenne d’un site

[<img class="alignnone size-full wp-image-228" src="/wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?fit=515%2C277" alt="Excel-SiteCreation.xlsx" srcset="/wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?resize=300%2C161 300w, /wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?w=515 515w" sizes="(max-width: 515px) 100vw, 515px" data-recalc-dims="1" />][4]

Comme vous le voyez, il est possible de tirer des informations de SharePoint sans outils. Mais la méthode McGyver a ses limites et le besoin de s’outiller se fait rapidement sentir. Cela sera l’occasion d’un article futur.

**Références:**

[Benjamin Niaulin – how to build an inventory before SharePoint migration][5]

[Phil Childs – Check size of SharePoint 2010 sites][6]

[Comment faire un audit d’un environnement SharePoint quand on ne dispose pas d’outil ?

On peut trouver sur codeplex des outils comme SharePoint Farm Poster ou Inventory Tool qui proposent des bonnes idées. Mais pour être plus adaptée aux besoins des environnements, il faudra utiliser PowerShell, Visio, Excel et de l’huile de coude.

Benjamin Niaulin a écrit un article permettant de créer un inventaire de SharePoint. Le script PowerShell permet de créer un fichier CSV contenant les données de l’environnement SharePoint. Puis on utilise l’assistant organigramme de Visio pour créer une hiérarchie à partie du CSV.

Comment ajouter du sens à cet organigramme pour aider à gérer l’environnement ?

Visio propose une fonctionnalité nommée Graphique de données (Data graphic en anglais) qui permet d’afficher des indicateurs en fonction des valeurs. Cela peut se traduire visuellement par l’ajout d’une vignette, d’une icône ou d’une couleur.

[<img class="alignnone size-full wp-image-226" src="/wp-content/uploads/2014/09/visio-extract.png?fit=326%2C168" alt="Visio-extract" srcset="/wp-content/uploads/2014/09/visio-extract.png?resize=300%2C155 300w, /wp-content/uploads/2014/09/visio-extract.png?w=326 326w" sizes="(max-width: 326px) 100vw, 326px" data-recalc-dims="1" />][1]

Parmi les informations facilement accessibles :

  * La taille de la collection de site
  * Le pourcentage de remplissage d’une collection de site
  * RequestAccessEmail
  * La dernière utilisation
  * Le nombre de contribution au newsfeed
  * Le nombre de liste
  * Le nombre d’utilisateurs

Astuce 1 : Comment sélectionner des objects/propriétés qui n’existent pas avec Select-Object ?

{{< highlight powershell>}}Select-Object Title, Url, {$_.Lists.Count}, @{Name="Size (Recursive)"; Expression = {GetWebSizes -StartWeb $_.URL }}</pre>

Astuce 2 : Comment afficher facilement une barre de progression dans un script Powershell? En utilisant la commande [Write-Progress][2]

{{< highlight powershell>}}$webapp.Sites | Get-SPWeb -Limit All | Select-Object Title […]|
%{
    Write-Progress -Id 3 -Activity 'Processing web ' -Status $_.Url -PercentComplete (($idxWebs / $nbWebs) * 100)
    $idxWebs++
} | Export-Csv [...]</pre>

Phil Childs propose un script pour connaitre la taille d’un SPWeb de manière récursive qui se montre très utile.

On peut ensuite créer des indicateurs en fonction des valeurs. Les captures ci-dessous montrent un indicateur basé sur le ratio d’utilisation du quota et le rendu.

[<img class="alignnone size-full wp-image-227" src="/wp-content/uploads/2014/09/visio-ratio.png?fit=622%2C470" alt="Visio-Ratio" srcset="/wp-content/uploads/2014/09/visio-ratio.png?resize=300%2C227 300w, /wp-content/uploads/2014/09/visio-ratio.png?w=622 622w" sizes="(max-width: 622px) 100vw, 622px" data-recalc-dims="1" />][3]

Ensuite, il reste à trouver des indicateurs pertinents par rapport à l’utilisation de l’environnement que l’on peut faire ressortir dans Excel :

  * les créations de sites dans le temps
  * la répartition entre les différents modèles de site
  * la durée d’utilisation moyenne d’un site

[<img class="alignnone size-full wp-image-228" src="/wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?fit=515%2C277" alt="Excel-SiteCreation.xlsx" srcset="/wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?resize=300%2C161 300w, /wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?w=515 515w" sizes="(max-width: 515px) 100vw, 515px" data-recalc-dims="1" />][4]

Comme vous le voyez, il est possible de tirer des informations de SharePoint sans outils. Mais la méthode McGyver a ses limites et le besoin de s’outiller se fait rapidement sentir. Cela sera l’occasion d’un article futur.

**Références:**

[Benjamin Niaulin – how to build an inventory before SharePoint migration][5]

[Phil Childs – Check size of SharePoint 2010 sites][6]

](http://spposter.codeplex.com/)

[Comment faire un audit d’un environnement SharePoint quand on ne dispose pas d’outil ?

On peut trouver sur codeplex des outils comme SharePoint Farm Poster ou Inventory Tool qui proposent des bonnes idées. Mais pour être plus adaptée aux besoins des environnements, il faudra utiliser PowerShell, Visio, Excel et de l’huile de coude.

Benjamin Niaulin a écrit un article permettant de créer un inventaire de SharePoint. Le script PowerShell permet de créer un fichier CSV contenant les données de l’environnement SharePoint. Puis on utilise l’assistant organigramme de Visio pour créer une hiérarchie à partie du CSV.

Comment ajouter du sens à cet organigramme pour aider à gérer l’environnement ?

Visio propose une fonctionnalité nommée Graphique de données (Data graphic en anglais) qui permet d’afficher des indicateurs en fonction des valeurs. Cela peut se traduire visuellement par l’ajout d’une vignette, d’une icône ou d’une couleur.

[<img class="alignnone size-full wp-image-226" src="/wp-content/uploads/2014/09/visio-extract.png?fit=326%2C168" alt="Visio-extract" srcset="/wp-content/uploads/2014/09/visio-extract.png?resize=300%2C155 300w, /wp-content/uploads/2014/09/visio-extract.png?w=326 326w" sizes="(max-width: 326px) 100vw, 326px" data-recalc-dims="1" />][1]

Parmi les informations facilement accessibles :

  * La taille de la collection de site
  * Le pourcentage de remplissage d’une collection de site
  * RequestAccessEmail
  * La dernière utilisation
  * Le nombre de contribution au newsfeed
  * Le nombre de liste
  * Le nombre d’utilisateurs

Astuce 1 : Comment sélectionner des objects/propriétés qui n’existent pas avec Select-Object ?

{{< highlight powershell>}}Select-Object Title, Url, {$_.Lists.Count}, @{Name="Size (Recursive)"; Expression = {GetWebSizes -StartWeb $_.URL }}</pre>

Astuce 2 : Comment afficher facilement une barre de progression dans un script Powershell? En utilisant la commande [Write-Progress][2]

{{< highlight powershell>}}$webapp.Sites | Get-SPWeb -Limit All | Select-Object Title […]|
%{
    Write-Progress -Id 3 -Activity 'Processing web ' -Status $_.Url -PercentComplete (($idxWebs / $nbWebs) * 100)
    $idxWebs++
} | Export-Csv [...]</pre>

Phil Childs propose un script pour connaitre la taille d’un SPWeb de manière récursive qui se montre très utile.

On peut ensuite créer des indicateurs en fonction des valeurs. Les captures ci-dessous montrent un indicateur basé sur le ratio d’utilisation du quota et le rendu.

[<img class="alignnone size-full wp-image-227" src="/wp-content/uploads/2014/09/visio-ratio.png?fit=622%2C470" alt="Visio-Ratio" srcset="/wp-content/uploads/2014/09/visio-ratio.png?resize=300%2C227 300w, /wp-content/uploads/2014/09/visio-ratio.png?w=622 622w" sizes="(max-width: 622px) 100vw, 622px" data-recalc-dims="1" />][3]

Ensuite, il reste à trouver des indicateurs pertinents par rapport à l’utilisation de l’environnement que l’on peut faire ressortir dans Excel :

  * les créations de sites dans le temps
  * la répartition entre les différents modèles de site
  * la durée d’utilisation moyenne d’un site

[<img class="alignnone size-full wp-image-228" src="/wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?fit=515%2C277" alt="Excel-SiteCreation.xlsx" srcset="/wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?resize=300%2C161 300w, /wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?w=515 515w" sizes="(max-width: 515px) 100vw, 515px" data-recalc-dims="1" />][4]

Comme vous le voyez, il est possible de tirer des informations de SharePoint sans outils. Mais la méthode McGyver a ses limites et le besoin de s’outiller se fait rapidement sentir. Cela sera l’occasion d’un article futur.

**Références:**

[Benjamin Niaulin – how to build an inventory before SharePoint migration][5]

[Phil Childs – Check size of SharePoint 2010 sites][6]

[Comment faire un audit d’un environnement SharePoint quand on ne dispose pas d’outil ?

On peut trouver sur codeplex des outils comme SharePoint Farm Poster ou Inventory Tool qui proposent des bonnes idées. Mais pour être plus adaptée aux besoins des environnements, il faudra utiliser PowerShell, Visio, Excel et de l’huile de coude.

Benjamin Niaulin a écrit un article permettant de créer un inventaire de SharePoint. Le script PowerShell permet de créer un fichier CSV contenant les données de l’environnement SharePoint. Puis on utilise l’assistant organigramme de Visio pour créer une hiérarchie à partie du CSV.

Comment ajouter du sens à cet organigramme pour aider à gérer l’environnement ?

Visio propose une fonctionnalité nommée Graphique de données (Data graphic en anglais) qui permet d’afficher des indicateurs en fonction des valeurs. Cela peut se traduire visuellement par l’ajout d’une vignette, d’une icône ou d’une couleur.

[<img class="alignnone size-full wp-image-226" src="/wp-content/uploads/2014/09/visio-extract.png?fit=326%2C168" alt="Visio-extract" srcset="/wp-content/uploads/2014/09/visio-extract.png?resize=300%2C155 300w, /wp-content/uploads/2014/09/visio-extract.png?w=326 326w" sizes="(max-width: 326px) 100vw, 326px" data-recalc-dims="1" />][1]

Parmi les informations facilement accessibles :

  * La taille de la collection de site
  * Le pourcentage de remplissage d’une collection de site
  * RequestAccessEmail
  * La dernière utilisation
  * Le nombre de contribution au newsfeed
  * Le nombre de liste
  * Le nombre d’utilisateurs

Astuce 1 : Comment sélectionner des objects/propriétés qui n’existent pas avec Select-Object ?

{{< highlight powershell>}}Select-Object Title, Url, {$_.Lists.Count}, @{Name="Size (Recursive)"; Expression = {GetWebSizes -StartWeb $_.URL }}</pre>

Astuce 2 : Comment afficher facilement une barre de progression dans un script Powershell? En utilisant la commande [Write-Progress][2]

{{< highlight powershell>}}$webapp.Sites | Get-SPWeb -Limit All | Select-Object Title […]|
%{
    Write-Progress -Id 3 -Activity 'Processing web ' -Status $_.Url -PercentComplete (($idxWebs / $nbWebs) * 100)
    $idxWebs++
} | Export-Csv [...]</pre>

Phil Childs propose un script pour connaitre la taille d’un SPWeb de manière récursive qui se montre très utile.

On peut ensuite créer des indicateurs en fonction des valeurs. Les captures ci-dessous montrent un indicateur basé sur le ratio d’utilisation du quota et le rendu.

[<img class="alignnone size-full wp-image-227" src="/wp-content/uploads/2014/09/visio-ratio.png?fit=622%2C470" alt="Visio-Ratio" srcset="/wp-content/uploads/2014/09/visio-ratio.png?resize=300%2C227 300w, /wp-content/uploads/2014/09/visio-ratio.png?w=622 622w" sizes="(max-width: 622px) 100vw, 622px" data-recalc-dims="1" />][3]

Ensuite, il reste à trouver des indicateurs pertinents par rapport à l’utilisation de l’environnement que l’on peut faire ressortir dans Excel :

  * les créations de sites dans le temps
  * la répartition entre les différents modèles de site
  * la durée d’utilisation moyenne d’un site

[<img class="alignnone size-full wp-image-228" src="/wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?fit=515%2C277" alt="Excel-SiteCreation.xlsx" srcset="/wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?resize=300%2C161 300w, /wp-content/uploads/2014/09/excel-sitecreation-xlsx.png?w=515 515w" sizes="(max-width: 515px) 100vw, 515px" data-recalc-dims="1" />][4]

Comme vous le voyez, il est possible de tirer des informations de SharePoint sans outils. Mais la méthode McGyver a ses limites et le besoin de s’outiller se fait rapidement sentir. Cela sera l’occasion d’un article futur.

**Références:**

[Benjamin Niaulin – how to build an inventory before SharePoint migration][5]

[Phil Childs – Check size of SharePoint 2010 sites][6]

](http://spposter.codeplex.com/)

](https://sharepointinventory.codeplex.com/)

 [1]: /wp-content/uploads/2014/09/visio-extract.png
 [2]: http://technet.microsoft.com/en-us/library/hh849902.aspx
 [3]: /wp-content/uploads/2014/09/visio-ratio.png
 [4]: /wp-content/uploads/2014/09/excel-sitecreation-xlsx.png
 [5]: http://en.share-gate.com/blog/how-to-build-an-inventory-before-sharepoint-migration
 [6]: http://get-spscripts.com/2010/08/check-size-of-sharepoint-2010-sites.html