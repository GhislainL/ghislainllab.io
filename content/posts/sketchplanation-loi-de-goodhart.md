---
title: Loi de Goodhart
author: Ghislain
date: 2017-11-11
url: /un-dessin/loi-de-goodhart/
categories:
  - Un dessin
tags:
  - sketchplanations
draft : false

---

![Loi de Goodhart](/un-dessin/sketchplanations-goodhart-law.jpg)

**Loi de Goodhart: lorsqu'une mesure devient un objectif, elle cesse d'être une bonne mesure.**

In other words, if you pick a measure to assess people’s performance, then we find a way to game it. I like the illustration of a nail factory that sets number of nails produced as their measure of productivity and the workers figure out they can get tons of tiny nails. And, if they switch it to the weight of nails made, they get a few giant heavy nails. Or perhaps the story of measuring fitness by steps from a pedometer only to find they get put on the dog.

Some strategies for helping this are to try and find better, harder to game measures, assess with multiple measures, or to allow a little discretion. More detail in this nice little article.

I also liked an idea I read in Measure What Matters of pairing a quantity measure with a quality measure, for example, assessing both number of nails and customer satisfaction of the nails.

Traduction de la publication de [Sketchplanations](https://www.sketchplanations.com/post/167369765942/goodharts-law-when-a-measure-becomes-a-target)