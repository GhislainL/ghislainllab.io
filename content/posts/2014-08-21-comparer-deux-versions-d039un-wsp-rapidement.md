---
title: "Comparer deux versions d'un WSP rapidement"
author: Ghislain

date: 2014-08-21T07:32:43+00:00
url: /2014/08/21/comparer-deux-versions-d039un-wsp-rapidement/
categories:
  - SharePoint
tags:
  - Outils
  - SharePoint
  - sharepoint 2007
  - SharePoint 2010
  - SharePoint 2013

---
WSPCompare est un outil qui permet de comparer 2 WSP.

Je l’ai croisé pour la première fois en 2009, autant vous dire qu’il n’est pas récent. Mais quand on a deux versions d’un WSP sans plus d’informations, il met en avant les différences entre les fichiers contenus dans les-dits WSP. Quand on double clique sur une ligne, l’outil passe dans l’onglet Details pour comparer les 2 fichiers comme dans un WinDiff simplifié.

[<img class="alignnone size-full wp-image-208" src="/wp-content/uploads/2014/08/wspcompare-1.png?fit=749%2C632" alt="WspCompare-1" srcset="/wp-content/uploads/2014/08/wspcompare-1.png?resize=300%2C253 300w, /wp-content/uploads/2014/08/wspcompare-1.png?w=749 749w" sizes="(max-width: 749px) 100vw, 749px" data-recalc-dims="1" />][1]

**Important :** bien penser à télécharger la version correspond à son environnement x86 ou x64.

**Référence :**

[WSPCompare sur codeplex][2]

 [1]: /wp-content/uploads/2014/08/wspcompare-1.png
 [2]: http://wspcompare.codeplex.com/