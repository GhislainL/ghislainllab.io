---
title: Log tournant et syslog avec Serilog
author: Ghislain

date: 2018-09-10T06:45:36+00:00
url: /2018/09/10/serilog-rolling-file-dotnet-core/
featuredImage: /wp-content/uploads/2018/08/DotNetCoreSerilog.png
categories:
  - DotNet Core
tags:
  - DotNet Core
  - Git
  - Logging
  - Serilog
  - Splunk
  - syslog
  - TFS

---
Lors de mon [article initial sur Serilog][1], j&#8217;avais utilisé un puits SQL Server pour l&#8217;exemple. Le problème est que si la connexion avec SQL est tombée, il n&#8217;y a plus de log. Or un log est sensé fonctionner dans tous les cas et le plus simplement possible. D&#8217;où l&#8217;intérêt d&#8217;avoir un mécanisme de fichier texte de log tournant.

{{< highlight json >}}"Serilog": {
    "Using": [ "Serilog.Sinks.File", "Serilog.Sinks.Syslog" ],
    "MinimumLevel": "Warning",
    "WriteTo": [
      {
        "Name": "File",
        "Args": {
         "path": "F:\\\\AwesomeAppLogs\\log.txt",
          "rollingInterval": "Day"
        }
      },
      {
        "Name": "UdpSyslog",
        "Args": {
          "host": "serveur-syslog.domain"
        }
      }
    ]
  }
{{< /highlight >}}

Pour syslog, j&#8217;ai testé 2 puits et sélectionné Serilog.Sinks.SyslogMessages. Par contre dans le fichier de configuration, il faut noter que :

  * using : Serilog.Sinks.Syslog
  * dans le parametrage, Name peut prendre 3 valeurs : UdpSyslog, TcpSyslog et LocalSyslog

Dans le cas où vous utilisez Splunk comme serveur syslog, le résultat ressemble à la capture ci-dessous:

<figure id="attachment_365" aria-describedby="caption-attachment-365" style="width: 900px" class="wp-caption alignnone"><img class="wp-image-365 size-large" src="https://ghislain-lerda.fr/wp-content/uploads/2018/09/Serilog-Syslog-Splunk-1024x550.png" alt="Syslog dans Splunk" width="900" height="483" srcset="https://ghislain-lerda.fr/wp-content/uploads/2018/09/Serilog-Syslog-Splunk-1024x550.png 1024w, https://ghislain-lerda.fr/wp-content/uploads/2018/09/Serilog-Syslog-Splunk-300x161.png 300w, https://ghislain-lerda.fr/wp-content/uploads/2018/09/Serilog-Syslog-Splunk-768x413.png 768w, https://ghislain-lerda.fr/wp-content/uploads/2018/09/Serilog-Syslog-Splunk.png 1070w" sizes="(max-width: 900px) 100vw, 900px" /><figcaption id="caption-attachment-365" class="wp-caption-text">Syslog dans Splunk</figcaption></figure>

Si vous avez configuré l&#8217;emplacement de vos logs de dev dans la solution, il faut penser à les ignorer dans le commit.

  * Pour TFS, il faut créer un fichier .tfignore et le compléter.
  * Pour Git, il s&#8217;agit du .gitignore.

D&#8217;ailleurs le contenu des deux fichiers est similaire.

{{< highlight plain >}}# See https://docs.microsoft.com/fr-fr/vsts/repos/tfvc/add-files-server?view=vsts#tfignore 
# for more about ignoring files.

# logs
\logs
{{< /highlight >}}

**Références :**
  
<https://github.com/serilog/serilog-sinks-file>
  
<https://github.com/IonxSolutions/serilog-sinks-syslog>
  
<https://docs.microsoft.com/fr-fr/vsts/repos/tfvc/add-files-server?view=vsts#tfignore>

 [1]: http://ghislain-lerda.fr/2018/08/20/serilog-dotnet-core-2-1/