---
title: Centraliser vos taches personnelles
author: Ghislain

date: 2015-03-09T08:48:32+00:00
url: /2015/03/09/centraliser-vos-taches-personnelles/
categories:
  - SharePoint
tags:
  - Exchange
  - Project
  - SharePoint
  - SharePoint 2013
  - Taches
  - Work Management Service

---
SharePoint se place comme élément central dans le travail collaboratif avec les intégrations multiples avec Outlook, Lync, Project. Si comme moi vous êtes sur plusieurs projets à la fois, des taches vous sont assignées dans tous les sites. Votre travail serait grandement facilité si vous pouviez suivre toutes vos taches en une seule fois. Et bien c’est possible grace au Work Management Service.

Le WMS est un service SharePoint qui permet d’agréger toutes les taches assignées à un utilisateur dans son site personnel. Ainsi l’utilisateur n’a plus besoin de courir après l’information, elle vient à lui.

WMS est construit pour agréger les taches et todo éparpillés dans SharePoint, Exchange et Project Server.

[<img class="wp-image-535 size-full" src="/wp-content/uploads/2015/02/wmsapres.png?fit=621%2C508" alt="WmsApres" srcset="/wp-content/uploads/2015/02/wmsapres.png?resize=300%2C245 300w, /wp-content/uploads/2015/02/wmsapres.png?w=621 621w" sizes="(max-width: 621px) 100vw, 621px" data-recalc-dims="1" />][1]  
_WMS activé avec SharePoint 2013, Exchange2013 et Project Server_

Vous vous rendrez compte sur la capture d’écran ci-dessous que :

  * Les taches sont agrégées;
  * Les taches peuvent être regroupées par site puis liste;
  * Vous pouvez interagir directement avec celle-ci
  * La frise chronologique permet de visualiser les taches.

[<img class="alignnone size-full wp-image-531" src="/wp-content/uploads/2015/02/wmstasks.png?fit=890%2C393" alt="WmsTasks" srcset="/wp-content/uploads/2015/02/wmstasks.png?resize=300%2C132 300w, /wp-content/uploads/2015/02/wmstasks.png?resize=1024%2C452 1024w, /wp-content/uploads/2015/02/wmstasks.png?w=1675 1675w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][2]

## Comment ça marche ? {#comment-ça-marche-}

Le service Work Management Service nécessite les services suivants :

  * Search Service Application
  * User Profile Service Application

Contraintes :

  * Le compte de service utilisé pour WMS doit avoir le contrôle total sur le User Profile Service Application.
  * Le compte de service utilisé pour WMS doit être le même que pour le pool d’application utilisé par les Web Applications

  1. Pour créer le WMS, s’il n’existe pas déjà, vous avez deux possibilités :

  * Créer par powershell : [New-SPWorkManagementServiceApplication][3]
  * Créer par la central administration : Application Management > Manage Service Applications puis News > Work Management Service Application

  1. Vérifier que le service Work Management Service est démarré. (Les services Search et UPS aussi)

[<img class="alignnone size-full wp-image-528" src="/wp-content/uploads/2015/02/wmsservicesstarted.png?fit=644%2C179" alt="WmsServicesStarted" srcset="/wp-content/uploads/2015/02/wmsservicesstarted.png?resize=300%2C83 300w, /wp-content/uploads/2015/02/wmsservicesstarted.png?w=644 644w" sizes="(max-width: 644px) 100vw, 644px" data-recalc-dims="1" />][4]

  1. Vérifier que le crawl de la recherche est configuré (continuous de préférence).

  2. Prendre un petit chocolat et ça devrait être fonctionnel.

 

<span style="color: #ff0000;"><strong>/! Attention si vous n’avez pas Exchange 2013 /!</strong></span>

Un bouton dans le ruban de la page permet de synchroniser ses taches avec Outlook.

[<img class="alignnone size-full wp-image-530" src="/wp-content/uploads/2015/02/wmssyncexchange2013-list1.png?fit=276%2C248" alt="WmsSyncExchange2013-List1" data-recalc-dims="1" />][5]

Mais cette fonctionnalité n’est disponible que si vous avez Exchange 2013.

Donc pour éviter l’incompréhension et la frustration des utilisateurs, désactiver la feature **Synchronisation des taches Exchange au niveau de la batterie** ce qui grisera le bouton dans le ruban.

[<img class="alignnone size-full wp-image-529" src="/wp-content/uploads/2015/02/wmssyncexchange2013.png?fit=702%2C108" alt="WmsSyncExchange2013" srcset="/wp-content/uploads/2015/02/wmssyncexchange2013.png?resize=300%2C46 300w, /wp-content/uploads/2015/02/wmssyncexchange2013.png?w=702 702w" sizes="(max-width: 702px) 100vw, 702px" data-recalc-dims="1" />][6]

## Références : {#références-}

<http://blogs.technet.com/b/praveenh/archive/2013/08/22/work-management-service-application.aspx>

Pour l’IT : [SharePoint 2013: Work Management Service Application][7]

Pour les développeurs : [Work Management service in SharePoint 2013: A Short Overview for Developers][8]

 [1]: /wp-content/uploads/2015/02/wmsapres.png
 [2]: /wp-content/uploads/2015/02/wmstasks.png
 [3]: https://technet.microsoft.com/en-us/library/fp161254.aspx
 [4]: /wp-content/uploads/2015/02/wmsservicesstarted.png
 [5]: /wp-content/uploads/2015/02/wmssyncexchange2013-list1.png
 [6]: /wp-content/uploads/2015/02/wmssyncexchange2013.png
 [7]: http://social.technet.microsoft.com/wiki/contents/articles/12525.sharepoint-2013-work-management-service-application.aspx
 [8]: http://blogs.msdn.com/b/mvpawardprogram/archive/2014/03/31/work-management-service-in-sharepoint-2013-a-short-overview-for-developers.aspx