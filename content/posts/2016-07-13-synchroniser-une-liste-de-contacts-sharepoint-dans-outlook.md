---
title: Synchroniser une liste de contacts SharePoint dans Outlook
author: Ghislain

date: 2016-07-13T00:00:00+00:00
url: /2016/07/13/synchroniser-une-liste-de-contacts-sharepoint-dans-outlook/
categories:
  - PowerShell
  - SharePoint
tags:
  - Outlook
  - PowerShell
  - SharePoint

---
Les listes de contacts SharePoint permettent à une équipe de gérer et partager un ensemble de contact de manière autonome. Vous utilisez la liste comme référentiel et vos collègues peuvent utiliser ces contacts directement dans Outlook. Les droits assignés à l’utilisateur dans SharePoint déterminent les opérations qu’il peut effectuer sur le contact dans Outlook. Comment synchroniser une liste de contacts SharePoint dans Outlook, méthode manuelle et automatisée.

## Méthode manuelle {#méthode-manuelle}

  1. Se rendre dans la liste de contact.
  2. Dans le ruban, cliquer sur l’onglet List puis le bouton Connect to Outlook.
  
  ![SharePoint - Connect to Outlook](/wp-content/uploads/2016/07/01.PNG)
  3. Accepter la connexion SharePoint &#8211; Outlook demandée par la pop-up d’Outlook.
  
  ![Internet Explorer - pop-up](/wp-content/uploads/2016/07/02.PNG)
  
  ![Outlook - pop-up](/wp-content/uploads/2016/07/03.PNG)

Maintenant vous pouvez voir la liste de contact dans les listes SharePoint synchronisées avec Outlook. De plus, vous verez les contacts s’afficher dans vos contacts sous le regroupement **Other contacts**.

## Méthode automatisée {#méthode-automatisée}

  1. Se rendre dans la liste de contact.
  2. Dans le ruban, cliquer sur l’onglet List puis le bouton Connect to Outlook.
  3. Accepter la connexion SharePoint &#8211; Outlook demandée par la pop-up d’Outlook.
  4. Dans Outlook, effectuer un clic droit sur la liste créée et choisir **share List name**.
  5. Envoyer ce mail à vous-même then check out the message headers.
  6. Chercher **x-sharing-config-url** dans l’entête du mail. C’est cette valeur qui sera utilisée dans le script.
  
  ![Entête du mail - x-sharing-config-url](/wp-content/uploads/2016/07/04.PNG)
  7. Sur les postes sélectionnés, il reste à utiliser l’une des méthodes suivantes selon vos affinités :

  * Dans un navigateur ou un mail insérer l’URL x-sharing-config-url, l’utilisateur clique sur le lien puis accepte la connexion.

  * Batch en tant qu’administrateur
  
{{< highlight bash >}}outlook.exe /share "stssync://sts/?ver=1.1&type=contacts&cmd=add-folder&base-url=https://[...]/sites/exchangecontacts&list-url=/Lists/contacts/&guid=%7bdbd723d5-e2dd-403c-baf5-56081f688ce6%7d&site-name=Exchange%20Contacts&list-name=contacts"
{{< / highlight >}}

  * PowerShell en tant d’administrateur
  
{{< highlight powershell >}}
. "C:\Program Files\Microsoft Office\Office14\OUTLOOK.EXE" /share 'stssync://sts/?ver=1.1&type=contacts&cmd=add-folder&base-url=https://[...]/sites/exchangecontacts&list-url=/Lists/contacts/&guid=%7bdbd723d5-e2dd-403c-baf5-56081f688ce6%7d&site-name=Exchange%20Contacts&list-name=contacts'
{{< / highlight >}}
Mais que fait _outlook.exe /share_ ? Permet de spécifier une URL partagée à laquelle connecter Outlook. Par exemple, utiliser stssync://URL pour connecter une liste SharePoint à Outlook.

Plutot pratique, n’est ce pas ?
