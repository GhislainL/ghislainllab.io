---
title: 'Créer les classes C# à partir d’un schéma XSD'
author: Ghislain

date: 2017-12-28T00:00:00+00:00
url: /2017/12/28/creer-les-classes-c-a-partir-dun-schema-xsd/
categories:
  - dotNet
tags:
  - Oracle
  - XML
  - XSD

---
Un collègue de boulot se retrouve avec un problème : il doit mettre en place un reporting au XML validé auprès d’un schéma XSD. Il pensait que cela lui prendrait longtemps et serait compliqué. Il fut véritablement surpris d’avoir un prototype de solution aussi facilement et rapidement.

![POCO C#][1]

## Créer un POCO à partir du XSD {#créer-un-poco-à-partir-du-xsd}

Dans le Developer Command Prompt for Visual Studio, taper la ligne suivante :

{{< highlight plain >}}
  Xsd myschema.xsd /classes 
{{< /highlight >}}

Cela crée un fichier myschema.cs qui contient des objets qui respectent le XSD

Pour les options : xsd /? ou [XML Schema Definition Tool (Xsd.exe)][2]

## Intégrer la classe dans Visual Studio {#intégrer-la-classe-dans-visual-studio}

Après avoir créer un nouveau projet dans Visual Studio et y avoir ajouter la classe générée précédemment, il faut ajouter de quoi sérialiser l’objet.
  
Comme il s’agit d’une classe partielle et que le XSD peut être amené à évoluer, je vous conseille de créer une autre classe partielle qui contiendra les méthodes associées à cet objet.
  
Voici deux méthodes utiles, l’une pour exporter au format texte pour le debug, l’autre pour exporter dans un fichier.

{{< highlight csharp >}}public string ToXML()
{
    var stringwriter = new StringWriter();
    var serializer = new XmlSerializer(this.GetType());
    serializer.Serialize(stringwriter, this);
    return stringwriter.ToString();
}

public void ToFile(string filePath)
{
    System.Xml.Serialization.XmlSerializer writer = new XmlSerializer(this.GetType());

    System.IO.FileStream file = System.IO.File.Create(filePath);

    writer.Serialize(file, this);
    file.Close();
}
{{< /highlight >}}

## Connexion aux données {#connexion-aux-données}

Je passe rapidement sur cette partie car ce n’est pas le sujet.

Ma source de données est une base Oracle. Autant dire que je m’attendais au pire. **Oracle.ManagedDataAccess** m’a énormémenent facilité la vie.
  
Ce post de stackoverflow fait gagner un temps pécieux : [Connecting to Oracle using Oracle.ManagedDataAccess][3].

[Oracle.ManagedDataAccess sur NuGet][4]

## Valider le XML généré {#valider-le-xml-généré}

Il est toujours bon de vérifier un résultat et en l’ocurrence valider le XML généré en fonction du schéma XSD.

{{< highlight csharp >}}//Constants.Path.Xsd  = System.AppDomain.CurrentDomain.BaseDirectory + @"Resources\CCSS.xsd";

XmlSchemaSet schemas = new XmlSchemaSet();
schemas.Add(AppSettings["XsdNamespace"], Constants.Path.Xsd);
XDocument xdoc = XDocument.Load(Constants.Path.TmpXmlExport);
bool errors = false;

StringBuilder sb = new StringBuilder();
xdoc.Validate(schemas, (o, ex) =>
{
    sb.AppendLine(string.Format("{0}", ex.Message));
    errors = true;
});

sb.Insert(0,string.Format("custOrdDoc {0}", errors ? "did not validate" : "validated") + Environment.NewLine, 1);
Txt1.Text = sb.ToString();
{{< /highlight >}}

Alors facile!

**Références**

[XML Schema Definition Tool (Xsd.exe)][2]

[Connecting to Oracle using Oracle.ManagedDataAccess][3].

[Oracle.ManagedDataAccess sur NuGet][4]

 [1]: /wp-content/uploads/2017/POCO-csharp.png
 [2]: https://msdn.microsoft.com/en-us/library/x6c1kb0s%28v=VS.100%29.aspx?f=255&MSPPError=-2147217396
 [3]: https://stackoverflow.com/questions/31564288/connecting-to-oracle-using-oracle-manageddataaccess
 [4]: https://www.nuget.org/packages/Oracle.ManagedDataAccess/