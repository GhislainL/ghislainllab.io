---
title: Entity Framework Core et les données spatiales
author: Ghislain

date: 2018-07-31T19:36:17+00:00
url: /2018/07/31/entity-framework-core-spatiales/
featuredImage: /wp-content/uploads/2018/07/dotnetcore-sq.png
categories:
  - Databases
  - dotNet
tags:
  - DotNet Core
  - Entity Framework Core
  - SQL Server

---
La gestion des données spatiales avec Entity Framework Core n&#8217;existe pas actuellement . Quand on essaie de créer un modèle d&#8217;une table contenant une colonne spatiale &#8211; que le type soit geography ou geometry -, un avertissement apparait :

{{< highlight powershell >}}PM> Scaffold-DbContext "Data Source=PC\SQLEXPRESS01;Initial Catalog=EventLocatorCore;Trusted_Connection=True;Integrated Security=True;TrustServerCertificate=True" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models -Force
Could not find type mapping for column 'dbo.Event.Location' with data type 'geography'. Skipping column.
{{< /highlight >}}

La mois d&#8217;aout débute et si l&#8217;on se fie aux roadmap d&#8217;ASP.Net Core 2.2 (1) et d&#8217;EF Core 2.2 (2), la version Preview 1 devrait arriver. L&#8217;une des nouveautés pour EF est les balbutiements du support des données spatiales avec Entity Framework Core. Cela permettra de rattraper le retard par rapport à PostgreSQL dont le provider les supporte depuis EF Core 2.1.

> **Preview of spatial support for SQL Server**
> 
> We are also focusing on adding spatial support as an extension to the SQL Server provider for EF Core.
  
> Note that the PostgreSQL provider (Npgsql) already added support in EF Core 2.1. Beside implementing this functionality for SQL Server, we are working closely with the developers of Npgsql to try to achieve a consistent and hopefully improved experience across providers.
> 
> Note that we aren&#8217;t committing to having a final version of the spatial support for the end of the current calendar year. But we are actively working on this now, and as we make progress, we should be able to provide a more precise estimate.

Le point est ouvert depuis 2014 (3), autant dire que cette fonctionnalité est attendue &#8211; à juste titre &#8211; par la communauté et sera regardée de près.

Liens :
  
1: [ASP.Net Core 2.2 roadmap][1]
  
2 : [EF Core 2.2 Roadmap][2]
  
3 : [Support Spatial Data Types on SQL Server and SQLite][3]

 [1]: https://github.com/aspnet/Announcements/issues/307
 [2]: https://github.com/aspnet/Announcements/issues/308
 [3]: https://github.com/aspnet/EntityFrameworkCore/issues/1100