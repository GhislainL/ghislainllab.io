---
title: Acronyme et sigle
author: Ghislain
date: 2018-06-26
url: /un-dessin/acronyme-sigle/
categories:
  - Un dessin
tags:
  - sketchplanations

---

![Acronyme et sigle](/un-dessin/sketchplanations-acronyme-sigle.jpg)

Nous savons tous que l'on crée un sigle en prénant - généralement - les lettres initiales de plusieurs mots. Par exemple, FFJDA pour **F**édération **F**rançaise de **J**udo et **D**isciplines **A**ssociées ou EDF pour **E**lectricité  **D**e **F**rance. 

Quand un sigle peut être prononcé comme un mot, on parle d'acronyme. Laser pour **l**ight **a**mplification by **s**timulated **e**mission of **r**adiation ou OTAN pour **O**rganisation du **T**raité de l'**A**tlantique **N**ord.

Traduction de la publication de [Sketchplanations](https://www.sketchplanations.com/post/176134687131/acronyms-and-initialisms-we-all-know-that-you)