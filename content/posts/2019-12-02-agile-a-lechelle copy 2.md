---
title: Agile à l'échelle selon Cédric Moulard
author: Ghislain
date: 2019-12-02
url: /2019/12/02/agile-a-lechelle/
categories:
  - Agile
tags:
  - Scrum
  - Kanban
  - SaFe

---
Un scrum master expérimenté - [Cédric Moulard (LinkedIn)](https://www.linkedin.com/in/cedricmoulard/) - aborde la méthodologie projet en informatique et dénonce l'agilité à l'échelle.

Partons du manifeste agile qui est défini comme suit (1) :
>Nous découvrons de meilleures approches du développement logiciel en le pratiquant et en aidant les autres à le pratiquer. Ce travail nous a amené à accorder de l'importance :
>* aux individus et leurs interactions plutôt qu'aux processus et aux outils ;
>* à un logiciel fonctionnel plutôt qu’à une documentation exhaustive ;
>* à la collaboration avec les clients plutôt qu'à la négociation contractuelle ;
>* à l’adaptation au changement plutôt qu'à l'exécution d’un plan.
Cela signifie que, bien qu'il y ait de la valeur dans les éléments situés à droite, notre préférence se porte sur les éléments qui se trouvent à gauche.

Il faut éviter le cherry picking (2). L'agile est constituer de ces 4 valeurs, sinon ce n'est plus de l'agile.
Il faut aussi éviter de remplacer "plutôt que" par "à la place". Faire un logiciel sans documentation en se disant qu'il est auto documenté est stupide.

Le principe d'agile repose sur itératif **et** incrémental.   
On fait des itérations et chaque itération apporte de la valeur supplémentaire par rapport à l'incrément précédent. Un incrément n'est pas là pour remettre en cause toute la valeur déjà produite dans les incréments précédents. L'agilité ne permet pas de partir sans carte et de changer de direction à chaque intération.

Classiquement un projet est défini sur 3 axes : périmètre (fonctionnalités), coût et délai.   
Théoriquement en cycle en V, le périmètre est fixe alors que les couts et délai sont variables.   
Changement de paradigme avec l'approche agile qui fixe le coût et délai pour un périmètre variable.   
Par conséquent quand on commence un projet agile avec la phrase "vous vous organisez comme vous voulez tant que j'ai tout à la fin", vous savez qu'il y a un soucis.

Il faut donc un long travail transformation pour intégrer ce changement de paradigme. Changer le nom des postes sans changer le fonctionnement sera un échec (renommer un chef de projet en scrum master).    
Les méthodologies de mise à l'échelle de l'agile placent l'équipe agile est en bout de chaine. Elle subit les décisions techniques et fonctionnelles prises par les strates supérieures De plus le PO n'a aucune marge de manoeuvre. 

**Conclusion**   
Si vous n'êtes pas prêt à faire des sprints, faites du **kanban** (3)   
Si vous n'êtes pas prêt à prioriser, faites du **cycle en V**.   
Si vous n'avez pas de vision globale du projet, bonne chance.   

**Plan du talk**
* Petit historique : l’évolution des pratiques sur les 20 dernières années
* Pourquoi ce bordel agile ?
* Finalement l’agilité c’est quoi ?
* Est-ce qu’il faut revenir au cycle en V ?

{{< youtube ciAORrJYHc8 >}}

>En 2019, le monde de l’informatique n’a que l’agilité à la bouche. Ca groome à tous les coins de rue, ça s’excite en daily, ça colle des post-it et ça empile les serious game en rétro. Bien sûr, ça gère la transformation numérique (désolé, même au second degré, je ne peux pas employer “digitale”) et au final plus personne ne comprend rien.

>Des chefs de projet subitement propulsés ScrumMaster qui ne savent pas ce qu’ils doivent faire. Des PO qui découvrent la priorisation. Des développeurs qui doivent composer avec des specs floues et changeantes sans vision globale du projet. Pour compléter le tableau, la subite apparition de coachs agile à peine sortis de l’école qui n’ont jamais vu un projet de leur vie et qui viennent expliquer des concepts qu’ils ne maîtrisent qu’à moitié.

>En 2019 le monde de l’informatique rêve secrètement de cycles en V, de stabilité, de spécifications générales et de dossier d’architecture.

>En 2019 le monde de l’informatique est au bord du burn-out agile.

**Références :**  
1. [Manifeste agile](https://agilemanifesto.org/)
2. [Cherry picking - Wikipedia](https://fr.wikipedia.org/wiki/Cherry_picking)
3. [Kanban - Wikipedia](https://fr.wikipedia.org/wiki/Kanban_(d%C3%A9veloppement))
