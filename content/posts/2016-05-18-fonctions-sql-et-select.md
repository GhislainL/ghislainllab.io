---
title: 'Fonctions SQL et select *'
author: Ghislain

date: 2016-05-18T00:00:00+00:00
url: /2016/05/18/fonctions-sql-et-select/
categories:
  - Databases
tags:
  - SQL Server

---
Un comportement qu’il faut connaitre quand on utilise des fonctions SQL avec SQL Server. Les fonctions sont optimisées par le moteur et changer le schéma d’une table peut avoir des comportements surprenant.

Créer une table People et y ajouter quelques amis

![Structure SQL - création][1]

Créer un fonction simple

{{< highlight sql>}}

SET ANSI_NULLS ON 
GO 
SET QUOTED_IDENTIFIER ON 
GO 
CREATE FUNCTION MyFunction(    ) 
RETURNS TABLE  
AS 
RETURN  
( 
        SELECT * from People 
) 
GO{{< / highlight  >}}

Appeler la fonction et obtenir le résultat escompté<figure class="highlight"> 

{{< highlight sql>}}
select * from dbo.MyFunction() {{< / highlight  >}}

![SQL - résultat][2]

Rajouter une colonne Superhero sur la table People et enrichir avec les informations

![Structure SQL - modification][3]

Rappeler la fonction et être surpris

![Structure SQL - init][2]

Solution :

Mettre à jour la fonction même si aucune modification effective n’est réalisée<figure class="highlight"> 

{{< highlight sql>}}
USE [gla] 
GO 
SET ANSI_NULLS ON 
GO 
SET QUOTED_IDENTIFIER ON 
GO 
ALTER FUNCTION [dbo].[MyFunction](    ) 
RETURNS TABLE  
AS 
RETURN  
( 
    SELECT * from People 
) {{< / highlight  >}}

Rappeler la fonction

![SQL - résultat][4]

Conclusion : S’il était encore besoin de le démontrer, le * c’est mal.

L’exemple servant d’illustration est simple mais enrobez cela avec de l’Entity Framework et dans un contexte où vous n’avez pas la main sur les évolutions SQL, cela devient bien plus sympathique.

 [1]: /wp-content/uploads/2016/05/sql-structure0.PNG
 [2]: /wp-content/uploads/2016/05/sql1.PNG
 [3]: /wp-content/uploads/2016/05/sql-structure1.PNG
 [4]: /wp-content/uploads/2016/05/sql2.PNG