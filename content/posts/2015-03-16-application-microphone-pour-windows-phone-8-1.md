---
title: Application microphone pour Windows Phone 8.1
author: Ghislain

date: 2015-03-16T08:24:13+00:00
url: /2015/03/16/application-microphone-pour-windows-phone-8-1/
categories:
  - Non classé
tags:
  - microphone
  - Windows Phone
  - windows phone 8

---
Si comme moi, vous avez occasionnellement besoin d’utiliser votre téléphone comme microphone, vous trouverez ci-dessous 5 applications sur Windows Phone 8.1 que j’ai pu tester. Elles sont gratuites et permettent d’enregistrer sur le téléphone ou dans OneDrive. Mais ce n’est pas pour autant qu’elles se valent.

Pour remettre en condition, j’ai enregistré les essais avec un [Lumia 925][1] en marchant dans une colline dans les Bouches du Rhône avec comme ami le [Mistral][2]. Autant dire que ça soufflait suffisamment pour tester les filtres des applications. Place aux avis.

[<img class="wp-image-553 size-full" src="/wp-content/uploads/2015/03/EnregistreurVocal.png?fit=126%2C126" alt="Enregistreur Vocal" data-recalc-dims="1" />][3]  
_Enregistreur Vocal_

Interface : 3/5

Réglage : aucun

Publicité : trop

Qualité sonore : 1/5

L’enregistrement était tellement mauvais que je ne vois pas l’intêret de le publier.

[<img class="wp-image-555 size-full" src="/wp-content/uploads/2015/03/FreeRecorder.png?fit=126%2C126" alt="Free Recorder" data-recalc-dims="1" />][4]  
_Free Recorder_

Interface : 4/5

Réglage : interface

Publicité : aucune

Qualité sonore : 3/5

[<img class="wp-image-554 size-full" src="/wp-content/uploads/2015/03/PerfectRecorder.png?fit=126%2C126" alt="Perfect Recorder" data-recalc-dims="1" />][5]  
_Perfect Recorder_

Interface : 5

Réglage : interface, échantillonnage

Publicité : aucune

Qualité sonore : 4/5

Format : wav

[<img class="wp-image-557 size-full" src="/wp-content/uploads/2015/03/TheSoundRecorder.png?fit=126%2C126" alt="The Sound Recorder" data-recalc-dims="1" />][6]  
_The Sound Recorder_

Interface : 4/5

Réglage : interface, echantillonage, skip silence

Publicité : oui

Qualité sonore : 4/5

Format: wav, m4a

Trop intrusif, il faut accorder trop d’autorisation (nom, age, sexe) pour la connexion à OneDrive

[<img class="wp-image-556 size-full" src="/wp-content/uploads/2015/03/FreeRecorderProPlus.png?fit=126%2C126" alt="Voice Recorder Pro Plus" data-recalc-dims="1" />][7]  
_Voice Recorder Pro Plus_

Interface : 4/5

Réglage : aucun

Publicité : oui

Qualité sonore : 4/5

 [1]: http://www.amazon.fr/gp/product/B00DAS0DSI/ref=as_li_tl?ie=UTF8&camp=1642&creative=19458&creativeASIN=B00DAS0DSI&linkCode=as2&tag=ghislainfo-21&linkId=DDTLMBKMU5EWQV4M
 [2]: http://fr.wikipedia.org/wiki/Mistral_%28vent%29 "Mistral"
 [3]: http://www.windowsphone.com/fr-fr/store/app/enregistreur-vocal/8dcfb4f0-7404-4016-8cdb-b46622ce756e
 [4]: http://www.windowsphone.com/fr-fr/store/app/free-recorder/d79e9518-3104-410c-b469-f68735e0c88c
 [5]: http://www.windowsphone.com/fr-fr/store/app/perfect-recorder/3afb6a29-986d-40be-8d08-e77b1d0ee6f1
 [6]: http://www.windowsphone.com/fr-fr/store/app/the-sound-recorder/819adab9-44dc-4fd6-a8a6-4522d0c7e9e3
 [7]: http://www.windowsphone.com/fr-fr/store/app/voice-recorder-pro/8049306f-d4d4-43ff-bf57-5e4b0f591279