---
title: Louis Derungs explique son mindset au TEDx
author: Ghislain

date: 2017-09-20T00:00:00+00:00
url: /2017/09/20/louis-derungs-explique-son-mindset-au-tedx/
categories:
  - Développement personnel
tags:
  - Louis Derungs
  - mindset
  - TED

---
Parfois vous vous sentez abattu par ce que la vie vous a réservé. Parfois vous vous sentez écrasé par le poids sur vos épaules. Imaginez ce qu’on ressent quand on perd ses deux bras à l’âge de 19 ans. Louis Derungs l’a vécu, en sort plus fort et partage son état d’esprit avec nous.

![Louis Derungs][1]

## Histoire personnelle de Louis Derungs {#histoire-personnelle-de-louis-derungs}

Il part du principe que la réussite personnelle ne correspond pas à la quantité d’argent que l’on a, mais à la capacité de faire ce que l’on aime vraiment.

A 19 ans, il encaisse une décharge de 15000 volts qui le plonge 6 semaines dans le coma. Au réveil, il est amputé des deux bras, brulé à 50%, ses reins et son foie ne fonctionnent plus. Les médecins lui prédisent une paralysie.

Il s’est battu et maintenant il est thérapeute, coach, chef de trois entreprises, auteur et conférencier. Quel état d’esprit lui a permis de se réaliser ainsi ?

## Etre maitre de son destin {#etre-maitre-de-son-destin}

Pour changer le monde, il faut changer sa vision de celui-ci.

Cela commence par comprendre que nous sommes maitre de notre destin et ainsi passer de spectateur à acteur. Nous devenons ainsi responsables de la construction de notre avenir, de notre réussite et de nos échecs. Il est surtout nécessaire de tirer des apprentissages de ses échecs.

« On a le pouvoir de façonner son avenir. »

## Etre capable de changer de point de vue. {#etre-capable-de-changer-de-point-de-vue}

Prenons comme exemple le mécanisme de mémoire. La mémoire est constituée des souvenirs, un choix d’éléments que nous choisissons de mémoriser. De plus, nous donnons plus de poids à l’un ou l’autre.

Notre mémoire est donc composée d’éléments fragmentaires pondérés qui constituent notre vision du monde. Nous pouvons donc modeler notre vision du monde en fonction de l’objectif que nous poursuivons.

## Sortir de la zone de confort {#sortir-de-la-zone-de-confort}

La zone de confort est reposante, sécurisante, relaxante et surtout encroûtante. Elle est en perpétuel mouvement, soit elle grandit soit elle rapetisse jusqu’à disparaitre. Quand nous en sortons, elle s’agrandit. Mais si nous restons dans notre zone de confort, ce que nous avons devient la norme et l’émerveillement disparait. Il est donc nécessaire de sortir de cette zone de confort. Et quand on le fait, il ne faut pas s’imposer de limite.

La limite est liée à la notion de peur. La peur marque l’importance de la chose à faire. Surmonter sa peur ne demande qu’une fraction de seconde.

Il n’y a personne d’autre que nous pour casser nous limites, pour transformer la peur en moteur.

{{< youtube 1RVp9xXWFMs >}}

Devenez maître de votre énergie brute | Louis Derungs | TEDxChambery

J’espère que cette vidéo vous a autant motivés que moi. D’ailleurs Louis Derungs a écrit **15 000 volts: Une méthode pour s’accomplir, un récit de résilience** qui contient plus de détails sur son état d’esprit et sa vie.

 [1]: /wp-content/uploads/2017/louis_derungs.jpg