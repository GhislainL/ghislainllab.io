---
title: Créer un formulaire InfoPath avec PowerShell
author: Ghislain
date: 2016-02-03T10:25:35+00:00
url: /2016/02/03/creer-un-formulaire-infopath-avec-powershell/
categories:
  - PowerShell
  - SharePoint
tags:
  - InfoPath
  - PowerShell
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013

---
La reprise des données est souvent problématique quand on remplace un process papier par un formulaire InfoPath dans SharePoint.

La saisie manuelle étant hors de question, nous allons voir comment créer un formulaire InfoPath avec PowerShell pour reprendre des données.

Pour les formulaires contenant peu de données, autant promouvoir les champs et renseigner les propriétés. Pour les formulaires plus conséquent, la méthode suivante est plus adaptée.

## 1. Créer un fichier de données contenant les données à reprendre (CSV) {#1-créer-un-fichier-de-données-contenant-les-données-à-reprendre-csv}

{{< highlight plain >}}tokDateDemande;tokIdentifiantACreer
2016-01-18;pouet
2016-01-18;tagada
2016-01-18;unicorn{{< / highlight >}}


## 2. Créer un modèle InfoPath {#2-créer-un-modèle-infopath}

### 2.1 Télécharger un exemplaire du formulaire de la bibliothèque (fichier XML) {#21-télécharger-un-exemplaire-du-formulaire-de-la-bibliothèque-fichier-xml}

### 2.2 Identifier chaque champs à renseigner et les remplacer par un mot clé unique {#22-identifier-chaque-champs-à-renseigner-et-les-remplacer-par-un-mot-clé-unique}

### 2.3 Sauvegarder le formulaire modifié en tant que modèle {#23-sauvegarder-le-formulaire-modifié-en-tant-que-modèle}

/!\ un mot cle ne doit pas contenir un autre mot clé, sous peine de ne pas fonctionner. exemple : tokGeneral1 et tokGeneral1Justif.

{{< highlight xml >}}<my:mesChamps [...] >
<my:entete></my:entete>
<my:Identifiant_A_Créer>tokIdentifiantACreer</my:Identifiant_A_Créer>
<my:sectionGLA>
<my:Date_Demande>tokDateDemande</my:Date_Demande>
</my:sectionGLA>
</my:mesChamps>
{{< /highlight >}}

## 3. Exécuter le script PowerShell qui iterera sur chaque ligne du fichier CVS, remplacera les mot clés par les valeurs et sauvegardera chaque nouveau formulaire. {#3-exécuter-le-script-powershell-qui-iterera-sur-chaque-ligne-du-fichier-cvs-remplacera-les-mot-clés-par-les-valeurs-et-sauvegardera-chaque-nouveau-formulaire}

/!\ Mes données proviennent d’un fichier Excel convertit au format CSV. Pour éviter les soucis avec les caractères accentués, j’ai du convertir le fichier au format unicode.

{{< highlight powershell>}}
#Créer des formulaires InfoPath à partir des données
$dirpck = Split-Path $MyInvocation.MyCommand.Path

$InfoPathTemplate = "$dirpck\template.xml"

# Modify the template XML file and insert the values from the CSV file
# Convert to unicode
Get-Content -Path "$dirpck\data.csv" | Out-File -FilePath "$dirpck\data-unicode.csv" -Encoding Unicode

$Employees = Import-CSV -Delimiter ';' -Path "$dirpck\data-unicode.csv"
$Employees | foreach {
   "New entry"
   "Getting values ..."
   $tokDateDemande = $_.tokDateDemande
   $tokIdentifiantACreer = $_.tokIdentifiantACreer

   "Building output path ..."
   $fileName = "Derog.$tokDateDemande.xml";
   $filePath = "$dirpck\Forms\$fileName";

   "Replacing token in template ..."
   (gc $InfoPathTemplate) |
      foreach-object { $_ -replace 'tokDateDemande', $tokDateDemande `
         -replace 'tokIdentifiantACreer', $tokIdentifiantACreer } | Set-Content $filePath -Encoding UTF8

   "Done : $tokIdentifiantACreer ( $filePath )"
}{{< / highlight >}}

4. Déposer les formulaires créés dans la bibliothèque de formulaire.

Méthode testée et validée avec une reprise de plusieurs centaines d’éléments.