---
title: Solutionisme technologique et cause racine
author: Ghislain
draft: true
date: 2020-06-15
url: /2020/06/15/solutionisme-technologique-cause-racine/
categories:
  - Méthode
tags:
  - solutionisme
  - problème

---

Travailler en informatique peut nous pousser vers le solutionisme technologique (1) ou plus exactement numérique. Cette manière d'envisager la recherche de solution est problématique sur au moins deux aspects. Le premier aspect consiste à appliquer une solution sur un a priori, c'est à dire sans étudier le problème. Le second aspect découle du premier et s'interesse plus à l'augmentation de la complexité et la pérennité de la solution.  

## Solutionisme technologique

Le solutionisme technologique est une _notion développée par Evgeny Morozov pour expliquer comment chaque problème humain (politique, social, sociétal) est systématiquement transformé en question technique puis adressé par les acteurs du numérique privés ou les États avec des solutions numériques traitant les effets des problèmes sans jamais s’intéresser à leurs causes._ (1) 

Les illustrations ne manquent pas, alors prennons un cas technique et un cas numérique :
- les océans sont pleins de plastiques ? On va envoyer une bateau pour réduire la pollution plastique dans l’océan et lutter contre la pauvreté (3). On va aussi créer des tortues pour capturer le plastique dans les ports.
- effectuer des recherches dans plusieurs référentiels hétérogènes consomme trop de temps ? On va automatiser ta recherche avec un RPA (Robotic Automatisation Process).

## La résolution de problème

La résoluton de problème permet d'identifier puis mettre en oeuvre une ou plusieurs solutions à un problème. Pour être pérenne, il est conseillé de la faire dans le cadre de l'amélioration continue (PDCA/roue de Deming).

La résolution de problème un processus qui nécessite de la méthodologie, de la rigueur et du temps. Ce n'est peut-être pas trivial vu qu'il existe plein de méthodes connues dont :
- l'analyse de cause racine 
- cinq pourquoi
- arbre des causes
- diagramme de Pareto
- diagramme de causes et effets
- Creative Problem Solving

Pour reprendre les deux cas d'illustration, il est évident qu'on agit sur une conséquence et que cela ne resoudra rien: 
- récupérer les plastiques qui sont dans l'océan n'empechera pas la prolifération du plastique.
- automatiser un process manuel sans l'étudier ne fera qu'amplifier le problème et le déportera dans le temps quand la charge ne sera plus soutenable.

Mais pourquoi autant d'engouement pour ces solutions fausses ou partielles ? Parce que la promesse technologique fait rêver tout en étant immédiate et indolore. 
S'il fallait interdire l'usage du plastique et le remplacer, cela serait douloureux pour beaucoup d'industrie (coût, existence) et personnes (habitudes).
S'il fallait étudier les référentiels et le processus de requétages, cela demande plus de temps et d'efforts pour analyser la procédure et éventuellement la changer.  

## Complexité et pérennité 

Chaque semaine, nous voyons fleurir des solutions géniales mais qui déportent le problème, comme avec l'économie du pétrole vers l'économie des terres rares (5).
Sur le second aspect, je vous conseille Philippe Bihouix avec les low tech (2).

__Références__ 

1. [Evgeny Morozov - solutionisme technologique](https://fr.wikipedia.org/wiki/Evgeny_Morozov)
2. [Philippe Bihouix - Basses technologies : une solution écologique ?](https://www.youtube.com/watch?v=jZCWDz3tfYE)
3. [Plastic Odyssey, le bateau qui nettoie les océans partira de Marseille en 2021](https://madeinmarseille.net/68742-plastic-odyssey-le-bateau-qui-nettoie-les-oceans-partira-de-marseille-en-2021/)
4. [Des étudiants français ont inventé un robot-tortue qui chasse le plastique et le mange](https://www.futura-sciences.com/tech/actualites/robotique-etudiants-francais-ont-invente-robot-tortue-chasse-plastique-mange-81420/)
5. [La guerre des métaux rares: La face cachée de la transition énergétique et numérique de Guillaume Pitron](https://www.babelio.com/livres/Pitron-La-guerre-des-metaux-rares/1019955)

pouet