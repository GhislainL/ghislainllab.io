---
title: Planifier l’exécution d’un package SSIS
author: Ghislain

date: 2016-01-13T08:03:52+00:00
url: /2016/01/13/planifier-lexecution-dun-package-ssis/
categories:
  - Databases
  - PowerShell
tags:
  - ETL
  - PowerShell
  - SQL Server
  - SSIS

---
Maintenant nous savons comment créer et paramétrer un package SSIS, il reste donc à planifier l’exécution d’un package SSIS.

Trois possibilités :

  * Créer un job SQL Server Agent manuellement
  * Créer un job SQL Server Agent avec PowerShell
  * Créer une tache planifiée qui exécute un script PowerShell exécutant le package SSIS.

## Créer un job dans SQL Server Agent manuellement {#créer-un-job-dans-sql-server-agent-manuellement}

Dans **SQL Server Management Studio**, déplier **SQL Server Agent**, clic droit sur **Jobs** et sélectionner **New Job …**

<a href="/wp-content/uploads/2016/01/SSIS-job-1.png" rel="attachment wp-att-1017"><img class="wp-image-1017 size-full" src="/wp-content/uploads/2016/01/SSIS-job-1.png?fit=281%2C249" alt="SSIS-job-1" data-recalc-dims="1" /></a>  
_Nouveau Job SQL Server Agent_

Dans la fenêtre New job, vous vous attarderez principalement sur les onglets

  * General pour spécifier le nom du job (SSIS – OdataSP) et le compte d’exécution
  * Steps pour sélectionner le job à exécuter et ses paramètres
  * Schedules pour configurer les exécutions
  * Notifications pour prévenir si quelque chose échoue.

L’onglet Steps permet d’ajouter des étapes. C’est là que vous renseigner que vous voulez exécuter un package SSIS depuis un serveur SQL spécifique et …

<a href="/wp-content/uploads/2016/01/SSIS-job-2.png" rel="attachment wp-att-1018"><img class="wp-image-1018 size-full" src="/wp-content/uploads/2016/01/SSIS-job-2.png?fit=704%2C633" alt="SSIS-job-2" srcset="/wp-content/uploads/2016/01/SSIS-job-2.png?resize=300%2C270 300w, /wp-content/uploads/2016/01/SSIS-job-2.png?w=704 704w" sizes="(max-width: 704px) 100vw, 704px" data-recalc-dims="1" /></a>  
_New Job Step – onglet Package_

… que vous spécifiez quel environnement utiliser pour l’exécution.

<a href="/wp-content/uploads/2016/01/SSIS-job-3.png" rel="attachment wp-att-1019"><img class="wp-image-1019 size-full" src="/wp-content/uploads/2016/01/SSIS-job-3.png?fit=703%2C633" alt="SSIS-job-3" srcset="/wp-content/uploads/2016/01/SSIS-job-3.png?resize=300%2C270 300w, /wp-content/uploads/2016/01/SSIS-job-3.png?w=703 703w" sizes="(max-width: 703px) 100vw, 703px" data-recalc-dims="1" /></a>  
_New Job Step – onglet Configuration_

Pour tester le job nouvellement créer, clic droit sur le job puis **Start Job as Step…**

<a href="/wp-content/uploads/2016/01/SSIS-job-4.png" rel="attachment wp-att-1020"><img class="wp-image-1020 size-full" src="/wp-content/uploads/2016/01/SSIS-job-4.png?fit=227%2C82" alt="SSIS-job-4" data-recalc-dims="1" /></a>  
_Job créé_

## Créer un job SQL Server Agent avec PowerShell {#créer-un-job-sql-server-agent-avec-powershell}

Une fois n’est pas coutume, je vous fournis un lien vers un article d’un blog MSDN. L’article contient les versions C# et PowerShell du code pour créer un job SQL Server Agent qui exécutera un package SSIS : [SSIS 2012 Automation – Part 3: Create SQL Agent Job which will execute SSIS package from SSIS catalog, Assign Environment Reference and Assign Schedule to Job][1]

## Créer une tache planifiée qui exécute un script PowerShell exécutant le package SSIS {#créer-une-tache-planifiée-qui-exécute-un-script-powershell-exécutant-le-package-ssis}

Le script suivant permet d’exécuter un package SSIS en lui spécifiant un environnement.

{{< highlight powershell>}}# Variables
$ProjectName = "IntegrationServicesProject1"
$FolderName = "GLA_PS"
$EnvironmentName = "Env_PS"
$CatalogName = "SSISDB"
$PackageName = "ODataSP.dtsx"

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Management.IntegrationServices") | Out-Null;

$ISNamespace = "Microsoft.SqlServer.Management.IntegrationServices"

"Connecting to server ...";
$sqlConnectionString = "Data Source=vw-x-dxx-07;Initial Catalog=master;Integrated Security=SSPI"
$sqlConnection = New-Object System.Data.SqlClient.SqlConnection $sqlConnectionString
$integrationServices = New-Object $ISNamespace".IntegrationServices" $sqlConnection

"Getting catalog $catalogName ..."
$ssisCatalog = $integrationServices.Catalogs[$CatalogName]

"Getting Folder $FolderName ..."
$folder = $ssisCatalog.Folders[$FolderName]

"Getting project $ProjectName ..."
$project = $folder.Projects[$ProjectName]

"Getting package $PackageName ..."
$package = $project.Packages[$PackageName]

"Getting environment $EnvironmentName ..."
$environmentReference = $project.References.Item($EnvironmentName, $folder.Name)
$environmentReference.Refresh()

"Executing package $ProjectName\$PackageName with environment $EnvironmentName ..."
$package.Execute($false, $environmentReference)

"Done"{{< /highlight >}}

A ce stade le package est déployé et son exécution est programmée. Il reste à superviser les exécutions pour s’assurer que tout se passe comme prévu.

 [1]: http://blogs.msdn.com/b/dataaccesstechnologies/archive/2013/10/08/ssis-2012-automation-part-3-create-sql-agent-job-which-will-execute-ssis-package-from-ssis-catalog-assign-environment-reference-and-assign-schedule-to-job.aspx