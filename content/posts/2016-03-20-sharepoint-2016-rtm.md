---
title: SharePoint 2016 RTM
author: Ghislain

date: 2016-03-20T00:00:00+00:00
url: /2016/03/20/sharepoint-2016-rtm/
categories:
  - SharePoint
tags:
  - sharepoint 2016

---
Une vague a déferlé sur la communauté SharePoint le 14 mars 2016 avec sortie de la RTM de SharePoint 2016.
  
Je vous propose une sélection de ressources pour briller dans les soirées mondaines après ces premiers jours d’effervescence.

[SharePoint 2016 RTM and the Future of SharePoint event][1]

[SharePoint Server 2016 Overview Guide &#8211; PDF 21 pages][2]

[La SP2016 FAQ de Vlad Catrinescu][3]

**Technique**

[What’s new in SharePoint 2016 for IT Professionals Webinar &#8211; SlideShare][4]

[What’s new for admins in SharePoint Server 2016 &#8211; YouTube][5]

[Le point d’entrée SharePoint Server 2016 sur la TechNet][6]

[What’s deprecated or removed from SharePoint Server 2016][7]

[Unattended Configuration for SharePoint Server 2016][8]

**Les futurs cours et certifications**

  * IT 
      * [Planning and Administering SharePoint 2016][9]
      * [Advanced Technologies of SharePoint 2016][10]
  * Développement 
      * Switch vers 0365 apps : [Additional Details on Upcoming Exam Retirements][11]

**Download**

[Téléchargement de SharePoint 2016 RTM trial][12]

**Twitter**

[Twitter @SharePoint][13]

[Twitter Bill Baer][14]

[Twitter #SP2016][15]

Enjoy.

 [1]: https://blogs.office.com/2016/03/14/sharepoint-2016-rtm-and-the-future-of-sharepoint-event/
 [2]: https://www.microsoft.com/en-us/download/details.aspx?id=48713
 [3]: https://absolute-sharepoint.com/2016/02/sharepoint-2016-frequently-asked-questions.html
 [4]: http://fr.slideshare.net/VladCatrinescu/whats-new-in-sharepoint-2016-for-it-professionals-webinar-with-crowcanyon
 [5]: https://www.youtube.com/watch?v=6EEvzJy1_-w
 [6]: https://technet.microsoft.com/en-us/library/cc303422%28v=office.16%29.aspx
 [7]: https://technet.microsoft.com/en-us/library/mt346112%28v=office.16%29.aspx
 [8]: http://thesharepointfarm.com/2016/03/unattended-configuration-for-sharepoint-server-2016/
 [9]: https://www.microsoft.com/en-sg/learning/course.aspx?cid=20339-1
 [10]: https://www.microsoft.com/en-sg/learning/course.aspx?cid=20339-2
 [11]: https://borntolearn.mslearn.net/b/weblog/archive/2016/02/18/additional-details-on-upcoming-exam-retirements
 [12]: https://www.microsoft.com/en-us/download/details.aspx?id=51493
 [13]: https://twitter.com/SharePoint
 [14]: https://twitter.com/williambaer
 [15]: https://twitter.com/hashtag/SP2016?src=hash