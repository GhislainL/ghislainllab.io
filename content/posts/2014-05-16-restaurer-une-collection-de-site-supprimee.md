---
title: Restaurer une collection de site supprimée
author: Ghislain

date: 2014-05-16T13:50:07+00:00
url: /2014/05/16/restaurer-une-collection-de-site-supprimee/
categories:
  - PowerShell
  - SharePoint
tags:
  - Get-SPDeletedSite
  - PowerShell
  - Restore-SPDeletedSite
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013

---
La suppression d’une collection de site résulte généralement d’une mûre réflexion. Mais il peut aussi s’agir d’une erreur et là – évidemment – c’est la panique.

Avec le Service Pack 1 de SharePoint 2010 sont apparues des commandes PowerShell pour rattraper ce genre d’erreur. Heureusement je n’ai eu besoin de m’en servir qu’avec la version 2013.

  * Get-SPDeletedSite : lister les collections de site supprimées
  * Remove-SPDeletedSite : supprimer définitivement une collection de site
  * Restore-SPDeletedSite : restaurer une collection de site supprimée

<table>
  <tr>
    <td>
      <strong>Attention : </strong>j’ai vu plusieurs fois la ligne suivante: _Get-SPDeletedSite -Identity /url/relative
    </td>
    
    <td>
      Restore-SPDeletedSite_. Cela fonctionne uniquement si la ferme n’héberge qu’une seule web application. Et oui <em>/url/relative</em> peut désigner autant <em>http://webapp1/url/relative</em> que <em>http://webapp2/url/relative</em><strong>**.</strong>**
    </td>
  </tr>
</table>

Le script ci-dessous permet de restaurer un site supprimé à partir de son URL absolue.

{{< highlight powershell>}}# URL of the deleted site you want to restore
$url = "http://london/sites/site2";

# convert URL to URI object
$uri = [System.Uri]$url

# get everything before the query
$cleanURL = $uri.Scheme + "://" + $uri.GetComponents([System.UriComponents]::HostAndPort, [System.UriFormat]::UriEscaped);

# retrieve deleted site
$deletedSite = Get-SPDeletedSite -webapplication $cleanURL | Where-Object Url -eq $url
if($deletedSite -ne $null)
{
   try
   {
      Restore-SPDeletedSite -Identity  $deletedSite.SiteId -Confirm:$false
      write-host "site collection $url restored successfully." -ForegroundColor Green
   }
   catch
   {
      write-host "error during restoration of site collection $url : $error[0].Exception.Message" -ForegroundColor Red
   }
}
else
{
   write-host "site collection $url was not find" -ForegroundColor Red
}
{{< /highlight >}}

Références:

[Get-SPDeletedSite][1]

[Remove-SPDeletedSite][2]

[Restore-SPDeletedSite][3]

 [1]: http://technet.microsoft.com/en-us/library/hh286316(v=office.15).aspx
 [2]: http://technet.microsoft.com/en-us/library/hh286317(v=office.15).aspx
 [3]: http://technet.microsoft.com/en-us/library/hh286319(v=office.15).aspx