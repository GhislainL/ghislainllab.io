---
title: Ecrire facilement du CAML pour SharePoint
author: Ghislain

date: 2014-08-12T07:51:41+00:00
url: /2014/08/12/ecrire-facilement-du-caml-pour-sharepoint/
categories:
  - SharePoint
tags:
  - CAML
  - camlex
  - CAMLjs
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013

---
Quiconque travaille avec SharePoint a dû ou doit travailler avec le CAML. Et soyons franc, ce langage est moche et verbeux.

Il existe des beaux outils comme CAML Designer ou feu U2U CAML Builder qui proposent une interface graphique facilitant sa génération. Mais question maintenabilité, cela demande toujours un effort certains.

Voici 2 librairies – l’une en JS et l’autre en C# –  qui permettent de générer du CAML directement dans le code avec une syntaxe accessible et maintenable.

# SharePoint JavaScript CAML Builder (CAMLjs) {#sharepoint-javascript-caml-builder-camljs}

Installation par le Package Manager

{{< highlight powershell>}}Install-Package CamlJs{{< /highlight  >}}

L’IntelliSense et l’aide contextuelle sont disponibles

[<img class="alignnone size-full wp-image-196" src="/wp-content/uploads/2014/08/camljs.png?fit=393%2C115" alt="camljs" srcset="/wp-content/uploads/2014/08/camljs.png?resize=300%2C88 300w, /wp-content/uploads/2014/08/camljs.png?w=393 393w" sizes="(max-width: 393px) 100vw, 393px" data-recalc-dims="1" />][1]

# Camlex.net {#camlexnet}

Camlex propose 2 bibliothèques la première pour C# « classique » …

{{< highlight powershell>}}Install-Package Camlex.NET.dll{{< /highlight  >}}

… et la seconde pour le client object model de SharePoint.

{{< highlight powershell>}}Install-Package Camlex.Client.dll{{< /highlight  >}}

Comme pour CAMLjs, l’IntelliSense et l’aide contextuelle sont disponibles

[<img class="alignnone size-full wp-image-197" src="/wp-content/uploads/2014/08/camlex.png?fit=532%2C105" alt="camlex" srcset="/wp-content/uploads/2014/08/camlex.png?resize=300%2C59 300w, /wp-content/uploads/2014/08/camlex.png?w=532 532w" sizes="(max-width: 532px) 100vw, 532px" data-recalc-dims="1" />][2]

# Exemple {#exemple}

Maintenant un exemple simple pour donner un aperçu de la syntaxe et du gain apporté par ces deux bibliothèques;

{{< highlight xml >}}<Where>
  <And>
    <BeginsWith>
      <FieldRef Name="Title" />
      <Value Type="Text">Ghislain</Value>
    </BeginsWith>
    <Contains>
      <FieldRef Name="Project" />
      <Value Type="Text">SharePoint</Value>
    </Contains>
  </And>
</Where>
<OrderBy>
  <FieldRef Name="ExpirationDate"/>
</OrderBy>{{< /highlight  >}}

{{< highlight js >}}var camlBuilder = new CamlBuilder();
var caml = camlBuilder
    .Where().TextField("Title").BeginsWith("Ghislain")
    .And().TextField("Project").Contains("SharePoint")
    .OrderBy("ExpirationDate")
    .ToString();{{< /highlight  >}}

{{< highlight csharp >}}string caml = Camlex.Query()
    .Where(x => ((string)x["Title"]).StartsWith("Ghislain")
        && ((string)x["Project"]).Contains("SharePoint"))
    .OrderBy(x => x["ExpirationDate"])
    .ToString();{{< /highlight  >}}

J’avais évoqué CAMLjs dans l’article concernant [l’export PDF avec jsPDF][3]. Maintenant vous avez les outils pour vous faciliter la vie en évitant d’écrire du CAML directement.

**Références :**

[SharePoint JavaScript CAML Builder][4]

[Camlex][5]

 [1]: /wp-content/uploads/2014/08/camljs.png
 [2]: /wp-content/uploads/2014/08/camlex.png
 [3]: http://ghislainfo.wordpress.com/2014/07/21/jspdf-sp2013/ "Exporter des données avec jsPDF dans SharePoint 2013"
 [4]: //camljs.codeplex.com/
 [5]: http://camlex.codeplex.com