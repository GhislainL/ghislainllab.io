---
title: 'Configurer un package SSIS selon l&rsquo;environnement avec PowerShell'
author: Ghislain

date: 2016-01-04T07:57:59+00:00
url: /2016/01/04/configurer-un-package-ssis-selon-lrsquoenvironnement-avec-powershell/
categories:
  - Databases
  - PowerShell
tags:
  - ETL
  - PowerShell
  - SQL Server
  - SSIS

---
Dans l’article précédent, vous avez pu voir [comment configurer un package SSIS selon l’environnement][1]. Le résultat est indéniablement intéressant mais tout était fait manuellement. Il faudrait industrialiser ces opérations pour faciliter la vie des opérationnels. PowerShell notre sauveur. Nous allons donc configurer un package SSIS selon l’environnement avec PowerShell.

Le script de cet article permet de :

  * créer un dossier de projet SSIS;
  * déployer un package SSIS (.ispac) dans le dossier créé;
  * créer un environnement;
  * créer des variables dans l’environnement créé;
  * associer l’environnement au projet;
  * associer les variables de l’environnement à l’un des packages du projet.

{{< highlight powershell>}}
# Variables
$ProjectFilePath = "E:\SSIS\Packages\IntegrationServicesProject1.ispac"
$ProjectName = "IntegrationServicesProject1"
$FolderName = "GLA_PS"
$EnvironmentName = "Env_PS"
$CatalogName = "SSISDB"
$PackageName = "ODataSP.dtsx"

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Management.IntegrationServices") | Out-Null;

$ISNamespace = "Microsoft.SqlServer.Management.IntegrationServices"

"Connecting to server ..."
$sqlConnectionString = "Data Source=vw-x-dxx-07;Initial Catalog=master;Integrated Security=SSPI;"
$sqlConnection = New-Object System.Data.SqlClient.SqlConnection $sqlConnectionString
$integrationServices = New-Object $ISNamespace".IntegrationServices" $sqlConnection

"Getting catalog $catalogName ..."
$ssisCatalog = $integrationServices.Catalogs[$CatalogName]

"Creating Folder $FolderName ..."
$folder = New-Object $ISNamespace".CatalogFolder" ($ssisCatalog, $FolderName, "Folder description")
$folder.Create()

"Deploying project $ProjectName ..."
[byte[]] $projectFile = [System.IO.File]::ReadAllBytes($ProjectFilePath)
$folder.DeployProject($ProjectName, $projectFile)

"Creating environment $EnvironmentName..."
$environment = New-Object $ISNamespace".EnvironmentInfo" ($folder, $EnvironmentName, "Description")
$environment.Create()

"Adding server variables ..."
# Constructor args: variable name, type, default value, sensitivity, description
$ssisEnvironmentProd_FFCM_CS = "FlatFileConnectionManager_ConnectionString"
$environment.Variables.Add($ssisEnvironmentProd_FFCM_CS, [System.TypeCode]::String , "e:\export\out_prd_ps.csv", $False, "PS FFCM")
$ssisEnvironmentProd_ODS_Url = "ODataSource_Url"
$environment.Variables.Add($ssisEnvironmentProd_ODS_Url, [System.TypeCode]::String , "http://prd.contoso.com/News/_vti_bin/listdata.svc", $False, "PS ODS")
$environment.Alter()

"Adding environment reference to project ..."
$project = $folder.Projects[$ProjectName]
$project.References.Add($EnvironmentName, $folder.Name)
$project.Alter()

"Setting environment variable on package ..."
$ssisPackage = $project.Packages.Item($PackageName)
# /!\ aucune erreur si les references sont fausses /!\
$ssisPackage.Parameters[$ssisEnvironmentProd_FFCM_CS].Set("Referenced", $ssisEnvironmentProd_FFCM_CS)
$ssisPackage.Parameters[$ssisEnvironmentProd_ODS_Url].Set("Referenced", $ssisEnvironmentProd_ODS_Url)
$ssisPackage.Alter()
{{< / highlight >}}


Une fois, le script exécuté vous verrez apparaitre un nouvel environnement dans SSMS.

<a href="/wp-content/uploads/2015/12/SSIS-Param-8.png" rel="attachment wp-att-988"><img class="wp-image-988 size-full" src="/wp-content/uploads/2015/12/SSIS-Param-8.png?fit=264%2C130" alt="SSIS-Param-8" data-recalc-dims="1" /></a>  
_SSMS – Environnement et association après exécution du script PowerShell_

Lors de la prochaine exécution du projet ODataSP, il s’exécutera avec les variables de l’environnement Env_SP.

Fini le guide et les actions manuelles. De quoi améliorer votre collaboration avec les opérationnels.

 [1]: http://ghislain-lerda.com/2015/12/21/configurer-un-package-ssis-selon-l-environnement/