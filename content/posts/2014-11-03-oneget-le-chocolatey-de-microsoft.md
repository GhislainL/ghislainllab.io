---
title: OneGet, le Chocolatey de Microsoft
author: Ghislain

date: 2014-11-03T08:33:03+00:00
url: /2014/11/03/oneget-le-chocolatey-de-microsoft/
categories:
  - PowerShell
tags:
  - Chocolatey
  - OneGet
  - Outils
  - PowerShell

---
En juillet dernier je vous parlais de Chocolatey, un gestionnaire de package pour Windows.

J’étais passé à côté d’une information importante annoncée lors de l’événement BUILD 2014. Microsoft travaille sur son propre gestionnaire de package: **OneGet**. Vous le trouverez dans la CTP de Windows Management Framework V5 et nativement dans Windows 10.

L’avantage est qu’il est composé de :

  * OneGet Core : les API managées et natives
  * OneGet PowerShell Module : les commandlets
  * Prototype Chocolatey Plugin : client spécifique pour Chocolatey

Initialement Chocolatey était le dépôt par défaut. Les choses évoluant rapidement, Chocolatey n’est plus le dépôt par défaut et on pourra à terme choisir ses dépôts. D’ailleurs Chocolatey se lance sur Kickstarter pour récolter des fonds afin d’évoluer.

**Comment ça marche ?**

Une fois OneGet installé et configuré, vous pourrez vous servir des commandes suivantes dans PowerShell :

{{< highlight powershell >}}# Lister les packages
Get-PackageSource

# Chercher un package
Find-Package -Name firefox –AllVersions

# Installer un package
Install-Package -name firefox

# Lister les package installés
Get-Package

# Desinstaller un package
Uninstall-Package firefox{{< /highlight >}}

Cela me parait être une bonne nouvelle, qu’en pensez vous ?

**Références :**

[En juillet dernier je vous parlais de Chocolatey, un gestionnaire de package pour Windows.

J’étais passé à côté d’une information importante annoncée lors de l’événement BUILD 2014. Microsoft travaille sur son propre gestionnaire de package: **OneGet**. Vous le trouverez dans la CTP de Windows Management Framework V5 et nativement dans Windows 10.

L’avantage est qu’il est composé de :

  * OneGet Core : les API managées et natives
  * OneGet PowerShell Module : les commandlets
  * Prototype Chocolatey Plugin : client spécifique pour Chocolatey

Initialement Chocolatey était le dépôt par défaut. Les choses évoluant rapidement, Chocolatey n’est plus le dépôt par défaut et on pourra à terme choisir ses dépôts. D’ailleurs Chocolatey se lance sur Kickstarter pour récolter des fonds afin d’évoluer.

**Comment ça marche ?**

Une fois OneGet installé et configuré, vous pourrez vous servir des commandes suivantes dans PowerShell :

{{< highlight powershell >}}# Lister les packages
Get-PackageSource

# Chercher un package
Find-Package -Name firefox –AllVersions

# Installer un package
Install-Package -name firefox

# Lister les package installés
Get-Package

# Desinstaller un package
Uninstall-Package firefox{{< /highlight >}}

Cela me parait être une bonne nouvelle, qu’en pensez vous ?

**Références :**

](https://github.com/OneGet/oneget)

[En juillet dernier je vous parlais de Chocolatey, un gestionnaire de package pour Windows.

J’étais passé à côté d’une information importante annoncée lors de l’événement BUILD 2014. Microsoft travaille sur son propre gestionnaire de package: **OneGet**. Vous le trouverez dans la CTP de Windows Management Framework V5 et nativement dans Windows 10.

L’avantage est qu’il est composé de :

  * OneGet Core : les API managées et natives
  * OneGet PowerShell Module : les commandlets
  * Prototype Chocolatey Plugin : client spécifique pour Chocolatey

Initialement Chocolatey était le dépôt par défaut. Les choses évoluant rapidement, Chocolatey n’est plus le dépôt par défaut et on pourra à terme choisir ses dépôts. D’ailleurs Chocolatey se lance sur Kickstarter pour récolter des fonds afin d’évoluer.

**Comment ça marche ?**

Une fois OneGet installé et configuré, vous pourrez vous servir des commandes suivantes dans PowerShell :

{{< highlight powershell >}}# Lister les packages
Get-PackageSource

# Chercher un package
Find-Package -Name firefox –AllVersions

# Installer un package
Install-Package -name firefox

# Lister les package installés
Get-Package

# Desinstaller un package
Uninstall-Package firefox{{< /highlight >}}

Cela me parait être une bonne nouvelle, qu’en pensez vous ?

**Références :**

[En juillet dernier je vous parlais de Chocolatey, un gestionnaire de package pour Windows.

J’étais passé à côté d’une information importante annoncée lors de l’événement BUILD 2014. Microsoft travaille sur son propre gestionnaire de package: **OneGet**. Vous le trouverez dans la CTP de Windows Management Framework V5 et nativement dans Windows 10.

L’avantage est qu’il est composé de :

  * OneGet Core : les API managées et natives
  * OneGet PowerShell Module : les commandlets
  * Prototype Chocolatey Plugin : client spécifique pour Chocolatey

Initialement Chocolatey était le dépôt par défaut. Les choses évoluant rapidement, Chocolatey n’est plus le dépôt par défaut et on pourra à terme choisir ses dépôts. D’ailleurs Chocolatey se lance sur Kickstarter pour récolter des fonds afin d’évoluer.

**Comment ça marche ?**

Une fois OneGet installé et configuré, vous pourrez vous servir des commandes suivantes dans PowerShell :

{{< highlight powershell >}}# Lister les packages
Get-PackageSource

# Chercher un package
Find-Package -Name firefox –AllVersions

# Installer un package
Install-Package -name firefox

# Lister les package installés
Get-Package

# Desinstaller un package
Uninstall-Package firefox{{< /highlight >}}

Cela me parait être une bonne nouvelle, qu’en pensez vous ?

**Références :**

](https://github.com/OneGet/oneget)

](https://github.com/OneGet/oneget/wiki/Q-and-A)

[Chocolatey sur KickStarter][1]

 [1]: https://www.kickstarter.com/projects/ferventcoder/chocolatey-the-alternative-windows-store-like-yum