---
title: Comment utiliser Material Design dans vos applications ?
author: Ghislain

date: 2015-09-22T06:34:07+00:00
url: /2015/09/22/comment-utiliser-material-design-dans-vos-applications/
categories:
  - UI
tags:
  - Angular
  - Bootstrap
  - EmberJS
  - material design
  - Polymer
  - ReactJS

---
Le Material Design est un ensemble de règle de design pour les interfaces graphiques proposées par Google.  Dans la mouvance du flat design, Google l’a rendu populaire à l’appliquant à ses services et applications en 2015. Dernierement il a été poussé encore plus sur le devant de la scène avec le logo de Google. Comment utiliser Material Design dans vos application ? Je vous propose un tour d’horizon des framework actuels.

## Materialize {#materialize}

[<img class="alignnone size-medium wp-image-941" src="/wp-content/uploads/2015/09/Materialize.png?fit=238%2C122" alt="Materialize" data-recalc-dims="1" />][1]

Framework responsive front-end. CSS, SCSS, JavaScript, icons, Roboto font.

<http://materializecss.com/>

## Material UI {#material-ui}

<img class="alignnone size-medium wp-image-940" src="/wp-content/uploads/2015/09/MaterialUI-300x167.png?fit=300%2C167" alt="MaterialUI" srcset="/wp-content/uploads/2015/09/MaterialUI.png?resize=300%2C167 300w, /wp-content/uploads/2015/09/MaterialUI.png?w=581 581w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />

Framework CSS et un ensemble de composant pour **React** qui implémente le Material Design de Google.

<http://material-ui.com/>

## MUI CSS Framework {#mui-css-framework}

[<img class="alignnone size-medium wp-image-944" src="/wp-content/uploads/2015/09/MUI-300x139.png?fit=300%2C139" alt="MUI" srcset="/wp-content/uploads/2015/09/MUI.png?resize=300%2C139 300w, /wp-content/uploads/2015/09/MUI.png?w=463 463w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />][2]

Un framework CSS léger comprenant des composants pour **React** et des **WebComponents**

<https://www.muicss.com/>

## Polymer {#polymer}

<img class="alignnone size-medium wp-image-943" src="/wp-content/uploads/2015/09/Polymer-300x115.png?fit=300%2C115" alt="Polymer" srcset="/wp-content/uploads/2015/09/Polymer.png?resize=300%2C115 300w, /wp-content/uploads/2015/09/Polymer.png?w=334 334w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />

Créer des composants web riches et réutilisables.

<https://www.polymer-project.org/1.0/>

## Angular Material {#angular-material}

<img class="alignnone size-medium wp-image-945" src="/wp-content/uploads/2015/09/AngularMaterial.png?fit=255%2C217" alt="AngularMaterial" data-recalc-dims="1" />

Une implementation du Material Design pour **AngularJS**

<https://material.angularjs.org/latest/#/>

## Ember {#ember}

### Ember Materialize {#ember-materialize}

<img class="alignnone size-medium wp-image-948" src="/wp-content/uploads/2015/09/EmberMaterialize.png?fit=289%2C66" alt="EmberMaterialize" data-recalc-dims="1" />

<https://www.npmjs.com/package/ember-cli-materialize>

### Ember Paper {#ember-paper}

<img class="alignnone size-full wp-image-947" src="/wp-content/uploads/2015/09/EmberPaper.png?fit=195%2C64" alt="EmberPaper" data-recalc-dims="1" />

<http://miguelcobain.github.io/ember-paper/>

## Ionic Material {#ionic-material}

<img class="alignnone size-medium wp-image-946" src="/wp-content/uploads/2015/09/IonicMaterial.png?fit=110%2C93" alt="IonicMaterial" data-recalc-dims="1" />

Une implementation pour **Ionic** (cordova + angularjs + love)

<http://ionicmaterial.com/>

## Material Design Lite {#material-design-lite}

<img class="alignnone size-full wp-image-942" src="/wp-content/uploads/2015/09/MDL.png?fit=233%2C192" alt="MDL" data-recalc-dims="1" />

Framework comprenant CSS, JS et icon

<http://www.getmdl.io/index.html>

## Bootstrap {#bootstrap}

si vous souhaitez continuer à exploiter vos connaissances **Bootstrap**, c’est possible :

### Paper Bootswatch for Bootstrap {#paper-bootswatch-for-bootstrap}

<http://bootswatch.com/paper/>

### Material Design for Bootstrap {#material-design-for-bootstrap}

<http://fezvrasta.github.io/bootstrap-material-design/>

Comme vous avez pu le voir, quelque soit le framework que vous utilisez, vous disposez d’une implémentation du Material Design.

  * Materialize ou un thème Bootstrap pour un projet « classique »;
  * Angular Material pour AngularJS
  * Material UI pour ReactJS
  * Ember Materialize ou Paper pour EmberJS
  * Ionic Material pour Ionic
  * Polymer pour les webcomponents

 

**Références:**

Spéc du Material Design : https://www.google.com/design/spec/material-design/introduction.html

 [1]: /wp-content/uploads/2015/09/Materialize.png
 [2]: /wp-content/uploads/2015/09/MUI.png