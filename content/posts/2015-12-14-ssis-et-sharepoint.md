---
title: SSIS et SharePoint
author: Ghislain

date: 2015-12-14T07:00:33+00:00
url: /2015/12/14/ssis-et-sharepoint/
categories:
  - Databases
  - SharePoint
tags:
  - ETL
  - Office365
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013
  - SQL Server
  - SSIS

---
SharePoint permet de structurer et stocker des informations. Ces informations vivent dans SharePoint et sont consultables par plusieurs biais (site, webservice, code). Nativement il est possible d’exporter ces données vers Excel. Vous pouvez aller encore plus loin dans l’export – et l’import- de données avec SSIS.

## Qu’est-ce que SSIS ? {#quest-ce-que-ssis-}

SSIS est l’ETL de Microsoft qui est une brique intégrée à SQL Server. ETL est l’acronyme de Extract Transform and Load. Comme son nom l’indique il s’agit d’un logiciel qui permet d’extraire les données d’une source A, de les travailler (filtrer, enrichir, combiner, convertir, …) puis de les exporter vers une destination B. La richesse d’un ETL vient de :

  * ses connecteurs, c’est à dire avec quoi peut-il communiquer en lecture/écriture (SQL Server, Oracle, fichier plat, SharePoint, …);
  * les opérations qu’il peut effectuer sur les données (filtre, agrégation, enrichissement, …).

## Outils nécessaires {#outils-nécessaires}

La démonstration consiste à récupérer les données d’une liste SharePoint, sélectionner les colonnes voulues, convertir les formats de données au besoin et exporter dans un fichier à plat.

Pour débuter dans le développement SSIS, vous aurez besoin de  :

  * SQL Server 2012 avec SSIS;
  * SQL Server Data tools (SSTD), l’environnement de développement basé sur Visual Studio qui permet de créer des packages SSIS. Il est précédemment appelé BIDS (Business Intelligence Development Studio).
  * un environnement SharePoint;
  * un connecteur SharePoint pour SSIS.

Maintenant vous devez choisir votre connecteur. Vous en trouverez 3 gratuit en cherchant :

  * SharePoint List Source and Destination, Microsoft SQL Server Community Samples: Integration Services, sur codeplex
  * SSIS Connector for Sharepoint Online sur codeplex
  * Microsoft® OData Source for Microsoft SQL Server® 2012

<table>
  <tr>
    <td width="192">
      <strong> </strong>
    </td>
    
    <td width="192">
      <strong>SP Connector</strong>
    </td>
    
    <td width="192">
      <strong>SSIS Connector for SP Online</strong>
    </td>
    
    <td width="192">
      <strong>OData Source</strong>
    </td>
  </tr>
  
  <tr>
    <td width="192">
      <strong>SP2010</strong>
    </td>
    
    <td width="192">
      X
    </td>
    
    <td width="192">
    </td>
    
    <td width="192">
      X
    </td>
  </tr>
  
  <tr>
    <td width="192">
      <strong>SP2013</strong>
    </td>
    
    <td width="192">
      X
    </td>
    
    <td width="192">
    </td>
    
    <td width="192">
      X
    </td>
  </tr>
  
  <tr>
    <td width="192">
      <strong>O365</strong>
    </td>
    
    <td width="192">
    </td>
    
    <td width="192">
      X
    </td>
    
    <td width="192">
      X
    </td>
  </tr>
</table>

Le choix le plus judicieux est donc le composant OData Source de Microsoft.

 

## Utilisation de OData Source pour SQL Server 2012 {#utilisation-de-odata-source-pour-sql-server-2012}

Une fois l’installation effectuée sur votre poste de développement, vous verrez apparaitre un nouvel élément dans VSDT.

[<img class="wp-image-974 size-medium" src="/wp-content/uploads/2015/12/SSIS-OData-173x300.png?fit=173%2C300" alt="SSIS-OData" data-recalc-dims="1" />][1]  
_Composant OData Source dans Data Tools_

Dans le dataflow, vous glissez-déplacer un OData Source, un Data Conversion et un Flat File Destination pour obtenir le schéma suivant.

<a href="/wp-content/uploads/2015/12/SSIS-OData-1.png" rel="attachment wp-att-978"><img class="wp-image-978 size-full" src="/wp-content/uploads/2015/12/SSIS-OData-1.png?fit=499%2C324" alt="SSIS-OData-1" srcset="/wp-content/uploads/2015/12/SSIS-OData-1.png?resize=300%2C195 300w, /wp-content/uploads/2015/12/SSIS-OData-1.png?w=499 499w" sizes="(max-width: 499px) 100vw, 499px" data-recalc-dims="1" /></a>  
_Dataflow_

Vous devez configurer les différents composants. Dans le OData Source, vous devez renseigner l’URL de votre site SharePoint suffixé de /\_vti\_bin/listdata.svc. Précisez aussi les informations nécessaires pour l’authentification (Windows authentication ou un couple login/password).

<a href="/wp-content/uploads/2015/12/SSIS-OData-2.png" rel="attachment wp-att-979"><img class="wp-image-979 size-full" src="/wp-content/uploads/2015/12/SSIS-OData-2.png?fit=561%2C363" alt="SSIS-OData-2" srcset="/wp-content/uploads/2015/12/SSIS-OData-2.png?resize=300%2C194 300w, /wp-content/uploads/2015/12/SSIS-OData-2.png?w=561 561w" sizes="(max-width: 561px) 100vw, 561px" data-recalc-dims="1" /></a>  
_Paramètre du gestionnaire de connexion OData_

Si vous utilisez Office 365, vous devrez le préciser en passant Microsoft Online Services Authentication à True comme dans la capture ci-dessous :

<a href="/wp-content/uploads/2015/12/SSIS-OData-3.png" rel="attachment wp-att-980"><img class="wp-image-980 size-full" src="/wp-content/uploads/2015/12/SSIS-OData-3.png?fit=559%2C361" alt="SSIS-OData-3" srcset="/wp-content/uploads/2015/12/SSIS-OData-3.png?resize=300%2C194 300w, /wp-content/uploads/2015/12/SSIS-OData-3.png?w=559 559w" sizes="(max-width: 559px) 100vw, 559px" data-recalc-dims="1" /></a>  
_Configuration de la connexion OData pour Office 365_

Ensuite vous devrez préciser la liste d’où vous voulez récupérer des données. Il vous est possible de passer des paramètres de requêtes. Vous devez ensuite choisir les colonnes que vous voulez récupérer.

<a href="/wp-content/uploads/2015/12/SSIS-OData-4.png" rel="attachment wp-att-981"><img class="wp-image-981 size-full" src="/wp-content/uploads/2015/12/SSIS-OData-4.png?fit=647%2C633" alt="SSIS-OData-4" srcset="/wp-content/uploads/2015/12/SSIS-OData-4.png?resize=300%2C294 300w, /wp-content/uploads/2015/12/SSIS-OData-4.png?w=647 647w" sizes="(max-width: 647px) 100vw, 647px" data-recalc-dims="1" /></a>  
_Choix de la source de données_

Dernières étapes, convertir les données si nécessaire puis faire le mapping entre les colonnes sources et les colonnes du fichier à plat.

Ensuite vous exécutez le package SSIS et – si tout est bien configuré – vous verrez SSDT passer en mode DEBUG et présenter l’exécution en temps réel.

<a href="/wp-content/uploads/2015/12/SSIS-OData-5.png" rel="attachment wp-att-977"><img class="wp-image-977 size-full" src="/wp-content/uploads/2015/12/SSIS-OData-5.png?fit=490%2C304" alt="SSIS-OData-5" srcset="/wp-content/uploads/2015/12/SSIS-OData-5.png?resize=300%2C186 300w, /wp-content/uploads/2015/12/SSIS-OData-5.png?w=490 490w" sizes="(max-width: 490px) 100vw, 490px" data-recalc-dims="1" /></a>  
_Exécution du Dataflow_

## Résolution des erreurs rencontrées {#résolution-des-erreurs-rencontrées}

### Error 1 : The package failed to load due to error 0xC0011008 {#error-1--the-package-failed-to-load-due-to-error-0xc0011008}

Si votre package de contient pas d’erreur lors de la conception mais que message suivant apparait lors de l’exécution :

> The package failed to load due to error 0xC0011008 « Error loading from XML. No further detailed error information can be specified for this problem because no Events object was passed where detailed error information can be stored. ». This occurs when CPackage::LoadFromXML fails.

Il faut configurer votre package pour ne plus s’exécuter en 64bits.

Solution Explorer> clic droit sur le projet, properties

Configuration properties > debugging > Run64BitRuntime : False

<a href="/wp-content/uploads/2015/12/SSIS-error1.png" rel="attachment wp-att-983"><img class="wp-image-983 size-full" src="/wp-content/uploads/2015/12/SSIS-error1.png?fit=625%2C408" alt="SSIS-error1" srcset="/wp-content/uploads/2015/12/SSIS-error1.png?resize=300%2C196 300w, /wp-content/uploads/2015/12/SSIS-error1.png?w=625 625w" sizes="(max-width: 625px) 100vw, 625px" data-recalc-dims="1" /></a>  
_Résolution : The package failed to load due to error 0xC0011008_

### Error 2 : The remote server returned an error: (407) Proxy Authentication Required {#error-2--the-remote-server-returned-an-error-407-proxy-authentication-required}

Si quand vous testez votre connexion aux données, le message suivant apparait :

> The remote server returned an error: (407) Proxy Authentication Required.

Alors vous devez configurer votre connexion dans Internet Explorer

<a href="/wp-content/uploads/2015/12/SSIS-error2.png" rel="attachment wp-att-982"><img class="wp-image-982 size-full" src="/wp-content/uploads/2015/12/SSIS-error2.png?fit=381%2C328" alt="SSIS-error2" srcset="/wp-content/uploads/2015/12/SSIS-error2.png?resize=300%2C258 300w, /wp-content/uploads/2015/12/SSIS-error2.png?w=381 381w" sizes="(max-width: 381px) 100vw, 381px" data-recalc-dims="1" /></a>  
_Résolution : The remote server returned an error: (407) Proxy Authentication Required_

Comme vous avez pu vous en rendre compte, il est relativement simple d’extraire des données de SharePoint avec SSIS. D’autant plus qu’aucune ligne de code n’est nécessaire dans notre exemple.

**Références:**

[SharePoint permet de structurer et stocker des informations. Ces informations vivent dans SharePoint et sont consultables par plusieurs biais (site, webservice, code). Nativement il est possible d’exporter ces données vers Excel. Vous pouvez aller encore plus loin dans l’export – et l’import- de données avec SSIS.

## Qu’est-ce que SSIS ? {#quest-ce-que-ssis--1}

SSIS est l’ETL de Microsoft qui est une brique intégrée à SQL Server. ETL est l’acronyme de Extract Transform and Load. Comme son nom l’indique il s’agit d’un logiciel qui permet d’extraire les données d’une source A, de les travailler (filtrer, enrichir, combiner, convertir, …) puis de les exporter vers une destination B. La richesse d’un ETL vient de :

  * ses connecteurs, c’est à dire avec quoi peut-il communiquer en lecture/écriture (SQL Server, Oracle, fichier plat, SharePoint, …);
  * les opérations qu’il peut effectuer sur les données (filtre, agrégation, enrichissement, …).

## Outils nécessaires {#outils-nécessaires-1}

La démonstration consiste à récupérer les données d’une liste SharePoint, sélectionner les colonnes voulues, convertir les formats de données au besoin et exporter dans un fichier à plat.

Pour débuter dans le développement SSIS, vous aurez besoin de  :

  * SQL Server 2012 avec SSIS;
  * SQL Server Data tools (SSTD), l’environnement de développement basé sur Visual Studio qui permet de créer des packages SSIS. Il est précédemment appelé BIDS (Business Intelligence Development Studio).
  * un environnement SharePoint;
  * un connecteur SharePoint pour SSIS.

Maintenant vous devez choisir votre connecteur. Vous en trouverez 3 gratuit en cherchant :

  * SharePoint List Source and Destination, Microsoft SQL Server Community Samples: Integration Services, sur codeplex
  * SSIS Connector for Sharepoint Online sur codeplex
  * Microsoft® OData Source for Microsoft SQL Server® 2012

<table>
  <tr>
    <td width="192">
      <strong> </strong>
    </td>
    
    <td width="192">
      <strong>SP Connector</strong>
    </td>
    
    <td width="192">
      <strong>SSIS Connector for SP Online</strong>
    </td>
    
    <td width="192">
      <strong>OData Source</strong>
    </td>
  </tr>
  
  <tr>
    <td width="192">
      <strong>SP2010</strong>
    </td>
    
    <td width="192">
      X
    </td>
    
    <td width="192">
    </td>
    
    <td width="192">
      X
    </td>
  </tr>
  
  <tr>
    <td width="192">
      <strong>SP2013</strong>
    </td>
    
    <td width="192">
      X
    </td>
    
    <td width="192">
    </td>
    
    <td width="192">
      X
    </td>
  </tr>
  
  <tr>
    <td width="192">
      <strong>O365</strong>
    </td>
    
    <td width="192">
    </td>
    
    <td width="192">
      X
    </td>
    
    <td width="192">
      X
    </td>
  </tr>
</table>

Le choix le plus judicieux est donc le composant OData Source de Microsoft.

 

## Utilisation de OData Source pour SQL Server 2012 {#utilisation-de-odata-source-pour-sql-server-2012-1}

Une fois l’installation effectuée sur votre poste de développement, vous verrez apparaitre un nouvel élément dans VSDT.

[<img class="wp-image-974 size-medium" src="/wp-content/uploads/2015/12/SSIS-OData-173x300.png?fit=173%2C300" alt="SSIS-OData" data-recalc-dims="1" />][1]  
_Composant OData Source dans Data Tools_

Dans le dataflow, vous glissez-déplacer un OData Source, un Data Conversion et un Flat File Destination pour obtenir le schéma suivant.

<a href="/wp-content/uploads/2015/12/SSIS-OData-1.png" rel="attachment wp-att-978"><img class="wp-image-978 size-full" src="/wp-content/uploads/2015/12/SSIS-OData-1.png?fit=499%2C324" alt="SSIS-OData-1" srcset="/wp-content/uploads/2015/12/SSIS-OData-1.png?resize=300%2C195 300w, /wp-content/uploads/2015/12/SSIS-OData-1.png?w=499 499w" sizes="(max-width: 499px) 100vw, 499px" data-recalc-dims="1" /></a>  
_Dataflow_

Vous devez configurer les différents composants. Dans le OData Source, vous devez renseigner l’URL de votre site SharePoint suffixé de /\_vti\_bin/listdata.svc. Précisez aussi les informations nécessaires pour l’authentification (Windows authentication ou un couple login/password).

<a href="/wp-content/uploads/2015/12/SSIS-OData-2.png" rel="attachment wp-att-979"><img class="wp-image-979 size-full" src="/wp-content/uploads/2015/12/SSIS-OData-2.png?fit=561%2C363" alt="SSIS-OData-2" srcset="/wp-content/uploads/2015/12/SSIS-OData-2.png?resize=300%2C194 300w, /wp-content/uploads/2015/12/SSIS-OData-2.png?w=561 561w" sizes="(max-width: 561px) 100vw, 561px" data-recalc-dims="1" /></a>  
_Paramètre du gestionnaire de connexion OData_

Si vous utilisez Office 365, vous devrez le préciser en passant Microsoft Online Services Authentication à True comme dans la capture ci-dessous :

<a href="/wp-content/uploads/2015/12/SSIS-OData-3.png" rel="attachment wp-att-980"><img class="wp-image-980 size-full" src="/wp-content/uploads/2015/12/SSIS-OData-3.png?fit=559%2C361" alt="SSIS-OData-3" srcset="/wp-content/uploads/2015/12/SSIS-OData-3.png?resize=300%2C194 300w, /wp-content/uploads/2015/12/SSIS-OData-3.png?w=559 559w" sizes="(max-width: 559px) 100vw, 559px" data-recalc-dims="1" /></a>  
_Configuration de la connexion OData pour Office 365_

Ensuite vous devrez préciser la liste d’où vous voulez récupérer des données. Il vous est possible de passer des paramètres de requêtes. Vous devez ensuite choisir les colonnes que vous voulez récupérer.

<a href="/wp-content/uploads/2015/12/SSIS-OData-4.png" rel="attachment wp-att-981"><img class="wp-image-981 size-full" src="/wp-content/uploads/2015/12/SSIS-OData-4.png?fit=647%2C633" alt="SSIS-OData-4" srcset="/wp-content/uploads/2015/12/SSIS-OData-4.png?resize=300%2C294 300w, /wp-content/uploads/2015/12/SSIS-OData-4.png?w=647 647w" sizes="(max-width: 647px) 100vw, 647px" data-recalc-dims="1" /></a>  
_Choix de la source de données_

Dernières étapes, convertir les données si nécessaire puis faire le mapping entre les colonnes sources et les colonnes du fichier à plat.

Ensuite vous exécutez le package SSIS et – si tout est bien configuré – vous verrez SSDT passer en mode DEBUG et présenter l’exécution en temps réel.

<a href="/wp-content/uploads/2015/12/SSIS-OData-5.png" rel="attachment wp-att-977"><img class="wp-image-977 size-full" src="/wp-content/uploads/2015/12/SSIS-OData-5.png?fit=490%2C304" alt="SSIS-OData-5" srcset="/wp-content/uploads/2015/12/SSIS-OData-5.png?resize=300%2C186 300w, /wp-content/uploads/2015/12/SSIS-OData-5.png?w=490 490w" sizes="(max-width: 490px) 100vw, 490px" data-recalc-dims="1" /></a>  
_Exécution du Dataflow_

## Résolution des erreurs rencontrées {#résolution-des-erreurs-rencontrées-1}

### Error 1 : The package failed to load due to error 0xC0011008 {#error-1--the-package-failed-to-load-due-to-error-0xc0011008-1}

Si votre package de contient pas d’erreur lors de la conception mais que message suivant apparait lors de l’exécution :

> The package failed to load due to error 0xC0011008 « Error loading from XML. No further detailed error information can be specified for this problem because no Events object was passed where detailed error information can be stored. ». This occurs when CPackage::LoadFromXML fails.

Il faut configurer votre package pour ne plus s’exécuter en 64bits.

Solution Explorer> clic droit sur le projet, properties

Configuration properties > debugging > Run64BitRuntime : False

<a href="/wp-content/uploads/2015/12/SSIS-error1.png" rel="attachment wp-att-983"><img class="wp-image-983 size-full" src="/wp-content/uploads/2015/12/SSIS-error1.png?fit=625%2C408" alt="SSIS-error1" srcset="/wp-content/uploads/2015/12/SSIS-error1.png?resize=300%2C196 300w, /wp-content/uploads/2015/12/SSIS-error1.png?w=625 625w" sizes="(max-width: 625px) 100vw, 625px" data-recalc-dims="1" /></a>  
_Résolution : The package failed to load due to error 0xC0011008_

### Error 2 : The remote server returned an error: (407) Proxy Authentication Required {#error-2--the-remote-server-returned-an-error-407-proxy-authentication-required-1}

Si quand vous testez votre connexion aux données, le message suivant apparait :

> The remote server returned an error: (407) Proxy Authentication Required.

Alors vous devez configurer votre connexion dans Internet Explorer

<a href="/wp-content/uploads/2015/12/SSIS-error2.png" rel="attachment wp-att-982"><img class="wp-image-982 size-full" src="/wp-content/uploads/2015/12/SSIS-error2.png?fit=381%2C328" alt="SSIS-error2" srcset="/wp-content/uploads/2015/12/SSIS-error2.png?resize=300%2C258 300w, /wp-content/uploads/2015/12/SSIS-error2.png?w=381 381w" sizes="(max-width: 381px) 100vw, 381px" data-recalc-dims="1" /></a>  
_Résolution : The remote server returned an error: (407) Proxy Authentication Required_

Comme vous avez pu vous en rendre compte, il est relativement simple d’extraire des données de SharePoint avec SSIS. D’autant plus qu’aucune ligne de code n’est nécessaire dans notre exemple.

**Références:**

](http://sqlsrvintegrationsrv.codeplex.com/releases/view/17652)

[SSIS Connector for Sharepoint Online sur codeplex][2]

[Microsoft® OData Source for Microsoft SQL Server® 2012][3]

[Introduction à SSIS et BIML sur MVA][4]

 

 

 [1]: /wp-content/uploads/2015/12/SSIS-OData.png
 [2]: https://ssisconnectorsharepointonline.codeplex.com/
 [3]: https://www.microsoft.com/en-us/download/details.aspx?id=42280
 [4]: https://mva.microsoft.com/fr-fr/training-courses/introduction-ssis-et-bimlfr-12169