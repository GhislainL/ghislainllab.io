---
title: 'Comment configurer un package SSIS selon l&rsquo;environnement ?'
author: Ghislain

date: 2015-12-21T07:59:41+00:00
url: /2015/12/21/comment-configurer-un-package-ssis-selon-lrsquoenvironnement/
categories:
  - Databases
tags:
  - ETL
  - SQL Server
  - SSIS

---
Dans l’article précédent, je vous ai expliqué [comment extraire et transformer des données de SharePoint avec SSIS][1]. Le package SSIS obtenu était configuré pour l’environnement de développement. Les URL et les chemins sont certainement différent dans les environnements qui serviront à valider la livraison jusqu’à la mise en production. Comment configurer un package SSIS selon l’environnement ?

Cela se passe en deux étapes :

  1. Dans SQL Server Data Tools, vous modifiez vos packages SSIS pour récupérer des valeurs depuis des paramètres ou lieu d’avoir une valeur fixée à la création;
  2. Dans SQL Server Management Studio, vous déployez votre projet SSIS, puis créez un environnement contenant les valeurs pour les paramètres et finalement associez les deux.

## SQL Server Data Tools {#sql-server-data-tools}

Dans la partie inférieur de l’onglet **Data Flow** du package SSIS, vous trouverez les connexions managers. Un clic droit sur l’un d’eux permet d’accéder à l’option **Parametrize…**

<a href="/wp-content/uploads/2015/12/SSIS-Param-1.png" rel="attachment wp-att-989"><img class="wp-image-989 size-full" src="/wp-content/uploads/2015/12/SSIS-Param-1.png?fit=408%2C515" alt="SSIS-Param-1" srcset="/wp-content/uploads/2015/12/SSIS-Param-1.png?resize=238%2C300 238w, /wp-content/uploads/2015/12/SSIS-Param-1.png?w=408 408w" sizes="(max-width: 408px) 100vw, 408px" data-recalc-dims="1" /></a>  
_SSDT – option Parametrize…_

La fenêtre **Parametrize** vous permet de sélectionner les propriétés à transformer en paramètre en spécifiant :

  * le nom;
  * la description;
  * la valeur par défaut;
  * le scope : package ou projet;
  * obligatoire ou facultatif pour l’exécution.

<a href="/wp-content/uploads/2015/12/SSIS-Param-2.png" rel="attachment wp-att-990"><img class="wp-image-990 size-full" src="/wp-content/uploads/2015/12/SSIS-Param-2.png?fit=414%2C498" alt="SSIS-Param-2" srcset="/wp-content/uploads/2015/12/SSIS-Param-2.png?resize=249%2C300 249w, /wp-content/uploads/2015/12/SSIS-Param-2.png?w=414 414w" sizes="(max-width: 414px) 100vw, 414px" data-recalc-dims="1" /></a>  
_SSDT – fenêtre Parametrize…_

Dans l’onglet **Parameters** du package, vous accéder à la synthèse de tous les paramètres de votre package. Cela permet de les gérer de manière centralisée.

<a href="/wp-content/uploads/2015/12/SSIS-Param-3.png" rel="attachment wp-att-991"><img class="alignnone wp-image-991 size-full" src="/wp-content/uploads/2015/12/SSIS-Param-3.png?fit=756%2C166" alt="SSIS-Param-3" srcset="/wp-content/uploads/2015/12/SSIS-Param-3.png?resize=300%2C66 300w, /wp-content/uploads/2015/12/SSIS-Param-3.png?w=756 756w" sizes="(max-width: 756px) 100vw, 756px" data-recalc-dims="1" /></a>

Une fois les modifications effectuées, vous avez un projet SSIS (extension .ispac) qui pourra être déployé avec SQL Server Management Studio.

## SQL Server Management Studio {#sql-server-management-studio}

Dans SSMS, connectez vous à votre base de données puis allez jusqu’à votre catalogue Integration Services. Si vous avez besoin de le créer, conserver précieusement le mot de passe saisi. En descendant jusqu’au répertoire Projects, vous pouvez déployer votre projet avec **Deploy Project…** Laissez vous porter par le wizard et le projet sera ensuite paramétrable et utilisable.

<a href="/wp-content/uploads/2015/12/SSIS-Param-4.png" rel="attachment wp-att-992"><img class="wp-image-992 size-full" src="/wp-content/uploads/2015/12/SSIS-Param-4.png?fit=600%2C316" alt="SSIS-Param-4" srcset="/wp-content/uploads/2015/12/SSIS-Param-4.png?resize=300%2C158 300w, /wp-content/uploads/2015/12/SSIS-Param-4.png?w=600 600w" sizes="(max-width: 600px) 100vw, 600px" data-recalc-dims="1" /></a>  
_SSMS – Deploiement du projet SSIS_

Un environnement est un conteneur logique regroupant les variables et leurs valeurs. Plusieurs environnements peuvent cohabiter ensemble. Pour la démonstration j’ai créé 3 environnements (RCT, QUAL et PRD) avec les valeurs associées. Clic droit sur le dossier Environments de votre projet puis cliquer sur **Create Environment…**

<a href="/wp-content/uploads/2015/12/SSIS-Param-5.png" rel="attachment wp-att-993"><img class="wp-image-993 size-full" src="/wp-content/uploads/2015/12/SSIS-Param-5.png?fit=1000%2C401" alt="SSIS-Param-5" srcset="/wp-content/uploads/2015/12/SSIS-Param-5.png?resize=300%2C120 300w, /wp-content/uploads/2015/12/SSIS-Param-5.png?resize=768%2C308 768w, /wp-content/uploads/2015/12/SSIS-Param-5.png?w=1000 1000w" sizes="(max-width: 1000px) 100vw, 1000px" data-recalc-dims="1" /></a>  
_SSMS – Gestion des environnements_

A ce stade, vous avez un package et un environnement, il reste à les associer – une opération en 2 étapes. Premièrement clic droit sur le projet pour choisir l’option **Configure**. Dans l’onglet **References**, vous pourrez choisir l’environnement désiré.

<a href="/wp-content/uploads/2015/12/SSIS-Param-6.png" rel="attachment wp-att-994"><img class="wp-image-994 size-full" src="/wp-content/uploads/2015/12/SSIS-Param-6.png?fit=750%2C495" alt="SSIS-Param-6" srcset="/wp-content/uploads/2015/12/SSIS-Param-6.png?resize=300%2C198 300w, /wp-content/uploads/2015/12/SSIS-Param-6.png?w=750 750w" sizes="(max-width: 750px) 100vw, 750px" data-recalc-dims="1" /></a>  
_SSMS – Association de l’environnement au projet_

Deuxièmement – une fois l’association environnement/projet effectuée – vous devez associée les variables aux paramètres. Cela se passe dans l’onglet **Parameters**.

<a href="/wp-content/uploads/2015/12/SSIS-Param-7.png" rel="attachment wp-att-995"><img class="wp-image-995 size-full" src="/wp-content/uploads/2015/12/SSIS-Param-7.png?fit=752%2C495" alt="SSIS-Param-7" srcset="/wp-content/uploads/2015/12/SSIS-Param-7.png?resize=300%2C197 300w, /wp-content/uploads/2015/12/SSIS-Param-7.png?w=752 752w" sizes="(max-width: 752px) 100vw, 752px" data-recalc-dims="1" /></a>  
_SSMS – association des variables aux paramètres_

Maintenant vous pouvez tester, exécuter le package pour vérifier la bonne configuration de votre projet. Les rapports d’exécution vous apporteront pleins d’informations intéressantes.

 [1]: http://ghislain-lerda.com/2015/12/14/ssis-et-sharepoint/