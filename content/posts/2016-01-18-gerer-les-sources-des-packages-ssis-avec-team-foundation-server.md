---
title: Gérer les sources des packages SSIS avec Team Foundation Server
author: Ghislain

date: 2016-01-18T08:35:07+00:00
url: /2016/01/18/gerer-les-sources-des-packages-ssis-avec-team-foundation-server/
categories:
  - Databases
tags:
  - SQL Server
  - SSIS
  - Team Foundation Server
  - TFS
  - Visual Studio

---
Maintenant que nous savons créer, déployer et planifier l’exécution d’un package SSIS, il serait bon de sécuriser notre travail. Nous allons voir comment gérer les sources des packages SSIS avec Team Foundation Server.

Dans le premier article de la série dédiée à SSIS, je vous ai parlé de Data Tools. L’IDE qui permet de développer les packages SSIS et qui est construite sur une base Visual Studio. Naturellement vous allez dans Tools > Options > Source Control pour sélectionner le plug-in TFS… mais surprise : aucun plug-in de contrôle de source.

<a href="/wp-content/uploads/2016/01/SSIS-TFS-1.png" rel="attachment wp-att-1028"><img class="alignnone wp-image-1028 size-full" src="/wp-content/uploads/2016/01/SSIS-TFS-1.png?fit=642%2C375" alt="SSIS-TFS-1" srcset="/wp-content/uploads/2016/01/SSIS-TFS-1.png?resize=300%2C175 300w, /wp-content/uploads/2016/01/SSIS-TFS-1.png?w=642 642w" sizes="(max-width: 642px) 100vw, 642px" data-recalc-dims="1" /></a>

Heureusement, Microsoft propose de télécharger gratuitement Visual Studio **Team Explorer**.

Après installation vous pouvez configurer SSDT pour utiliser le plug-in **Visual Studio Team Foundation Server**.

<a href="/wp-content/uploads/2016/01/SSIS-TFS-2.png" rel="attachment wp-att-1029"><img class="alignnone wp-image-1029 size-full" src="/wp-content/uploads/2016/01/SSIS-TFS-2.png?fit=645%2C376" alt="SSIS-TFS-2" srcset="/wp-content/uploads/2016/01/SSIS-TFS-2.png?resize=300%2C175 300w, /wp-content/uploads/2016/01/SSIS-TFS-2.png?w=645 645w" sizes="(max-width: 645px) 100vw, 645px" data-recalc-dims="1" /></a>

Après avoir renseigné le serveur TFS, choisi votre Team Project Collection puis votre Team Project, vous pourrez sauvegarder vos projets Integration Services et vos scripts PowerShell dans TFS.

<a href="/wp-content/uploads/2016/01/SSIS-TFS-3.png" rel="attachment wp-att-1030"><img class="alignnone wp-image-1030 size-full" src="/wp-content/uploads/2016/01/SSIS-TFS-3.png?fit=598%2C310" alt="SSIS-TFS-3" srcset="/wp-content/uploads/2016/01/SSIS-TFS-3.png?resize=300%2C156 300w, /wp-content/uploads/2016/01/SSIS-TFS-3.png?w=598 598w" sizes="(max-width: 598px) 100vw, 598px" data-recalc-dims="1" /></a>

Cela sécurise tout le monde, n’est ce pas ?

**Références**

[Team Explorer pour Microsoft Visual Studio 2013][1]

 [1]: https://www.microsoft.com/fr-fr/download/details.aspx?id=40776