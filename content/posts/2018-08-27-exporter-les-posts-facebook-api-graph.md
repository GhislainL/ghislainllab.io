---
title: Exporter les posts d’une page Facebook
author: Ghislain

date: 2018-08-27T06:47:50+00:00
url: /2018/08/27/exporter-les-posts-facebook-api-graph/
featuredImage: /wp-content/uploads/2018/08/Facebook-scraping.png
categories:
  - Non classé
tags:
  - Facebook
  - JSON
  - scraping

---
Exporter les posts d&#8217;une page Facebook est moins simple que l&#8217;on pourrait le croire. Normal vu que cette plateforme veut conserver ses utilisateurs et quel meilleur moyen pour cela que de les priver d&#8217;une partie de leur &#8220;histoire&#8221; s&#8217;ils partent. Heureusement avec l&#8217;API Graph et un peu de compétence, il est possible de récupérer partiellement nos données.

Première étape, récupérer l&#8217;identifiant de votre page. Pour cela il faut saisir le nom de la page dans le champ de requête de la page suivante : <https://developers.facebook.com/tools/explorer/>

<figure id="attachment_344" aria-describedby="caption-attachment-344" style="width: 942px" class="wp-caption aligncenter"><img class="size-full wp-image-344" src="http://ghislain-lerda.fr/wp-content/uploads/2018/08/Facebook-API-Graph.png" alt="" width="942" height="340" srcset="https://ghislain-lerda.fr/wp-content/uploads/2018/08/Facebook-API-Graph.png 942w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/Facebook-API-Graph-300x108.png 300w, https://ghislain-lerda.fr/wp-content/uploads/2018/08/Facebook-API-Graph-768x277.png 768w" sizes="(max-width: 942px) 100vw, 942px" /><figcaption id="caption-attachment-344" class="wp-caption-text">Facebook &#8211; API Graph</figcaption></figure>

Seconde étape, muni de l&#8217;identifiant il est possible de récupérer les posts de votre page. La requête suivante permet d&#8217;obtenir le minimum d&#8217;informations utiles. Sachez qu&#8217;il est aussi possibles d&#8217;enrichir l&#8217;extraction avec les likes, les commentaires et les partages.

{{< highlight plain >}}[page_id]/posts/?fields=message,link,permalink_url,created_time,type,name,id&limit=100{{< /highlight >}}
Voici un extrait du JSON obtenu :

{{< highlight json >}}{
  "data": [
    {
      "message": "Veeraphol Sahaprom distribue des pralines dans cette compilation des meilleurs moments. Originaire de Thaïlande, il a été champion du monde des poids coqs WBA et à défendu son titre 14fois.
#boxeanglaise #WBA  Veerapol Nakhonluang
https://www.facebook.com/MuayThaiScholar/videos/10160621726760298/",
      "link": "https://www.facebook.com/MuayThaiScholar/videos/10160621726760298/",
      "permalink_url": "https://www.facebook.com/757819574382190/posts/1062881273876017/",
      "created_time": "2018-08-13T06:45:00+0000",
      "type": "video",
      "name": "Muay Thai Scholar",
      "id": "757819574382190_1062881273876017"
    },
    {
      "message": "J'ai regardé presque toutes les vidéos de la série \"Au bout du tunnel\" faites par GONG - Gi Or No Gi. Pour vous donner envie de les voir, je pense que cette compilation d'extraits est la meilleure des voies.
https://www.youtube.com/watch?v=fC3NVJItt78",
      "link": "https://www.youtube.com/watch?v=fC3NVJItt78",
      "permalink_url": "https://www.facebook.com/757819574382190/posts/1062565847240893/",
      "created_time": "2018-08-07T06:45:01+0000",
      "type": "video",
      "name": "Fighting Spirit : Au bout du tunnel",
      "id": "757819574382190_1062565847240893"
    }
  ],
  "paging": {
    "cursors": {
      "before": "Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5UTNOVGM0TVRrMU56UXpPREl4T1RBNkxUa3lNRGMzT1RJME1USTRNVEEyTURrNE5qRVBER0ZA3YVY5emRHOXllVjlwWkE4ZA056VTNPREU1TlRjME16ZA3lNVGt3WHpFd056ZAzBPVE0xTnpnNU9ERTBOVE1QQkhScGJXVUdXM3IxckFFPQZDZD",
      "after": "Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5TTNOVGM0TVRrMU56UXpPREl4T1RBNk1UVXpOakExTVRnME1qYzRPVEV5T1RrNE5nOE1ZAWEJwWDNOMGIzSjVYMmxrRHg4M05UYzRNVGsxTnpRek9ESXhPVEJmT0RrM05qRTFNRFV6TnpNMU9UYzBEd1IwYVcxbEJsb2NVb1FC"
    },
    "next": "https://graph.facebook.com/v3.1/757819574382190/posts?access_token=--------&pretty=0&fields=message%2Clink%2Cpermalink_url%2Ccreated_time%2Ctype%2Cname%2Cid&limit=100&after=Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5TTNOVGM0TVRrMU56UXpPREl4T1RBNk1UVXpOakExTVRnME1qYzRPVEV5T1RrNE5nOE1ZAWEJwWDNOMGIzSjVYMmxrRHg4M05UYzRNVGsxTnpRek9ESXhPVEJmT0RrM05qRTFNRFV6TnpNMU9UYzBEd1IwYVcxbEJsb2NVb1FC"
  }
}{{< /highlight >}}

Maintenant vous pouvez travailler vos données. Si vous êtes à l&#8217;aise avec le JSON, c&#8217;est parfait. Sinon vous pouvez les convertir en CSV. Par sécurité, j&#8217;évite d&#8217;utiliser les convertisseurs en ligne mais comme les données sont déjà disponibles sur Internet par scraping ou par l&#8217;API Graph&#8230; Par exemple : <https://json-csv.com>

Vous pouvez aussi utiliser des scripts pour accélérer l&#8217;opération. J&#8217;ai trouvé &#8211; mais pas testé &#8211; un script Python qui devrait faire l&#8217;affaire : <https://github.com/naveendennis/Python-Facebook-Extract-Page-Data>

&nbsp;