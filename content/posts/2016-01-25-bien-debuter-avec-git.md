---
title: Bien débuter avec Git
author: Ghislain

date: 2016-01-25T08:53:58+00:00
url: /2016/01/25/bien-debuter-avec-git/
categories:
  - Formation
tags:
  - Bitbucket
  - Git
  - GitHub
  - GitLab
  - Team Foundation Server
  - TFS

---
J’ai utilisé Git de manière assez basique avec GitHub et Bitbucket pour des tests et des POC personnels. Dernièrement j’ai découvert un GitLab dans ma boite, alors que les projets de l’équipe étaient dans un Team Foundation Express 2012. Tout cela Ajoutés à d’autres évènements, cela m’a poussé à conclure qu’il fallait chercher comment bien débuter avec Git.

Je ne vais pas dire que Git est un logiciel de gestion de versions décentralisé contrairement à SVN, TFS, qu’il permet de travailler avec les dépôts de – non exhaustif – GitHub, GitLab, Bitbucket, et qu’il faut utiliser _git init_ pour initier un dépôt. Il existe déjà un grand nombre d’article de ce style.

Je vous propose une sélection de ressources qui permettent de découvrir Git de manière efficace et plaisante.

[Site officiel de Git][1]

## Serious Gaming {#serious-gaming}

### [Try Git – CodeSchool][2] {#try-git--codeschool}

Comme tous les cours de CodeSchool, celui d’introduction a Git est bien construit et fourni les bases nécessaires pour faire ses premiers pas.

<a href="/wp-content/uploads/2016/01/TryGit.png" rel="attachment wp-att-1039"><img class="wp-image-1039 size-medium" src="/wp-content/uploads/2016/01/TryGit-300x295.png?fit=300%2C295" alt="TryGit" srcset="/wp-content/uploads/2016/01/TryGit.png?resize=300%2C295 300w, /wp-content/uploads/2016/01/TryGit.png?resize=768%2C754 768w, /wp-content/uploads/2016/01/TryGit.png?w=931 931w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" /></a>  
_Try Git – CodeSchool_

### [Git Immersion][3] {#git-immersion}

Le credo du site est Parce qu’on retient mieux ce que l’on fait, vous allez donc faire les manipulations sur un projet Ruby sur votre poste.

<a href="/wp-content/uploads/2016/01/GitImmersion.png" rel="attachment wp-att-1040"><img class="wp-image-1040 size-medium" src="/wp-content/uploads/2016/01/GitImmersion-300x258.png?fit=300%2C258" alt="GitImmersion" srcset="/wp-content/uploads/2016/01/GitImmersion.png?resize=300%2C258 300w, /wp-content/uploads/2016/01/GitImmersion.png?resize=768%2C660 768w, /wp-content/uploads/2016/01/GitImmersion.png?resize=1024%2C880 1024w, /wp-content/uploads/2016/01/GitImmersion.png?resize=1020%2C876 1020w, /wp-content/uploads/2016/01/GitImmersion.png?w=1164 1164w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" /></a>  
_Git Immersion_

### [Learn Git Branching][4] {#learn-git-branching}

Un cours pour bien appréhender le branching dans Git. Une belle construction autant sur la progression du cours que l’interface graphique qui permet de visualiser les actions sur les branches.

<a href="/wp-content/uploads/2016/01/LearnGitBranching.png" rel="attachment wp-att-1042"><img class="wp-image-1042 size-medium" src="/wp-content/uploads/2016/01/LearnGitBranching-300x215.png?fit=300%2C215" alt="LearnGitBranching" srcset="/wp-content/uploads/2016/01/LearnGitBranching.png?resize=300%2C215 300w, /wp-content/uploads/2016/01/LearnGitBranching.png?resize=768%2C550 768w, /wp-content/uploads/2016/01/LearnGitBranching.png?resize=1024%2C734 1024w, /wp-content/uploads/2016/01/LearnGitBranching.png?resize=1020%2C731 1020w, /wp-content/uploads/2016/01/LearnGitBranching.png?w=1327 1327w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" /></a>  
_Learn Git Branching_

## MOOC {#mooc}

### [Manage your code with Git and GitHub – OpenClassrooms][5] {#manage-your-code-with-git-and-github--openclassrooms}

Un MOOC disponible en anglais, espagnol et français qui apporte les bases pour travailler avec Git et GitHub.

<a href="/wp-content/uploads/2016/01/OpenClassRooms-Git.png" rel="attachment wp-att-1043"><img class="wp-image-1043 size-medium" src="/wp-content/uploads/2016/01/OpenClassRooms-Git-300x300.png?fit=300%2C300" alt="OpenClassRooms-Git" srcset="/wp-content/uploads/2016/01/OpenClassRooms-Git.png?resize=150%2C150 150w, /wp-content/uploads/2016/01/OpenClassRooms-Git.png?resize=300%2C300 300w, /wp-content/uploads/2016/01/OpenClassRooms-Git.png?w=1002 1002w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" /></a>  
_Manage your code with Git and GitHub – OpenClassrooms_

## Documentation {#documentation}

### [Git – the simple guide][6] {#git--the-simple-guide}

Plus proche de la cheatsheet que de la doc, the simple guide est un récapitulatif des commandes indispensables. _No deep shit &#x1f609;_ comme il est annoncé sur le site.

<a href="/wp-content/uploads/2016/01/GitSimpleGuide.png" rel="attachment wp-att-1041"><img class="wp-image-1041 size-medium" src="/wp-content/uploads/2016/01/GitSimpleGuide-300x158.png?fit=300%2C158" alt="GitSimpleGuide" srcset="/wp-content/uploads/2016/01/GitSimpleGuide.png?resize=300%2C158 300w, /wp-content/uploads/2016/01/GitSimpleGuide.png?resize=768%2C406 768w, /wp-content/uploads/2016/01/GitSimpleGuide.png?w=867 867w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" /></a>  
_Git – The simple guide_

### [La documentation officielle][7] {#la-documentation-officielle}

Quand la documentation officielle est bien faite, il serait dommage de ne pas s’en servir.

…

Et si cela ne suffit pas, sachez que Octocat est la mascotte de GitHub. Il s’agit d’un dessin mignon de chat avec un corps de poulpe.

<a href="/wp-content/uploads/2016/01/octocat-original.png" rel="attachment wp-att-1038"><img class="wp-image-1038 size-full" src="/wp-content/uploads/2016/01/octocat-original.png?fit=200%2C200" alt="octocat-original" srcset="/wp-content/uploads/2016/01/octocat-original.png?resize=150%2C150 150w, /wp-content/uploads/2016/01/octocat-original.png?w=200 200w" sizes="(max-width: 200px) 100vw, 200px" data-recalc-dims="1" /></a>  
_Octocat – GitHub’s mascot_

En parcourant [Octodex][8], vous pourrez trouver des versions thématiques de l’Octocat faites par la communauté. DaftPunk, Mummy, PI, etc.

Vous avez les ressources nécessaires pour bien débuter avec Git. Happy coding.

 [1]: http://git-scm.com/
 [2]: http://try.github.com/
 [3]: http://gitimmersion.com/
 [4]: http://pcottle.github.io/learnGitBranching/
 [5]: https://openclassrooms.com/courses/manage-your-code-with-git-and-github
 [6]: http://rogerdudler.github.io/git-guide/
 [7]: http://git-scm.com/doc
 [8]: https://octodex.github.com/