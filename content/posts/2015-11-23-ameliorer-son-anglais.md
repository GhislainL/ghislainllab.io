---
title: Améliorer son anglais
author: Ghislain

date: 2015-11-23T08:05:27+00:00
url: /2015/11/23/ameliorer-son-anglais/
categories:
  - Développement personnel
tags:
  - anglais
  - anglaisfacile.com
  - duolingo
  - english

---
Dans notre métier, un niveau d’anglais minimum est nécessaire. Nos environnements techniques sont installés en anglais, les informations les plus fraiches sont en anglais, les documentations sont en anglais, les communautés les plus actives sont en anglais. Nous avons généralement un anglais technique nous permettant de nous en sortir. Améliorer son anglais pour échanger de manière compréhensible représente donc un axe d’amélioration professionnel et personnel.

## Duolingo {#duolingo}

Duolingo propose gratuitement un site et une application mobile très userfriendly pour revoir les bases de l’anglais. La gamification omniprésente permet d’apprendre tout en s’amusant et en se lançant des défis.

Les leçons sont construites par thématiques tandis que la progression permet de lisser la difficulté. Ainsi la révision du présent se fait avant le plus que parfait et le vocabulaire lié aux dates avant celui lié aux affaires ou à la politique.

Pour chaque leçon comporte cinq niveaux matérialisés par la jauge latérale. Quand vous êtes complètement opérationnel sur celle-ci, le symbole devient doré. Pour y parvenir il faut répondre à une série de questions qui peuvent être :

  * Traduction anglais vers français
  * Traduction français vers anglais
  * Ecrire la phrase anglaise qui a été prononcée
  * Prononcer de l’anglais une phrase écrite en français
  * Trouver la ou les bonnes réponses pour une traduction
  * Trouver le mot juste parmi plusieurs propositions pour compléter une phrase

[<img class="alignnone size-medium wp-image-959" src="/wp-content/uploads/2015/11/Duolingo-lessons.png?fit=290%2C147" alt="Duolingo-lessons" data-recalc-dims="1" />][1]

Une fois les 55 leçons validées, vous obtiendrez un trophée et un niveau de maitrise de l’anglais.

[<img class="alignnone size-full wp-image-957" src="/wp-content/uploads/2015/11/Duolingo-trophy.png?fit=142%2C140" alt="Duolingo-trophy" data-recalc-dims="1" />][2]

Comme Duolingo n’a pas la prétention de vous rendre bilingue, le niveau de maitrise est limité à environ 50% d’après les commentaires sur le forum. Au moment où j’écris cet article, mon niveau est de 48%.

[<img class="wp-image-965 size-full" src="/wp-content/uploads/2015/11/Master.png?fit=186%2C154" alt="Maitrise d'une langue" data-recalc-dims="1" />][3]  
_Ma maitrise actuelle de l’anglais_

Le système est intéressant car sans assiduité les niveaux baissent, il faut donc pratiquer pour maintenir ses connaissances.

Pour aller plus loin, vous pourrez participer à des traductions ou des échanges dans la communauté.

<http://duolingo.com/>

## AnglaisFacile.com {#anglaisfacilecom}

AnglaisFacile.com est un site français très complet et gratuit pour apprendre ou s’améliorer en anglais. Son interface semble dater des années 90 mais son contenu est une référence.

Chaque niveau comporte des thématiques et chaque thématique comporte 3 leçons, elle-même composée d’un cours théorique et d’un exercice de validation. Une sensation de bienveillance transparait par cet accompagnement pédagogique. Vous pourrez en bénéficier dans tous les thèmes suivants:

  * Débutant : 53 thèmes
  * Intermédiaire : 43 thèmes
  * Avancé : 58 thèmes

[<img class="alignnone wp-image-958 size-full" src="/wp-content/uploads/2015/11/AnglaisFacile-screen.png?fit=785%2C401" alt="AnglaisFacile-screen" srcset="/wp-content/uploads/2015/11/AnglaisFacile-screen.png?resize=300%2C153 300w, /wp-content/uploads/2015/11/AnglaisFacile-screen.png?w=785 785w" sizes="(max-width: 785px) 100vw, 785px" data-recalc-dims="1" />][4]

Afin de varier les plaisirs, le site vous proposera aussi des quizz et des jeux en anglais. Cela permet de s’amuser tout en progressant en anglais.

La communauté est très active et vous pourrez trouver des thématiques sur des horizons très variés (aéronautique, plongée sous-marine, …).

Même si l’habillage ou l’ergonomie demande plus d’effort que Duolingo, le contenu et la richesse d’AnglaisFacile.com le mérite amplement.

<http://www.anglaisfacile.com/>

## Valider son niveau en anglais. {#valider-son-niveau-en-anglais}

Plusieurs examens existent pour valider son niveau anglais dont voici les plus connus:

  * TOEFL (*): Test of English as a Foreign Language
  * TOEIC (*): Test of English for International Communication
  * IELTS: International English Language Testing System
  * BULATS (*): Business Language Testing Service

(*) : éligible CPF (Compte Personnel de Formation)

Vous me demanderez pourquoi je parle d’examen alors que l’article parle d’améliorer son anglais ?

Deux raisons.

Premièrement il peut s’agir d’une bonne motivation pour se décider à améliorer son anglais.

Deuxièmement vous trouverez une grande quantité d’exercices pour préparer ces examens en cherchant sur Internet.

## Le plaisir {#le-plaisir}

Le plus d’avoir un niveau convenable en anglais réside aussi dans le plaisir et le confort  :

  * Discuter plus facilement durant vos voyages à l’étranger ;
  * Lire des livres non traduits en français (technique, développement personnel, roman, comics) ;
  * Regarder des films ou séries en VO pour mieux apprécier le jeu d’acteur et l’ambiance.

Le meilleur c’est qu’il s’agit d’un cercle vertueux.

 

Qu’est ce qui vous motive à améliorer votre anglais et qu’allez vous entreprendre pour vous améliorer ?

 [1]: /wp-content/uploads/2015/11/Duolingo-lessons.png
 [2]: /wp-content/uploads/2015/11/Duolingo-trophy.png
 [3]: /wp-content/uploads/2015/11/Master.png
 [4]: /wp-content/uploads/2015/11/AnglaisFacile-screen.png