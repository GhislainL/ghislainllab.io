---
title: 'Active Directory avec PowerShell : les basiques'
author: Ghislain

date: 2015-05-26T11:57:10+00:00
url: /2015/05/26/active-directory-avec-powershell-les-basiques/
categories:
  - PowerShell
tags:
  - Active Directory

---
Si comme moi, vous utilisez Active Directory de manière basique, cet article est fait pour vous. Vous y découvrirez le B.A. BA pour manipuler Active Directory avec PowerShell.

## Pré-requis {#pré-requis}

Il est possible que vous ne disposiez pas des commandes PowerShell lié à Active Directory suivant la configuration du serveur. Pour pallier à cela, vous devez ajouter le module Active Directory pour PowerShell. Deux possibilités s’offrent à vous :

  * PowerShell

{{< highlight powershell>}}Import-Module ServerManager
Add-WindowsFeature RSAT-AD-PowerShell</pre>

  * Configuration manuelle

– Server Manager

– Features

– Add features

– Remote Server Administration Tools > Role Administration Tools > AD DS and AD LDS Tools > Active Directory module for Windows PowerShell

[<img class="alignnone wp-image-801 size-full" src="/wp-content/uploads/2015/05/ServerManager-AD-PS-Feature.png?fit=779%2C573" alt="ServerManager-AD-PS-Feature" srcset="/wp-content/uploads/2015/05/ServerManager-AD-PS-Feature.png?resize=300%2C221 300w, /wp-content/uploads/2015/05/ServerManager-AD-PS-Feature.png?w=779 779w" sizes="(max-width: 779px) 100vw, 779px" data-recalc-dims="1" />][1]

Pour importer le module Active Directory :

{{< highlight powershell>}}Import-Module ActiveDirectory
# lister les commandlets du module
Get-Command –Module ActiveDirectory</pre>

## Comptes Active Directory {#comptes-active-directory}

### Trouver un compte AD {#trouver-un-compte-ad}

{{< highlight powershell>}}Get-ADUser U2BH3E
DistinguishedName : CN=LERDA Ghislain,OU=Standard,OU=Users,DC=societe,DC=org
Enabled           : True
GivenName         : Ghislain
Name             : LERDA Ghislain
ObjectClass       : user
ObjectGUID       : 4ce91fbf-989a-47dc-97b5-363298825252
SamAccountName   : lerdagh
SID               : S-1-5-21-484763869-861567501-725345543-67414
Surname           : LERDA
UserPrincipalName : ghislain.lerda@mail.com

Get-ADUser -Filter {GivenName -eq "Ghislain"}
DistinguishedName : CN=LERDA Ghislain,OU=Standard,OU=Users,DC=societe,DC=org
Enabled           : True
GivenName        : Ghislain
Name             : LERDA Ghislain
ObjectClass       : user
ObjectGUID       : 4ce91fbf-989a-47dc-97b5-363298825252
SamAccountName   : lerdagh
SID               : S-1-5-21-484763869-861567501-725345543-67414
Surname           : LERDA
UserPrincipalName : ghislain.lerda@mail.com</pre>

Si plusieurs résultats, Get-ADUser renvoie un tableau. Voici donc un script permet de requêter selon une propriété et de récupérer quelques données au format CSV.

{{< highlight powershell>}}Import-Module ActiveDirectory -EA 0
function GetUsersInfo{
   param(
      $filter,
      $arrValues
   )
   foreach( $val in $arrValues){
      $found = Get-ADUser -Filter {$filter -eq $val}
      if($found){
         foreach($f in $found){
            "$val ; $($f.Name) ;$($f.SamAccountName)"
         }
      }else{
         "$val"
      }
   }
}

# Exemples d'appel
GetUsersInfo -filter "GivenName" -arrValues @("Ghislain")
GetUsersInfo -filter "SurName" - arrValues @("Lerda")</pre>

### Activer / désactiver un compte AD {#activer--désactiver-un-compte-ad}

{{< highlight powershell>}}# Activer un compte
Enable-ADAccount Login
# Désactiver un compte
Disable-ADAccount Login
# Désactiver plusieurs comptes AD
Get-ADGroupMember "BlackList" | Disable-ADAccount</pre>

### Déverrouiller un compte {#déverrouiller-un-compte}

{{< highlight powershell>}}# Déverouiller un compte AD
Unlock-ADAccount Login</pre>

### Supprimer un compte AD {#supprimer-un-compte-ad}

Vous devez manipuler cette commande avec circonspection vu la simplicité de sa syntaxe

{{< highlight powershell>}}# Supprimer un compte AD
Remove-ADUser Login

# Supprimer plusieurs comptes AD
Get-ADUser -Filter {GivenName -eq "BadBoy"} | Remove-ADUser</pre>

### Changer le mot de passe {#changer-le-mot-de-passe}

{{< highlight powershell>}}$newPassword=Read-Host "Enter the new password" –AsSecureString
Set-ADAccountPassword Login -NewPassword $new

# Obliger l’utilisateur a changer de mot de passe à la prochaine connexion
Set-ADUser Login -ChangePasswordAtLogon $True</pre>

### Ajouter un compte dans un groupe {#ajouter-un-compte-dans-un-groupe}

{{< highlight powershell>}}# ajouter un compte dans un groupe
Add-ADGroupMember "Nom_Du_Groupe" -Members Login

# Ajouter plusieurs comptes AD dans un groupe
Add-ADGroupMember "BlackList" -Member (Get-ADUser -Filter {GivenName -eq "BadBoy"})</pre>

## AD PSDrive {#ad-psdrive}

Le module Active Directory de PowerShell apporte aussi le PSDrive d’Active Directory. Cela vous permettra de parcourir votre AD comme s’il s’agissait d’un disque dur (avec cd et dir). Les domaines, OU et autres containers se comportent comme des dossiers.

{{< highlight powershell>}}cd AD:
dir</pre>

**Pour aller plus loin :**

[Hey, Scripting Guy! Blog – Active Directory Week: Get Started with Active Directory PowerShell][2]

[Si comme moi, vous utilisez Active Directory de manière basique, cet article est fait pour vous. Vous y découvrirez le B.A. BA pour manipuler Active Directory avec PowerShell.

## Pré-requis {#pré-requis-1}

Il est possible que vous ne disposiez pas des commandes PowerShell lié à Active Directory suivant la configuration du serveur. Pour pallier à cela, vous devez ajouter le module Active Directory pour PowerShell. Deux possibilités s’offrent à vous :

  * PowerShell

{{< highlight powershell>}}Import-Module ServerManager
Add-WindowsFeature RSAT-AD-PowerShell</pre>

  * Configuration manuelle

– Server Manager

– Features

– Add features

– Remote Server Administration Tools > Role Administration Tools > AD DS and AD LDS Tools > Active Directory module for Windows PowerShell

[<img class="alignnone wp-image-801 size-full" src="/wp-content/uploads/2015/05/ServerManager-AD-PS-Feature.png?fit=779%2C573" alt="ServerManager-AD-PS-Feature" srcset="/wp-content/uploads/2015/05/ServerManager-AD-PS-Feature.png?resize=300%2C221 300w, /wp-content/uploads/2015/05/ServerManager-AD-PS-Feature.png?w=779 779w" sizes="(max-width: 779px) 100vw, 779px" data-recalc-dims="1" />][1]

Pour importer le module Active Directory :

{{< highlight powershell>}}Import-Module ActiveDirectory
# lister les commandlets du module
Get-Command –Module ActiveDirectory{{< /highlight >}}

## Comptes Active Directory {#comptes-active-directory-1}

### Trouver un compte AD {#trouver-un-compte-ad-1}

{{< highlight powershell>}}Get-ADUser U2BH3E
DistinguishedName : CN=LERDA Ghislain,OU=Standard,OU=Users,DC=societe,DC=org
Enabled           : True
GivenName         : Ghislain
Name             : LERDA Ghislain
ObjectClass       : user
ObjectGUID       : 4ce91fbf-989a-47dc-97b5-363298825252
SamAccountName   : lerdagh
SID               : S-1-5-21-484763869-861567501-725345543-67414
Surname           : LERDA
UserPrincipalName : ghislain.lerda@mail.com

Get-ADUser -Filter {GivenName -eq "Ghislain"}
DistinguishedName : CN=LERDA Ghislain,OU=Standard,OU=Users,DC=societe,DC=org
Enabled           : True
GivenName        : Ghislain
Name             : LERDA Ghislain
ObjectClass       : user
ObjectGUID       : 4ce91fbf-989a-47dc-97b5-363298825252
SamAccountName   : lerdagh
SID               : S-1-5-21-484763869-861567501-725345543-67414
Surname           : LERDA
UserPrincipalName : ghislain.lerda@mail.com{{< /highlight >}}

Si plusieurs résultats, Get-ADUser renvoie un tableau. Voici donc un script permet de requêter selon une propriété et de récupérer quelques données au format CSV.

{{< highlight powershell>}}Import-Module ActiveDirectory -EA 0
function GetUsersInfo{
   param(
      $filter,
      $arrValues
   )
   foreach( $val in $arrValues){
      $found = Get-ADUser -Filter {$filter -eq $val}
      if($found){
         foreach($f in $found){
            "$val ; $($f.Name) ;$($f.SamAccountName)"
         }
      }else{
         "$val"
      }
   }
}

# Exemples d'appel
GetUsersInfo -filter "GivenName" -arrValues @("Ghislain")
GetUsersInfo -filter "SurName" - arrValues @("Lerda"){{< /highlight >}}

### Activer / désactiver un compte AD {#activer--désactiver-un-compte-ad-1}

{{< highlight powershell>}}# Activer un compte
Enable-ADAccount Login
# Désactiver un compte
Disable-ADAccount Login
# Désactiver plusieurs comptes AD
Get-ADGroupMember "BlackList" | Disable-ADAccount{{< /highlight >}}

### Déverrouiller un compte {#déverrouiller-un-compte-1}

{{< highlight powershell>}}# Déverouiller un compte AD
Unlock-ADAccount Login{{< /highlight >}}

### Supprimer un compte AD {#supprimer-un-compte-ad-1}

Vous devez manipuler cette commande avec circonspection vu la simplicité de sa syntaxe

{{< highlight powershell>}}# Supprimer un compte AD
Remove-ADUser Login

# Supprimer plusieurs comptes AD
Get-ADUser -Filter {GivenName -eq "BadBoy"} | Remove-ADUser{{< /highlight >}}

### Changer le mot de passe {#changer-le-mot-de-passe-1}

{{< highlight powershell>}}$newPassword=Read-Host "Enter the new password" –AsSecureString
Set-ADAccountPassword Login -NewPassword $new

# Obliger l’utilisateur a changer de mot de passe à la prochaine connexion
Set-ADUser Login -ChangePasswordAtLogon $True{{< /highlight >}}

### Ajouter un compte dans un groupe {#ajouter-un-compte-dans-un-groupe-1}

{{< highlight powershell>}}# ajouter un compte dans un groupe
Add-ADGroupMember "Nom_Du_Groupe" -Members Login

# Ajouter plusieurs comptes AD dans un groupe
Add-ADGroupMember "BlackList" -Member (Get-ADUser -Filter {GivenName -eq "BadBoy"})
{{< /highlight >}}

## AD PSDrive {#ad-psdrive-1}

Le module Active Directory de PowerShell apporte aussi le PSDrive d’Active Directory. Cela vous permettra de parcourir votre AD comme s’il s’agissait d’un disque dur (avec cd et dir). Les domaines, OU et autres containers se comportent comme des dossiers.

{{< highlight powershell>}}cd AD:
dir{{< /highlight >}}

**Pour aller plus loin :**

[Hey, Scripting Guy! Blog – Active Directory Week: Get Started with Active Directory PowerShell][2]

](http://www.tomsitpro.com/articles/powershell-active-directory-cmdlets,2-801.html)

 [1]: /wp-content/uploads/2015/05/ServerManager-AD-PS-Feature.png
 [2]: http://blogs.technet.com/b/heyscriptingguy/archive/2014/11/24/active-directory-week-get-started-with-active-directory-powershell.aspx