---
title: Comment nettoyer la base Usage and Health
author: Ghislain

date: 2015-03-18T08:30:32+00:00
url: /2015/03/18/comment-nettoyer-la-base-usage-and-health/
categories:
  - PowerShell
  - SharePoint
tags:
  - PowerShell
  - SharePoint
  - SharePoint 2013
  - SPUsageDefinition
  - usage and health

---
Dernièrement j’ai dû installer un nouveau serveur de développement SharePoint 2013. [AutoSpInstaller][1] facilite grandement la vie. Mais au bout d’un moment, le disque de données est plein. Souvenez vous de la notification Windows _Low disk space warning_, tellement plein qu’aucun 0 ou 1 ne pouvait s’y inscrire.

[<img class="wp-image-648 size-full" src="/wp-content/uploads/2015/03/YourDriveIsFull.png?fit=191%2C71" alt="YourDriveIsFull" data-recalc-dims="1" />][2]  
_Votre disque est plein_

Un utilitaire d’analyse de disque m’indique de la base Usage And Health prend plus de 20Go. Vous en conviendrez c’est exagérer pour un environnement de développement.

## 1 – Réduire la durée de rétention des informations d’usage {#1--réduire-la-durée-de-rétention-des-informations-dusage}

{{< highlight powershell>}}Get-SPUsageDefinition | ForEach-Object {Set-SPUsageDefinition -Identity $_.name -DaysRetained 1}</pre>

## 2 – Exécuter le timer job qui va purger la table {#2--exécuter-le-timer-job-qui-va-purger-la-table}

{{< highlight powershell>}}Get-SPTimerJob | Where-Object { $_.title -like "*usage data*" } | Start-SPTimerJob</pre>

## 3 – Effectuer un shrink de la base Usage And Health dans SQL Management Studio {#3--effectuer-un-shrink-de-la-base-usage-and-health-dans-sql-management-studio}

[<img class="wp-image-634 size-full" title="Skrink de la base Usage And Health" src="/wp-content/uploads/2015/03/UsageAndHealth.png?fit=705%2C632" alt="UsageAndHealth" srcset="/wp-content/uploads/2015/03/UsageAndHealth.png?resize=300%2C269 300w, /wp-content/uploads/2015/03/UsageAndHealth.png?w=705 705w" sizes="(max-width: 705px) 100vw, 705px" data-recalc-dims="1" />][3]  
_Espace libre disponible sur la base Usage And Health_

Voilà une petite astuce qui peut sauver votre environnement.

## Mise à jour du 27/03/2015 {#mise-à-jour-du-27032015}

La commande **Set-SPUsageService -UsageLogMaxSpaceGB X** qui permettait de limiter la taille maximale ne fonctionne pas pour SharePoint 2013.

Si vous voulez vous débarrasser complètement de ces informations.

{{< highlight powershell>}}Set-SPUsageService -LoggingEnabled $False

Get-SPUsageDefinition | ForEach-Object {Set-SPUsageDefinition -Identity $_.name -Enable:$False}</pre>

 [1]: %20http://autospinstaller.codeplex.com/
 [2]: /wp-content/uploads/2015/03/YourDriveIsFull.png
 [3]: /wp-content/uploads/2015/03/UsageAndHealth.png