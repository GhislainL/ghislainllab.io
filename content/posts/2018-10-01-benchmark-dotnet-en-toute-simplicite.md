---
title: Benchmark dotnet en toute simplicité
author: Ghislain

date: 2018-10-01T13:59:45+00:00
url: /2018/10/01/benchmark-dotnet-en-toute-simplicite/
featuredImage: /wp-content/uploads/2018/10/BenchmarkDotNet.png
categories:
  - dotNet
  - DotNet Core
tags:
  - benchmark
  - DotNet Core

---
Le benchmark ou test de performance permet &#8211; en informatique &#8211; de comparer les performances de plusieurs solutions. La méthode la plus simple consiste à utiliser la méthode Stopwatch() mais elle se révèle rapidement limitée. Découvrons [BenchmarkDotNet][1], un framework de benchmark open-source pour dotnet, que j&#8217;ai utilisé sur le projet [LeetMe][2]. &#8220;_The quick brown fox jumps over the lazy dog_&#8221; s&#8217;écrit en leet &#8220;_T#3 qu!(k 8r0w~ f0* jump5 0v3r 7#3 142y d09_&#8220;. Nous allons voir comment déterminer la méthode qui l&#8217;écrit le plus rapidement.

<!--more-->

LeetMe remplace des caractères par d&#8217;autres en se basant sur un dictionnaire. L&#8217;aspect algorithmie (for ascendant, for descendant, foreach) ainsi que l&#8217;aspect technique (un tableau, un dictionnaire, une liste) sont amusants mais il faut savoir ce qui fonctionne le mieux. Pour le déterminer, j&#8217;ai implémenté chaque possibilité dans une classe distincte.

ArrayInline : tableaux déclarés à la volée dans un tableau

ArrayDefined : tableaux instanciés puis ajoutés dans un tableau

DicoCharArray : tableaux instanciés puis ajoutés dans un dictionnaire

DicoCharList : listes instanciés puis ajoutées dans un dictionnaire

L&#8217;objectif est de comparer la rapidité d&#8217;exécution et la consommation mémoire de plusieurs méthodes qui réalisent la même tache.

Le code suivant permet de tester et comparer les 4 méthodes décorée par _[Benchmark]_ avec Benchmarkdotnet. Chaque méthode est appelée plusieurs fois afin de pouvoir construire des statistiques correctes sur la moyenne et l&#8217;écart de temps d&#8217;exécution et sur la consommation de mémoire _[MemoryDiagnoser]_.

&nbsp;

{{< highlight csharp >}}using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using System;

    [CoreJob]
    [RPlotExporter, RankColumn]
    [MemoryDiagnoser]
    public class BenchLeet
    {
        private readonly string sentence;

        private readonly LeetMe.LeetMeArrayInline LeetMeInstance1 = new LeetMe.LeetMeArrayInline();
        private readonly LeetMe.LeetMeArrayDefined LeetMeForInstance2 = new LeetMe.LeetMeArrayDefined();
        private readonly LeetMe.LeetMeDicoCharArray LeetMeInstance3 = new LeetMe.LeetMeDicoCharArray();
        private readonly LeetMe.LeetMeDicoCharList LeetMeForInstance4 = new LeetMe.LeetMeDicoCharList();

        public BenchLeet()
        {
            // a random text
            sentence = LoremNET.Lorem.Sentence(5, 10);
        }

        [Benchmark]
        public string NoobArrayInline()
        {
            return LeetMeUpInstance1.Translate(sentence, LeetMe.LeetLevel.Noob);
        }

        [Benchmark]
        public string NoobArrayDefined()
        {
            return LeetMeUpInstance2.Translate(sentence, LeetMe.LeetLevel.Noob);
        }

        [Benchmark]
        public string NoobDicoCharArray()
        {
            return LeetMeUpForDescDeInstance3.Translate(sentence, LeetMe.LeetLevel.Noob);
        }

        [Benchmark]
        public string NoobDicoCharList()
        {
            return LeetMeUpForDescInstance4.Translate(sentence, LeetMe.LeetLevel.Noob);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run();
            Console.ReadLine();
        }
    }
{{< /highlight >}}

Lancer en mode Release sinon vous aurez un message d&#8217;erreur.

Après un certain temps et beaucoup de ligne dans la fenêtre d&#8217;exécution

{{< highlight plain >}}// * Summary *

BenchmarkDotNet=v0.11.1, OS=Windows 10.0.14393.2363 (1607/AnniversaryUpdate/Redstone1), VM=VMware
Intel Xeon CPU E5-2650 v4 2.20GHz, 1 CPU, 2 logical and 2 physical cores
Frequency=2148435 Hz, Resolution=465.4551 ns, Timer=TSC
.NET Core SDK=2.1.400
  [Host] : .NET Core 2.1.2 (CoreCLR 4.6.26628.05, CoreFX 4.6.26629.01), 64bit RyuJIT  [AttachedDebugger]
  Core   : .NET Core 2.1.2 (CoreCLR 4.6.26628.05, CoreFX 4.6.26629.01), 64bit RyuJIT

Job=Core  Runtime=Core

            Method |     Mean |     Error |    StdDev | Rank |  Gen 0 | Allocated |
------------------ |---------:|----------:|----------:|-----:|-------:|----------:|
   NoobArrayInline | 5.093 us | 0.1071 us | 0.1603 us |    2 | 1.0834 |   6.77 KB |
  NoobArrayDefined | 3.982 us | 0.0631 us | 0.0527 us |    1 | 0.5493 |   3.45 KB |
 NoobDicoCharArray | 5.048 us | 0.0996 us | 0.2329 us |    2 | 0.4807 |   3.05 KB |
  NoobDicoCharList | 6.497 us | 0.1276 us | 0.1614 us |    3 | 0.9613 |   6.01 KB |
{{< /highlight >}}

La présentation de BenchmarkDotNet est maintenant finie. J&#8217;espère vous avoir présenter un outil intéressant à l&#8217;aide d&#8217;un exemple ludique. 

**Références :**

[BenchmarkDotNet][1]
  
[LeetMe][2]

 [1]: https://benchmarkdotnet.org
 [2]: https://github.com/GhislainL/LeetMe