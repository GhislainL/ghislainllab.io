---
title: "Mesurer la durée d'exécution d'un script PowerShell"
author: Ghislain

date: 2014-12-01T08:22:41+00:00
url: /2014/12/01/mesurer-la-duree-d039execution-d039un-script-powershell/
categories:
  - PowerShell

---
Quand vous  avez des scripts PowerShell qui demandent un certains temps d’exécution, vous avez besoin d’avoir une estimation précise pour planifier une exécution.

**Measure-Command** est la commande la plus simple.

{{< highlight powershell>}}PS C:&gt;Measure-Command { .monScript.ps1 }

Days : 0
Hours : 0
Minutes : 0
Seconds : 0
Milliseconds : 257
Ticks : 2576708
TotalDays : 2,98230092592593E-06
TotalHours : 7,15752222222222E-05
TotalMinutes : 0,00429451333333333
TotalSeconds : 0,2576708
TotalMilliseconds : 257,6708{{< /highlight >}}

Le désavantage de cette commande est que vous ne voyez plus la sortie standard à moins d’utiliser l’astuce suivante.

{{< highlight powershell>}}#Afficher l'exécution
PS C:&gt; Measure-Command { .monScript.ps1 | Out-Default }

#Loguer l'éxécution
PS C:&gt; Start-Transcript ts-monScript.txt
PS C:&gt; Measure-Command { .monScript.ps1 | Out-Default }
PS C:&gt; Stop-Transcript{{< /highlight >}}

Vous en conviendrez, Measure-Command n’est plus vraiment adaptée. Pour être simple et efficace, vous préfèrerez l’objet .net **Stopwatch**.

{{< highlight powershell >}}Start-Transcript ts-monScript.txt
$sw = [Diagnostics.Stopwatch]::StartNew()
#contenu de monScript.ps1
$sw.Stop()
$sw.Elapsed
Stop-Transcript{{< /highlight >}}

Vous avez maintenant la possibilité de créer simplement des scripts dont toute l’exécution est tracée et minutée.

**Références:**

[Using the Measure-Command Cmdlet][1]

[Stopwatch (msdn)][2]

[Start-Transcript/Stop-Transcript][3]

 [1]: http://technet.microsoft.com/en-us/library/ee176899.aspx
 [2]: http://msdn.microsoft.com/fr-fr/library/system.diagnostics.stopwatch%28v=vs.110%29.aspx
 [3]: http://ghislainfo.wordpress.com/2014/10/30/loguer-facilement-avec-powershell/ "Loguer facilement avec PowerShell"