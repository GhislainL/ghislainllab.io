---
title: Bonnes pratiques de déploiement des formulaires InfoPath
author: Ghislain

date: 2015-01-19T08:26:05+00:00
url: /2015/01/19/bonnes-pratiques-de-deploiement-des-formulaires-infopath/
categories:
  - SharePoint
tags:
  - InfoPath
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013

---
Vos utilisateurs avancés créent des formulaires InfoPath dans leurs sites mais les mettre à jour se fait sans filet.

Vous devez faire évoluer une application composite utilisant des formulaires InfoPath qui est déjà en production.

Comment travailler de manière pérenne et mettre en place un filet de sécurité pour vous et vos utilisateurs ?

En utilisant **InfoPath Forms Service**.

Vous pourrez faire évoluer vos formulaires de manière sûre et travailler sur plusieurs environnements en séparant les connexions de données des modèles de formulaire.

[<img class="alignnone size-full wp-image-409" src="/wp-content/uploads/2014/12/infopathformsservicesdiagram.png?fit=487%2C358" alt="InfoPathFormsServicesDiagram" srcset="/wp-content/uploads/2014/12/infopathformsservicesdiagram.png?resize=300%2C221 300w, /wp-content/uploads/2014/12/infopathformsservicesdiagram.png?w=487 487w" sizes="(max-width: 487px) 100vw, 487px" data-recalc-dims="1" />][1]

<table>
  <tr>
    <td width="614">
      <strong>Note : </strong>
    </td>
  </tr>
  
  <tr>
    <td width="614">
      InfoPath est une ligne de produit qui n’évoluera plus (1). Mais le support sera assuré par Microsoft jusqu’en avril 2023. A l’écriture de l’article, aucun remplaçant n’est annoncé clairement. Tobias Zimmergren a fait un état des lieux de la situation (2).
    </td>
  </tr>
</table>

<!--more-->

InfoPath Forms Services est un service SharePoint accessible depuis la centrale d’administration de SharePoint qui permet la centralisation des formulaires InfoPath et des connexions de données ainsi que leurs diffusions au sein de la ferme.

[<img class="wp-image-408 size-full" src="/wp-content/uploads/2014/12/infopathformsservices.png?fit=561%2C72" alt="InfoPathFormsServices" srcset="/wp-content/uploads/2014/12/infopathformsservices.png?resize=300%2C39 300w, /wp-content/uploads/2014/12/infopathformsservices.png?w=561 561w" sizes="(max-width: 561px) 100vw, 561px" data-recalc-dims="1" />][2]  
_InfoPath Forms Services dans la centrale d’administration SP2013_

# Publication des formulaires et connexions de données {#publication-des-formulaires-et-connexions-de-données}

Dans la pratique, vous devrez effectuer les opérations suivantes pour publier un formulaire et les connexions de données associées :

  1. Concevoir le formulaire dans une bibliothèque de document de test.
  2. Signer le formulaire InfoPath ou le définir comme un formulaire approuvé par l’administrateur
  3. Gérer les connexions de données 
      1. Convertir les connexions de données en fichier (.udcx) dans une bibliothèque locale
      2. Modifier les connexions de données pour bibliothèque de connexion à gestion centralisée
      3. Télécharger tous les fichiers de connexion dans InfoPath Forms Services
  4. Publier le formulaire InfoPath dans InfoPath Forms Services
  5. Activer le formulaire pour une ou plusieurs collections de site spécifiques
  6. Autoriser les types de contenu dans la bibliothèque de formulaire et ajouter le type de contenu Microsoft InfoPath> monFormulaire

# Mise à jour de formulaire {#mise-à-jour-de-formulaire}

Lors de la mise à jour d’un formulaire, les modifications sont poussées à toutes les collections de site consommatrices (comme avec le concentrateur de type de contenu). Il faut alors spécifier si les enregistrements avec la précédente version du formulaire sont compatibles ou perdus.

Exemple simple en utilisant la compatibilité à chaque fois

<table>
  <tr>
    <td width="125">
    </td>
    
    <td width="64">
      Champ1
    </td>
    
    <td width="64">
      Champ2 (promu)
    </td>
    
    <td width="81">
      Champ3 (promu)
    </td>
  </tr>
  
  <tr>
    <td width="125">
      V1
    </td>
    
    <td width="64">
      OK
    </td>
    
    <td width="64">
      OK
    </td>
    
    <td width="81">
      &#8211;
    </td>
  </tr>
  
  <tr>
    <td width="125">
      V2
    </td>
    
    <td width="64">
      OK
    </td>
    
    <td width="64">
      OK
    </td>
    
    <td width="81">
      OK
    </td>
  </tr>
  
  <tr>
    <td width="125">
      V3
    </td>
    
    <td width="64">
      OK
    </td>
    
    <td width="64">
      Supprimé
    </td>
    
    <td width="81">
      OK
    </td>
  </tr>
</table>

[<img class="wp-image-404 size-full" src="/wp-content/uploads/2014/12/infopath-v1.png?fit=682%2C403" alt="InfoPath.v1" srcset="/wp-content/uploads/2014/12/infopath-v1.png?resize=300%2C177 300w, /wp-content/uploads/2014/12/infopath-v1.png?w=682 682w" sizes="(max-width: 682px) 100vw, 682px" data-recalc-dims="1" />][3]  
_Formulaire InfoPath v1_ [<img class="wp-image-406 size-full" src="/wp-content/uploads/2014/12/infopath-v2-2.png?fit=650%2C244" alt="InfoPath.v2-2" srcset="/wp-content/uploads/2014/12/infopath-v2-2.png?resize=300%2C113 300w, /wp-content/uploads/2014/12/infopath-v2-2.png?w=650 650w" sizes="(max-width: 650px) 100vw, 650px" data-recalc-dims="1" />][4]  
_Formulaire InfoPath v2_ [<img class="wp-image-407 size-full" src="/wp-content/uploads/2014/12/infopath-v3.png?fit=663%2C245" alt="InfoPath.v3" srcset="/wp-content/uploads/2014/12/infopath-v3.png?resize=300%2C111 300w, /wp-content/uploads/2014/12/infopath-v3.png?w=663 663w" sizes="(max-width: 663px) 100vw, 663px" data-recalc-dims="1" />][5]  
_Formulaire InfoPath v3_

Voilà qui devrait faciliter l’utilisation et la maintenance des formulaires InfoPath dans vos fermes SharePoint.

**Références :**

(1) : <http://blogs.office.com/2014/01/31/update-on-infopath-and-sharepoint-forms/>

(2) : <http://zimmergren.net/business/the-future-of-forms-with-sharepoint-and-office-365>

[InfoPath Forms Services in SharePoint Server 2010][6]

[InfoPath forms administration (SharePoint Server 2010)][7]

 [1]: /wp-content/uploads/2014/12/infopathformsservicesdiagram.png
 [2]: /wp-content/uploads/2014/12/infopathformsservices.png
 [3]: /wp-content/uploads/2014/12/infopath-v1.png
 [4]: /wp-content/uploads/2014/12/infopath-v2-2.png
 [5]: /wp-content/uploads/2014/12/infopath-v3.png
 [6]: http://msdn.microsoft.com/en-us/library/office/aa701145%28v=office.14%29.aspx
 [7]: http://technet.microsoft.com/en-us/library/cc303431%28v=office.14%29.aspx