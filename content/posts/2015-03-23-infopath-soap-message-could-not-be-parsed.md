---
title: 'InfoPath : SOAP message could not be parsed'
author: Ghislain

date: 2015-03-23T08:40:07+00:00
url: /2015/03/23/infopath-soap-message-could-not-be-parsed/
categories:
  - SharePoint
tags:
  - calculated column
  - champ calculé
  - InfoPath
  - SharePoint
  - SharePoint 2010

---
Dernièrement, un utilisateur vient me demander pourquoi il ne peut plus publier son formulaire InfoPath attaché à une bibliothèque SharePoint. Il vient d’ajouter une règle de validation et lorsqu’il tente de le republier, il se fait agresser – après une longue attente – par le message d’erreur suivant : **Impossible d’analyser le message SOAP**.

[<img class="wp-image-607 size-full" src="/wp-content/uploads/2015/03/InfoPath-SoapMessageCouldNotBeParsed.png?fit=670%2C269" alt="InfoPath-SoapMessageCouldNotBeParsed" srcset="/wp-content/uploads/2015/03/InfoPath-SoapMessageCouldNotBeParsed.png?resize=300%2C120 300w, /wp-content/uploads/2015/03/InfoPath-SoapMessageCouldNotBeParsed.png?w=670 670w" sizes="(max-width: 670px) 100vw, 670px" data-recalc-dims="1" />][1]  
_Erreur InfoPath lors de la publication : Impossible d’analyser le message SOAP_

La nouvelle règle de validation semble propre, donc est mise hors de cause.

Si je tente juste de rajouter du texte dans le formulaire, j’obtiens le même message d’erreur.

Avant d’aller plus loin et pour éviter de faire des bêtises, je restore la collection de site sur un environnement de développement. J’obtiens le message d’erreur mais en anglais : **SOAP message could not be parsed**.

Comme la bibliothèque comporte plusieurs milliers d’éléments, il s’agit peut-être d’un time-out. Augmenter le timeout d’InfoPath Forms Services ne change rien.

A ce moment je me souviens qu’à l’ouverture du formulaire, InfoPath dit qu’il aimerait mettre à jour le formulaire pour intégrer les nouvelles colonnes. La comparaison entre les colonnes du formulaire et celles de la bibliothèque montre que les nouvelles colonnes sont de type champ calculé.

Je supprime ces colonnes puis modifie le formulaire et finalement clique sur le bouton publication rapide. Le symbole d’attente apparait, une petite tension aussi, et la publication réussit.

**Morale de l’histoire** : si vous utilisez des formulaires InfoPath sur une bibliothèque qui contient beaucoup d’éléments, vous allez avoir des soucis. Il semblerait que la publication d’un formulaire, entraine le recalcul de toutes les colonnes de type champ calculé pour tous les éléments.

**Solutions** :

–    Remplacer les formules calculées avant la mise à jour du formulaire et les rétablir ensuite.

–    Supprimer les colonnes calculées

–    En augmentant le timeout à des valeurs indécentes, cela résout éventuellement le problème.

 [1]: /wp-content/uploads/2015/03/InfoPath-SoapMessageCouldNotBeParsed.png