---
title: Utiliser le peoplepicker côté client de SharePoint
author: Ghislain

date: 2016-09-01T00:00:00+00:00
url: /2016/09/01/utiliser-le-peoplepicker-cote-client-de-sharepoint/
categories:
  - JavaScript
  - SharePoint

---
Le peoplepicker est un composant SharePoint qui permet de sélectionner des utilisateurs ou des groupes. Il existe une version serveur et une version client.

L’article de la MSDN [Use the client-side People Picker control in SharePoint-hosted SharePoint Add-ins][1]
  
comporte un exemple basique et fonctionnel. Par contre changer l’ID de la div accueillant le composant, et ça marche moins bien.

La ligne qui pose problème est la suivante :

{{< highlight js >}}_var peoplePicker = this.SPClientPeoplePicker.SPClientPeoplePickerDict.peoplePickerDiv_TopSpan;_
{{< / highlight >}}
Le dictionnaire SPClientPeoplePickerDict contient les SPClientPeoplePicker associés aux ID.

Si vous créez 2 peopleplicker &#8211; div1, div2 -, this.SPClientPeoplePicker.SPClientPeoplePickerDict contiendra deux objets div1\_TopSpan et div2\_TopSpan.

Voici une petite correction qui rendra la composant utilisable.

{{< highlight js >}}// Run your custom code when the DOM is ready.
(document).ready(function () {
    // Specify the unique ID of the DOM element where the
    // picker will render.
    initializePeoplePicker('peoplePickerDiv');
});

// Render and initialize the client-side People Picker.
function initializePeoplePicker(peoplePickerElementId) {
    // Create a schema to store picker properties, and set the properties.
    var schema = {};
    schema['PrincipalAccountType'] = 'User,DL,SecGroup,SPGroup';
    schema['SearchPrincipalSource'] = 15;
    schema['ResolvePrincipalSource'] = 15;
    schema['AllowMultipleValues'] = true;
    schema['MaximumEntitySuggestions'] = 50;
    schema['Width'] = '280px';

    this.SPClientPeoplePicker_InitStandaloneControlWrapper(peoplePickerElementId, null, schema);
}

// Query the picker for user information.
function getUserInfo() {
    // Get the people picker object from the page.
    var peoplePicker = this.SPClientPeoplePicker.SPClientPeoplePickerDict.peoplePickerDiv_TopSpan;
    // Get information about all users.
    var users = peoplePicker.GetAllUserInfo();
    doSth(users);
}
{{< /highlight >}} 

Pour configurer le peoplepicker cela se fait à la création dans le schema.

_this.SPClientPeoplePicker_InitStandaloneControlWrapper(peoplePickerElementId, null, schema);_

La page **Class SPClientPeoplePicker** propose les propriétés disponibles. Il faudra probablement un peu tatonner.

Par exemple _schema[‘SharePointGroupID’] = spGroupId;_ permet de limiter la recherche du people picker à un groupe SP.

Le 2nd parametre permet d’initialiser le people picker avec des utilisateurs pré sélectionnés. Ci-après l’objet attendu:
{{< highlight js >}}users = [{
        'AutoFillDisplayText': '',
        'AutoFillKey': '',
        'Description': '',
        'DisplayText': '',
        'EntityType': '',
        'IsResolved': '',
        'Key': '',
        'Resolved': ''
    }];
    {{< /highlight >}}
**Références :**

[Class SPClientPeoplePicker][2]

[MSDN &#8211; Use the client-side People Picker control in SharePoint-hosted SharePoint Add-ins][1]

[SharePoint 2013: Client Side People Picker][3]

 [1]: https://msdn.microsoft.com/en-us/library/office/jj713593.aspx
 [2]: http://definitelytyped.org/docs/sharepoint--SharePoint/classes/spclientpeoplepicker.html
 [3]: http://ranaictiu-technicalblog.blogspot.com/2014/11/sharepoint-2013-client-side-people.html