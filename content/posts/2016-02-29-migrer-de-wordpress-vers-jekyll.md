---
title: Migrer de WordPress vers Jekyll
author: Ghislain
# draft: true
date: 2016-02-29T00:00:00+00:00
url: /2016/02/29/migrer-de-wordpress-vers-jekyll/
featuredImage: /wp-content/uploads/2016/03/jekyllrb.png
categories:
  - Blog
tags:
  - Git
  - GitHub
  - Jekyll
  - Ruby
  - WordPress

---
Cet article décrit comment migrer un blog de WordPress vers Jekyll hébergé avec GitHub pages.

Techniquement parlant, WordPress est un CMS utilisant PHP et MySQL tandis que Jekyll est un générateur de site statique en Ruby.

![Fonctionnement schématique du couple Jekyll/GitHub][1]

## 1 Récupérer le contenu wordpress {#1-récupérer-le-contenu-wordpress}

Il faut installer l’extension Jekyll Exporter dans WordPress : [Jekyll Exporter][2].

L’utilisation est intuitive, cliquer sur Tools > Jekyll Export. Après un certain temps de calcul, vous aurez une archive jekyll-export.zip contenant les posts, pages et contenus.

/!\ Les commentaires doivent être repris séparément. Il semble que Disqus soit le favoris de la communauté pour cette fonctionnalité. /!\

## 2 Installation de Jekyll sur Windows {#2-installation-de-jekyll-sur-windows}

### 2.1 Installation de Ruby {#21-installation-de-ruby}

Si vous disposez de [Chocolatey][3] [tag][4], utilisez l’invite de commande PowerShell 

{{< highlight powershell>}}
cinst ruby
cinst ruby2.devkit
{{< / highlight >}}

Sinon vous pouvez utiliser [RubyInstaller][5].

### 2.2 Installation de Jekyll {#22-installation-de-jekyll}

Vous choisirez en fonction de votre hébergeur que faire mais dans les grandes lignes :

Invite de commande ruby

{{< highlight powershell>}}
# installation de Jekyll nu
gem install jekyll

# installation de Jekyll avec des packages supportés par Github pages
gem install github-pages
{{< / highlight >}}


https://pages.github.com/versions/

### 2.3 Les commandes de bases {#23-les-commandes-de-bases}
<figure class="highlight"> 

{{< highlight powershell>}}
# créer un blog a partir du modèle par défaut
jekyll new blog
# se placer dans le répertoire du blog
cd blog
# Générer le site courant dans ./_site
jekyll build 
#A development server will run at http://localhost:4000/
jekyll serve
{{< / highlight >}}


Plus d’information sur la configuration : [Jekyll &#8211; Configuration][6]

## 3 Publier sur GitHub {#3-publier-sur-github}

### 3.1 Créer un dépôt Github {#31-créer-un-dépôt-github}

Il faut créer un dépôt Github public qui se nomme **username.github.io**.

Mon username github étant **ghislainl**, le dépôt se nomme **ghislainl.github.io**.

### 3.2 Initier votre blog {#32-initier-votre-blog}

Se placer dans le répertoire qui contiendra ce projet<figure class="highlight"> 

{{< highlight powershell>}}
git clone https://github.com/username/username.github.io
cd username.github.io
# Copier votre blog préalablement créé
# pusher le blog sur le dépôt github
git add --all
git commit -m "Initial commit"
git push -u origin master
{{< / highlight >}}

### 3.3 Voir le résultat {#33-voir-le-résultat}

Après le push, Github va générer le site statique automatiquement à l’adresse [http://username.github.io][7].

Vous recevrez un mail de **support@github.com** sur votre boite de contact s’il y a un soucis lors de la génération. Je fus agréablement surpris d’être prévenu que le moteur de Markdown que j’utilisais n’allait plus être maintenu au 1 mai. Cela permet de s’organiser pour mettre à jour sa configuration.

## 4 Plugins et tweaks {#4-plugins-et-tweaks}

Jekyll dispose de beaucoup de plug-in et peut compter sur le support de sa communauté. Tous ne sont pas disponibles sur GitHub Pages mais la liste suivante vous donnera des idées : [Jekyll &#8211; Plug-ins][8].

### 4.1 Tags et categories {#41-tags-et-categories}

L’entête YAML d’un post peut contenir des categories et des tags qui ne sont pas utilisés dans le template par defaut.

Dans un post, le templating avec Liquid permet d’y accéder avec **post.categories** et **post.tags**.

Plus d’informations sur le templating : [Jekyll Templates][9]

### 4.2 Pagination {#42-pagination}

Le plug-in jekyll-paginate est nécessaire pour disposer de la pagination sur le blog. Il faut le référencer dans le fichier _config.yml.<figure class="highlight"> 

{{< highlight yml>}}
gems: [jekyll-paginate]{{< / highlight  >}}

Vous trouverez les objets à utiliser et un exemple de code sur la page [Jekyll &#8211; Pagination][10].

### 4.3 Sitemap {#43-sitemap}

Le plug-in jekyll-sitemap crée automatiquement un fichier sitemap.xml lors du build. Il faut le référencer dans le fichier _config.yml.<figure class="highlight"> 

{{< highlight yml>}}
gems: [jekyll-sitemap]{{< / highlight  >}}

### 4.4 Choix du syntax highlighter {#44-choix-du-syntax-highlighter}

Jekyll propose plusieurs moteur de coloration syntaxique, les plus connus étant :

  * Rouge (ruby) : [77 langages][11]
  * Pygment (python) : [300 langages][12]
  
    Pour configurer celui qui vous convient, cela se passe dans le fichier _config.yml.<figure class="highlight"> 

{{< highlight yml>}}
>highlighter: rouge{{< / highlight  >}}

### 4.5 Page introuvable {#45-page-introuvable}

Pour personnaliser la page 404 de votre blog, il faut créer un fichier 404.html ou 404.md à la racine :<figure class="highlight"> 

{{< highlight html>}}
---
layout: page
title: 404
permalink: /404.html
---
Are you lost ?{{< / highlight  >}}



### 4.6 Themes {#46-themes}

[GitHub &#8211; Jekyll themes][13]

[JekyllThemes.org][14]

[Jekyll Bootstrap][15]

### 4.7 Nom de domaine {#47-nom-de-domaine}

Si vous avez votre propre nom de domaine, il faut créer un fichier CNAME à la racine avec votre nom de domaine dedans.

[GitHub &#8211; Using a custom domain with GitHub Pages][16]

### 4.8 Les images issues de WordPress {#48-les-images-issues-de-wordpress}

Il y a deux soucis avec les images dans les articles : l’URL est celle du site initial et les captions ne sont pas gérés.
  
Il faut donc utiliser des petites expressions régulières :

  * pour l’URL des images: remplacer **<url_site>/** par **{{ site.baseurl }}/**
  * pour le début du caption : remplacer **<figure id=”(.\*?)” style=”(.\*?)” class=”(.*?)”>** par rien
  * pour la fin du caption : 
      * en conservant le texte : **<figcaption class=”wp-caption-text”>(.*?)</figcaption></figure>** par **<br/><i>$1</i>**
      * en perdant le texte : **<figcaption class=”wp-caption-text”>(.*?)</figcaption></figure>** par rien

**Références**

[Jekyll][17]

[GitHub Pages][18]

[Git Guide &#8211; no deep shit][19]

[Qu’est-ce qu’un site statique [EN]][20]

 [1]: /wp-content/uploads/2016/02/Jekyll-GitHubPages.png
 [2]: https://wordpress.org/plugins/jekyll-exporter/ "Jekyll Exporter"
 [3]: https://chocolatey.org/ "Chocolatey"
 [4]: http://ghislainl.github.io/tags/#tag-Chocolatey "tag Chocolatey"
 [5]: http://rubyinstaller.org/ "RubyInstaller"
 [6]: http://jekyllrb.com/docs/configuration/ "Jekyll - Configuration"
 [7]: http://username.github.io "http://username.github.io"
 [8]: http://jekyllrb.com/docs/plugins/ "Jekyll - Plug-ins"
 [9]: http://jekyllrb.com/docs/templates/ "Jekyll Templates"
 [10]: http://jekyllrb.com/docs/pagination/ "Jekyll - Pagination"
 [11]: https://github.com/jneen/rouge/wiki/list-of-supported-languages-and-lexers "77 langages"
 [12]: http://pygments.org/languages/ "300 langages"
 [13]: https://github.com/jekyll/jekyll/wiki/Themes "GitHub - Jekyll themes"
 [14]: http://jekyllthemes.org/ "JekyllThemes.org"
 [15]: http://jekyllbootstrap.com "Jekyll Bootstrap"
 [16]: https://help.github.com/articles/using-a-custom-domain-with-github-pages/ "GitHub - Using a custom domain with GitHub Pages"
 [17]: http://jekyllrb.com/ "Jekyll"
 [18]: https://pages.github.com/ "GitHub Pages"
 [19]: http://rogerdudler.github.io/git-guide/index.fr.html "Git Guide - no deep shit"
 [20]: http://nilclass.com/courses/what-is-a-static-website/ "Qu'est-ce qu'un site statique [EN]"