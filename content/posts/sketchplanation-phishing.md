---
title: Type de phishing
author: Ghislain
date: 2021-01-06
url: /un-dessin/phishing/
categories:
  - Un dessin
tags:
  - sketchplanations
  - cybersecurité
---

![Type de phishing](/un-dessin/sketchplanations-phishing.jpg)

Phishing - en français hameçonnage

Inciter les gens à partager des informations personnelles et des identifiants de connexion en se faisant passer pour quelqu'un d'autre - connu sous le nom de phishing - est étonnamment efficace. 
Certaines des personnes les plus intelligentes que je connaisse en ont été séduites et il ne faut qu’un instant de déconcentration ou d’incertitude pour tomber dans le piège. 
Le calcul est simple: il ne coûte presque rien d'envoyer un e-mail à 100 000 personnes et il suffit de quelques personnes pour se faire prendre au filet pour que cela porte ses fruits.

Faites également attention aux attaques ciblées de spear-phishing où les escrocs peuvent sembler connaître des détails spécifiques sur vous-même pour rendre l'attaque plus crédible.
Ou ce que l’on appelle la chasse à la baleine - le ciblage de personnalités de premier plan, telles que des chefs d’entreprise ou des célébrités, avec des projets élaborés et parfois très convaincants.

Restez vigilants.

Vous pouvez également vérifier si vous avez été victime d'une violation de données grace à [Have I been Pwned](https://haveibeenpwned.com/)

Voir également l'[authentification à 2 facteurs](https://sketchplanations.com/2-factor-authentication)

original : [Sketchplanations](https://sketchplanations.com/types-of-phishing)
