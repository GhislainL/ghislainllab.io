---
title: ".Net décompilateur, éditeur d'assembly et obfuscateur"
author: Ghislain

date: 2014-11-17T08:38:29+00:00
url: /2014/11/17/net-decompilateur-editeur-d039assembly-et-obfuscateur/
categories:
  - dotNet

---
Je ne sais pas vous mais je décompile rarement des DLL pour le plaisir. **IlSpy** est un très bon décompilateur qui me suffit pour comprendre la logique d’un code.

Mais parfois faut aller plus loin. Cela vous est probablement arrivé de ne pas retrouver les sources d’un vieux développement à maintenir. Voir de travailler avec une dll ou un exécutable dont les variables sont écrites en dur dans le code. Avec ILSpy, vous pouvez exporter le code pour ensuite l’éditer dans Visual Studio.

[<img class="wp-image-310 size-full" src="/wp-content/uploads/2014/11/ilspy.png?fit=890%2C563" alt="ILSpy" srcset="/wp-content/uploads/2014/11/ilspy.png?resize=300%2C190 300w, /wp-content/uploads/2014/11/ilspy.png?w=948 948w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][1]  
_Mon assembly de démonstration dans ILSpy_

Vous pouvez aussi avoir envie de modifier directement le code ou ne pas avoir de Visual Studio sous la main. Pour cela j’ai trouvé un éditeur d’assembly : **Reflexil**. Mais il ne fonctionne qu’avec **JustDecompile** ou **Reflector**.

Je vous propose un exemple simple dont voici le code :

{{< highlight csharp >}}using System;
namespace NoParam
{
   public class SiteColl
   {
      private string PrivateUrl { get; set; }
      public string PublicUrl { get; set; }
      public SiteColl()
      {
         this.PrivateUrl = "http://private.sps2013";
         this.PublicUrl = "http://public.sps2013";
      }
      public override string ToString()
      {
         return string.Format("SiteColl.PrivateUrl : {0} | SiteColl.PublicUrl : {1} ",
                              this.PrivateUrl, this.PublicUrl);
      }
   }
}{{< /highlight >}}

<table>
  <tr>
    <td>
      Comme vous l’imaginez son execution affiche : _SiteColl.PrivateUrl : http://private.sps2013
    </td>
    
    <td>
      SiteColl.PublicUrl : http://public.sps2013_
    </td>
  </tr>
</table>

[<img class="wp-image-306 size-full" src="/wp-content/uploads/2014/11/reflexil.png?fit=578%2C330" alt="Reflexil" srcset="/wp-content/uploads/2014/11/reflexil.png?resize=300%2C171 300w, /wp-content/uploads/2014/11/reflexil.png?w=578 578w" sizes="(max-width: 578px) 100vw, 578px" data-recalc-dims="1" />][2]  
_Modification d’une valeur avec Reflexil 1.8 dans JustDecompile_

<table>
  <tr>
    <td>
      Après un petit passage avec Reflexil, je vous laisse apprécier le résultat : _SiteColl.PrivateUrl : http://modified.sps2013
    </td>
    
    <td>
      SiteColl.PublicUrl : http://public.sps2013_
    </td>
  </tr>
</table>

A ce point, vous allez me dire qu’en obfusquant le code, tout cela ne sert à rien. D’autant plus qu’il existe pléthore d’obfuscateurs pour .Net comme en témoigne Wikipedia. Et comme vous l’imaginez, il existe des desobfuscateurs. C’est là qu’entre en jeu **De4Dot** et que je me demande s’il y a vraiment un moyen de protéger son code et s’il existe une fin à ce cycle.

**Références :**

[ILSpy][3]

[JustDecompile de Telerik][4]

[Reflector de Red Gate][5]

Reflexil : <http://reflexil.net/> et <http://sourceforge.net/projects/reflexil/>

[Je ne sais pas vous mais je décompile rarement des DLL pour le plaisir. **IlSpy** est un très bon décompilateur qui me suffit pour comprendre la logique d’un code.

Mais parfois faut aller plus loin. Cela vous est probablement arrivé de ne pas retrouver les sources d’un vieux développement à maintenir. Voir de travailler avec une dll ou un exécutable dont les variables sont écrites en dur dans le code. Avec ILSpy, vous pouvez exporter le code pour ensuite l’éditer dans Visual Studio.

[<img class="wp-image-310 size-full" src="/wp-content/uploads/2014/11/ilspy.png?fit=890%2C563" alt="ILSpy" srcset="/wp-content/uploads/2014/11/ilspy.png?resize=300%2C190 300w, /wp-content/uploads/2014/11/ilspy.png?w=948 948w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][1]  
_Mon assembly de démonstration dans ILSpy_

Vous pouvez aussi avoir envie de modifier directement le code ou ne pas avoir de Visual Studio sous la main. Pour cela j’ai trouvé un éditeur d’assembly : **Reflexil**. Mais il ne fonctionne qu’avec **JustDecompile** ou **Reflector**.

Je vous propose un exemple simple dont voici le code :

{{< highlight csharp >}}using System;
namespace NoParam
{
   public class SiteColl
   {
      private string PrivateUrl { get; set; }
      public string PublicUrl { get; set; }
      public SiteColl()
      {
         this.PrivateUrl = "http://private.sps2013";
         this.PublicUrl = "http://public.sps2013";
      }
      public override string ToString()
      {
         return string.Format("SiteColl.PrivateUrl : {0} | SiteColl.PublicUrl : {1} ",
                              this.PrivateUrl, this.PublicUrl);
      }
   }
}{{< /highlight >}}

<table>
  <tr>
    <td>
      Comme vous l’imaginez son execution affiche : _SiteColl.PrivateUrl : http://private.sps2013
    </td>
    
    <td>
      SiteColl.PublicUrl : http://public.sps2013_
    </td>
  </tr>
</table>

[<img class="wp-image-306 size-full" src="/wp-content/uploads/2014/11/reflexil.png?fit=578%2C330" alt="Reflexil" srcset="/wp-content/uploads/2014/11/reflexil.png?resize=300%2C171 300w, /wp-content/uploads/2014/11/reflexil.png?w=578 578w" sizes="(max-width: 578px) 100vw, 578px" data-recalc-dims="1" />][2]  
_Modification d’une valeur avec Reflexil 1.8 dans JustDecompile_

<table>
  <tr>
    <td>
      Après un petit passage avec Reflexil, je vous laisse apprécier le résultat : _SiteColl.PrivateUrl : http://modified.sps2013
    </td>
    
    <td>
      SiteColl.PublicUrl : http://public.sps2013_
    </td>
  </tr>
</table>

A ce point, vous allez me dire qu’en obfusquant le code, tout cela ne sert à rien. D’autant plus qu’il existe pléthore d’obfuscateurs pour .Net comme en témoigne Wikipedia. Et comme vous l’imaginez, il existe des desobfuscateurs. C’est là qu’entre en jeu **De4Dot** et que je me demande s’il y a vraiment un moyen de protéger son code et s’il existe une fin à ce cycle.

**Références :**

[ILSpy][3]

[JustDecompile de Telerik][4]

[Reflector de Red Gate][5]

Reflexil : <http://reflexil.net/> et <http://sourceforge.net/projects/reflexil/>

](https://en.wikipedia.org/wiki/List\_of\_obfuscators\_for\_.NET)

[Je ne sais pas vous mais je décompile rarement des DLL pour le plaisir. **IlSpy** est un très bon décompilateur qui me suffit pour comprendre la logique d’un code.

Mais parfois faut aller plus loin. Cela vous est probablement arrivé de ne pas retrouver les sources d’un vieux développement à maintenir. Voir de travailler avec une dll ou un exécutable dont les variables sont écrites en dur dans le code. Avec ILSpy, vous pouvez exporter le code pour ensuite l’éditer dans Visual Studio.

[<img class="wp-image-310 size-full" src="/wp-content/uploads/2014/11/ilspy.png?fit=890%2C563" alt="ILSpy" srcset="/wp-content/uploads/2014/11/ilspy.png?resize=300%2C190 300w, /wp-content/uploads/2014/11/ilspy.png?w=948 948w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][1]  
_Mon assembly de démonstration dans ILSpy_

Vous pouvez aussi avoir envie de modifier directement le code ou ne pas avoir de Visual Studio sous la main. Pour cela j’ai trouvé un éditeur d’assembly : **Reflexil**. Mais il ne fonctionne qu’avec **JustDecompile** ou **Reflector**.

Je vous propose un exemple simple dont voici le code :

{{< highlight csharp >}}using System;
namespace NoParam
{
   public class SiteColl
   {
      private string PrivateUrl { get; set; }
      public string PublicUrl { get; set; }
      public SiteColl()
      {
         this.PrivateUrl = "http://private.sps2013";
         this.PublicUrl = "http://public.sps2013";
      }
      public override string ToString()
      {
         return string.Format("SiteColl.PrivateUrl : {0} | SiteColl.PublicUrl : {1} ",
                              this.PrivateUrl, this.PublicUrl);
      }
   }
}{{< /highlight >}}

<table>
  <tr>
    <td>
      Comme vous l’imaginez son execution affiche : _SiteColl.PrivateUrl : http://private.sps2013
    </td>
    
    <td>
      SiteColl.PublicUrl : http://public.sps2013_
    </td>
  </tr>
</table>

[<img class="wp-image-306 size-full" src="/wp-content/uploads/2014/11/reflexil.png?fit=578%2C330" alt="Reflexil" srcset="/wp-content/uploads/2014/11/reflexil.png?resize=300%2C171 300w, /wp-content/uploads/2014/11/reflexil.png?w=578 578w" sizes="(max-width: 578px) 100vw, 578px" data-recalc-dims="1" />][2]  
_Modification d’une valeur avec Reflexil 1.8 dans JustDecompile_

<table>
  <tr>
    <td>
      Après un petit passage avec Reflexil, je vous laisse apprécier le résultat : _SiteColl.PrivateUrl : http://modified.sps2013
    </td>
    
    <td>
      SiteColl.PublicUrl : http://public.sps2013_
    </td>
  </tr>
</table>

A ce point, vous allez me dire qu’en obfusquant le code, tout cela ne sert à rien. D’autant plus qu’il existe pléthore d’obfuscateurs pour .Net comme en témoigne Wikipedia. Et comme vous l’imaginez, il existe des desobfuscateurs. C’est là qu’entre en jeu **De4Dot** et que je me demande s’il y a vraiment un moyen de protéger son code et s’il existe une fin à ce cycle.

**Références :**

[ILSpy][3]

[JustDecompile de Telerik][4]

[Reflector de Red Gate][5]

Reflexil : <http://reflexil.net/> et <http://sourceforge.net/projects/reflexil/>

[Je ne sais pas vous mais je décompile rarement des DLL pour le plaisir. **IlSpy** est un très bon décompilateur qui me suffit pour comprendre la logique d’un code.

Mais parfois faut aller plus loin. Cela vous est probablement arrivé de ne pas retrouver les sources d’un vieux développement à maintenir. Voir de travailler avec une dll ou un exécutable dont les variables sont écrites en dur dans le code. Avec ILSpy, vous pouvez exporter le code pour ensuite l’éditer dans Visual Studio.

[<img class="wp-image-310 size-full" src="/wp-content/uploads/2014/11/ilspy.png?fit=890%2C563" alt="ILSpy" srcset="/wp-content/uploads/2014/11/ilspy.png?resize=300%2C190 300w, /wp-content/uploads/2014/11/ilspy.png?w=948 948w" sizes="(max-width: 890px) 100vw, 890px" data-recalc-dims="1" />][1]  
_Mon assembly de démonstration dans ILSpy_

Vous pouvez aussi avoir envie de modifier directement le code ou ne pas avoir de Visual Studio sous la main. Pour cela j’ai trouvé un éditeur d’assembly : **Reflexil**. Mais il ne fonctionne qu’avec **JustDecompile** ou **Reflector**.

Je vous propose un exemple simple dont voici le code :

{{< highlight csharp >}}using System;
namespace NoParam
{
   public class SiteColl
   {
      private string PrivateUrl { get; set; }
      public string PublicUrl { get; set; }
      public SiteColl()
      {
         this.PrivateUrl = "http://private.sps2013";
         this.PublicUrl = "http://public.sps2013";
      }
      public override string ToString()
      {
         return string.Format("SiteColl.PrivateUrl : {0} | SiteColl.PublicUrl : {1} ",
                              this.PrivateUrl, this.PublicUrl);
      }
   }
}{{< /highlight >}}

<table>
  <tr>
    <td>
      Comme vous l’imaginez son execution affiche : _SiteColl.PrivateUrl : http://private.sps2013
    </td>
    
    <td>
      SiteColl.PublicUrl : http://public.sps2013_
    </td>
  </tr>
</table>

[<img class="wp-image-306 size-full" src="/wp-content/uploads/2014/11/reflexil.png?fit=578%2C330" alt="Reflexil" srcset="/wp-content/uploads/2014/11/reflexil.png?resize=300%2C171 300w, /wp-content/uploads/2014/11/reflexil.png?w=578 578w" sizes="(max-width: 578px) 100vw, 578px" data-recalc-dims="1" />][2]  
_Modification d’une valeur avec Reflexil 1.8 dans JustDecompile_

<table>
  <tr>
    <td>
      Après un petit passage avec Reflexil, je vous laisse apprécier le résultat : _SiteColl.PrivateUrl : http://modified.sps2013
    </td>
    
    <td>
      SiteColl.PublicUrl : http://public.sps2013_
    </td>
  </tr>
</table>

A ce point, vous allez me dire qu’en obfusquant le code, tout cela ne sert à rien. D’autant plus qu’il existe pléthore d’obfuscateurs pour .Net comme en témoigne Wikipedia. Et comme vous l’imaginez, il existe des desobfuscateurs. C’est là qu’entre en jeu **De4Dot** et que je me demande s’il y a vraiment un moyen de protéger son code et s’il existe une fin à ce cycle.

**Références :**

[ILSpy][3]

[JustDecompile de Telerik][4]

[Reflector de Red Gate][5]

Reflexil : <http://reflexil.net/> et <http://sourceforge.net/projects/reflexil/>

](https://en.wikipedia.org/wiki/List\_of\_obfuscators\_for\_.NET)

](https://github.com/0xd4d/de4dot)

 [1]: /wp-content/uploads/2014/11/ilspy.png
 [2]: /wp-content/uploads/2014/11/reflexil.png
 [3]: https://github.com/icsharpcode/ILSpy
 [4]: http://www.telerik.com/products/decompiler.aspx
 [5]: http://www.red-gate.com/products/dotnet-development/reflector/