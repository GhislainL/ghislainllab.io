---
title: Changer son fond d’écran comme un geek
author: Ghislain

date: 2016-10-07T00:00:00+00:00
url: /2016/10/07/changer-son-fond-decran-comme-un-geek/
categories:
  - dotNet
  - PowerShell

---
J’aime bien personnaliser mon fond d’écran. Je n’aime pas perdre cette petite liberté.
  
D’autant plus que chaque mois, Smashing Magazine (smashingmagazine.com) propose une [sélection de wallpaper][1]
  
Je ne sais pas ce qui m’a le plus motivé mais aujourd’hui nous allons voir comment changer son fond d’écran comme un geek.

Je vous passe la possibilité de l’UI.

Cela ne fonctionne qu’avac les .JPG et .BMP

## 1. Regedit simple {#1-regedit-simple}

Si la sécurité est inexistante, ça peut marcher
  
HKEY\_CURRENT\_USER\Control Panel\Desktop.
  
Clé &#8220;Wallpaper&#8221; and double-click on it.

## 2. Regedit moins simple {#2-regedit-moins-simple}

On sait jamais sur un malentendu…
  
HKEY\_CURRENT\_USER\Software\Microsoft\Windows\CurrentVersion\Policies\System
  
clé de type [string] nommée Wallpaper

Si un GPO interdit la modification de cette clé dans la base de registre, on escalade.

## 3. Dev csharp {#3-dev-csharp}

Pas besoin d’être admin.

{{< highlight plain >}}private const int SPI_SETDESKWALLPAPER = 20;
private const int SPIF_UPDATEINIFILE = 0x1;
private const int SPIF_SENDWININICHANGE = 0x2;

[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
public static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);

public void ChangeWallpaper(string wallpaperPath){
    SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, "(None)", SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
    SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, wallpaperPath, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
}
{{< /highlight >}}

Si l’executable est bloqué ou si l’on ne dispose pas d’environnement de dev, on grimpe encore

## 4. Script PS {#4-script-ps}

Toujours pas besoin d’être admin.

{{< highlight powershell >}}$setwallpapersource = @"
using System.Runtime.InteropServices;
public class wallpaper
{
   public const int SetDesktopWallpaper = 20;
   public const int UpdateIniFile = 0x01;
   public const int SendWinIniChange = 0x02;
   [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
   private static extern int SystemParametersInfo (int uAction, int uParam, string lpvParam, int fuWinIni);
   public static void SetWallpaper ( string path )
   {
      SystemParametersInfo( SetDesktopWallpaper, 0, path, UpdateIniFile | SendWinIniChange );
   }
}
"@
Add-Type -TypeDefinition $setwallpapersource
[wallpaper]::SetWallpaper($wallpaperPath)
{{< /highlight >}}

Merci [Siva Mulpuru][2]

Si l’invite de commande PowerShell n’est pas accessible, on peut tenter de la lancer depuis l’invite de commande classique.

Voilà une bonne chose de faite.

> Il faut traiter les choses légères avec sérieux et il faut traiter les choses graves et sérieuses avec légèreté.
> 
>   * Alphonse

 [1]: https://www.smashingmagazine.com/tag/wallpapers/
 [2]: https://smulpuru.wordpress.com/2015/04/01/change-wallpaper-using-windows-api-systemparametersinfo-from-user32-dll/