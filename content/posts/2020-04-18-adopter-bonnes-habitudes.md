---
title: 9 conseils pour adopter de bonnes habitudes et les conserver
author: Ghislain
date: 2020-04-18
url: /2020/04/18/adopter-de-bonnes-habitudes/
categories:
  - Développement personnel

---
Il est facile de décider d’adopter des bonnes habitudes. Par contre l’adoption effective est souvent plus problématique. Pour illustration, seulement 12% des résolutions de la nouvelle année réussissent (source). Voici 9 conseils pour adopter de bonnes habitudes et les conserver.

>  Cet article a été originellement publié le 26 février 2016 par Jacqueline Whitmore : [9 Ways to Actually Adopt the Better Habits You Know Will Help You Succeed](https://www.entrepreneur.com/article/270701)

Chaque année, nous nous fixons des objectifs, souvent appelés résolutions. Ce que nous imaginons réellement est un futur meilleur, qui découle du changement désiré dans notre routine ou nos habitudes quotidiennes. Vous voulez peut-être perdre du poids, augmenter le nombre de vos clients ou arrêter de fumer. Cela veut dire que vous devez modifier un comportement existant et le remplacer par un nouveau.
Changer est toujours difficile, mais c’est possible, avec un peu de volonté. Essayez ces étapes simples pour vous permettre d’établir de meilleures habitudes.

1. Décidez ce qui est important et pourquoi.
Quel est votre motivation pour vouloir développer une nouvelle habitude ? Est-ce que cela va améliorer votre vie ? Imaginez le résultat si vous n’effectuez pas le changement. Ensuite décidez comment vous allez mettre en place ce changement. Si vous voulez plus de clients, ayez plus de contacts; si vous voulez effectuer plus de chose dans une journée, levez-vous plus tôt; si vous voulez être plus en forme, incluez un programme d’exercices dans votre routine quotidienne. Fixez un objectif et ensuite imposez-vous un planning pour l’atteindre.

2. Créez une habitude raisonnable.
Si une nouvelle habitude est trop difficile à maintenir, elle a de grande chance d’être abandonnée en cours de route. Choisissez un nouvelle habitude réaliste que vous désirez et que vous savez pouvoir accomplir. Commencez petit, comme diminuer le nombre de café que vous buvez chaque jour, ou prévoir un déjeuner par semaine avec un client potentiel.

3. Choisissez une habitude à la fois.
Tenter de changer plusieurs habitudes à la fois est trop difficile, quelle que soit la personne. Concentrez-vous plutôt sur un changement de comportement jusqu’à ce que cela devienne la routine, puis ajoutez en un second. Si vous êtes submergé par trop de changements, vous ne réussirez probablement aucun d’entre eux.

4. Donnez-vous du temps.
Changer une habitude demande du temps. D’après une étude du professeur en psychologie de la santé Phillippa Lally (University College London), il faut plus de deux mois, ou 66 jours, pour forger une nouvelle habitude.

5. N’attendez pas la perfection.
Vous allez inexorablement commettre au moins une bévue durant l’implantation de votre nouvelle habitude. Soyez patient envers vous-même et acceptez cette erreur. Puis engagez-vous de nouveau à l’implantation de cette nouvelle habitude.

6. Parlez-en autour de vous.
Etre tenu responsable d’un nouveau comportement est un très bon moyen pour renforcer ce comportement. Dites autour de vous ce que vous tentez de faire, et acceptez les propositions d’aides. Ne vous énervez pas s’ils vous rappellent votre objectif de temps en temps. C’est peut-être ce qu’il vous faut pour rester sur la voie Vous pourriez même les motiver à se fixer des objectifs eux aussi.

7. Soyez consistant/régulier.
La régularité est la clé pour établir une nouvelle habitude. Faites la même chose jour après jour pour l’ancrer profondément dans votre esprit et votre routine. Si possible, établissez un planning pour le nouveau comportement et respectez-le. Soyez responsable, soit en suivant les progrès de l’ancrage de votre nouvelle habitude, soit en échangent avec autrui.

8. Replacez une mauvaise habitude par une bonne habitude.
Pour abandonner une mauvaise habitude, essayer de la remplacer par une meilleure. Si votre bureau propose un pot à bonbons, replacez son contenu par des grignotages sains, comme des noisettes ou des amandes. Ou si vous restez éveillé tard en regardant la télé, lisez plutôt dans votre lit.

9. Récompensez-vous.
Fixez des normes de conduite et récompensez-vous à la fin de 30 ou 60 jours. Mais assurez-vous que la récompense n’inclue pas l’ancien comportement. Par exemple, ne célébrez pas avec un gros repas votre nouvelle habitude de moins manger aux repas.

Respectez votre objectif et vous obtiendrez les changements que vous désirez. Comme l’a dit l’entrepreneur, auteur et conférencier en motivation Jim Rohn:


> Le succès n’est rien de plus que quelques disciplines simples, appliquées tous les jours.</p>
>
> Success is nothing more than a few simple disciplines, practiced every day.</p>
>
> — <cite>Jim Rohn</cite>
