---
title: "Obtenir facilement le nom d'une assembly dans Visual Studio"
author: Ghislain

date: 2014-10-01T13:09:37+00:00
url: /2014/10/01/obtenir-facilement-le-nom-d039une-assembly-dans-visual-studio/
categories:
  - dotNet
tags:
  - Visual Studio

---
Si vous travaillez avec SharePoint, vous avez probablement eu besoin de récupérer le nom d’une assembly. Depuis 2010, Visual vous facilite la vie avec les paramètres remplaçables:

> <span id="mt25" class="sentence SentenceHover">$SharePoint.Project.AssemblyFullName$</span>

Mais comment récupérer le nom directement ?

  * Dans Visual Studio, ouvrir Outils puis cliquer sur Outils externes.
  * Cliquer sur Ajouter.
  * Cocher Utiliser la fenêtre Sortie
  * Saisir les valeurs suivantes : 
      * Titre : StrongName
      * Commande : powershell.exe
      * Arguments : -command « [System.Reflection.AssemblyName]::GetAssemblyName(« $(TargetPath) »).FullName »
  * Cliquer sur OK

–[<img class="alignnone size-full wp-image-249" src="/wp-content/uploads/2014/10/strongname-outilsexterne.png?fit=396%2C391" alt="StrongName-OutilsExterne" srcset="/wp-content/uploads/2014/10/strongname-outilsexterne.png?resize=300%2C296 300w, /wp-content/uploads/2014/10/strongname-outilsexterne.png?w=396 396w" sizes="(max-width: 396px) 100vw, 396px" data-recalc-dims="1" />][1]

Maintenant vous disposez d’une option supplémentaire StrongName dans l’onglet Outils …

[<img class="alignnone size-full wp-image-250" src="/wp-content/uploads/2014/10/strongname-tools.png?fit=436%2C496" alt="StrongName-Tools" srcset="/wp-content/uploads/2014/10/strongname-tools.png?resize=264%2C300 264w, /wp-content/uploads/2014/10/strongname-tools.png?w=436 436w" sizes="(max-width: 436px) 100vw, 436px" data-recalc-dims="1" />][2]

… qui permet d’obtenir le nom de l’assembly contenant le fichier actif.

[<img class="alignnone size-full wp-image-251" src="/wp-content/uploads/2014/10/strongname-output.png?fit=601%2C81" alt="StrongName-Output" srcset="/wp-content/uploads/2014/10/strongname-output.png?resize=300%2C40 300w, /wp-content/uploads/2014/10/strongname-output.png?w=601 601w" sizes="(max-width: 601px) 100vw, 601px" data-recalc-dims="1" />][3]

**Références :**

[Managing external tools][4]

[Si vous travaillez avec SharePoint, vous avez probablement eu besoin de récupérer le nom d’une assembly. Depuis 2010, Visual vous facilite la vie avec les paramètres remplaçables:

> <span id="mt25" class="sentence SentenceHover">$SharePoint.Project.AssemblyFullName$</span>

Mais comment récupérer le nom directement ?

  * Dans Visual Studio, ouvrir Outils puis cliquer sur Outils externes.
  * Cliquer sur Ajouter.
  * Cocher Utiliser la fenêtre Sortie
  * Saisir les valeurs suivantes : 
      * Titre : StrongName
      * Commande : powershell.exe
      * Arguments : -command « [System.Reflection.AssemblyName]::GetAssemblyName(« $(TargetPath) »).FullName »
  * Cliquer sur OK

–[<img class="alignnone size-full wp-image-249" src="/wp-content/uploads/2014/10/strongname-outilsexterne.png?fit=396%2C391" alt="StrongName-OutilsExterne" srcset="/wp-content/uploads/2014/10/strongname-outilsexterne.png?resize=300%2C296 300w, /wp-content/uploads/2014/10/strongname-outilsexterne.png?w=396 396w" sizes="(max-width: 396px) 100vw, 396px" data-recalc-dims="1" />][1]

Maintenant vous disposez d’une option supplémentaire StrongName dans l’onglet Outils …

[<img class="alignnone size-full wp-image-250" src="/wp-content/uploads/2014/10/strongname-tools.png?fit=436%2C496" alt="StrongName-Tools" srcset="/wp-content/uploads/2014/10/strongname-tools.png?resize=264%2C300 264w, /wp-content/uploads/2014/10/strongname-tools.png?w=436 436w" sizes="(max-width: 436px) 100vw, 436px" data-recalc-dims="1" />][2]

… qui permet d’obtenir le nom de l’assembly contenant le fichier actif.

[<img class="alignnone size-full wp-image-251" src="/wp-content/uploads/2014/10/strongname-output.png?fit=601%2C81" alt="StrongName-Output" srcset="/wp-content/uploads/2014/10/strongname-output.png?resize=300%2C40 300w, /wp-content/uploads/2014/10/strongname-output.png?w=601 601w" sizes="(max-width: 601px) 100vw, 601px" data-recalc-dims="1" />][3]

**Références :**

[Managing external tools][4]

](http://msdn.microsoft.com/fr-fr/library/ee231545.aspx)

 

 [1]: /wp-content/uploads/2014/10/strongname-outilsexterne.png
 [2]: /wp-content/uploads/2014/10/strongname-tools.png
 [3]: /wp-content/uploads/2014/10/strongname-output.png
 [4]: http://msdn.microsoft.com/en-us/library/76712d27.aspx