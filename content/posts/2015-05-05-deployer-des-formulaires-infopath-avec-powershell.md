---
title: Déployer des formulaires InfoPath avec PowerShell
author: Ghislain

date: 2015-05-05T07:30:01+00:00
url: /2015/05/05/deployer-des-formulaires-infopath-avec-powershell/
categories:
  - PowerShell
  - SharePoint
tags:
  - Enable-SPInfoPathFormTemplate
  - InfoPath
  - Install-SPDataConnectionFile
  - Install-SPInfoPathFormTemplate
  - PowerShell
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013

---
Comment pouvez vous utiliser InfoPath Forms Service de manière rapide tout en suivant les [bonnes pratiques de déploiement des formulaires InfoPath][1]? Pour cela, vous devez déployer des formulaires InfoPath avec PowerShell. Comme souvent, PowerShell vous facilitera rapidement et grandement la vie.

## Formulaires InfoPath {#formulaires-infopath}

### Préparer vos formulaires {#préparer-vos-formulaires}

Premièrement, vous devez préparer votre formulaire pour qu’il puisse être ajouter dans InfoPath Forms Service. Le cas échéant vous obtiendrez le message : <strong>The XSN cannot be used on the server</strong>. Pour ajouter tester votre formulaire, vous pouvez utiliser <strong>Test-SPInfoPathFormTemplate</strong>. S’il est valide, alors vous utiliserez <strong>Install-SPInfoPathFormTemplate</strong>.


{{< highlight powershell>}}# erreur obtenue si le formulaire n'est pas valide
Install-SPInfoPathFormTemplate -Path "E:ressourcesinfopathformulaire.xsn"
Install-SPInfoPathFormTemplate : The XSN cannot be used on the server.
At line:1 char:31
+ Install-SPInfoPathFormTemplate <<<<  -Path E:ressourcesinfopathformulaire.xsn
+ CategoryInfo          : InvalidData: (Microsoft.Offic...mTemplateCmdlet:
InstallFormTemplateCmdlet) [Install-SPInfoPathFormTemplate], ConverterExce
ption
+ FullyQualifiedErrorId : Microsoft.Office.InfoPath.Server.Cmdlet.InstallF
ormTemplateCmdlet

# tester le formulaire
Test-SPInfoPathFormTemplate -Path "E:ressourcesinfopathformulaire.xsn"
This form template requires administrator approval.
To publish the form template, click the File tab, click the SharePoint Server button on the Publish tab,
and select the Administrator-approved option in the publishing wizard.
{{< / highlight >}}



Afin de pouvoir publier votre formulaire dans InfoPath Forms Service, vous devez effectuer 2 opérations préalables dans InfoPath :

  1. Modifier le niveau de sécurité du formulaire:

[<img class="wp-image-775 size-full" src="/wp-content/uploads/2015/04/InfoPath-Admin-Approved.png?fit=674%2C475" alt="InfoPath-Admin-Approved" srcset="/wp-content/uploads/2015/04/InfoPath-Admin-Approved.png?resize=300%2C211 300w, /wp-content/uploads/2015/04/InfoPath-Admin-Approved.png?w=674 674w" sizes="(max-width: 674px) 100vw, 674px" data-recalc-dims="1" />][2]  
_Paramétrer le niveau de sécurité en Full Trust_

  1. Publier le formulaire dans une bibliothèque et se laisser porter par le Wizard.

[<img class="wp-image-776 size-full" src="/wp-content/uploads/2015/04/InfoPath-Admin-Approved2.png?fit=678%2C463" alt="InfoPath-Admin-Approved2" srcset="/wp-content/uploads/2015/04/InfoPath-Admin-Approved2.png?resize=300%2C205 300w, /wp-content/uploads/2015/04/InfoPath-Admin-Approved2.png?w=678 678w" sizes="(max-width: 678px) 100vw, 678px" data-recalc-dims="1" />][3]  
_Publier le formulaire InfoPath avec la méthode adaptée_

### Publier et mettre à jour vos formulaires {#publier-et-mettre-à-jour-vos-formulaires}

Maintenant que votre formulaire est valide pour un déploiement, vous pouvez le déployer. Le principe reste le même que pour l’opération manuelle:  déploiement puis activation sur un ou plusieurs collections de site. La mise à jour nécessite de désinstaller, réinstaller puis mettre à jour le formulaire afin que les formulaires déjà utilisés soient mis à jour.

{{< highlight powershell >}}Install-SPInfoPathFormTemplate -Path "E:ressourcesinfopathformulaire-approved.xsn"
Enable-SPInfoPathFormTemplate -Identity "formulaire-approved.xsn" -Site "http://www.contoso.com"
Uninstall-SPInfoPathFormTemplate -Identity "formulaire-approved.xsn"
Install-SPInfoPathFormTemplate -Path "E:ressourcesinfopathformulaire-approved.xsn"
Update-SPInfoPathFormTemplate -Identity "formulaire-approved.xsn"
{{< / highlight >}}


{{< highlight powershell >}}# google it
function WaitForJobToFinish([string]$JobName){}

$dirpck = Split-Path $MyInvocation.MyCommand.Path

function InstallOrUpdateInfoPathForm{
   param(
      $fileInfoPath,
      $webUrl
   )
   Uninstall-SPInfoPathFormTemplate -Identity $fileInfoPath -EA 0
   if($? –eq $true){
     Disable-SPInfoPathFormTemplate -Identity $fileInfoPath -Site $webUrl
     "Uninstalling..."
     WaitForJobToFinish "solution-deployment-form-*"
     "Installing..."
     Install-SPInfoPathFormTemplate -Path "$dirpckressources$fileInfoPath" -Confirm:$false
     "Updating..."
     Update-SPInfoPathFormTemplate
   }else{
     "Installing..."
     Install-SPInfoPathFormTemplate -Path "$dirpckressources$fileInfoPath" -Confirm:$false
     "Enabling..."
     Enable-SPInfoPathFormTemplate -Identity $fileInfoPath -Site $webUrl
   }
}

InstallOrUpdateInfoPathForm -fileInfoPath "formulaire.xsn" -webUrl "http://www.contoso.com"{{< / highlight >}}

**Coup de pouce** : pour créer la liste contenant les formulaires par PowerShell, vous utiliserez le modèle **Form Library**

## Fichiers de connexion {#fichiers-de-connexion}

Comme vous vous en doutez, PowerShell permet aussi le déploiement des fichiers de connexion.

{{< highlight powershell >}}Install-SPDataConnectionFile –Identity "connexion.udcx"
Uninstall-SPDataConnectionFile –Identity "connexion.udcx"{{< / highlight >}}


Vous avez maintenant tout ce qu’il faut pour faire d’InfoPath un allié et vous faciliter la vie.

**Références:**

[Install-SPInfoPathFormTemplate][4]

[Install-SPDataConnectionFile][5]

 [1]: http://blog.ghislain-lerda.com/sharepoint/infopath-deployment-best-practices/
 [2]: /wp-content/uploads/2015/04/InfoPath-Admin-Approved.png
 [3]: /wp-content/uploads/2015/04/InfoPath-Admin-Approved2.png
 [4]: https://technet.microsoft.com/fr-fr/library/ff608053%28v=office.14%29.aspx
 [5]: https://technet.microsoft.com/fr-fr/library/ff608028%28v=office.14%29.aspx