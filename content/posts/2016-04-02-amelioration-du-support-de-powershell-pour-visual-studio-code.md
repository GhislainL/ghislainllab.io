---
title: Amélioration du support de PowerShell pour Visual Studio Code
author: Ghislain

date: 2016-04-02T00:00:00+00:00
url: /2016/04/02/amelioration-du-support-de-powershell-pour-visual-studio-code/
categories:
  - PowerShell
tags:
  - VSCode

---
Visual Studio Code est une version légère de Visual Studio qui a le vent en poupe. Si vous travaillez fréquemment avec PowerShell, vous allez aimer la nouvelle extension vscode.PowerShell.

## Installation {#installation}

Une connexion internet est requise pour installer une extension VSCode.

  * Ouvrir Visual Studio Code
  * Effectuer _Ctrl + P_ pour afficher la palette
  * Taper _ext install powershell_ + Enter
  * Rédemarrer VSCode

![Installation][1]
  
_Installation_

C’est fini. Rapide, n’est ce pas ?

## Tester {#tester}

Pour tester les différentes nouvelles fonctionnalités, les créateurs ont mis à disposition un jeu de test à l’emplacement suivant :

_c:\Users<yourusername>.vscode\extensions\ms-vscode.PowerShell\examples_

![Dossier d'exemples][2]
  
_Dossier d’exemples_

## Nouvelles fonctionnalités {#nouvelles-fonctionnalités}

Grace à cette extension, vous pourrez utiliser les fonctionnalités suivantes directement dans VSCode.

  * IntelliSense
  * Signature des cmdlets
  * Go to definition, meme dans les fichiers inclus _. .\path\to\my\file.ps1_
  * Find references
  * Lister les variables et fonctions du fichier
  * Analyse en temps réel du code
  * Débogage local avec surveillance des variables

Les utilisateurs de Visual Studio retrouveront leurs marques car les raccourcis sont identiques.
  
La vie de l’ensemble des utilisateurs de PowerShell en sera améliorée.

![VS Code - complétion powershell][3]

_VS Code &#8211; complétion powershell_

![VS Code - signature de cmdlet][4]

_VS Code &#8211; signature de cmdlet_

![VS Code - debug][5]

_VS Code &#8211; debug_

L’article [Announcing PowerShell language support for Visual Studio Code and more! de David Wilson][6] détaille chacune des fonctionnalités, GIF à l’appui.

Plus de détails sur le débogage dans l’article [Debugging PowerShell Script with Visual Studio Code de Keith Hill][7]

**Références :**

[Visual Studio Marketplace &#8211; vscode.PowerShell][8]

[GitHub repo de vscode.PowerShell][9]

 [1]: /wp-content/uploads/2016/04/VSCode-InstallPS.png
 [2]: /wp-content/uploads/2016/04/VSCode-ExamplesFolder.png
 [3]: /wp-content/uploads/2016/04/VSCode-Completion.png
 [4]: /wp-content/uploads/2016/04/VSCode-Signature.png
 [5]: /wp-content/uploads/2016/04/VSCode-Debug.png
 [6]: https://blogs.msdn.microsoft.com/powershell/2015/11/16/announcing-powershell-language-support-for-visual-studio-code-and-more/
 [7]: https://rkeithhill.wordpress.com/2015/12/27/debugging-powershell-script-with-visual-studio-code/
 [8]: https://marketplace.visualstudio.com/items?itemName=ms-vscode.PowerShell
 [9]: https://github.com/PowerShell/vscode-powershell