---
title: Effet Dunning-Kruger
author: Ghislain
date: 2020-07-07
url: /un-dessin/effet-dunning-kruger/
categories:
  - Un dessin
tags:

---

![Effet Dunning-Kruger](/un-dessin/un-dessin-effet_dunning_kruger.png)

L’effet Dunning-Kruger, ou effet de surconfiance, est un biais cognitif selon lequel les moins qualifiés dans un domaine surestiment leur compétence. 

En 1999, David Dunning et Justin Kruger ont publié les résultats de leurs expériences démontrant ce phénomène. Cet effet ne fait pas l’unanimité et a d’ailleurs reçu le prix igNobel de psychologie en 2000.

Cette étude suggère aussi les effets corollaires : les personnes les plus qualifiées auraient tendance à sous-estimer leur niveau de compétence et penseraient à tort que des tâches faciles pour elles le sont aussi pour les autres.

> Les cons ça ose tout. C'est même à ça qu'on les reconnaît - Michel Audiard

Il est possible de rapprocher l’effet de surconfiance de l'ultracrépidarianisme.

[Schéma mis à dispostion par Arjuna Filips sous licence CC-BY-SA-4.0](https://commons.wikimedia.org/wiki/File:2019-06-19_effet_dunning_kruger.png)
