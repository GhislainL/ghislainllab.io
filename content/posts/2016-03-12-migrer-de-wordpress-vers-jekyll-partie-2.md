---
title: Migrer de WordPress vers Jekyll – partie 2
author: Ghislain
date: 2016-03-12T00:00:00+00:00
url: /2016/03/12/migrer-de-wordpress-vers-jekyll-partie-2/
featuredImage: /wp-content/uploads/2016/03/jekyllrb.png
categories:
  - Blog
tags:
  - Git
  - GitHub
  - Jekyll
  - Ruby
  - WordPress

---
Quelques manipulations et fonctionnalités qu’il est bon d’utiliser avec Jekyll :

  * Utilisation d’un nom de domaine avec GitHub Pages
  * Commentaires avec Disqus
  * Flux RSS
  * SEO

## Nom de domaine {#nom-de-domaine}

Créer un fichier CNAME avec un seul nom de domaine à la racine du dépôt.

Si votre fichier CNAME contient **domain-name.com**, www.domain-name.com redirigera vers domain-name.com.

Si votre fichier CNAME contient **www.domain-name.com**, domain-name.com redirigera vers www.domain-name.com.

Concernant l’enregistrement DNS, il est possible de choisir un ALIAS/ANAME vers http://username.github.io ou un enregistrement A vers les IP 192.30.252.153 et 192.30.252.154.

Chez OVH, je n’ai réussi à le faire fonctionner qu’avec l’enregistrement A. Un collègue chez 1&1 a rencontré le même soucis.

[GitHub &#8211; Setting up an apex domain and www subdomain][1]

## Commentaires avec Disqus {#commentaires-avec-disqus}

Jekyll permet de générer des sites statiques, il faut donc confier la gestion des commentaires à un service tiers. Dans l’univers Jekyll, le fournisseur de service le plus utilisé est Disqus.

1 &#8211; Créer un compte [Disqus][2]

![Disqus – Importer les commentaires][3]

2 &#8211; Exporter le contenu de votre site wordpress au format WXR. /!\ Si l’URL de votre site a changé, il faudra mettre à jour les liens dans le fichier WXR pour que Disqus associe les commentaires aux bonnes pages.

3 &#8211; Importer les commentaires en passant par <https://import.disqus.com>

![Disqus – Importer les commentaires][4]

4 &#8211; Ajouter le [code JavaScript Universel][5] fourni par Disqus dans le modèle de vos posts (par défaut: _layouts/post.html)

5 &#8211; Modifier le code JavaScript pour configurer Disqus en fonction des liens de votre site.<figure class="highlight"> 

{{< highlight js>}}

var disqus_config = function () {
   this.page.url = {{site.baseurl}}/{{page.url}};  // Replace PAGE_URL with your page's canonical URL variable
   this.page.url = {{site.baseurl}}/{{page.url}};  // Replace PAGE_URL with your page's canonical URL variable
   this.page.identifier = {{page.id}}; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
   this.page.title = {{page.title}};
};{{< / highlight  >}}

6 &#8211; Activer/désactiver les commentaires sur une page

Pour activer ou désactiver les commentaires sur une page, ajouter une variable dans l’entête YAML de la page ou du post, puis n’insérer le code JavaScript Disqus que si cette varialbe est vraie.

Entête YAML 

{{< highlight yml>}}
---
title: SEO and Jekyll
author: Ghislain
layout: post
comments: true
---
{{< / highlight  >}}

Modèle de page/post 
{{< highlight html>}}
{% if page.comments == true %}
    <script>
        # code JavaScript Disqus
    </script>
{% endif %}{{< / highlight  >}}

[Disqus &#8211; JavaScript configuration variables][6]

## Flux RSS {#flux-rss}

Ajouter le gem **jekyll-feed** dans le fichier _config.xml. Un fichier ATOM nommé feed.xml sera construit à chaque création du site statique.

[Feed ATOM du www.ghislain-lerda.com][7]

## SEO {#seo}

### Sitemap {#sitemap}

Ajouter le gem **jekyll-sitemap** dans le fichier _config.xml. Un fichier sitemap.xml sera construit à chaque création du site statique.

### Robots.txt {#robotstxt}

Le fichier **robots.txt** indique au robot d’indexation des moteurs de recherche ce qu’ils peuvent voir et ce qu’ils doivent éviter.
  
Plus d’informations sur les liens suivants :

  * [robotstxt][8]
  * [Google &#8211; Learn about robots.txt files][9]

Créer un fichier robots.txt à la racine qui contient par exemple :<figure class="highlight"> 

{{< highlight html>}}
User-agent: *
Sitemap: {{ site.url }}/sitemap.xml{{< / highlight  >}}

### Meta description et keywords pour chaque page {#meta-description-et-keywords-pour-chaque-page}

Il est bon d’avoir une description spécifique pour chaque page. Le texte utilisé peut être une variable existante comme **page.excerpt** ou créée spécifiquement. Dans le second cas, il faut ajouter la variable et sa valeur dans l’entête YAML du post.<figure class="highlight"> 

{{< highlight yml>}}
---
title: SEO and Jekyll
author: Ghislain
layout: post
metadescription: description dedicated to SEO 
keywords : "jekyll, SEO"
---{{< / highlight  >}}<figure class="highlight"> 

{{< highlight html>}}

<meta name="description" content="{{ page.excerpt }}">
<!-- plus travaillé -->
<meta name="description" content="{% if page.metadescritpion %}{{ page.metadescritpion | truncate: 160 }}{% else %}{{ page.excerpt }}{% endif %}">{% endhighlight %}
{% if page.keywords %}
<meta name="keywords" content="{{ page.keywords }}" />
{% endif %}{{< / highlight  >}}

Si vous trouvez ce post intéressant, je vous invite à tester le service de commentaire ci-dessous.

 [1]: https://help.github.com/articles/setting-up-an-apex-domain-and-www-subdomain/
 [2]: https://disqus.com
 [3]: /wp-content/uploads/2016/03/DisqusCreate.PNG
 [4]: /wp-content/uploads/2016/03/DisqusImport.PNG
 [5]: http://docs.disqus.com/developers/universal/
 [6]: https://help.disqus.com/customer/en/portal/articles/472098-javascript-configuration-variables
 [7]: http://ghislainl.github.io/feed.xml
 [8]: http://www.robotstxt.org
 [9]: www.google.com/support/webmasters/bin/answer.py?hl=en&answer=156449