---
title: Optimiser rapidement son jQuery 1.x
author: Ghislain

date: 2015-03-02T08:33:46+00:00
url: /2015/03/02/optimiser-rapidement-son-jquery-1-x/
categories:
  - JavaScript
tags:
  - jquery

---
Quand vous avez peu de temps pour diminuer le temps d’exécution d’un gros bloc de jQuery, le plus rapide est d’optimiser le code pour respecter les bonnes pratiques.

Les optimisations suivantes sont valides pour jQuery 1.x vu que la page était initialement avec cette version.

[<img class="alignnone size-full wp-image-522" src="/wp-content/uploads/2015/02/fastersnail.png?fit=457%2C185" alt="FasterSnail" srcset="/wp-content/uploads/2015/02/fastersnail.png?resize=300%2C121 300w, /wp-content/uploads/2015/02/fastersnail.png?w=457 457w" sizes="(max-width: 457px) 100vw, 457px" data-recalc-dims="1" />][1]

# Selecteur {#selecteur}

Facilitez le parcours du DOM, identifier la zone où chercher puis les éléments à trouver.

{{< highlight js >}}// mal
$(".allp");
// bien
$("#container p.allp");
//mieux (les deux notations sont équivalentes)
$("#container").find("p.allp");
$("p.allp", "#container");

// non optimisé
$( "div#container .allp" );
// optimisé
$( "#container p.allp" );{{< /highlight >}}

# Chaining {#chaining}

Enchainez les actions sur le même selecteur quand c’est possible.

{{< highlight js >}}// mal
$("#object").addClass("myClass");
$("#object").css("color","#f00");
$("#object").height(806);

// bien
$("#object").addClass("myClass").css("color", "#f00").height(806);{{< /highlight >}}

# Caching {#caching}

Stockez vos éléments dans une variable avant de faire vos opérations dessus.

{{< highlight js >}}// bien
var myElements = $("#container").find("li");
$("#hideElements").click(function() {
   myElements.fadeOut();
});
$("#showElements").click(function() {
   myElements.fadeIn();
});{{< /highlight >}}

# Event delegation {#event-delegation}

Attachez la gestion d’événement sur le container et ciblez les éléments déclencheurs.

{{< highlight js >}}// mal
$('#container').find('td.trigger').click(function() {
   $(this).toggleClass('bang');
});

// bien
$('#container').on('click','td.trigger',function() {
   $(this).toggleClass('bang');
});
{{< /highlight >}}

http://learn.jquery.com/events/event-delegation/

# Manipulation du DOM {#manipulation-du-dom}

Assemblez dans une boucle mais ajoutez hors de celle-ci

{{< highlight js >}}// mal
$.each(array, function(count, item) {
   var newElement = buildNewElement(item);
   $('#Elements').append(newElement);
});

// bien
var tmp = '';
$.each(array, function(count, item) {
   tmp += buildNewElement(item);
});
$('#Elements').append(tmp);</pre>

# Boucle {#boucle}

  1. Préférez le for au each

  2. Mettez en cache la taille de la collection

{{< highlight js >}}// mal
$.each (array, function (i) {
   doSomething(i);
});

// bien
var arrLength = array.length;
for ( var i = 0; i < arrLength ; i++ ) {
   doSomething(i);
}{{< /highlight >}}

# Manipulation de CSS {#manipulation-de-css}

Utilisez AddClass plutôt que css() pour plusieurs éléments.

Nous sommes d’accord que cela ne vaut pas une optimisation en bonne et dûe forme.

Mais si comme dans mon cas, votre code initial est loin d’être propre, vous serez surpris du gain inhérent à règles.

 [1]: /wp-content/uploads/2015/02/fastersnail.png