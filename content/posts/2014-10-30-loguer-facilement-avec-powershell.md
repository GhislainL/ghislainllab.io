---
title: Loguer facilement avec PowerShell
author: Ghislain

date: 2014-10-30T07:40:14+00:00
url: /2014/10/30/loguer-facilement-avec-powershell/
categories:
  - PowerShell
tags:
  - PowerShell
  - Start-Transcript
  - Stop-Transcript

---
Lorsque vous écrivez un script PowerShell, vous voulez garder une trace de son exécution. Les raisons peuvent être multiples : étudier les éventuelles erreurs, savoir comment s’est déroulée une exécution planifiée, comprendre ce qu’il se passe sur un environnement sur lequel vous n’avez pas la main, etc.

Nativement PowerShell met à votre disposition un couple de commande : **Start-Transcript** et **Stop-Transcript**. Cela permet de dupliquer les lignes de l’invite de commande dans un fichier en rajoutant un entête et un pied de page horodatés.

{{< highlight powershell >}}"avant le transcript"
Start-Transcript "c:Transcript.txt"
dir
Stop-transcript
"après le transcript"</pre>

Le fichier de log ne contient que les lignes comprises entre les *-Transcript.

{{< highlight plain >}}**********************
Windows PowerShell Transcript Start
Start time: 20141029191656
Username  : sps2010ghislain
Machine      : sps2010 (Microsoft Windows NT 6.1.7601 Service Pack 1)
**********************
Transcript started, output file is c:Transcript.txt

    Directory: C:Users&#092;&#048;0svcSPFarmDevDesktop

Mode                LastWriteTime     Length Name                                                                   
----                -------------     ------ ----                                                                   
d----        19/08/2014     17:46            Governance                                                             
d----        03/06/2013     17:18            ILSpy_Master_2.1.0.1603_RTW_Binaries                                   
d----        05/06/2014     14:14            Permission Reporting                                                  
d----        04/02/2014     14:32            SharePoint Manager 2010                                                
-a---        22/05/2013     10:02      90624 AutoSPInstaller.zip           
-a---        29/04/2014     14:31   12649871 PowerGUI.3.8.0.129.zip                                                 
-a---        11/12/2013     10:52     290816 SharePointLogViewer.exe                                                
-----        11/08/2011     21:38     557056 smtp4dev.exe                                                           
-a---        29/10/2014     18:16         67 transcript.ps1                                                                 
**********************
Windows PowerShell Transcript End
End time: 20141029191656
**********************</pre>

Une autre méthode plus _artisanale_ mais qui vous offre un meilleur découpage du temps :

{{< highlight powershell >}}$logFileName = "c:log.txt"
Function Log($text) {
    Write-Host $text
    "[" + $(Get-Date -f o) + "] [Info] : " + $text &gt;&gt; $logFileName
}

Function LogSuccess($text) {
    Write-Host $text -foregroundcolor Green
    "[" + $(Get-Date -f o) + "] [Success] : " + $text &gt;&gt; $logFileName
}

Function LogError($text) {
    Write-Host $text -foregroundcolor Red
    "[" + $(Get-Date -f o) + "] [Error] : " + $text &gt;&gt; $logFileName
}</pre>

Avec ça, vous êtes paré.

**Références :**

[Start-Transcript][1]

[Stop-Transcript][2]

 [1]: http://technet.microsoft.com/fr-fr/library/hh849687.aspx
 [2]: http://technet.microsoft.com/fr-fr/library/hh849688.aspx