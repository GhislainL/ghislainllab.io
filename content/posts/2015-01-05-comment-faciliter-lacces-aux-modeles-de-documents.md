---
title: Comment faciliter l’accès aux modèles de documents ?
author: Ghislain

date: 2015-01-05T08:31:15+00:00
url: /2015/01/05/comment-faciliter-lacces-aux-modeles-de-documents/
categories:
  - SharePoint
tags:
  - content type hub
  - ECM
  - SharePoint
  - SharePoint 2013

---
Précédemment vous avez vu l’intérêt [des types de contenu][1] et [des modèles de document][2]. Ces fonctionnalités ont pour cadre d’action la collection de site. Heureusement vous pouvez les généraliser à l’aide du concentrateur de type de contenu.

# **Description du Concentrateur de type de contenu** {#description-du-concentrateur-de-type-de-contenu}

Le concentrateur de type de contenu – CTH par la suite pour Content Type Hub – est une fonctionnalité de collection de site. Si vous l’activez, elle permet de rendre disponible les types de contenu de cette collection de site dans les autres collections de site consommant le même service de métadonnées gérées.

Vous pouvez le considérer comme un référentiel de type de contenu. Vous avez ainsi la possibilité de centraliser les types de contenu fréquemment utilisés dans l’entreprise ainsi que les modèles de document de l’entreprise.

Concrètement, vous pouvez maintenir les modèles de document de chaque service de l’entreprise dans un seul endroit. Vous rendez ainsi accessible, en un clic, le dernier modèle pour tous les utilisateurs dans la ferme.

[<img class="alignnone size-full wp-image-369" src="/wp-content/uploads/2014/12/contenttypehub.png?fit=487%2C358" alt="ContentTypeHub" srcset="/wp-content/uploads/2014/12/contenttypehub.png?resize=300%2C221 300w, /wp-content/uploads/2014/12/contenttypehub.png?w=487 487w" sizes="(max-width: 487px) 100vw, 487px" data-recalc-dims="1" />][3]

<!--more-->

# **Mise à disposition du CTH** {#mise-à-disposition-du-cth}

## Création d’un CTH {#création-dun-cth}

Sur le site qui servira de CTH, activer de la feature **Content Type Syndication Hub** de scope site collection

[<img class="alignnone size-full wp-image-368" src="/wp-content/uploads/2014/12/contenttypefeature.png?fit=776%2C229" alt="ContentTypeFeature" srcset="/wp-content/uploads/2014/12/contenttypefeature.png?resize=300%2C89 300w, /wp-content/uploads/2014/12/contenttypefeature.png?w=776 776w" sizes="(max-width: 776px) 100vw, 776px" data-recalc-dims="1" />][4]

## Configuration du service de gestion des métadonnées {#configuration-du-service-de-gestion-des-métadonnées}

Dans la Centrale d’administration

Modifier les propriétés de votre **Managed MetaData Service** pour renseigner l’adresse de votre content type hub.

[<img class="alignnone size-full wp-image-366" src="/wp-content/uploads/2014/12/mmsconfig.png?fit=827%2C657" alt="MMSConfig" srcset="/wp-content/uploads/2014/12/mmsconfig.png?resize=300%2C238 300w, /wp-content/uploads/2014/12/mmsconfig.png?w=827 827w" sizes="(max-width: 827px) 100vw, 827px" data-recalc-dims="1" />][5]

Modifier la **connexion du MMS** pour accepter le CTH

[<img class="alignnone size-full wp-image-367" src="/wp-content/uploads/2014/12/mmsconnectionconfig.png?fit=825%2C659" alt="MMSConnectionConfig" srcset="/wp-content/uploads/2014/12/mmsconnectionconfig.png?resize=300%2C240 300w, /wp-content/uploads/2014/12/mmsconnectionconfig.png?w=825 825w" sizes="(max-width: 825px) 100vw, 825px" data-recalc-dims="1" />][6]

## Création d’un type de contenu à partager {#création-dun-type-de-contenu-à-partager}

De retour sur votre collection de site CTH, créer un type de contenu. Par exemple : DocGLA dans le groupe _Ghislain (pour qu’il soit en premier), qui hérite de document, avec une colonne Categories et un modèle de document

[<img class="alignnone size-full wp-image-364" src="/wp-content/uploads/2014/12/ctpublishing.png?fit=325%2C444" alt="CTPublishing" data-recalc-dims="1" />][7]

<table>
  <tr>
    <td width="614">
      <strong>Astuce</strong>
    </td>
  </tr>
  
  <tr>
    <td width="614">
      Pour que vos modèles de document soient répliqués et utilisables dans toutes les collections de site consommatrices, vous devez laisser SharePoint associer le modèle à votre type de contenu (/_cts/<contentTypeName>). Si vous choisissez une bibliothèque personnalisée pour votre modèle, il ne sera pas répliqué.<a href="/wp-content/uploads/2014/12/cttemplate.png"><img class="alignnone size-full wp-image-365" src="/wp-content/uploads/2014/12/cttemplate.png?fit=655%2C128" alt="CTTemplate" srcset="/wp-content/uploads/2014/12/cttemplate.png?resize=300%2C59 300w, /wp-content/uploads/2014/12/cttemplate.png?w=655 655w" sizes="(max-width: 655px) 100vw, 655px" data-recalc-dims="1" /></a><br /> </td><br /> </tr><br /> </table></p> 
      
      <p>
        ## Publication d&rsquo;un type de contenu
      </p>
      
      <p>
        Publier ensuite votre type de contenu en cliquant sur Manage publishing for this content type. Et Choisir Publish. Vous remarquez la possibilité de mettre à jour ou retirer un type de contenu publié.
      </p>
      
      <p>
        [<img class="alignnone size-full wp-image-363" src="/wp-content/uploads/2014/12/ctpublish.png?fit=758%2C409" alt="CTPublish" srcset="/wp-content/uploads/2014/12/ctpublish.png?resize=300%2C162 300w, /wp-content/uploads/2014/12/ctpublish.png?w=758 758w" sizes="(max-width: 758px) 100vw, 758px" data-recalc-dims="1" />](/wp-content/uploads/2014/12/ctpublish.png)
      </p>
      
      <p>
        ## Exécution des taches planifiées liées aux CTH
      </p>
      
      <p>
        Le CTH utilise 2 taches planifiées (cf tableau ci-dessous) pour gérer la publication des types de contenu. Si vous voulez voir publié immédiatement vos types de contenu, il faudra les exécuter depuis la centrale d’administration.
      </p>
      
      <table>
        <tr>
          <td width="150">
            Nom
          </td>
          
          <td width="163">
            Description
          </td>
          
          <td width="165">
            Planification
          </td>
          
          <td width="140">
            Scope
          </td>
        </tr>
        
        <tr>
          <td width="150">
            Content type   Hub
          </td>
          
          <td width="163">
            Tracks content type log maintenance and managers unpublished content types
          </td>
          
          <td width="165">
            Daily
          </td>
          
          <td width="140">
            Farm
          </td>
        </tr>
        
        <tr>
          <td width="150">
            Content Type Subscriber
          </td>
          
          <td width="163">
            Retrieves content types packages from the hub and applies them to the local content type gallery
          </td>
          
          <td width="165">
            Hourly
          </td>
          
          <td width="140">
            webapp
          </td>
        </tr>
      </table>
      
      <p>
        Maintenant vous pouvez consommer ces types de contenu publiés.
      </p>
      
      <table>
        <tr>
          <td width="614">
            <strong>Astuce</strong>
          </td>
        </tr>
        
        <tr>
          <td width="614">
            Comme la distribution des types de contenu partagés se fait par le Managed Metadata Service, vous pouvez constater qu&rsquo;ils sont absents dans certaines de vos collections de site consommatrices. Pour y remédier , vous devez activer la taxonomie sur celles-ci :</p> </p> 
            
            {{< highlight powershell>}}Enable-SPFeature -Identity "TaxonomyFieldAdded" -Url http://site.contoso.com</pre>
          </td>
        </tr>
      </table>
      
      <p>
        Dernier point, vous pouvez savoir si une collection de site est consommatrice en cliquant sur **Content type publishing** dans la section **Site Collection Administration**.
      </p>
      
      <p>
        [<img class="alignnone size-full wp-image-362" src="/wp-content/uploads/2014/12/contenttypepublishinghub.png?fit=677%2C438" alt="ContentTypePublishingHub" srcset="/wp-content/uploads/2014/12/contenttypepublishinghub.png?resize=300%2C194 300w, /wp-content/uploads/2014/12/contenttypepublishinghub.png?w=677 677w" sizes="(max-width: 677px) 100vw, 677px" data-recalc-dims="1" />](/wp-content/uploads/2014/12/contenttypepublishinghub.png)
      </p>
      
      <p>
        Avec cela, vous faciliterez la vie de vos utilisateurs qui gagneront un temps incroyable et augmenterez l&rsquo;adoption de SharePoint.<br /> </contentTypeName></td> </tr> </table>

 [1]: https://ghislainfo.wordpress.com/2014/09/08/pourquoi-supprimer-les-dossiers-dans-sharepoint/ "Pourquoi supprimer les dossiers dans SharePoint?"
 [2]: https://ghislainfo.wordpress.com/2014/10/20/pourquoi-lier-un-modele-de-document-a-un-type-de-contenu/
 [3]: /wp-content/uploads/2014/12/contenttypehub.png
 [4]: /wp-content/uploads/2014/12/contenttypefeature.png
 [5]: /wp-content/uploads/2014/12/mmsconfig.png
 [6]: /wp-content/uploads/2014/12/mmsconnectionconfig.png
 [7]: /wp-content/uploads/2014/12/ctpublishing.png