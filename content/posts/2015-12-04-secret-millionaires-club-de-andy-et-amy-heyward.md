---
title: Secret Millionaires Club de Andy et Amy Heyward
author: Ghislain

date: 2015-12-04T00:00:00+00:00
url: /2015/12/04/secret-millionaires-club-de-andy-et-amy-heyward/
categories:
  - Développement personnel
tags:
  - Heyward
  - livres
  - Secret Millionaires Club

---
**Secret Millionaires Club** de Andy et Amy Heyward, 26 secrets pour réussir dans la vie professionnelle

Ce livre en anglais écrit en association avec Warren Buffett se présente comme du contenu ayant un but pour enfant. L’expression contenu ayant un but signifie qu’il permet de comprendre le monde dans lequel on vit et qu’il apporte des leçons de vie précieuses qui vous serviront toute votre vie. Ne vous y trompez pas, les 26 secrets présentés sont aussi valables pour les adultes.

![Secret Millionaires Club de Andy et Amy Heyward][1]

Chaque secret reprend le modèle suivant :

  * Secret
  * La catch phrase
  * Explication
  * Exemple tiré de l’épisode télévisé 
      * Situation problématique
      * Analyse du problème
      * Résolution
      * Analyse des effets de la solution
  * Rappel de la leçon

Avant tout, il faut savoir que plus vous apprendrez, plus vous gagnerez

> The more you learn, the more you earn. 

Mais quels sont ces secrets ?

  1. N’ayez pas peur de faire des erreurs
  2. N’empruntez pas d’argent
  3. Aimez ce que vous faites
  4. Trouvez du temps pour le travail comme pour le divertissement
  5. Ecoutez ce que les autres peuvent vous apprendre
  6. Pensez toujours à de nouvelles idées
  7. Ayez toujours un plan
  8. Si vous échouez, essayer encore
  9. Protégez votre réputation
 10. Le client passe en premier
 11. Un esprit ouvert laisse venir de meilleures idées
 12. Toujours vous promouvoir
 13. L’emplacement est très important
 14. Ne soyez pas dupe
 15. Ne jugez pas les autres
 16. Soyez prêt à changer vos idées
 17. Votre image est importante
 18. Soyez attentionné envers les autres
 19. Faites attentions aux détails
 20. Comment vous présenter
 21. Vos décisions affectent votre futur
 22. Economisez pour le futur
 23. Soyez quelqu’un en demande
 24. La confiance vient avec la compréhension
 25. Une vie d’apprentissage
 26. Les partenaires vous facilitent les choses

Petit par la taille mais grand par sa valeur, Secret Millionaires Club constitue un memento et un résume des dessins animés de la série du même nom. Si vous voulez allez plus loin, vous trouverez toujours une ressource. Par exemple, le #21 Vos décisions affectent votre futur est développé dans L’effet cumulé de Darren Hardy.

Bonne lecture et mise en application.

 [1]: /wp-content/uploads/2016/03/SecretMillionairesClub.jpg