---
title: Plus loin que l’évident Désolé. Vous n’avez pas accès à cette page
author: Ghislain

date: 2016-05-24T00:00:00+00:00
url: /2016/05/24/plus-loin-que-levident-desole-vous-navez-pas-acces-a-cette-page/
categories:
  - SharePoint
tags:
  - SharePoint 2013

---
Désolé. Vous n’avez pas accès à cette page.
  
Sorry, this site hasn’t been share with you

Ce message explicite dit que l’utilisateur n’a pas les droits pour accéder à la page ou au site SharePoint 2013. Mais parfois il faut passer outre l’évidence.
  
Le site était inaccessible mais les pages du _layouts étaient accessibles. Même un compte avec des accès donnés au niveau de la web application ne pouvait y accéder.

![SP2013 - Vous n'avez pas accès à cette page][1]

Pour résoudre ce problème, j’ai croisé plusieurs explications. Je vous liste ces 8 qui nécessitent d’aller plus loin que l’évidence:

  1. Les crédentials sont stockées dans le cache du navigateur. Videz-le en employant la méthode la plus efficace pour votre navigateur.
  2. Vous avez installé un patch/CU/Service Pack SharePoint sans exécuter le wizard. Exécutez le product and configuration wizard
  3. Vous avez installé un patch/CU/Service Pack SharePoint et vos bases de contenu sont en lecture seule. Passez par SQL Management pour basculer vos bases de contenu en lecture/écriture
  4. Arrêtez et redémarrez le service Microsoft SharePoint Foundation Web Application sur les frontaux depuis la centrale admin.
  5. Vous avez migré depuis SP2010 ou réaliser un backup/restore voir un import/export, vérifiez que le mode d’authentification est de type Claims. Le cas échéant le changer.
  6. Vous avez configuré les comptes SuperUser et SuperReader mais avez oublié des données les droits appropriés. Donnez les droits Full read à SuperReader et Full control à SuperUser sur les webapplications.
  7. Nettoyez le cache distribué et effectuer un IISreset
  8. Vérifiez qu’aucun des comptes utilisés pour exécuter les pools n’est bloqué ou a vu son mot de passe changer.

J’ai eu le plaisir de faire face au 2, 3 et 6.
  
Avez vous déjà rencontré ce soucis ?

 [1]: /wp-content/uploads/2016/05/NotShareWithYou.png