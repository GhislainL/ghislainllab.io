---
title: Migration massive de documents dans SharePoint
author: Ghislain

date: 2014-12-15T08:35:13+00:00
url: /2014/12/15/migration-massive-de-documents-dans-sharepoint/
categories:
  - SharePoint
tags:
  - Office365
  - Outils
  - SharePoint
  - SharePoint 2010
  - SharePoint 2013

---
L’un des leviers d’adoption de SharePoint réside dans sa substitution au stockage de document par système de fichier (ordinateur personnel ou partage réseau). Cette opération est plus ou moins longue et complexe en fonction de la quantité de document à intégrer et à qualifier (saisir les métadonnées). Si vous avez peu de document à migrer avec des types de contenu simple, cela sera plus aisé que pour des milliers de documents avec des dizaines d’informations à remplir pour chacun.

Vous disposez des options natives de SharePoint suivantes pour uploader des documents:

  * ajouter un document : ajout puis qualification
  * ajouter plusieurs documents : ajout unique puis qualification de chaque
  * travailler en mode explorateur : ajout par l’explorateur, retour dans l’interface SharePoint pour qualification de chaque

Vous vous rendez compte que la meilleure solution native est l’utilisateur conjointe de l’explorateur Windows avec la vue Feuille de données. Cette combinaison fonctionne pour des petites volumétries mais montre rapidement ses limites :

  * Risque d’interruption lors de gros transferts
  * Non automatisable

Bref ce n’est pas la panacée.

**Heureusement** vous disposez de plusieurs logiciels gratuits ou payants, s’exécutant sur un poste client ou directement sur un serveur SharePoint, dont le fonctionnement est assez similaire.

  1. Vous avez des documents à importer dans une arborescence nécessitera probablement un peu d’organisation et nettoyage:

[<img class="alignnone size-full wp-image-351" src="/wp-content/uploads/2014/12/bulk-arbo.png?fit=570%2C382" alt="Bulk-Arbo" srcset="/wp-content/uploads/2014/12/bulk-arbo.png?resize=300%2C201 300w, /wp-content/uploads/2014/12/bulk-arbo.png?w=570 570w" sizes="(max-width: 570px) 100vw, 570px" data-recalc-dims="1" />][1]

  1. Vous définissez un type de contenu qui permet de les qualifier dans SharePoint.

Type de contenu : **Budget** qui hérité de Document

Colonnes :

  * <table>
      <tr>
        <td>
          <strong>Année </strong>
        </td>
        
        <td>
          Choix [2013 ;2014 ;2015]
        </td>
      </tr>
    </table>

  * <table>
      <tr>
        <td>
          <strong>Compagnie </strong>
        </td>
        
        <td>
          Choix [CompagnieA ;CompagnieB ; CompagnieC]
        </td>
      </tr>
    </table>

  * <table>
      <tr>
        <td>
          <strong>TypeBudget</strong>
        </td>
        
        <td>
          Choix [Arrêtés mensuels ;Divers ;Estimés ;Facturation]
        </td>
      </tr>
    </table>

Vous créez un dictionnaire associant les métadonnées et la destination pour chaque emplacement dans l’arborescence. Ainsi vous retrouverez tous les documents de partageBudget2013CompagnieAArretesMensuels dans <http://compta.contoso.com/Budget> avec les métadonnées renseignées suivantes :

  * Année : 2013
  * Compagnie CompagnieA
  * TypeBudget : Arrêtés mensuels

[<img class="alignnone size-full wp-image-352" src="/wp-content/uploads/2014/12/bulk-dico.png?fit=862%2C264" alt="Bulk-Dico" srcset="/wp-content/uploads/2014/12/bulk-dico.png?resize=300%2C92 300w, /wp-content/uploads/2014/12/bulk-dico.png?w=862 862w" sizes="(max-width: 862px) 100vw, 862px" data-recalc-dims="1" />][2]

Vous trouverez ci-dessous un tableau comparatif de solutions que j’ai eu l’occasion de croiser, donc non exhaustif. J’espère que cela vous aidera à choisir en fonction de vos besoins et contraintes.

<table width="734">
  <tr>
    <td width="260">
      <strong>Nom</strong>
    </td>
    
    <td width="80">
      <strong>Coût</strong>
    </td>
    
    <td width="85">
      <strong>Compatible 2013</strong>
    </td>
    
    <td width="85">
      <strong>Compatible Office365</strong>
    </td>
    
    <td width="103">
      <strong>Métadonnées</strong>
    </td>
    
    <td width="122">
      <strong>Exécution</strong>
    </td>
  </tr>
  
  <tr>
    <td width="260">
      <strong>Document importer for SP</strong>
    </td>
    
    <td width="80">
      <a href="/wp-content/uploads/2014/12/free-icon.png"><img class="alignnone size-full wp-image-349" src="/wp-content/uploads/2014/12/free-icon.png?fit=24%2C24" alt="free-icon" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="103">
      Pas encore
    </td>
    
    <td width="122">
      Client
    </td>
  </tr>
  
  <tr>
    <td width="260">
      <strong>SP2010 Bulk document importer</strong>
    </td>
    
    <td width="80">
      <a href="/wp-content/uploads/2014/12/free-icon.png"><img class="alignnone size-full wp-image-349" src="/wp-content/uploads/2014/12/free-icon.png?fit=24%2C24" alt="free-icon" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
      &#8211;
    </td>
    
    <td width="85">
    </td>
    
    <td width="103">
    </td>
    
    <td width="122">
      Client
    </td>
  </tr>
  
  <tr>
    <td width="260">
      <strong>FCI SharePoint Upload PowerShell script</strong>
    </td>
    
    <td width="80">
      <a href="/wp-content/uploads/2014/12/free-icon.png"><img class="alignnone size-full wp-image-349" src="/wp-content/uploads/2014/12/free-icon.png?fit=24%2C24" alt="free-icon" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
    </td>
    
    <td width="85">
    </td>
    
    <td width="103">
    </td>
    
    <td width="122">
      WebService
    </td>
  </tr>
  
  <tr>
    <td width="260">
      <strong>SharePoint Multiple File Upload Script</strong>
    </td>
    
    <td width="80">
      <a href="/wp-content/uploads/2014/12/free-icon.png"><img class="alignnone size-full wp-image-349" src="/wp-content/uploads/2014/12/free-icon.png?fit=24%2C24" alt="free-icon" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
      &#8211;
    </td>
    
    <td width="85">
    </td>
    
    <td width="103">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="122">
      Serveur
    </td>
  </tr>
  
  <tr>
    <td width="260">
      <strong>Metalogic FileShare Migration Manager for SharePoint</strong>
    </td>
    
    <td width="80">
      <a href="/wp-content/uploads/2014/12/euro_coin.png"><img class="alignnone size-full wp-image-350" src="/wp-content/uploads/2014/12/euro_coin.png?fit=24%2C24" alt="euro_coin" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
      &#8211;
    </td>
    
    <td width="85">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="103">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="122">
      &#8211;
    </td>
  </tr>
  
  <tr>
    <td width="260">
      <strong>Vyapin &#8211; DocKit</strong>
    </td>
    
    <td width="80">
      <a href="/wp-content/uploads/2014/12/euro_coin.png"><img class="alignnone size-full wp-image-350" src="/wp-content/uploads/2014/12/euro_coin.png?fit=24%2C24" alt="euro_coin" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="103">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="122">
      &#8211;
    </td>
  </tr>
  
  <tr>
    <td width="260">
      <strong>DocAve File System Migrator for Microsoft SharePoint</strong>
    </td>
    
    <td width="80">
      <a href="/wp-content/uploads/2014/12/euro_coin.png"><img class="alignnone size-full wp-image-350" src="/wp-content/uploads/2014/12/euro_coin.png?fit=24%2C24" alt="euro_coin" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="103">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="122">
      &#8211;
    </td>
  </tr>
  
  <tr>
    <td width="260">
      <strong>Migration Dragon 2013</strong>
    </td>
    
    <td width="80">
      <a href="/wp-content/uploads/2014/12/free-icon.png"><img class="alignnone size-full wp-image-349" src="/wp-content/uploads/2014/12/free-icon.png?fit=24%2C24" alt="free-icon" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
    </td>
    
    <td width="103">
    </td>
    
    <td width="122">
      Client
    </td>
  </tr>
  
  <tr>
    <td width="260">
      <strong>Migration Dragon 2010</strong>
    </td>
    
    <td width="80">
      <a href="/wp-content/uploads/2014/12/free-icon.png"><img class="alignnone size-full wp-image-349" src="/wp-content/uploads/2014/12/free-icon.png?fit=24%2C24" alt="free-icon" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
      &#8211;
    </td>
    
    <td width="85">
    </td>
    
    <td width="103">
    </td>
    
    <td width="122">
      Client
    </td>
  </tr>
  
  <tr>
    <td width="260">
      <strong>Office 365 Bulk Uploader</strong>
    </td>
    
    <td width="80">
      <a href="/wp-content/uploads/2014/12/euro_coin.png"><img class="alignnone size-full wp-image-350" src="/wp-content/uploads/2014/12/euro_coin.png?fit=24%2C24" alt="euro_coin" data-recalc-dims="1" /></a>
    </td>
    
    <td width="85">
       &#8211;
    </td>
    
    <td width="85">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="103">
      <a href="/wp-content/uploads/2014/10/addcheck.png"><img class="alignnone size-full wp-image-259" src="/wp-content/uploads/2014/10/addcheck.png?fit=24%2C24" alt="addCheck" data-recalc-dims="1" /></a>
    </td>
    
    <td width="122">
      Client
    </td>
  </tr>
</table>

Une fois équipé de l’outil adéquat, il vous faudra du temps, de l’organisation et probablement une bonne dose de patience.

**Références :**

<https://difs.codeplex.com/>

<https://spbulkdocumentimport.codeplex.com/>

<https://gallery.technet.microsoft.com/office/f538c34c-4f74-4645-9649-fd25e49805d6>

<https://spfileupload.codeplex.com/>

[http://www.metalogix.com/][3]

[http://www.vyapin.com/][4]

[http://www.avepoint.com/][5]

<https://gallery.technet.microsoft.com/office/The-Migration-Dragon-for-c0880e59>

<https://gallery.technet.microsoft.com/office/The-Migration-Dragon-for-628acae0>

[http://www.office365experts.be/en/products/office-365-bulk-uploader][6]

 [1]: /wp-content/uploads/2014/12/bulk-arbo.png
 [2]: /wp-content/uploads/2014/12/bulk-dico.png
 [3]: //www.metalogix.com/
 [4]: //www.vyapin.com/
 [5]: //www.avepoint.com/
 [6]: //www.office365experts.be/en/products/office-365-bulk-uploader