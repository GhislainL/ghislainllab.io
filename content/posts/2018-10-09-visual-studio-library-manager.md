---
title: Client Side Package management with Library Manager
author: admin7364

date: 2018-10-09T12:17:39+00:00
url: /2018/10/09/visual-studio-library-manager/
featuredImage: /wp-content/uploads/2018/10/LibraryManager.jpg
categories:
  - dotNet
  - DotNet Core
tags:
  - Visual Studio
format: video

---
{{< youtube ZSOsROOkpA4 >}}