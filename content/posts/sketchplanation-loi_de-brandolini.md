---
title: Loi de Brandolini
author: Ghislain
date: 2020-08-20
url: /un-dessin/loi-de-brandolini/
categories:
  - Un dessin
tags:
  - sketchplanations

---

![Loi de Brandolini](/un-dessin/sketchplanations-brandolini-s-law.jpg)

En janvier 2013, le développeur italien Alberto Brandolini formule une observation sur Twitter (1) qui deviendra la loi de Brandolini, aussi connue sous le nom de principe d'asymétrie des idioties : 

> The amount of energy needed to refute bullshit is an order of magnitude bigger than to produce it.

> La quantité d'énergie nécessaire pour réfuter des idioties est supérieure d'un ordre de grandeur à celle nécessaire pour les produire.

C’est l’une des raisons pour lesquelles il ne faut pas renverser la charge de la preuve (2).

Phil Williamson a écrit dans un article dans Nature mettant en avant que nous pourrions utiliser notre temps et énergie pour corriger la désinformation où nous le pouvons (3). Dans cet article, il émet l’idée que la communauté scientifique mondiale pourrait mettre en place son propre propre système de notation pour les sites web qui prétendent présenter des informations scientifiques. Elle pourrait s’appeler Scientific Honesty and Integrity Tracker et attribuer aux conneries en ligne une notation SHIT. Une sorte de WOT (Web of Trust) scientifique en quelque sorte.

Quelques références concernant la désinformation :
- Petit cours d'autodéfense intellectuelle de Normand Baillargeon
- Propaganda de Edward Bernays
- Calling Bullshit: The Art of Skepticism in a Data-Driven World de Carl Bergstrom and Jevin West
- [Defakator](https://www.youtube.com/channel/UCU0FhLr6fr7U9GOn6OiQHpQ) et [Defakator – Vite Fait](https://www.youtube.com/c/DefakatorViteFait/featured)

__Références__ :
1. [Tweet de Albert Brandolini](https://twitter.com/krelnik/status/472046082135162881)
2. [Renverser la charge de la preuve sur Wikipedia](https://fr.wikipedia.org/wiki/Renverser_la_charge_de_la_preuve_(philosophie))
3. [Take the time and effort to correct misinformation - Nature](https://www.nature.com/news/take-the-time-and-effort-to-correct-misinformation-1.21106)

Adaptation de la publication de [Sketchplanations](https://www.sketchplanations.com/post/625980171437195264/the-bs-asymmetry-principle-also-known-as)