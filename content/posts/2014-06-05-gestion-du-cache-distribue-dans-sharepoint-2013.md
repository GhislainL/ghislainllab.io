---
title: Gestion du cache distribué dans SharePoint 2013
author: Ghislain

date: 2014-06-05T08:34:44+00:00
url: /2014/06/05/gestion-du-cache-distribue-dans-sharepoint-2013/
categories:
  - PowerShell
  - SharePoint
tags:
  - Add-SPDistributedCacheServiceInstance
  - distributed cache
  - PowerShell
  - SharePoint
  - SharePoint 2013
  - SPDistributedCacheService

---
La gestion du cache distribué compte parmi l’un des points les plus laborieux de l’administration SharePoint 2013. Premièrement il est géré par [AppFabric][1] et non SharePoint. Deuxièmement il est administrable uniquement par PowerShell. Troisièmement s’il manque de mémoire, plusieurs services de la ferme fonctionneront de manière erratique, voire pas du tout.

Je me permets ces commentaires car il m’a donné du fil à retordre lors de l’installation et configuration des fermes. Et dernièrement, on s’est retrouvé 2 fois de suite avec un newsfeed vide au petit matin en production. D’ailleurs l’avertissement sur la technet est explicite :

> Le service de cache distribué peut se retrouver dans un état inopérationnel ou irrécupérable si vous ne suivez pas les procédures répertoriées dans cet article. Dans des situations extrêmes, vous pourriez avoir à recréer la batterie de serveurs. Le cache distribué dépend du prérequis Windows Server AppFabric. N’administrez pas le **Service de mise en cache AppFabric** depuis la fenêtre **Services** des **Outils d’administration** du **Panneau de configuration**. N’utilisez pas les applications dans le dossier nommé **AppFabric pour Windows Server** dans le menu **Démarrer**.

ou encore :

> <span style="color: #ff0000;">L’arrêt du cache engendre une perte de données partielle.</span> Le cache de flux dépend du service de cache distribué. Les balises et les activités de document sont enregistrées uniquement dans le cache de flux ; elles ne sont pas persistantes dans les bases de données de contenu. Lorsque le service de cache distribué est arrêté, les balises et les activités de document sont perdues. […] Une manière de conserver les balises et les activités de document consiste à utiliser la méthode décrite dans la section [Effectuer un arrêt approprié du service de cache distribué][2] plus loin dans cet article. Lorsque la méthode de l’arrêt approprié du service de cache distribué est utilisée, toutes les données du cache sont déplacées d’un serveur vers un autre avant l’arrêt du service de cache distribué.

La cache distribué peut être hébergé soit sur un seul serveur soit sur un cluster de cache et l’administration sera différente dans les deux cas.

<!--more-->

# **Un seul hôte** {#un-seul-hôte}

## Démarrer {#démarrer}

{{< highlight powershell>}}$instanceName ="SPDistributedCacheService Name=AppFabricCachingService"
$serviceInstance = Get-SPServiceInstance | ? {($_.service.tostring()) -eq $instanceName -and ($_.server.name) -eq $env:computername}
$serviceInstance.Provision(){{< /highlight >}}

## Arréter {#arréter}

{{< highlight powershell>}}$instanceName ="SPDistributedCacheService Name=AppFabricCachingService"
$serviceInstance = Get-SPServiceInstance | ? {($_.service.tostring()) -eq $instanceName -and ($_.server.name) -eq $env:computername}
$serviceInstance.Unprovision()
{{< /highlight >}}

# **Cluster de cache** {#cluster-de-cache}

Dans une batterie de serveurs SharePoint Server 2013, il existe un seul cache qui lequel s’étend sur tout le cluster de cache. Pour supprimer un hôte du cluster de cache, il fait l’arrêter proprement ce qui permet de transférer ses données dans les autres hôtes du cluster.

Dixit la technet _L’exécution du processus de transfert nécessite au moins 15 minutes selon le nombre d’éléments existant dans le cache._

## Ajouter un serveur au cluster de cache et démarrer le service de cache distribué à l’aide de Windows PowerShell {#ajouter-un-serveur-au-cluster-de-cache-et-démarrer-le-service-de-cache-distribué-à-laide-de-windows-powershell}

{{< highlight powershell>}}Add-SPDistributedCacheServiceInstance{{< /highlight >}}

## Supprimer un serveur du cluster de cache à l’aide de Windows PowerShell. {#supprimer-un-serveur-du-cluster-de-cache-à-laide-de-windows-powershell}

<span style="color: #ff0000;">Le Stop[…] –Graceful est nécessaire pour éviter de perdre des données lors de l’arret.</span>

{{< highlight powershell>}}Stop-SPDistributedCacheServiceInstance -Graceful
Remove-SPDistributedCacheServiceInstance
{{< /highlight >}}

## Réparer le cache {#réparer-le-cache}

Généralement on se sert de ces commandes quand rien d’autres ne fonctionne ou que l’on a l’erreur suivante : **cacheHostInfo is null**. Concrètement cela consiste à le supprimer puis le recréer.

{{< highlight powershell>}}$instance = "SPDistributedCacheService Name=AppFabricCachingService"
$service = Get-SPServiceInstance |?{($_.service.tostring()) -eq $instancename -and ($_.server.name) -eq $env:COMPUTERNAME}
$service.delete()
Add-SPDistributedCacheServiceInstance
{{< /highlight >}}

# **Gestion de la mémoire** {#gestion-de-la-mémoire}

Par défaut 10% de la mémoire de la machine mais en cas de modification de la mémoire il faut réallouer à la main avec la règle suivante :

> Mémoire pour la cache distribué = min(16Go, (mémoire du serveur – 2Go)/2)

Important : quand l’environnement est virtualisé, il faut que l’allocation de mémoire pour les serveurs hébergeant le cache distribué soit fixe. Par conséquent il faut éviter l’option Dynamic Memory.

[Planification du cache distribué][3]

[Les commandes PowerShell][4]

# **Configuration des hôtes** {#configuration-des-hôtes}

Firewall Windows, autoriser les services/applications suivant(e)s :

  * AppFabric Caching Service (TCP-In)
  * File and Printer Sharing (pour le ping)

[/wp-content/uploads/2014/06/appdistributedcachefirewall.jpg][5]

Vérifier les connexions réseau des serveurs:

  * Connection > properties > Internet Protocol Version 4 (TCP/IPv4)
  * Vérifier que tous les serveurs utilisent les même DNS primaire et secondaire

#  **Références** {#références}

[Gérer le service de cache distribué dans SharePoint Server 2013][6]

Pour aller plus loin

[Beside the Point – AppFabric Caching and SharePoint: Concepts and Examples (Part 1)][7]

[Beside the Point – AppFabric Caching (and SharePoint): Configuration and Deployment (Part 2)][8]

[Samuel Betts – Troobleshooting][9]

 [1]: http://msdn.microsoft.com/fr-fr/library/ee677312%28v=azure.10%29.aspx
 [2]: http://technet.microsoft.com/fr-fr/library/jj219613%28v=office.15%29.aspx#graceful
 [3]: http://technet.microsoft.com/library/jj219572%28office.15%29.aspx
 [4]: http://technet.microsoft.com/fr-fr/library/jj219613%28v=office.15%29.aspx#memory
 [5]: /wp-content/uploads/2014/06/appdistributedcachefirewall.jpg
 [6]: http://technet.microsoft.com/fr-fr/library/jj219613%28v=office.15%29.aspx
 [7]: http://blogs.msdn.com/b/besidethepoint/archive/2013/03/27/appfabric-caching-and-sharepoint-1.aspx
 [8]: http://blogs.msdn.com/b/besidethepoint/archive/2013/03/27/appfabric-caching-and-sharepoint-2.aspx
 [9]: http://blogs.msdn.com/b/sambetts/archive/2014/03/19/sharepoint-2013-distributed-cache-appfabric-troubleshooting.aspx