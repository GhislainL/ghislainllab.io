---
title: Echec de l’indexation à cause d’un certificat SSL
author: Ghislain

date: 2016-03-11T00:00:00+00:00
url: /2016/03/11/echec-de-lindexation-a-cause-dun-certificat-ssl/
categories:
  - SharePoint

---
Lors d’une mise en recette, pleins de petits soucis ont décidé de survenir pour égayer ma journée. Parmi eux, le service de recherche ne voulait plus indexer de contenu pour la bonne excuse que **The secure sockets layer (SSL) certificate sent by the server was invalid and this item will not be crawled**. Je vous propose deux solutions :

## Solution temporaire {#solution-temporaire}

La solution temporaire consiste à forcer la rechercher à indexer du contenu même si le nom du certificat n’est pas adéquat.

Central Administration > Application Management > Search Service > Farm Search Administration

Sur la page Farm Search Administration page, cliquer sur **SSL Certificate Warning Configuration**, cliquer sur **Ignore SSL warning**. Dans la pop-in, cocher **Ignore SSL certificate name warnings** puis cliquer sur **OK**.

![SharePoint – Farm search administration][1]

![SharePoint – Farm search administration – SSL settings][2]

## Solution pérenne {#solution-pérenne}

La solution pérenne consiste à déclarer le certificat dans les relations de confiance.

Central Administration > Security > Manage Trust

Cliquer sur **New** pour créer une nouvelle relation de confiance.

Dans la section **Root certificate**, parcourir et sélectionner le certificat.

Cliquer sur **OK** puis relancer un crawl.

![SharePoint – Trust relationships][3]

![SharePoint – Add trust relationship ][4]

C’est le genre d’information qui se révèle utile suite à un renouvellement de certificat.

 [1]: /wp-content/uploads/2016/03/NoSSL1.png
 [2]: /wp-content/uploads/2016/03/NoSSL2.png
 [3]: /wp-content/uploads/2016/03/ManagerTrust1.png
 [4]: /wp-content/uploads/2016/03/ManagerTrust2.png