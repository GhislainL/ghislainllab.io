---
title: Tsundoku
author: Ghislain
date: 2020-01-11
url: /un-dessin/tsundoku/
categories:
  - Un dessin
tags:
  - sketchplanations

---

![Tsundoku](/un-dessin/sketchplanations-tsundoku.jpg)

Un magnifique mot japonais pour l'accumulation et l'empilement de livres sans les lire.  Il s'agit d'un mot-valise construit à partir de **tsund**e-oku (pour désigner les tas de choses laissés pour une utilisation ultérieure) et **doku**sho (lecture).

Eviter de confondre avec la bibliomanie qui est un trouble obsessionnel compulsif impliquant la collection ou l'accumulation de livres à un point où les relations sociales ou la santé sont endommagées.

Traduction de la publication de [Sketchplanations](https://www.sketchplanations.com/post/183656939324/tsundoku-a-beautiful-japanese-word-for-the)
