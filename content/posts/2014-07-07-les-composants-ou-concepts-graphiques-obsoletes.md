---
title: Les composants ou concepts graphiques obsolètes
author: Ghislain

date: 2014-07-07T07:40:55+00:00
url: /2014/07/07/les-composants-ou-concepts-graphiques-obsoletes/
categories:
  - UI

---
Quand on travaille avec un outil qui permet de faire des sites – SharePoint ou autre – les mêmes composants reviennent fréquemment. Comme dans tous les domaines, il y a des effets de mode. De même qu’il faut oser dire à quelqu’un que son pantalon à patte d’eph en velours est has been, il faut savoir quand conseiller de nouveaux composants.

**Les carrousels** : peu d’utilisateurs attendent que le carrousel tourne pour voir toutes les vignettes.

=>  Classer son contenu pour afficher uniquement ce qui est pertinent

**Mega menu** : un menu de navigation trop dense est contre-productif. Les utilisateurs sont rebutés par la quantité d’information et la difficulté de trouver le bon lien.

=>  Simplifier ou rationaliser les catégories. Ne pas hésiter à avoir un ou deux clics pour guider l’utilisateur

**Le texte dans les images** : le problème d’accessibilité est flagrant si les balises alternatives sont mal renseignées. De plus, il est irritant pour un utilisateur de ne pas arriver à sélectionner un texte.

=> Utiliser des web fonts et du CSS

**Pré chargement de page** : les utilisateurs ont maintenant l’habitude de voir les pages apparaitre progressivement puis gagner en fonctionnalités.

=>  Permettre le chargement naturel ou le lazy loading.

**Intégration sociale** : les boutons permettant de publier un lien vers le contenu du site sur un reseau social prennent de plus en plus de place. Tous ces symboles et couleurs détournent l’attention de l’utilisateur du contenu.

=>  Le nombre de plateformes sociales ne fait que croitre. Ne proposer que les majeures en relation avec le sujet du site.

**Liens externes dans le contenu** : un lien vers un contenu extérieur dans le corps d’un page est un excellent moyen pour que l’utilisateur n’arrive jamais au bout du contenu.

=>  Mettre les références sous le contenu dans un bloc dédié.

**Media en démarrage auto** : lancer automatiquement la lecture d’un fichier audio ou vidéo est déconseillé pour plusieurs raisons. 1/ l’utilisateur à l’impression de perdre le contrôle vu qu’il subit une action. 2/ l’utilisateur peut être dérangé par le son ou déranger ceux à proximité.

=> Aucune alternative, il faut éviter.

**La pagination infinie** : comment retrouver le point où l’on s’était arrêté lors de la navigation précédente ? Pour consulter l’historique, ce concept est contre intuitif

=>  Considérer l’utilisation avec attention. La pagination reste une solution pérenne.

Un concept que l’on voit sur un site au cours de sa navigation n’est peut-être pas adaptable à son site. Il faut donc toujours se demander s’il est utile et judicieux d’utiliser un concept ou composant.

**Références :**

<https://github.com/north/north#outdated-ux-patterns>

<http://sideproject.io/outdated-ux-patterns/>

<http://mashable.com/2014/01/14/outdated-web-features/>