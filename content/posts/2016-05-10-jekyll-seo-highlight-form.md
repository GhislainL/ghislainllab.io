---
title: Jekyll – formulaires, coloration syntaxique et SEO
author: Ghislain
date: 2016-05-10T00:00:00+00:00
url: /2016/05/10/jekyll-seo-highlight-form/
featuredImage: /wp-content/uploads/2016/03/jekyllrb.png
categories:
  - Blog
tags:
  - Git
  - GitHub
  - Jekyll
  - Ruby
  - WordPress

---
Quelques manipulations et fonctionnalités qu’il est bon d’utiliser avec Jekyll :

  * Formulaire soumis par mail
  * Coloration syntaxique
  * SEO avec jekyll-seo-tag

## Formulaire soumis par mail {#formulaire-soumis-par-mail}

Si vous pensez qu’il faut mettre à disposition un formulaire de contact en plus de vos comptes sur les réseaux sociaux, FormSpree peut vous aider.
  
Il s’agit d’un service gratuit (1000 messages / email / mois ) ne nécessitant que du HTML dans votre code pour recevoir des formulaires par mail.
  
Simple à mettre en oeuvre **mais** votre adresse mail est visible dans le source de la page.

Lors de la première utilisation du formulaire, il faut valider que les mails en provenance du formulaire sont désirés. Cela se fait par un mail de vérification de formspree.

[FormSpree][1]

## Coloration syntaxique {#coloration-syntaxique}

### Balise highlight {#balise-highlight}

Pour utiliser la balise highlight, il faut encadrer le code par _{% highlight language %}_ et _{% endhighlight %}_.
  
Cela peut etre génant quand il s’agit de code interpreté (Liquid ou Ruby) auquel cas il faut utiliser _{% raw %}_ et _{% endraw %}_.

### jekyll-gist {#jekyll-gist}

Gist permet de stocker des bouts de code dans GitHub en dehors de dépôt. Cela se révèle bien plus pratique que le code dans un post par le côté réutilisable. Jekyll-Gist permet d’afficher des Gists.

Pour cela, il faut ajouter le gem **jekyll-gist** dans le fichier _config.yml. La balise gist est maintenant disponible et s’utilise comme suit

{{< highlight html >}}
<form action="https://formspree.io/your@email.com" method="POST">
  <input type="email" name="_replyto" placeholder="Your email" />
  <input type="text" name="message" placeholder="Your message">
  
  <input type="hidden" name="_subject" value="Contact form" />
  <input type="text" name="_gotcha" style="display:none" />
    
  <input type="submit" value="Send">
</form>
{{< / highlight >}}

[Jekyll::Gist][2]

Si vous rencontrez l’erreur suivante lors de l’exécution _Liquid Exception: SSL_connect returned=1 errno=0 state=SSLv3 read server certificate B:_,
  
alors je vous conseille de lire l’aide suivante: [Download a cacert.pem for RailsInstaller][3].

## SEO avec jekyll-seo-tag {#seo-avec-jekyll-seo-tag}

Jekyll SEO Tag permet d’intégrer simplement les principes de base du SEO à travers l’ajout des méta tags suivants:

  * Titre des pages
  * Description de la page
  * URL canonique
  * URLs suivantes et précédentes
  * Métadonnées JSON-LD de site et post
  * Informations pour Open graph
  * Métadonnées pour les cartes Twitter

Pour cela, il faut :

  1. ajouter le gem **jekyll-seo-tag** dans le fichier _config.yml
  2. configurer le plugin dans le fichier _config.yml.
  3. ajouter le code _{% seo %}_ juste avant la balise </head> dans votre modèle de site.

Voici ce qui apparait ensuite dans votre header :
{{< highlight html >}}
<title>Ghislain’s Blog - Les tribulations d’un connaisseur en SharePoint.</title>
<meta property="og:title" content="Ghislain’s Blog" />
<meta name="description" content="Les tribulations d’un connaisseur en SharePoint." />
<meta property="og:description" content="Les tribulations d’un connaisseur en SharePoint." />
<link rel="canonical" href="http://www.ghislain-lerda.com/" />
<meta property="og:url" content="http://www.ghislain-lerda.com/" />
<meta property="og:site_name" content="Ghislain’s Blog" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@ghislainlerda" />
<meta name="twitter:creator" content="@Ghislain" />
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "WebSite",
    "name": "Ghislain Lerda",
    "headline": "Ghislain’s Blog",
    "description": "Les tribulations d’un connaisseur en SharePoint.",
    "sameAs": ["https://twitter.com/ghislainlerda","https://www.linkedin.com/in/ghislainlerda","https://github.com/ghislainl","https://mva.microsoft.com/Profile.aspx?alias=2181828"],
    "url": "http://www.ghislain-lerda.com/"
  }
</script>
{{< / highlight >}}

[Jekyll SEO Tag][4]

Si vous trouvez ce post intéressant, je vous invite à m’en faire part ci-dessous.

 [1]: https://formspree.io/
 [2]: https://github.com/jekyll/jekyll-gist
 [3]: https://gist.github.com/fnichol/867550#the-manual-way-boring
 [4]: https://github.com/jekyll/jekyll-seo-tag