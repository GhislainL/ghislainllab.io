---
title: Digital et numérique
author: Ghislain
date: 2019-12-27
url: /2019/12/27/digital-et-numerique/
categories:
  - IMHO
tags:
  - digital
  - numérique

---

Nous sommes actuellement la cible d'un matraquage digital. Entre la transformation digitale, l’entreprise digitale, les équipes digitales et la digitalisation des processus nous ne savons plus où donner de la tête. Ou plutôt du doigt, car rappelons qu’en français « digital » se rapporte au doigt (1 - 2)

Les personnes qui se gargarisent de digital ont la mémoire courte. Souvenons-nous qu’il y a une dizaine d’années, nous avons subi la même chose avec le numérique. Dès 2010, le gouvernement français utilisait le terme de transition numérique. Une recherche permet de trouver des traces du débat numérique/digital datant de 2015 (3). Il s’agit donc de donner un nouveau nom à quelque chose qui existait déjà. Peut-être fallait-il utiliser un « rebranding marketing » pour montrer qu’il y a eu des évolutions technologiques et méthodologiques en 10 ans ou simplement utiliser un anglicisme pour la forme.

La transformation digitale représente-t-elle une seconde chance pour les entreprises qui ont loupé la transformation numérique ?

**Références :**
1.	[Numérique ou digital ?](https://toucher.rectal.digital/)
2.	[CCI Paris IdF - La différence entre le numérique et le digital ?](https://www.entreprises.cci-paris-idf.fr/o/get_pdf%3Fp_l_id%3D5900681%26groupId%3D20152%26articleId%3D5900671%26templateId%3DddmTemplate_ARTICLE_CMS_ADT%26repositoryId%3D20152)
3.	[11 fev 2015 - Faut-il dire numérique ou digital ?](https://www.blogdumoderateur.com/numerique-ou-digital/)
