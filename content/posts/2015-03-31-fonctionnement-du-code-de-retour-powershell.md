---
title: Fonctionnement du code de retour PowerShell
author: Ghislain

date: 2015-03-31T08:00:08+00:00
url: /2015/03/31/fonctionnement-du-code-de-retour-powershell/
categories:
  - PowerShell
tags:
  - ErrorLevel
  - LastExitCode
  - PowerShell

---
Vous savez que le support de Windows 2003 Server prend fin le 14 juillet prochain. A cette occasion, je fais un peu d’archéologie en reprenant des scripts VBS et Batch (incluant du Shell) avec PowerShell. Rien de mieux pour approfondir le fonctionnement du code de retour PowerShell.

Par **convention**, le code de retour vaut 0 si tout s’est bien passé. Les autres valeurs sont des erreurs. Il est possible d’avoir un tableau de description pour les codes de retour.

En Batch, le code de retour se trouve dans **%ERRORLEVEL%**.

En PowerShell, le code de retour se trouve dans **$LASTEXITCODE**. En plus de cela, nous disposons de **$?** qui permet de savoir si la dernière commande s’est déroulée convenablement.

Comme rien ne vaut un exemple, nous allons exécuter un script PowerShell depuis l’invite de commande classique et PowerShell. Le script en question – test.ps1 – possède 3 comportements (normal, exception non catchée, exception catchée). Un modulo 3 des secondes au moment de l’exécution détermine le comportement.

Pour modifier la valeur du retour, il faut utiliser la commande **exit**.

Attention : **return** permet de retourner une valeur mais ne modifie ni %errorlevel% ni $LASTEXITCODE

{{< highlight powershell >}}Try
{
   $modRes = $(Get-Date).Second % 3
   switch ($modRes)
   {
      1 {
           throw 'axe'
        }
      2 {
           throw [System.IO.FileNotFoundException] "$file not found."
        }
      0 { exit 0 }
   }
}
Catch [System.IO.FileNotFoundException]
{
   exit 33
}{{< /highlight >}}

Premièrement l’appel du script depuis l’invite de commande.

{{< highlight bash >}}#OK
&gt; POWERSHELL.EXE –FILE test.ps1
&gt; %ERRORLEVEL%
0

#FILENOTFOUND
&gt; POWERSHELL.EXE –FILE test.ps1
&gt; %ERRORLEVEL%
33

#AXE
&gt; POWERSHELL.EXE –FILE test.ps1
&gt; %ERRORLEVEL%
0

#AXE
&gt; POWERSHELL.EXE –Command .test.ps1
&gt; %ERRORLEVEL%
1</pre>

Comme vous le voyez avec le cas l’exception non catchée, il faut faire attention à utiliser **powershell.exe -command** afin d’être sûr d’avoir un code de retour consistant.

Ensuite l’appel du script depuis l’invite de commande PowerShell pour voir le fonctionnement de $? et $LASTEXITCODE.

{{< highlight powershell >}}#OK
&gt; .Test.ps1
&gt; $?
True
&gt; $LASTEXITCODE
0

#FILENOTFOUND
&gt; .Test.ps1
&gt; $?
False
&gt; $LASTEXITCODE
33

#AXE
&gt; .Test.ps1
&gt; $?
False
&gt; $LASTEXITCODE
1
{{< /highlight >}}

Maintenant vous savez comment fonctionne le code de retour PowerShell – **$LASTEXITCODE** – et son ami état de l’exécution – **$?**. Vous avez même le petit plus qui permet de conserver la cohérence lors de l’appel d’un script PowerShell depuis l’invite de commande ( **-command** au lieu de -file).

**Références :**

[Fin de support de Windows 2003 Server][1]

 [1]: http://www.microsoft.com/fr-fr/server-cloud/products/windows-server-2003/ "Fin de support Windows 2003 Server"