---
title: Quelques astuces utiles pour PowerShell
author: Ghislain

date: 2015-04-14T08:00:58+00:00
url: /2015/04/14/quelques-astuces-utiles-pour-powershell/
categories:
  - PowerShell
tags:
  - like
  - match
  - Measure-Object
  - PowerShell
  - PsVersionTable
  - ScheduledTask
  - schtasks
  - split

---
A force de travailler sur SharePoint et plus généralement sur les environnements Windows, on dipose de quelques astuces Powershell qui peuvent faire gagner du temps. J’aimerai en partager quelques unes avec vous.

## Astuce 1 – Obtenir la version de Powershell {#astuce-1--obtenir-la-version-de-powershell}

{{< highlight powershell>}}$PsVersionTable.PSVersion
Major  Minor  Build  Revision
-----  -----  -----  --------
3      0      -1     -1
$PSVersionTable.PSVersion.Major
3
</pre>

Utile pour vérifier la version de powershell avant l’exécution du script.

{{< highlight powershell>}}If($PSVersionTable.PSVersion.Major -lt 3)
{
	Write-host "Ce script nécessite PowerShell 3.0 minimum pour s'exécuter" -foregroundcolor red
	Exit 1
}</pre>

## Astuce 2 – Compter les caractères, mots, lignes dans un fichier {#astuce-2--compter-les-caractères-mots-lignes-dans-un-fichier}

{{< highlight powershell>}}# nombre de lignes
Get-Content C:dossierfichier.txt | Measure-Object –Line
# nombre de caractères
Get-Content C:dossierfichier.txt | Measure-Object -Character
# nombre de mots
Get-Content C:dossierfichier.txt | Measure-Object –Word</pre>

## Astuce 3 – Rechercher dans une chaine de caractères {#astuce-3-rechercher-dans-une-chaine-de-caractères}

-like recherche une concordance dans une chaine et supporte les wildcards (métacaractères en français)

-match recherche une concordance dans une chaine et supporte les expressions régulières

{{< highlight powershell>}}# utilisation de -like
"bloc1.bloc2.zip" -like '.zip'
&gt; False
"bloc1.bloc2.zip" -like '*.zip'
&gt; True
# utilisation de -match
"bloc1.bloc2.zip" -match '(w+.){3}(zip)'
&gt; False
"bloc1.bloc2.bloc3.zip" -match '(w+.){3}(zip)'
&gt; True
</pre>

-contains est un faux ami. Cela fonctionne uniquement avec les tableaux et permet de savoir si une valeur en fait partie.

{{< highlight powershell>}}$coll = @('one','two','three','four')
$coll -contains 'one'</pre>

## Astuce 4 – Découper une chaine de caractères {#astuce-4--découper-une-chaine-de-caractères}

 

{{< highlight powershell>}}$returnCode = Select-String 'Retour=' $LogSuperVision -CaseSensitive | Select -exp line | %{ $_.split(' ')[7] }</pre>

## Astuce 5 – Concatener des variables {#astuce-5--concatener-des-variables}

{{< highlight powershell>}}$rep_test = 'e:test'
$nom_index = 'myIndex'
$idx = 5
# mauvaise méthode
"$rep_test$nom_index_$idx"
&gt; e:text5
# bonne méthode
"$rep_test$nom_index`_$idx"
&gt; e:textmyIndex_5 
"longueur : $($coll.length)"
&gt; longueur : 4
</pre>

## Astuce 6 – Travailler avec les taches planifiées {#astuce-6--travailler-avec-les-taches-planifiées}

A partir de Windows 8.1 et Windows Server 2012 R2 (PowerShell 4.0), vous pouvez utiliser de nouvelles commandlets ***-ScheduledTask** (**New-ScheduledTask, Get-ScheduledTask, Stop-ScheduledTask**, …)

[<span style="text-decoration: underline;"><span style="color: #800080;">https://technet.microsoft.com/en-us/library/jj649816.aspx</span></span>][1]

Mais sur les OS antérieur il faut passer par la commande **schtasks**.

{{< highlight powershell>}}# créer une tache planifiée
Schtasks /Create /TN myTask /TR notepad.exe /sc DAILY /ST 08:00 /RU administrateur /RP Pa$$w0rd
# exécuter une tache planifiée
schtasks /Run /TN myTask
# supprimer une tache planifiée
schtasks /Delete /TN myTask/F</pre>

## Astuce 7 – Trouver les fichiers ou dossiers antérieurs à une date {#astuce-7--trouver-les-fichiers-ou-dossiers-antérieurs-à-une-date}

La commande Get-ChildItem permet de lister le contenu d’une arborescence et dispose d’option très pratique. Le _piping_ permet de combiner d’autres sélecteur de manière facile et efficace.

{{< highlight powershell>}}# Fichiers et dossiers qui ont plus de 30j d'existence
Get-ChildItem –Path $folderPath | Where-Object LastWriteTime –lt (Get-Date).AddDays(-30)
# Fichiers qui ont plus de 30j d'existence et dont le nom correspond au modèle de l'expression régulière $regexPattern
Get-ChildItem -Path "$folderPath*" -Include $regexPattern -File | Where-Object LastWriteTime –lt (Get-Date).AddDays(-30)
# Dossier qui ont plus de 30j d'existence
Get-ChildItem -Directory –Path $folderPath | Where-Object LastWriteTime –lt (Get-Date).AddDays($nbDays)</pre>

Si comme moi vous utilisez PowerShell, vous avez probablement noté vos propres astuces.

 [1]: https://technet.microsoft.com/en-us/library/jj649816.aspx