---
title: Créer un entrainement fractionné avec Svelte
author: Ghislain
date: 2020-04-27
url: /2020/04/27/svelte-interval-training/
featuredImage: /wp-content/uploads/2018/08/DotNetCore-Hangfire.png
# draft: true
categories:
  - Javascript
tags:
  - Svelte

---

[Svelte](https://svelte.dev/) est un framework Javascript qui fait parler de lui depuis quelques mois pour son approche différente des gros framework comme Angular, React et VueJs : léger, moins verbeux, compilation lors du build et sans DOM virtuel. Il me faut fallait un cas pratique pour découvrir Svelte ainsi que le CI/CD (Continuous Integration/Continuous Delivery) de GitLab.

Voici le résultat : [Cardey training](https://ghislainl.gitlab.io/cardey-training/)


## Qu'est qu'un entrainement fractionné ?

Le Cardey est un exercice que j'ai découvert durant mes années de lutte au club de Levallois Perret. Je l'ai nommé ainsi en remerciement à Jean-Marc Cardey qui nous a fait transpirer avec.

L'idée est de travailler en fractionné de 30s/30s sur des cycles d'exercices de renforcement. Un cycle est constitué successivement d'exercices de jambes, de bras, d'abdominaux et de planches.

L'objectif est de tout donner sur la phase de travail et non d'effectuer un nombre de répétition défini.

Pour éviter de rentrer dans une routine, les exercices sont sélectionnés aléatoirement dans un panel. 

![Presentation Cardey](/wp-content/uploads/2020/04/cardey-cycle-min.png)


## Préparation

Au préalable il faut avoir [NodeJS](https://nodejs.org/). Ensuite pour créer le premier projet, c'est super simple :
{{< highlight javascript >}}
npx degit sveltejs/template my-svelte-project
cd my-svelte-project
npm install
npm run dev
{{< /highlight >}}

Pour mon site, j'ai identifié des besoins clés :
- une gestion des requêtes AJAX : natif avec [XMLHttpRequest](https://developer.mozilla.org/fr/docs/Web/API/XMLHttpRequest)
- une gestion du stockage : natif avec [Window.sessionStorage](https://developer.mozilla.org/fr/docs/Web/API/Window/sessionStorage)
- une gestion du son : natif avec [AudioContext](https://developer.mozilla.org/fr/docs/Web/API/AudioContext)
- une gestion du temps : natif avec [setInterval](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/setInterval)
- un routeur pour Svelte : [svelte-spa-router](https://github.com/ItalyPaleAle/svelte-spa-router#readme) m'a convaincu par sa simplicité d'utilisation et son élégance.
- une bibliothèque pour gérer du YAML : [JS YAML](https://github.com/nodeca/js-yaml)

## Code

Maintenant que tous les ingrédients sont rassemblés, on peut commencer à regarder les [exemples de code Svelte](https://svelte.dev/examples).
Un composant Svelte correspond à un fichier <composant>.svelte qui a la structure suivante :
{{< highlight html >}}
<script>
	// code javascript
  let name = 'world';
  let heros = [
		{ id: 'fsdgh', name: 'Hercule' },
		{ id: 'bvbnu', name: 'Ulysse' },
		{ id: 'mpowa', name: 'Achille' }
	];
</script>

<style>
  /* code css */
  p {
    color: purple;
		font-family: 'Comic Sans MS', cursive;
		font-size: 2em;
	}
</style>

<!-- code HTML avec templating -->
<p>Hello {name.toUpperCase()}!</p>
<ul>
  {#each heros as hero, i}
    <li>
      {i + 1}: {hero.name}
    </li>
  {/each}
</ul>
{{< / highlight >}}

Quelqu'un qui a l'habitude des frameworks JS récents retrouvera rapidement ses marques. Je ne vais donc pas copier des bouts de code, vu que les [sources](https://gitlab.com/GhislainL/cardey-training/) du site sont disponibles sur GitLab.

## Deploiement

Après compilation, un site embarquant du Svelte correspond à un ensemble de fichier HTML, JS, CSS. Pareil que les autres gros framework JS. Pour faire un déploiement rapide sur GitLab, il faut créer un fichier **.gitlab-ci.yml** à la racine de la solution pour bénéficier des pipelines CI/CD de GitLab :

{{< highlight yml >}}
image: node:latest

pages:
  stage: deploy
  script:
    - npm install
    - npm run build
  artifacts:
    paths:
    - public
  only:
  - master
{{< / highlight >}}

Un poil de configuration pour avec des [pages publiques GitLab](https://docs.gitlab.com/ee/user/project/pages/) et ça roule tout seul.

Voici le résultat : [Cardey training](https://ghislainl.gitlab.io/cardey-training/)

## Conclusion

J'ai trouvé ce framework très agréable et facile à prendre en main. Il m'a fait penser à [Marko](https://markojs.com/) que j'avais utilisé avec plaisir pour feu mon projet OuTaper.fr. 
Pour l'instant, il manque d'outil notamment pour les tests mais devrait s'étoffer et peut-être deviendra grand.